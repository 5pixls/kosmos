<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Enquiry extends Admin_Controller {
    public $sch_setting_detail = array();
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('mailsmsconf');
        $this->load->model("enquiry_model");
        $this->load->model("category_model");
        $this->config->load("payroll");
        $this->enquiry_status = $this->config->item('enquiry_status');
        $this->sch_setting_detail = $this->setting_model->getSetting();
    }

    public function index() {

        if (!$this->rbac->hasPrivilege('admission_enquiry', 'can_view')) {
            access_denied();
        }
        

        $this->session->set_userdata('top_menu', 'front_office');
        $this->session->set_userdata('sub_menu', 'admin/enquiry');
        $data['class_list'] = $this->class_model->get();
        $data["source_select"] = "";
        $data["status"] = "";
        $date_from=date('Y-m-d',strtotime("-30 day"));
        $date_to=date('Y-m-d');

        if (isset($_POST["search"])) {
            $enquiry_date = $this->input->post("enquiry_date");
            $source = $this->input->post("source");
            $status = $this->input->post("status");
            $enquiryid = $this->input->post("enquiryid");
            if (!empty($enquiry_date)) {
                $exp = explode(" - ", $enquiry_date);
                $date_from =date("Y-m-d", strtotime(str_replace("/","-",$exp[0])));
                $date_to =date("Y-m-d", strtotime(str_replace("/","-",$exp[1])));
            } 
            
            $data["source_select"] = $source;
            $data["status"] = $status;
            $enquiry_list = $this->enquiry_model->searchEnquiry($source, $status, $date_from, $date_to,$enquiryid);
           
        } else {

            $enquiry_list = $this->enquiry_model->getenquiry_list(null,null,$date_from,$date_to);
            
        }
       
        foreach ($enquiry_list as $key => $value) {

            $follow_up = $this->enquiry_model->getFollowByEnquiry($value["id"]);

            $enquiry_list[$key]["followupdate"] = $follow_up["date"];
            $enquiry_list[$key]["next_date"] = $follow_up["next_date"];
            $enquiry_list[$key]["response"] = $follow_up["response"];
            $enquiry_list[$key]["note"] = $follow_up["note"];
            $enquiry_list[$key]["followup_by"] = $follow_up["followup_by"];
        }
        $data['enquiry_list'] = $enquiry_list;
        $data['staffusers'] = $this->enquiry_model->getuser_list();
        $data['sDate']=$date_from;
        $data['eDate']=$date_to;
        $userdata  = $this->customlib->getUserData();
        $role_id = $userdata["role_id"];

        $data['enquiry_status'] = $this->enquiry_status;
        if(in_array($role_id, array(9,10,11))) $data['enquiry_status'] = $this->config->item('enquiry_status'.'_'.$role_id);
        $data['session_list'] = $this->enquiry_model->get_sessions();
        $data['Reference'] = $this->enquiry_model->get_reference();
        $data['sourcelist'] = $this->enquiry_model->getComplaintSource();
        $data['country_list'] = $this->enquiry_model->getCountrylist();
        $data['role_id'] = $role_id;
        
        $this->load->view('layout/header');
        $this->load->view('admin/frontoffice/enquiryview', $data);
        $this->load->view('layout/footer');
    }

    public function kanban() {
        if (!$this->rbac->hasPrivilege('admission_enquiry', 'can_view')) {
            access_denied();
        }
        

        $this->session->set_userdata('top_menu', 'front_office');
        $this->session->set_userdata('sub_menu', 'admin/enquiry/kanban');
        $data['class_list'] = $this->class_model->get();
        $data["source_select"] = "";
        $data["status"] = "";

        $enquiry_list = $this->enquiry_model->getenquiry_list();
       
        foreach ($enquiry_list as $key => $value) {

            $follow_up = $this->enquiry_model->getFollowByEnquiry($value["id"]);

            $enquiry_list[$key]["followupdate"] = $follow_up["date"];
            $enquiry_list[$key]["next_date"] = $follow_up["next_date"];
            $enquiry_list[$key]["response"] = $follow_up["response"];
            $enquiry_list[$key]["note"] = $follow_up["note"];
            $enquiry_list[$key]["followup_by"] = $follow_up["followup_by"];
        }
        $data['enquiry_list'] = $enquiry_list;

        $userdata  = $this->customlib->getUserData();
        $role_id = $userdata["role_id"];

        $data['enquiry_status'] = $this->enquiry_status;
        if(in_array($role_id, array(9,10,11))) $data['enquiry_status'] = $this->config->item('enquiry_status'.'_'.$role_id);

        $data['enquiry_counts']= $this->enquiry_model->get_enquiry_counts($data['enquiry_status']);
        $data['session_list'] = $this->enquiry_model->get_sessions();
        $data['Reference'] = $this->enquiry_model->get_reference();
        $data['sourcelist'] = $this->enquiry_model->getComplaintSource();
        $data['country_list'] = $this->enquiry_model->getCountrylist();
        


        $this->load->view('layout/header');
        $this->load->view('admin/frontoffice/enquiryviewkanban', $data);
        $this->load->view('layout/footer');
    }

    /*public function task_working_on($status, $task_id)
    {
        if (!$task_id or !$status) {
            return false;
        }
        $userdata  = $this->customlib->getUserData();
        $user_id = $userdata["id"];
        if ($status == 1) {
            $data['tasks_working_on'] = DB_BOOL_TRUE;

            // Stop other working task
            $this->db->query("UPDATE tasks SET tasks_working_on = '" . DB_BOOL_FALSE . "' WHERE tasks_id IN (SELECT tasks_working_periods_task_id FROM tasks_working_periods WHERE tasks_working_periods_user = '{$user_id}' AND tasks_working_periods_stop IS NULL)");
            $this->db->query("UPDATE tasks_working_periods SET tasks_working_periods_stop = CURRENT_TIMESTAMP WHERE tasks_working_periods_user = '{$user_id}' AND tasks_working_periods_stop IS NULL");


            // Start time tracker
            $data_working['tasks_working_periods_task_id'] = $task_id;
            $data_working['tasks_working_periods_user'] = $user_id;
            $data_working['tasks_working_periods_start'] = date('Y-m-d h:i:s', time());
            $this->apilib->create("tasks_working_periods", $data_working);
        } else if ($status == 2) {

            $data['tasks_working_on'] = DB_BOOL_FALSE;

            // Stop time tracker
            $this->db->query("UPDATE tasks_working_periods SET tasks_working_periods_stop = CURRENT_TIMESTAMP WHERE tasks_working_periods_task_id = '{$task_id}' AND tasks_working_periods_user = '{$user_id}' AND tasks_working_periods_stop IS NULL");
        }

        $this->apilib->edit('tasks', $task_id, $data);
        echo json_encode(array('status' => 2));
    }*/

    public function editTask($task_id)
    {
        if (!$task_id)
            return;

        $data = $this->input->post();
        //print_r($data);
        // Is this a column change? If yes, to done column?
        $userdata  = $this->customlib->getUserData();
        if ($data['tasks_column']) {
            $status = $this->enquiry_model->getenquiryStatus($task_id);
            if(($status >= 9) && ($data['tasks_column'] <= 9) || $data['tasks_column'] == 3) echo true;
            else {
                //$role_id = $userdata["role_id"];
                $enquirydata = $this->enquiry_model->getenquiry(null,$task_id);
                $this->db->query("UPDATE enquiry SET status= ".$data['tasks_column']." WHERE id = '{$task_id}'");   
                $enquiry_status = $this->config->item('enquiry_status');
                if($enquiry_status[$enquirydata['status']] != $enquiry_status[$data['tasks_column']]){
                    $log_data=array('lead_id'=>$task_id,'staff_id'=>$userdata["id"],'description' => 'Status change from '.$enquiry_status[$enquirydata['status']].' to '.$enquiry_status[$data['tasks_column']] );
                    $res= insert_lead_logs($log_data); 
                }
                echo true;            
            }
             
        }
       
    }

    public function admissionlink()
    {
        $array=array();
        $postData = $this->input->post();
        
        
        if ($postData["enquiryid"]) {
            $enquiryid = $postData["enquiryid"];
            $data = $this->enquiry_model->getenquiry(null,$enquiryid);
            
            if($data['father_firstname'] !='' && $data['father_email'] !='' && $data['key'] !='') {
               
                $encrypted_key = base64_encode(time());        
                $personalised_link = base_url().'registration/getAdmissionFee?key='.$encrypted_key;
                $date1 = strtotime("+3 day", time());
                $date = date('d-M-Y', $date1);
                $valid_till = date('Y-m-d H:i:s', $date1);
                $sender_details = array(
                    'subject' => "Admission Reminder",
                    'session' => $data['session'],
                    'title' => $data['father_title'], 
                    'firstname' => $data['father_firstname'], 
                    'lastname' => $data['father_lastname'], 
                    'email' => $data['father_email'],
                    'personalised_link' => $personalised_link,
                    'weburl' => 'https://thekhaitanschool.org/', 
                    'logourl' => 'https://thekhaitanschool.org/wp-content/uploads/2015/09/TKS-Big.jpg',                      
                    'date' => $date, 
                    'copyright' => date('Y'),
                   // 'checklist' => '/uploads/school_content/Check_List.pdf',
                   // 'feenotice' => '/uploads/school_content/Fees_Notice_2021-22.pdf',
                   // 'medical' => '/uploads/school_content/Medical_and_Vaccination_Form.pdf',
                    'transportation' => '/uploads/school_content/Transportation_Form.pdf'
                );
                
                $response=$this->mailsmsconf->mailsms('student_admission_granted',$sender_details);
                //print_r($response);die("----dfdfd");
                $response = true;
                $userdata  = $this->customlib->getUserData();
                if($response) {
                    $array = array('status' => 'success', 'error' => '', 'message' => 'The admission link has been sent successfully.');
                    $log_data = array('lead_id'=>$enquiryid,'staff_id'=>$userdata["id"],'description' => 'Admission link has been sent to the father.' );
                    //$this->enquiry_model->enquiry_update($enquiryid, array('status' =>2,'reg_date' => date('Y-m-d')));
                    $this->enquiry_model->insert_admission_link(array('enquiry_id' =>$postData["enquiryid"],'fee_amount_before_discount'=>($postData["admissionFeeBeforeDiscount"]*100),'discount_amount'=>($postData["discountAmount"]*100),'fee_amount' => ($postData["admissionFee"]*100), 'encrypted_link' => $encrypted_key, 'description' => $postData["admissionFeeDesc"], 'activated' => 1, 'valid_till' => $valid_till));
                    

                }else { 
                    $array = array('status' => 'fail', 'error' => true, 'message' => 'Error in sending registration link.');
                    $log_data=array('lead_id'=>$enquiryid,'staff_id'=>$userdata["id"],'description' => 'Error in sending registration link.' );
                }              
                
                $res= insert_lead_logs($log_data);      
    
            } 
        }
       echo json_encode($array);
       
    }

    public function registrationlink()
    {
        $array=array();
        $enquiryid= $this->input->post('enquiryid');
        if ($enquiryid) {
            $data = $this->enquiry_model->getenquiry(null,$enquiryid);
            if($data['father_firstname'] !='' && $data['father_email'] !='' && $data['key'] !='') {
               $personalised_link= base_url().'registration/details?key='.$data['key'];
               $date = strtotime("+3 day", time());
               $date = date('d-M-Y', $date);
               $sender_details = array(
                    'title' => $data['father_title'], 
                    'firstname' => $data['father_firstname'], 
                    'email' => $data['father_email'],
                    'personalised_link' => $personalised_link,
                    'weburl' => 'https://thekhaitanschool.org/', 
                    'logourl' => 'https://thekhaitanschool.org/wp-content/uploads/2015/09/TKS-Big.jpg',                      
                    'date' => $date, 
                    'copyright' => date('Y')
                );
               
               $response=$this->mailsmsconf->mailsms('student_registration_link',$sender_details);
               $userdata  = $this->customlib->getUserData();
               if($response) {
                    $array = array('status' => 'success', 'error' => '', 'message' => 'The registration link has been sent successfully.');
                    $log_data=array('lead_id'=>$enquiryid,'staff_id'=>$userdata["id"],'description' => 'Registration link has been sent to the father.' );
                    $this->enquiry_model->enquiry_update($enquiryid, array('status' =>2,'reg_date' => date('Y-m-d')));

               }else { 
                    $array = array('status' => 'fail', 'error' => true, 'message' => 'Error in sending registration link.');
                    $log_data=array('lead_id'=>$enquiryid,'staff_id'=>$userdata["id"],'description' => 'Error in sending registration link.' );
               }              
               
               $res= insert_lead_logs($log_data);      
    
            } 
        }
       echo json_encode($array);
       
    }

    public function add() {
        if (!$this->rbac->hasPrivilege('admission_enquiry', 'can_add')) {
            access_denied();
        }

        $this->form_validation->set_rules('name', 'Child First Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('last_name', 'Child Last Name', 'trim|required|xss_clean');
         $this->form_validation->set_rules('dob', 'Date of birth', 'trim|required|xss_clean');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required|xss_clean');
        $this->form_validation->set_rules('session_id', 'Session', 'trim|required|xss_clean');
        $this->form_validation->set_rules('class', 'Class', 'trim|required|xss_clean');
        
        $this->form_validation->set_rules('father_firstname', 'Father First Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('father_mobile', 'Father Mobile', 'trim|required|xss_clean');
        $this->form_validation->set_rules('source', $this->lang->line('source'), 'trim|required|xss_clean');
       // $this->form_validation->set_rules('date', $this->lang->line('date'), 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {

            $msg = array(
                'name' => form_error('name'),
                'last_name' => form_error('last_name'),
                'dob' => form_error('dob'),
                'gender' => form_error('gender'),
                'class' => form_error('class'),
                'session_id' => form_error('session_id'),
                'father_firstname' => form_error('father_firstname'),
                'father_mobile' => form_error('father_mobile'),
                'source' => form_error('source')
            );

            $array = array('status' => 'fail', 'error' => $msg, 'message' => '');
            echo json_encode($array);

        } 
        if(empty($this->input->post('father_email_pref')) && empty($this->input->post('mother_email_pref'))){
                $array = array('status' => 'fail', 'error' => array('Please select email prefrence for either father OR mother.'));
                echo json_encode($array);
        }
        if(empty($this->input->post('father_sms_pref')) && empty($this->input->post('mother_sms_pref'))){
                $array = array('status' => 'fail', 'error' => array('Please select SMS prefrence for either father OR mother.'));
                echo json_encode($array);
        }
        else {
            $enquiry = array(               
                'name' => $this->input->post('name'),
                'last_name' => $this->input->post('last_name'),
                'session_id' => $this->input->post('session_id'),
                'address' => $this->input->post('address'),
                'address2' => $this->input->post('address2'),
                'city' => $this->input->post('city'),
                'state_id' => $this->input->post('state_id'),
                'country_id' => $this->input->post('country_id'),
                'gender' => $this->input->post('gender'),
                'father_title' => $this->input->post('father_title'),
                'father_firstname' => $this->input->post('father_firstname'),
                'father_lastname' => $this->input->post('father_lastname'),
                'father_email' => $this->input->post('father_email'),
                'father_mobile' => $this->input->post('father_mobile'),
                'mother_title' => $this->input->post('mother_title'),
                'mother_firstname' => $this->input->post('mother_firstname'),
                'mother_lastname' => $this->input->post('mother_lastname'),
                'mother_email' => $this->input->post('mother_email'),
                'mother_email' => $this->input->post('mother_email'),                
                'dob' => date('Y-m-d', strtotime($this->input->post('dob'))),
                'reference' => $this->input->post('reference'),
                'date' => date('Y-m-d'),
                'description' => $this->input->post('description'),                
               // 'note' => $this->input->post('note'),
                'source' => $this->input->post('source'),
                'email' => $this->input->post('email'),
                'assigned' => $this->input->post('assigned'),
                'class' => $this->input->post('class'),
               // 'no_of_child' => $this->input->post('no_of_child'),
                'status' => 1,
                'father_email_pref' => $this->input->post('father_email_pref'),
                'father_sms_pref' => $this->input->post('father_sms_pref'),
                'mother_email_pref' => $this->input->post('mother_email_pref'),
                'mother_sms_pref' => $this->input->post('mother_sms_pref')
            );
            if($this->input->post('follow_up_date') !='') $enquiry['follow_up_date'] = date('Y-m-d h:i:s', strtotime(str_replace(' - ', ' ',$this->input->post('follow_up_date'))));

            $enquiryid=$this->enquiry_model->add($enquiry);
            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('success_message'));
            $userdata  = $this->customlib->getUserData();
            $log_data=array('lead_id'=>$enquiryid,'staff_id'=>$userdata["id"],'description' => 'Lead has been created.' );
            $res= insert_lead_logs($log_data); 
            echo json_encode($array);
        }
        
    }
    public function getStates() {        
            $country_id = $this->input->post('country_id');
            $array=$this->enquiry_model->getStateCountrylist($country_id);
            $html='';
            foreach ($array as $value) {
                $html .="<option value='".$value['id']."'>".$value['state']."</option>";
            }
            echo json_encode($html);
    }


    public function delete($id) {
        if (!$this->rbac->hasPrivilege('admission_enquiry', 'can_delete')) {
            access_denied();
        }
        if (!empty($id)) {
            $this->enquiry_model->enquiry_delete($id);
            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('delete_message'));
            $userdata  = $this->customlib->getUserData();
            $log_data=array('lead_id'=>$id,'staff_id'=>$userdata["id"],'description' => 'The lead has been deleted.' );
            $res= insert_lead_logs($log_data); 
        }
        echo json_encode($array);
        
    }



 public function follow_up($enquiry_id, $status) {
   
         if(!$this->rbac->hasPrivilege('follow_up_admission_enquiry','can_view')){
        access_denied();
        }
        $data['id'] = $enquiry_id;
        $enquiry_data = $this->enquiry_model->getenquiry_list($enquiry_id, $status);
        $data['enquiry_data']=$enquiry_data;
        $data['next_date']=$this->enquiry_model->next_follow_up_date($enquiry_id);
        //$data['enquiry_status'] = $this->enquiry_status;
        $userdata  = $this->customlib->getUserData();
        $role_id = $userdata["role_id"];

        $enquiry_status = $this->config->item('enquiry_status');
        if(in_array($role_id, array(9,10,11))) $enquiry_status = $this->config->item('enquiry_status'.'_'.$role_id);
        $data['statuses'] =$enquiry_status;
        $data['assigned_user'] =$this->enquiry_model->getassigneduser($enquiry_data['id']);

        $this->load->view('admin/frontoffice/follow_up_modal', $data);
    }

    function follow_up_insert() {
        if (!$this->rbac->hasPrivilege('follow_up_admission_enquiry', 'can_add')) {
            access_denied();
        }

        $this->form_validation->set_rules('response', $this->lang->line('response'), 'trim|required|xss_clean');
        //$this->form_validation->set_rules('date', $this->lang->line('follow_up_date'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('follow_up_date', $this->lang->line('next_follow_up_date'), 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {

            $msg = array(
                'response' => form_error('response'),
                'follow_up_date' => form_error('follow_up_date'),
             //   'date' => form_error('date'),
            );

            $array = array('status' => 'fail', 'error' => $msg, 'message' => '');
        } else {
            $admin = $this->customlib->getLoggedInUserData();
            $enquiryid = $this->input->post('enquiry_id');
            $follow_up = array(
                'next_date' => date('Y-m-d h:i:s', strtotime($this->input->post('follow_up_date'))),
                'response' => $this->input->post('response'),
                'other_response' => $this->input->post('other_response'),
                'note' => $this->input->post('note'),
                'followup_by' => $admin['id'],
                'enquiry_id' => $enquiryid
            );
            $last_follow=$this->enquiry_model->lastFollowByEnquiry($this->input->post('enquiry_id'));
            if(!empty($last_follow)) $follow_up['date']=$last_follow['next_date'];

            $this->enquiry_model->add_follow_up($follow_up);

            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('success_message'));
            $userdata  = $this->customlib->getUserData();
            $log_data=array('lead_id'=>$enquiryid,'staff_id'=>$userdata["id"],'description' => 'Next follow up has been created.' );
            $res= insert_lead_logs($log_data); 
        }

        echo json_encode($array);
    }

    function follow_up_list($id) {

        $data['id'] = $id;
        $data['follow_up_list'] = $this->enquiry_model->getfollow_up_list($id);
        $userdata  = $this->customlib->getUserData();
        $role_id = $userdata["role_id"];

        $enquiry_status = $this->config->item('enquiry_status');
        if(in_array($role_id, array(9,10,11))) $enquiry_status = $this->config->item('enquiry_status'.'_'.$role_id);
        $data['statuses'] =$enquiry_status;
        $this->load->view('admin/frontoffice/followuplist', $data);
    }

    function follow_up_call_loglist($id) {

        $data['id'] = $id;
        $data['ozonetel_activity_log'] = $this->enquiry_model->get_ozonetel_activity_log($id);
        echo $this->load->view('admin/frontoffice/calllogview', $data, TRUE);
    }

    function lead_loglist($id) {

        $data['id'] = $id;
        $data['lead_activity_log'] = $this->enquiry_model->get_lead_activity_log($id);
        echo $this->load->view('admin/frontoffice/leadlogview', $data, TRUE);
    }

    public function leaddetails($id) {

        if (!$this->rbac->hasPrivilege('admission_enquiry', 'can_view')) {
            access_denied();
        }
        

        $this->session->set_userdata('top_menu', 'front_office');
        $this->session->set_userdata('sub_menu', 'admin/enquiry');
        $userdata  = $this->customlib->getUserData();
        $role_id = $userdata["role_id"];

        $enquiry_status = $this->config->item('enquiry_status');
        if(in_array($role_id, array(9,10,11))) $enquiry_status = $this->config->item('enquiry_status'.'_'.$role_id);
        $data['statuses'] =$enquiry_status;
        $data['staffusers'] = $this->enquiry_model->getuser_list();
        $data['source'] = $this->enquiry_model->getComplaintSource();
        $data['enquiry_type'] = $this->enquiry_model->get_enquiry_type();
        $data['session_list'] = $this->enquiry_model->get_sessions();
        $data['Reference'] = $this->enquiry_model->get_reference();
        $data['class_list'] = $this->enquiry_model->getclasses();
        $enquiry_data = $this->enquiry_model->getenquiry_list($id,$status);
        $data['enquiry_data'] = $enquiry_data;
        $data['country_list'] = $this->enquiry_model->getCountrylist();
        $data['state_list'] = $this->enquiry_model->getStateCountrylist($enquiry_data['country_id']);
        $category             = $this->category_model->get();
        $data['categorylist'] = $category;
        $data['sch_setting']     = $this->sch_setting_detail;
        $data['payment'] = $this->enquiry_model->getPaymentInfo($id);
        $data['documents'] = $this->enquiry_model->getDocumentsInfo($id);

        $this->load->view('layout/header');
        $this->load->view('admin/frontoffice/lead_edit', $data);
        $this->load->view('layout/footer');
    }


    function details($id,$status) {

        if (!$this->rbac->hasPrivilege('admission_enquiry', 'can_view')) {
            access_denied();
        }

        $userdata  = $this->customlib->getUserData();
        $role_id = $userdata["role_id"];

        $enquiry_status = $this->config->item('enquiry_status');
        if(in_array($role_id, array(9,10,11))) $enquiry_status = $this->config->item('enquiry_status'.'_'.$role_id);
        $data['statuses'] =$enquiry_status;
        $data['staffusers'] = $this->enquiry_model->getuser_list();
        $data['source'] = $this->enquiry_model->getComplaintSource();
        $data['enquiry_type'] = $this->enquiry_model->get_enquiry_type();
        $data['session_list'] = $this->enquiry_model->get_sessions();
        $data['Reference'] = $this->enquiry_model->get_reference();
        $data['class_list'] = $this->enquiry_model->getclasses();
        $enquiry_data = $this->enquiry_model->getenquiry_list($id,$status);
        $data['enquiry_data'] = $enquiry_data;
        $data['country_list'] = $this->enquiry_model->getCountrylist();
        $data['state_list'] = $this->enquiry_model->getStateCountrylist($enquiry_data['country_id']);
        $this->load->view('admin/frontoffice/enquiryeditmodalview', $data);

    }

    function editpost($id) {

        if (!$this->rbac->hasPrivilege('admission_enquiry', 'can_edit')) {
            access_denied();
        }
        $this->form_validation->set_rules('name', 'Child first name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('last_name', 'Child last name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dob', 'Date of birth', 'trim|required|xss_clean');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required|xss_clean');
        $this->form_validation->set_rules('session_id', 'Session', 'trim|required|xss_clean');
        $this->form_validation->set_rules('class', 'Class', 'trim|required|xss_clean');
        
        $this->form_validation->set_rules('father_firstname', 'Father first name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('father_mobile', 'Father mobile', 'trim|required|xss_clean');
        $this->form_validation->set_rules('father_email', 'Father email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('source', $this->lang->line('source'), 'trim|required|xss_clean');
        //$this->form_validation->set_rules('date', $this->lang->line('date'), 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {

            $msg = array(
                'name' => form_error('name'),
                'last_name' => form_error('last_name'),
                'dob' => form_error('dob'),
                'gender' => form_error('gender'),
                'class' => form_error('class'),
                'session_id' => form_error('session_id'),
                'father_firstname' => form_error('father_firstname'),
                'father_mobile' => form_error('father_mobile'),
                'father_email' => form_error('father_email'),
                'source' => form_error('source')
               // 'date' => form_error('date')
            );

            $array = array('status' => 'fail', 'error' => $msg, 'message' => '');
            echo json_encode($array);
            return false;
        }
        if(empty($this->input->post('father_email_pref')) && empty($this->input->post('mother_email_pref'))){
                $array = array('status' => 'fail', 'error' => array('Please select email prefrence for either father OR mother.'));
                echo json_encode($array);
                return false;
        }
        if(empty($this->input->post('father_sms_pref')) && empty($this->input->post('mother_sms_pref'))){
                $array = array('status' => 'fail', 'error' => array('Please select SMS prefrence for either father OR mother.'));
                echo json_encode($array);
                return false;
        } else {
            $status = $this->input->post('status');
            $oldstatus = $this->enquiry_model->getenquiryStatus($id);
            if(($oldstatus >=9) && ($status <= 9)) $status=$oldstatus;

            $enquiry_update = array(
                'name' => $this->input->post('name'),
                'last_name' => $this->input->post('last_name'),
                'gender' => $this->input->post('gender'),
                'dob' => date('Y-m-d', strtotime($this->input->post('dob'))),
                'class' => $this->input->post('class'),
                'session_id' => $this->input->post('session_id'),
                'father_title' => $this->input->post('father_title'),
                'father_firstname' => $this->input->post('father_firstname'),
                'father_lastname' => $this->input->post('father_lastname'),
                'father_email' => $this->input->post('father_email'),
                'father_mobile' => $this->input->post('father_mobile'),
                'mother_title' => $this->input->post('mother_title'),
                'mother_firstname' => $this->input->post('mother_firstname'),
                'mother_lastname' => $this->input->post('mother_lastname'),
                'mother_email' => $this->input->post('mother_email'),
                'address' => $this->input->post('address'),
                'address2' => $this->input->post('address2'),
                'city' => $this->input->post('city'),
                'state_id' => $this->input->post('state_id'),
                'country_id' => $this->input->post('country_id'),               
                'date' => date('Y-m-d', $this->customlib->datetostrtotime($this->input->post('date'))),
                //'note' => $this->input->post('note'),
                'source' => $this->input->post('source'),
                'email' => $this->input->post('email'),
                'assigned' => $this->input->post('assigned'),
                'reference' => $this->input->post('reference'),
                'status' => $status,
                'father_email_pref' => $this->input->post('father_email_pref'),
                'father_sms_pref' => $this->input->post('father_sms_pref'),
                'mother_email_pref' => $this->input->post('mother_email_pref'),
                'mother_sms_pref' => $this->input->post('mother_sms_pref')
                
                //'no_of_child' => $this->input->post('no_of_child')
            );
            if($this->input->post('follow_up_date') !='') $enquiry_update['follow_up_date'] = date('Y-m-d h:i:s', strtotime(str_replace(' - ', ' ',$this->input->post('follow_up_date'))));
            $this->enquiry_model->enquiry_update($id, $enquiry_update);

            ///Send email notification for no vacancy and granted
            if($enquiry_update['status'] == 7){
                $sender_details = array(
                    'child_first_name' => $enquiry_update['name'], 
                    'child_last_name' => $enquiry_update['last_name'], 
                    'title' => $enquiry_update['father_title'], 
                    'firstname' => $enquiry_update['father_firstname'], 
                    'email' => $enquiry_update['father_email'],
                    'weburl' => 'https://thekhaitanschool.org/', 
                    'logourl' => 'https://thekhaitanschool.org/wp-content/uploads/2015/09/TKS-Big.jpg',                      
                    'copyright' => date('Y'),
                    'subject' => 'Admission Update-'.$enquiry_update['name'].' '.$enquiry_update['last_name']
                );
                //print_r($sender_details);die;
                $response=$this->mailsmsconf->mailsms('student_no_vacancy',$sender_details);
            }
            ///end email sending//////
            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('update_message'));
            $userdata  = $this->customlib->getUserData();
            $log_data=array('lead_id'=>$id,'staff_id'=>$userdata["id"],'description' => 'The lead details has been updated.' );
            $res= insert_lead_logs($log_data);
            echo json_encode($array);
        }
        
    }
    function editpost_register($id) {
        //print_r($this->input->post());die;
        if (!$this->rbac->hasPrivilege('admission_enquiry', 'can_edit')) {
            access_denied();
        }
        $this->form_validation->set_rules('nationality', 'Nationality', 'trim|required|xss_clean');
        $this->form_validation->set_rules('blood_group', 'Blood Group', 'trim|required|xss_clean');
        $this->form_validation->set_rules('mother_tongue', 'Mother Tongue', 'trim|required|xss_clean');
        $this->form_validation->set_rules('home_town', 'Home Town', 'trim|required|xss_clean');
        $this->form_validation->set_rules('locality', 'Locality', 'trim|required|xss_clean');
        $this->form_validation->set_rules('religion', 'Religion', 'trim|required|xss_clean');
        $this->form_validation->set_rules('social_category', 'Social Category', 'trim|required|xss_clean');
        $this->form_validation->set_rules('father_qualification', 'Father Qualification', 'trim|required|xss_clean');
        $this->form_validation->set_rules('father_employer', 'Mother Employer', 'trim|required|xss_clean');
        $this->form_validation->set_rules('mother_employer', 'Father Employer', 'trim|required|xss_clean');
        $this->form_validation->set_rules('mother_qualification', 'Mother Qualification', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('guardian_employer', 'Guardian Employer', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('guardian_qualification', 'Guardian Qualification', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {

            $msg = array(
                'nationality' => form_error('nationality'),
                'blood_group' => form_error('blood_group'),
                'mother_tongue' => form_error('mother_tongue'),
                'home_town' => form_error('home_town'),
                'locality' => form_error('locality'),
                'religion' => form_error('religion'),
                'social_category' => form_error('social_category'),
                'religion' => form_error('religion'),
                'father_qualification' => form_error('father_qualification'),
                'father_employer' => form_error('father_employer'),
                'mother_qualification' => form_error('mother_qualification'),
                'mother_employer' => form_error('mother_employer')
                //'guardian_qualification' => form_error('guardian_qualification'),
                //'guardian_employer' => form_error('guardian_employer')

            );

            $array = array('status' => 'fail', 'error' => $msg, 'message' => '');
        } else {
            $enquiry_update = array(
                'child_aadhaar_no' => $this->input->post('child_aadhaar_no'),
                'nationality' => $this->input->post('nationality'),
                'blood_group' => $this->input->post('blood_group'),
                'child_image' => $this->input->post('child_image'),
                'mother_tongue' => $this->input->post('mother_tongue'),
                'home_town' => $this->input->post('home_town'),
                'locality' => $this->input->post('locality'),
                'religion' => $this->input->post('religion'),
                'social_category' => $this->input->post('social_category'),

                'last_school' => $this->input->post('last_school'),
                'medium_instruction' => $this->input->post('medium_instruction'),
                'previous_board' => $this->input->post('previous_board'),
                'previous_class' => $this->input->post('previous_class'),

                'father_qualification' => $this->input->post('father_qualification'),
                'father_qualification_other' => $this->input->post('father_qualification_other'),

                'father_employer' => $this->input->post('father_employer'),
                'father_aadhar_no' => $this->input->post('father_aadhar_no'),
                'father_occupation' => $this->input->post('father_occupation'),
                'father_occupation_other' => $this->input->post('father_occupation_other'),

                'father_designation' => $this->input->post('father_designation'),
                'father_designation_other' => $this->input->post('father_designation_other'),

                'father_address' => $this->input->post('father_address'),
                'father_city' => $this->input->post('father_city'),
                'father_city_other' => $this->input->post('father_city_other'),

                'father_pincode' => $this->input->post('father_pincode'),
                'father_workphone' => $this->input->post('father_workphone'),
                'father_annualincome' => $this->input->post('father_annualincome'),
                'father_woking_tks' => $this->input->post('father_woking_tks'),

                'mother_qualification' => $this->input->post('mother_qualification'),
                'mother_qualification_other' => $this->input->post('mother_qualification_other'),

                'mother_employer' => $this->input->post('mother_employer'),
                'mother_aadhar_no' => $this->input->post('mother_aadhar_no'),
                'mother_occupation' => $this->input->post('mother_occupation'),
                'mother_occupation_other' => $this->input->post('mother_occupation_other'),

                'mother_designation' => $this->input->post('mother_designation'),
                'mother_designation_other' => $this->input->post('mother_designation_other'),

                'mother_address' => $this->input->post('mother_address'),
                'mother_city' => $this->input->post('mother_city'),
                'mother_city_other' => $this->input->post('mother_city_other'),

                'mother_pincode' => $this->input->post('mother_pincode'),
                'mother_workphone' => $this->input->post('mother_workphone'),
                'mother_annualincome' => $this->input->post('mother_annualincome'),
                'mother_woking_tks' => $this->input->post('mother_woking_tks'),

                'guardian_title' => $this->input->post('guardian_title'),
                'guardian_name' => $this->input->post('guardian_name'),
                'guardian_lastname' => $this->input->post('guardian_lastname'),
                'guardian_email' => $this->input->post('guardian_email'),
                'guardian_mobile' => $this->input->post('guardian_mobile'),
                'guardian_aadhar_no' => $this->input->post('guardian_aadhar_no'),
                'guardian_qualification' => $this->input->post('guardian_qualification'),
                'guardian_qualification_other' => $this->input->post('guardian_qualification_other'),

                'guardian_occupation' => $this->input->post('guardian_occupation'),
                'guardian_occupation_other' => $this->input->post('guardian_occupation_other'),

                'guardian_employer' => $this->input->post('guardian_employer'),
                'guardian_designation' => $this->input->post('guardian_designation'),
                'guardian_designation_other' => $this->input->post('guardian_designation_other'),

                'guardian_annualincome' => $this->input->post('guardian_annualincome'),
                'guardian_woking_tks' => $this->input->post('guardian_woking_tks'),
                'guardian_address' => $this->input->post('guardian_address'),
                'guardian_city' => $this->input->post('guardian_city'),
                'guardian_city_other' => $this->input->post('guardian_city_other'),

                'guardian_state' => $this->input->post('guardian_state'),
                'guardian_pincode' => $this->input->post('guardian_pincode'),
                'guardian_telephone' => $this->input->post('guardian_telephone'),
                'guardian_relation' => $this->input->post('guardian_relation'),
                'school_bus' => $this->input->post('school_bus'),
                'guadian_email_pref' => $this->input->post('guadian_email_pref'),
                'guadian_sms_pref' => $this->input->post('guadian_sms_pref')
            );
            //print_r($_FILES["child_image"]);
            if (isset($_FILES["child_image"]) && !empty($_FILES['child_image']['name'])) {
                $fileInfo = pathinfo($_FILES["child_image"]["name"]);
                $img_name = $id . "register_child_" . '.' . $fileInfo['extension'];
                move_uploaded_file($_FILES["child_image"]["tmp_name"], "./uploads/student_images/" . $img_name);
                $enquiry_update['child_image'] = 'uploads/student_images/' . $img_name;
            }
            if (isset($_FILES["previous_result"]) && !empty($_FILES['previous_result']['name'])) {
                $fileInfo = pathinfo($_FILES["previous_result"]["name"]);
                $img_name = $id . "_previous_result_" . '.' . $fileInfo['extension'];
                move_uploaded_file($_FILES["previous_result"]["tmp_name"], "./uploads/student_images/" . $img_name);
                $enquiry_update['previous_result'] = 'uploads/student_images/' . $img_name;
            }

            $insert_id=$this->enquiry_model->enquiry_update($id, $enquiry_update);
            
            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('update_message'));
            $userdata  = $this->customlib->getUserData();
            $log_data=array('lead_id'=>$id,'staff_id'=>$userdata["id"],'description' => 'The registration details has been updated.' );
            $res= insert_lead_logs($log_data); 
        }

        echo json_encode($array);
        
    }

    function editpost_questionnair($id) {

        if (!$this->rbac->hasPrivilege('admission_enquiry', 'can_edit')) {
            access_denied();
        }
        

        
            $enquiry_update = array(
                'school_last_attended' => $this->input->post('school_last_attended'),
                'languages_child_can_speak' => $this->input->post('languages_child_can_speak'),
                'sibbling_class' => $this->input->post('sibbling_class'),
                'sibbling_section' => $this->input->post('sibbling_section'),
                'special_traits' => $this->input->post('special_traits'),
                'interaction_with_peer_group' => $this->input->post('interaction_with_peer_group'),
                'nuclear_joint_family' => $this->input->post('nuclear_joint_family'),
                'definition_good_school' => $this->input->post('definition_good_school'),
                'inclusive_education' => $this->input->post('inclusive_education'),
                'definition_good_school' => $this->input->post('definition_good_school'),
                'inculcate_values' => $this->input->post('inculcate_values'),
                'spendtime_importance' => $this->input->post('spendtime_importance'),
                'takes_care_child' => $this->input->post('takes_care_child'),
                'spend_with_your_child' => $this->input->post('spend_with_your_child'),
                'interests_hobbies' => $this->input->post('interests_hobbies'),
                'logical_answers' => $this->input->post('logical_answers'),
                'physical_illness' => $this->input->post('physical_illness'),
                'kps_impression' => $this->input->post('kps_impression')
            );
            //$this->db->where('id', $id);
            //$this->db->update('enquiry', $enquiry_update);
            //echo $this->db->last_query();
            $this->enquiry_model->enquiry_update($id, $enquiry_update);
            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('update_message'));
            $userdata  = $this->customlib->getUserData();
            $log_data=array('lead_id'=>$id,'staff_id'=>$userdata["id"],'description' => 'The questionnaire details has been updated.' );
            $res= insert_lead_logs($log_data); 
        

        echo json_encode($array);
        
    }

    public function follow_up_delete($follow_up_id, $enquiry_id) {
        if (!$this->rbac->hasPrivilege('follow_up_admission_enquiry', 'can_delete')) {
            access_denied();
        }
        $this->enquiry_model->delete_follow_up($follow_up_id);
        $data['id'] = $enquiry_id;
        $data['follow_up_list'] = $this->enquiry_model->getfollow_up_list($enquiry_id);
        $userdata  = $this->customlib->getUserData();
        $log_data=array('lead_id'=>$enquiry_id,'staff_id'=>$userdata["id"],'description' => 'The follow up has been deleted.' );
        $res= insert_lead_logs($log_data); 
       
        $this->load->view('admin/frontoffice/followuplist', $data);
    }

    public function check_default($post_string) {
        return $post_string == '' ? FALSE : TRUE;
    }

    public function change_status() {

        $id = $this->input->post("id");
        $status = $this->input->post("status");

        if (!empty($id)) {
            $data = array('id' => $id, 'status' => $status);

            $this->enquiry_model->changeStatus($data);
            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('success_message'));
        } else {

            $array = array('status' => 'fail', 'error' => '', 'message' =>$this->lang->line('update_message'));
        }

        echo json_encode($array);
    }

    public function callOzonetel(){
        //ini_set('display_errors', 1); 
        //ini_set('display_startup_errors', 1); 
        //error_reporting(E_ALL); 
        $userdata  = $this->customlib->getUserData();
        $loggedInUserId = $userdata["id"];

        $request = $this->input->post();
        $ozonetelInfo = $this->enquiry_model->get_ozonetel_info($loggedInUserId);

        $leadInfo = $this->enquiry_model->get_lead_phone($request["lead_id"]);
        $response = [];
        $ozonetal_apikey=$this->config->item('ozonetel_api_key');
        $ozonetel_username=$this->config->item('ozonetel_username');
        $ozonetel_did=$this->config->item('ozonetel_did');
        if($ozonetel_username == "" || $ozonetal_apikey == "" || $userdata["ozonetal_phonename"] == ""){
            $response["status"] = "error";
            $response["messages"] = ["Either Username or API Key is not available in our System"];
        }else if($leadInfo->phonenumber == ""){
            $response["status"] = "error";
            $response["messages"] = ["Lead's phone number do not exist."];
        }else{
             $curlUrl = "https://api1.cloudagent.in/CAServices/PhoneManualDial.php?apiKey=".$ozonetal_apikey."&userName=".$ozonetel_username."&did=".$ozonetel_did."&phoneName=".$userdata["ozonetal_phonename"]."&custNumber=".str_replace(" ","",$leadInfo->phonenumber);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $curlUrl);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
            // This is what solved the issue (Accepting gzip encoding)
            curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");     
            $response = curl_exec($ch);
            curl_close($ch);
            
            $response = json_decode($response, true);
            //print_r($response);die;
            $this->enquiry_model->insert_tblozonetel_response($loggedInUserId, $request["lead_id"],$response);
        }
        
        echo $response = json_encode($response);
        exit;
        
    }

    public function import()
    {
        if (!$this->rbac->hasPrivilege('admission_enquiry', 'can_add')) {
            access_denied();
        }
        $data['title']      = 'Import Leads';
        $data['title_list'] = 'Recently Added Leads';
        $userdata           = $this->customlib->getUserData();

        $fields = array('name','last_name','child_aadhaar_no','nationality','blood_group','child_image','mother_tongue','home_town','locality','religion','social_category','last_school','medium_instruction','previous_board','previous_class','session_id','contact','address','address2','city','state_id','country_id','pincode','landline','reference','dob','gender','father_title','father_aadhar_no','father_firstname','father_lastname','father_email','father_mobile','father_qualification','father_qualification_other','father_occupation','father_occupation_other','father_employer','father_designation','father_designation_other','father_address','father_city','father_city_other','father_pincode','father_workphone','father_annualincome','father_woking_tks','mother_title','mother_firstname','mother_lastname','mother_email','mother_mobile','mother_aadhar_no','mother_qualification','mother_qualification_other','mother_occupation','mother_occupation_other','mother_employer','mother_designation','mother_designation_other','mother_address','mother_city','mother_city_other','mother_pincode','mother_workphone','mother_annualincome','mother_woking_tks','guardian_title','guardian_name','guardian_lastname','guardian_email','guardian_mobile','guardian_aadhar_no','guardian_qualification','guardian_qualification_other','guardian_occupation','guardian_occupation_other','guardian_employer','guardian_designation','guardian_designation_other','guardian_address','guardian_city','guardian_city_other','guardian_state','guardian_pincode','guardian_annualincome','guardian_woking_tks','guardian_telephone','guardian_relation','school_bus','date','description','follow_up_date','note','source','email','assigned','class','no_of_child','school_last_attended','languages_child_can_speak','sibbling_class','sibbling_school','special_traits','interaction_with_peer_group','father_age','mother_age','nuclear_joint_family','definition_good_school','inclusive_education','inculcate_values','takes_care_child','spendtime_importance','spend_with_your_child','interests_hobbies','logical_answers','physical_illness','kps_impression','status','father_email_pref','father_sms_pref','mother_email_pref','mother_sms_pref','guadian_email_pref','guadian_sms_pref','previous_result');

        $data["fields"]       = $fields;
        //$this->form_validation->set_rules('file', $this->lang->line('image'), 'callback_handle_csv_upload');
        
        //if ($this->form_validation->run() == false) {
        if (empty($_FILES["file"])){
            $this->load->view('layout/header', $data);
            $this->load->view('admin/frontoffice/import', $data);
            $this->load->view('layout/footer', $data);
        } else {
            //$session = $this->setting_model->getCurrentSession();
            if (isset($_FILES["file"]) && !empty($_FILES['file']['name'])) {
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                if ($ext == 'csv') {
                    $file = $_FILES['file']['tmp_name'];
                    $this->load->library('CSVReader');
                    $this->load->library('encoding_lib');
                    $result = $this->csvreader->parse_file($file);

                    if (!empty($result)) {
                        $rowcount = 0;
                        for ($i = 1; $i <= count($result); $i++) {

                            $lead_data[$i] = array();
                            $n                = 0;
                            foreach ($result[$i] as $key => $value) {

                                $lead_data[$i][$fields[$n]] = $this->encoding_lib->toUTF8($result[$i][$key]);

                                //$lead_data[$i]['is_active'] = 'yes'; //default fields
                                $n++;
                            }
                            //print_r($lead_data[$i]);echo "<BR>";
                            $insert_id = $this->enquiry_model->add($lead_data[$i]);
                        }
                        die;
                        $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Leads imported successfully.</div>');
                    } else {

                        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">' . $this->lang->line('no_record_found') . '</div>');
                    }
                } else {

                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">' . $this->lang->line('please_upload_CSV_file_only') . '</div>');
                }
            }

            redirect('admin/enquiry/import');
        }
    }

    public function exportformat()
    {
        $this->load->helper('download');
        $filepath = "./backend/import/import_lead_sample_file.csv";
        $data     = file_get_contents($filepath);
        $name     = 'import_lead_sample_file.csv';

        force_download($name, $data);
    }

    public function user_lead_followup() {

        $data=array();
        $userdata = $this->customlib->getUserData();
        $followup=$this->enquiry_model->todays_follow_up($userdata["id"]);
        $new=$this->enquiry_model->new_enquiry($userdata["id"]);
        $html="Hi <strong>".$userdata["name"]."</strong>,<BR><BR>You have <strong>".$new."</strong> New Leads and <strong>".$followup."</strong> Follow Up Leads.<BR><BR><a  class='btn btn-sm btn-primary' href='/admin/enquiry/kanban'>Click to continue</a><BR><BR>";
        echo json_encode(array('html'=>$html,'new'=>$new,'followup'=>$followup),true);
        exit;
    }
    

}
