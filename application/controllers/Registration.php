<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Registration extends Front_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('frontcms_setting_model', 'complaint_Model', 'Visitors_model', 'onlinestudent_model','enquiry_model','category_model'));
        $this->load->library('mailsmsconf');
        $this->config->load("payroll");
        $this->enquiry_status = $this->config->item('enquiry_status');
    }

    public function index()
    {
        $key='';
        $this->data['source'] = $this->enquiry_model->getComplaintSource();
        $this->data['enquiry_type'] = $this->enquiry_model->get_enquiry_type();
        $this->data['session_list'] = $this->enquiry_model->get_sessions();
        $this->data['class_list'] = $this->enquiry_model->getclasses();
        $this->data['country_list'] = $this->enquiry_model->getCountrylist();
        $this->data['state_list'] = array();
        $this->data['categorylist']  = $this->category_model->get();
        $this->data['sch_setting']     = $this->sch_setting_detail;

        if($this->input->get('key')) { 
            $key=$this->input->get('key');
            
            $enquiry_data = $this->enquiry_model->getenquiry($key);
            $this->data['enquiryid'] =$enquiry_data['id'];
            $this->data['enquiry_data'] = $enquiry_data;
            $this->data['state_list'] = $this->enquiry_model->getStateCountrylist($enquiry_data['country_id']);            
        }

        $page = array('title' => 'Online Registration Form', 'meta_title' => 'online registration form', 'meta_keyword' => 'online registration form', 'meta_description' => 'online registration form');
        $this->data['page']    = $page;
        $setting               = $this->frontcms_setting_model->get();
        $this->data['page_side_bar'] = $setting->is_active_sidebar;
        $this->load_theme('registration');
    }

    public function details()
    {
        $key='';
        $this->data['source'] = $this->enquiry_model->getComplaintSource();
        $this->data['enquiry_type'] = $this->enquiry_model->get_enquiry_type();
        $this->data['session_list'] = $this->enquiry_model->get_sessions();
        $this->data['class_list'] = $this->enquiry_model->getclasses();
        $this->data['country_list'] = $this->enquiry_model->getCountrylist();
        $this->data['state_list'] = array();
        $this->data['categorylist']  = $this->category_model->get();
        $this->data['sch_setting']     = $this->sch_setting_detail;
        $this->data['staffusers'] = $this->enquiry_model->getuser_list();
        $this->data['Reference'] = $this->enquiry_model->get_reference();
        $this->data['statuses'] =$this->config->item('enquiry_status');

        if($this->input->get('key')) { 
            $key=$this->input->get('key');
            
            $enquiry_data = $this->enquiry_model->getenquiry($key);
            $this->data['enquiryid'] =$enquiry_data['id'];
            $this->data['enquiry_data'] = $enquiry_data;
            $this->data['state_list'] = $this->enquiry_model->getStateCountrylist($enquiry_data['country_id']);            
        }

        $page = array('title' => 'Online Registration Details', 'meta_title' => 'online registration details', 'meta_keyword' => 'online registration details', 'meta_description' => 'online registration details');
        $this->data['page']    = $page;
        $setting               = $this->frontcms_setting_model->get();
        $this->data['page_side_bar'] = $setting->is_active_sidebar;
        $this->load_theme('registration_details');
    }


    public function registerpost($id=null) {

        $enquiry_update = $this->input->post();        
        if($this->input->post('dob')) $enquiry_update['dob'] = date('Y-m-d', strtotime($this->input->post('dob')));
        if(isset($id)) $insert_id=$this->enquiry_model->enquiry_update($id, $enquiry_update);
        else $insert_id=$this->enquiry_model->add($enquiry_update);

        if (isset($_FILES["child_image"]) && !empty($_FILES['child_image']['name'])) {
            $fileInfo = pathinfo($_FILES["child_image"]["name"]);
            $img_name = $insert_id . "register_child_" . '.' . $fileInfo['extension'];
            move_uploaded_file($_FILES["child_image"]["tmp_name"], "./uploads/student_images/" . $img_name);
            $image_update['child_image'] = 'uploads/student_images/' . $img_name;
            $insert_id=$this->enquiry_model->enquiry_update($id, $image_update);
        }
        if (isset($_FILES["previous_result"]) && !empty($_FILES['previous_result']['name'])) {
            $fileInfo = pathinfo($_FILES["previous_result"]["name"]);
            $img_name = $insert_id . "_previous_result_" . '.' . $fileInfo['extension'];
            move_uploaded_file($_FILES["previous_result"]["tmp_name"], "./uploads/student_images/" . $img_name);
            $image_update['previous_result'] = 'uploads/student_images/' . $img_name;
            $insert_id=$this->enquiry_model->enquiry_update($id, $image_update);
        }
        $fields = "father_firstname, father_lastname, father_email, father_mobile";
        $enquiryFields = $this->enquiry_model->getenquiryfields($insert_id, $fields);
        $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('update_message'),'enquiryid'=>$insert_id, 'enquiry_fields'=>$enquiryFields);
        echo json_encode($array);
    }

    public function insertRegistrationFee(){
        $res = array('status' => 'success', 'error' => '', 'message' => 'Payment successfull');
        $data = $_POST;
        $inserted_id = $this->enquiry_model->insert_registration_fee($_POST);
        if($inserted_id){

            // capture payment
            $curlData = json_encode(array("amount"=>$data["fee_amount"], "currency"=>"INR"));
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"https://api.razorpay.com/v1/payments/".$data["razorpay_payment_id"]."/capture");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$curlData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, $this->config->item('razorpay_key') . ':' . $this->config->item('razorpay_key_secret'));
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $server_output = curl_exec($ch);

            curl_close ($ch);
            $curl_response = json_decode($server_output, true);
            $capture_data = [];
            if(isset($curl_response["error"])){
                $message = $curl_response["error"]["description"];
                echo $message;
                $capture_data = ["status"=>"captured-failed", "capture_response"=>$server_output];
                
            }else{
                $capture_data = ["status"=>"captured", "capture_response"=>$server_output];
                
            }
            $this->enquiry_model->update_registration_fee($data["razorpay_payment_id"], $capture_data);
            
            $this->enquiry_model->enquiry_update($_POST["enquiry_id"], array("assigned"=>6, "status"=>3,"reg_date"=>null));
            $this->sendRegistrationSuccessEmail($_POST["enquiry_id"]);
            echo json_encode($res);
        }else{
            $res["status"] = "fail";
            $res["message"] = "There is some error while storing payment";
            echo json_encode($res);
        }
        return;
    }

    public function sendRegistrationSuccessEmail($enquiryid){
        $data = $this->enquiry_model->getenquiry(null,$enquiryid);//$enquiryid
        
        if($data['father_firstname'] !='' && $data['father_email'] !='') {
           $sender_details = array(
                'title' => $data['father_title'],
                'firstname' => $data['father_firstname'], 
                'email' => $data['father_email'],
                'weburl' => 'https://thekhaitanschool.org/', 
                'logourl' => 'https://thekhaitanschool.org/wp-content/uploads/2015/09/TKS-Big.jpg',                      
                'copyright' => date('Y'),
                'child_name' => $data['name'],
                'classname' => $data['classname'],
                'sessionname' => $data['session'],
                'enquiryid' => $data['enquiryid']
            );
           $response=$this->mailsmsconf->mailsms('student_registration_success',$sender_details);
           ///////////////
           $user=$this->enquiry_model->getAdminIncharge(4); //Anu Gupta's id
           if($user['name'] !='' && $user['email'] !='') {
                $sender_details = array(
                    'firstname' => $user['name'].' '.$user['surname'], 
                    'logourl' => 'https://thekhaitanschool.org/wp-content/uploads/2015/09/TKS-Big.jpg',
                    'email' => $data['email'],
                    'weburl' => 'https://thekhaitanschool.org/', 
                    'copyright' => date('Y'),
                    'child_name' => $data['name'],
                    'enquiry_url' => base_url().'admin/enquiry/leaddetails/'.$enquiryid,
                    'enquiryid' => $data['enquiryid']
                );
               $response=$this->mailsmsconf->mailsms('student_registration_incharge',$sender_details);
           }
        } 
    }

    public function thankyou()
    {
        $razorpay_payment_id = $_GET["key"];

        $response = $this->enquiry_model->findFee($razorpay_payment_id);
        $invalid_paypemnt_id = 0;
        if(count($response) < 1){
            
            $invalid_paypemnt_id = 1;
        }

        $page = array('title' => 'Thank You', 'meta_title' => 'Thank You', 'meta_keyword' => 'Thank You', 'meta_description' => 'Thank You');
        $this->data['page']    = $page;
        $setting               = $this->frontcms_setting_model->get();
        $this->data['page_side_bar'] = $setting->is_active_sidebar;
        $this->data['razorpay_payment_id'] = $razorpay_payment_id;
        $this->data['invalid_paypemnt_id'] = $invalid_paypemnt_id;
        $this->load_theme('thankyou');
    }
    public function getStates() {        
        $country_id = $this->input->post('country_id');
        $array=$this->enquiry_model->getStateCountrylist($country_id);
        $html='';
        foreach ($array as $value) {
            $html .="<option value='".$value['id']."'>".$value['state']."</option>";
        }
        echo json_encode($html);
    }
    
    public function getAdmissionFee(){
        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        //error_reporting(E_ALL);
        $request = $this->input->get();
        if(isset($request["key"]) && $request["key"]){
            $result = $this->enquiry_model->validateAdmissionLink($request["key"]);
            if(count($result) > 0){ 
                $this->data['details'] = $result;
                $enquiry_data = $this->enquiry_model->getenquiry(null, $result[0]["enquiry_id"]);
                $this->data['enquiryid'] = $result[0]["enquiry_id"];
                $this->data['enquiry_data'] = $enquiry_data;
                $this->data['key'] = $request["key"];
            }/*else{
                echo "Payment URL is invalid or Expired.";
                die;
            }*/
        }
        $page = array('title' => 'Upload Document/Pay Admission Fee', 'meta_title' => 'Upload Document/Pay Admission Fee', 'meta_keyword' => 'Pay Admission Fee', 'meta_description' => 'Pay Admission Fee');
        $this->data['page']    = $page;
        $this->data['page_side_bar'] = 0;
        $this->load_theme('get_admission_files');
    }

    public function payAdmissionFee(){
        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        //error_reporting(E_ALL);
        $request = $this->input->get();
        if(isset($request["key"]) && $request["key"]){
            $result = $this->enquiry_model->validateAdmissionLink($request["key"]);
            if(count($result) > 0){ 
                $this->data['details'] = $result;
                $enquiry_data = $this->enquiry_model->getenquiry(null, $result[0]["enquiry_id"]);
                $this->data['enquiryid'] = $result[0]["enquiry_id"];
                $this->data['enquiry_data'] = $enquiry_data;
                $this->data['key'] = $request["key"];
            }/*else{
                echo "Payment URL is invalid or Expired.";
                die;
            }*/
        }
        $page = array('title' => 'Upload Document/Pay Admission Fee', 'meta_title' => 'Upload Document/Pay Admission Fee', 'meta_keyword' => 'Pay Admission Fee', 'meta_description' => 'Pay Admission Fee');
        $this->data['page']    = $page;
        $this->data['page_side_bar'] = 0;
        $this->load_theme('get_admission_fee');
    }

    public function insertAdmissionFee(){
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $res = array('status' => 'success', 'error' => '', 'message' => 'Payment successfull');
        $data = $_POST;
        
        $finalData = array(
                            "enquiry_id"          => $data["details"]["enquiry_id"],
                            "razorpay_payment_id" => $data["razorpay_payment_id"],
                            "fee_amount_before_discount" => $data["details"]["fee_amount_before_discount"],
                            "discount_amount" => $data["details"]["discount_amount"],
                            "fee_amount"          => $data["details"]["fee_amount"],
                            "type"                => 2,
                            "description"         => $data["details"]["description"] 
        );
        $inserted_id = $this->enquiry_model->insert_registration_fee($finalData);
        if($inserted_id){
            //$this->enquiry_model->enquiry_update($_POST["enquiry_id"], array("assigned"=>6, "status"=>3,"reg_date"=>null));
            $this->enquiry_model->disable_admission_link($data["details"]["enquiry_id"], array("activated"=>0));
            
            //$this->sendRegistrationSuccessEmail($_POST["enquiry_id"]);
            // Need to write code for Capture payment
            
            // capture payment
            $curlData = json_encode(array("amount"=>$data["details"]["fee_amount"], "currency"=>"INR"));
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"https://api.razorpay.com/v1/payments/".$data["razorpay_payment_id"]."/capture");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$curlData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, $this->config->item('razorpay_key') . ':' . $this->config->item('razorpay_key_secret'));
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $server_output = curl_exec($ch);

            curl_close ($ch);
            $curl_response = json_decode($server_output, true);
            $capture_data = [];
            if(isset($curl_response["error"])){
                $message = $curl_response["error"]["description"];
                echo $message;
                $capture_data = ["status"=>"captured-failed", "capture_response"=>$server_output];
                
            }else{
                $capture_data = ["status"=>"captured", "capture_response"=>$server_output];
                
            }
            $log_data = array('lead_id'=>$data["details"]["enquiry_id"],'staff_id'=>0,'description' => 'Father has submitted the Admission Fee.' );
            $CI = &get_instance();
            $CI->db->insert('lead_logs', $log_data);
            $this->enquiry_model->update_registration_fee($data["razorpay_payment_id"], $capture_data);
            // update enquiry status to  
            $this->enquiry_model->enquiry_update($data["details"]["enquiry_id"], array('status' =>10));
            echo json_encode($res);
        }else{
            $res["status"] = "fail";
            $res["message"] = "There is some error while storing payment";
            echo json_encode($res);
        }
        return;
    }

    public function admissionsuccess(){
        $razorpay_payment_id = $_GET["key"];

        $response = $this->enquiry_model->findFee($razorpay_payment_id);
        $invalid_paypemnt_id = 0;
        if(count($response) < 1){
            
            $invalid_paypemnt_id = 1;
        }else{
            

        }

        $page = array('title' => 'Thank You', 'meta_title' => 'Thank You', 'meta_keyword' => 'Thank You', 'meta_description' => 'Thank You');
        $this->data['page']    = $page;
        $setting               = $this->frontcms_setting_model->get();
        $this->data['page_side_bar'] = 0;
        $this->data['razorpay_payment_id'] = $razorpay_payment_id;
        $this->data['invalid_paypemnt_id'] = $invalid_paypemnt_id;
        $this->load_theme('admissionsuccess_thankyou');
    }

    public function uploaddocuments(){
                $enquiry_id=$this->input->post('id');
                $uploaddir = './uploads/student_documents/'.date('Y').'/enquiry/';
                if (isset($_FILES["first_doc"]) && !empty($_FILES['first_doc']['name'])) {
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo    = pathinfo($_FILES["first_doc"]["name"]);
                    $first_title = $this->input->post('first_title');
                    $file_name   = $_FILES['first_doc']['name'];
                    $exp         = explode(' ', $file_name);
                    $imp         = implode('_', $exp);
                    $img_name    = $uploaddir . $imp;
                    move_uploaded_file($_FILES["first_doc"]["tmp_name"], $img_name);
                    $data_img = array('enquiry_id' => $enquiry_id, 'title' => $first_title, 'doc' => $imp);
                    $this->enquiry_model->adddoc($data_img);
                }
                if (isset($_FILES["second_doc"]) && !empty($_FILES['second_doc']['name'])) {
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo     = pathinfo($_FILES["second_doc"]["name"]);
                    $second_title = $this->input->post('second_title');
                    $file_name    = $_FILES['second_doc']['name'];
                    $exp          = explode(' ', $file_name);
                    $imp          = implode('_', $exp);
                    $img_name     = $uploaddir . $imp;
                    move_uploaded_file($_FILES["second_doc"]["tmp_name"], $img_name);
                    $data_img = array('enquiry_id' => $enquiry_id, 'title' => $second_title, 'doc' => $imp);
                    $this->enquiry_model->adddoc($data_img);
                }

                if (isset($_FILES["third_doc"]) && !empty($_FILES['third_doc']['name'])) {
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo     = pathinfo($_FILES["third_doc"]["name"]);
                    $third_title = $this->input->post('third_title');
                    $file_name    = $_FILES['third_doc']['name'];
                    $exp          = explode(' ', $file_name);
                    $imp          = implode('_', $exp);
                    $img_name     = $uploaddir . $imp;
                    move_uploaded_file($_FILES["third_doc"]["tmp_name"], $img_name);
                    $data_img = array('enquiry_id' => $enquiry_id, 'title' => $third_title, 'doc' => $imp);
                    $this->enquiry_model->adddoc($data_img);
                }

                if (isset($_FILES["fourth_doc"]) && !empty($_FILES['fourth_doc']['name'])) {
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo     = pathinfo($_FILES["fourth_doc"]["name"]);
                    $fourth_title = $this->input->post('fourth_title');
                    $file_name    = $_FILES['fourth_doc']['name'];
                    $exp          = explode(' ', $file_name);
                    $imp          = implode('_', $exp);
                    $img_name     = $uploaddir . $imp;
                    move_uploaded_file($_FILES["fourth_doc"]["tmp_name"], $img_name);
                    $data_img = array('enquiry_id' => $enquiry_id, 'title' => $fourth_title, 'doc' => $imp);
                    $this->enquiry_model->adddoc($data_img);
                }
                if (isset($_FILES["fifth_doc"]) && !empty($_FILES['fifth_doc']['name'])) {
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo    = pathinfo($_FILES["fifth_doc"]["name"]);
                    $fifth_title = $this->input->post('fifth_title');
                    $file_name   = $_FILES['fifth_doc']['name'];
                    $exp         = explode(' ', $file_name);
                    $imp         = implode('_', $exp);
                    $img_name    = $uploaddir . $imp;

                    move_uploaded_file($_FILES["fifth_doc"]["tmp_name"], $img_name);
                    $data_img = array('enquiry_id' => $enquiry_id, 'title' => $fifth_title, 'doc' => $imp);
                    $this->enquiry_model->adddoc($data_img);
                }

                if (isset($_FILES["sixth_doc"]) && !empty($_FILES['sixth_doc']['name'])) {
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo    = pathinfo($_FILES["sixth_doc"]["name"]);
                    $sixth_title = $this->input->post('sixth_title');
                    $file_name   = $_FILES['sixth_doc']['name'];
                    $exp         = explode(' ', $file_name);
                    $imp         = implode('_', $exp);
                    $img_name    = $uploaddir . $imp;

                    move_uploaded_file($_FILES["sixth_doc"]["tmp_name"], $img_name);
                    $data_img = array('enquiry_id' => $enquiry_id, 'title' => $sixth_title, 'doc' => $imp);
                    $this->enquiry_model->adddoc($data_img);
                }

                if (isset($_FILES["seventh_doc"]) && !empty($_FILES['seventh_doc']['name'])) {
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo    = pathinfo($_FILES["seventh_doc"]["name"]);
                    $seventh_title = $this->input->post('seventh_title');
                    $file_name   = $_FILES['seventh_doc']['name'];
                    $exp         = explode(' ', $file_name);
                    $imp         = implode('_', $exp);
                    $img_name    = $uploaddir . $imp;

                    move_uploaded_file($_FILES["seventh_doc"]["tmp_name"], $img_name);
                    $data_img = array('enquiry_id' => $enquiry_id, 'title' => $seventh_title, 'doc' => $imp);
                    $this->enquiry_model->adddoc($data_img);
                }

                if (isset($_FILES["eigth_doc"]) && !empty($_FILES['eigth_doc']['name'])) {
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo    = pathinfo($_FILES["eigth_doc"]["name"]);
                    $eigth_title = $this->input->post('eigth_title');
                    $file_name   = $_FILES['eigth_doc']['name'];
                    $exp         = explode(' ', $file_name);
                    $imp         = implode('_', $exp);
                    $img_name    = $uploaddir . $imp;

                    move_uploaded_file($_FILES["eigth_doc"]["tmp_name"], $img_name);
                    $data_img = array('enquiry_id' => $enquiry_id, 'title' => $eigth_title, 'doc' => $imp);
                    $this->enquiry_model->adddoc($data_img);
                }

                if (isset($_FILES["ninth_doc"]) && !empty($_FILES['ninth_doc']['name'])) {
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo    = pathinfo($_FILES["ninth_doc"]["name"]);
                    $ninth_title = $this->input->post('ninth_title');
                    $file_name   = $_FILES['ninth_doc']['name'];
                    $exp         = explode(' ', $file_name);
                    $imp         = implode('_', $exp);
                    $img_name    = $uploaddir . $imp;

                    move_uploaded_file($_FILES["ninth_doc"]["tmp_name"], $img_name);
                    $data_img = array('enquiry_id' => $enquiry_id, 'title' => $ninth_title, 'doc' => $imp);
                    $this->enquiry_model->adddoc($data_img);
                }

                if (isset($_FILES["tenth_doc"]) && !empty($_FILES['tenth_doc']['name'])) {
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo    = pathinfo($_FILES["tenth_doc"]["name"]);
                    $tenth_title = $this->input->post('tenth_title');
                    $file_name   = $_FILES['tenth_doc']['name'];
                    $exp         = explode(' ', $file_name);
                    $imp         = implode('_', $exp);
                    $img_name    = $uploaddir . $imp;

                    move_uploaded_file($_FILES["tenth_doc"]["tmp_name"], $img_name);
                    $data_img = array('enquiry_id' => $enquiry_id, 'title' => $tenth_title, 'doc' => $imp);
                    $this->enquiry_model->adddoc($data_img);
                }

                if (isset($_FILES["eleventh_doc"]) && !empty($_FILES['eleventh_doc']['name'])) {
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo    = pathinfo($_FILES["eleventh_doc"]["name"]);
                    $eleventh_title = $this->input->post('eleventh_title');
                    $file_name   = $_FILES['eleventh_doc']['name'];
                    $exp         = explode(' ', $file_name);
                    $imp         = implode('_', $exp);
                    $img_name    = $uploaddir . $imp;

                    move_uploaded_file($_FILES["eleventh_doc"]["tmp_name"], $img_name);
                    $data_img = array('enquiry_id' => $enquiry_id, 'title' => $eleventh_title, 'doc' => $imp);
                    $this->enquiry_model->adddoc($data_img);
                }

                if (isset($_FILES["twelveth_doc"]) && !empty($_FILES['twelveth_doc']['name'])) {
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo    = pathinfo($_FILES["twelveth_doc"]["name"]);
                    $twelveth_title = $this->input->post('twelveth_title');
                    $file_name   = $_FILES['twelveth_doc']['name'];
                    $exp         = explode(' ', $file_name);
                    $imp         = implode('_', $exp);
                    $img_name    = $uploaddir . $imp;

                    move_uploaded_file($_FILES["twelveth_doc"]["tmp_name"], $img_name);
                    $data_img = array('enquiry_id' => $enquiry_id, 'title' => $twelveth_title, 'doc' => $imp);
                    $this->enquiry_model->adddoc($data_img);
                }

                $array = array('status' => 'success', 'error' => '', 'message' => 'The document has been uploaded successfully.');
                echo json_encode($array);
    }

}

?>