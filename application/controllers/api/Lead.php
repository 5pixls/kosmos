<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Lead extends REST_Controller {
    	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
     //  header('Access-Control-Allow-Origin: *');
      // header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    }
     
	/*public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("items", ['id' => $id])->row_array();
        }else{
            $data = $this->db->get("items")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}*/
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $input = $this->input->post();
        $input = array_filter($input);
        //echo "FATHER---".$_REQUEST['father_firstname'];
        //print_r($input).'---';die;
        
        $requiredparameters = array('childname','father_firstname','father_mobile','father_email','class','city','source'); 
        /*$requiredparameters = array(
                'name','gender','class','dob','social_category','mother_tongue','home_town','session_id','religion','nationality','blood_group','child_aadhaar_no','last_school','previous_board','previous_class','address','address2','city','state_id','father_title','father_firstname','father_email','father_mobile','father_employer','mother_title','mother_firstname','mother_email','mother_mobile','mother_employer');*/
        /*$requiredparameters = array(
                'name','last_name','gender','class','dob','social_category','mother_tongue','home_town','session_id','religion','nationality','blood_group','child_aadhaar_no','last_school','medium_instruction','previous_board','previous_class','address','address2','city','state_id','country_id','father_title','father_firstname','father_lastname','father_email','father_mobile','father_qualification','father_employer','father_aadhar_no' ,'father_occupation','father_designation','father_address','father_city','father_pincode','father_workphone','father_annualincome','father_woking_tks' ,'mother_title','mother_firstname','mother_lastname' ,'mother_email','mother_qualification','mother_employer','mother_aadhar_no','mother_occupation','mother_designation','mother_address','mother_city','mother_pincode','mother_workphone','mother_annualincome','mother_woking_tks','guardian_name','guardian_address','guardian_telephone','guardian_relation');*/
        $validation = $this->parameterValidation($requiredparameters,  $input); //$_POST holds post values
        if($validation=='valid') {
            $input['status']=1;
            $input['date']=date('Y-m-d');
            $nameArray = explode(" ", $input['father_firstname']);
            $input['father_firstname']=$nameArray[0];
            $input['father_lastname']=$nameArray[1];
            $childnameArray = explode(" ", $input['childname']);
            $input['name']=$childnameArray[0];
            $input['last_name']=$childnameArray[1];
            $input['class']=$this->class_model->getCallbyName($input['class']);
            if($input['session_id'] != 0) $input['session_id']=$this->class_model->getSessionName($input['session_id']);
            unset($input['childname']);
            //$this->db->insert('enquiry',$input);
            $this->load->model("enquiry_model");
            $enquiryid=$this->enquiry_model->add($input); 
            //$id=$this->db->insert_id();
            //$this->db->where('id', $id);
            //$this->db->update('enquiry', array('key' => md5($id) ));
            //echo $this->db->last_query();
            $this->response(['enquiry created successfully.'], REST_Controller::HTTP_OK);
        }
        else {
            $this->response([$validation], REST_Controller::HTTP_BAD_REQUEST);
        }
        
    } 
     
    public function parameterValidation($required,$postvalues)
    {
        $missing = array();
        foreach($required as $field) {
            if(!isset($postvalues[$field])) {
            $error = true;
            $missing[] = $field;
            }
        }
        if(count($missing)>=1) {
            $count = (count($missing)>=2?'are':'is');
            return implode(", ", $missing).' '.$count.' required.';
        }
        else {
            return 'valid';
        } 
    }

    

    public function sanitize_post($item, $key)
    {
        $clean_values[$key] = trim($item);
        //optional further cleaning ex) htmlentities
    }
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    /*public function index_put($id)
    {
        $input = $this->put();
        $this->db->update('items', $input, array('id'=>$id));
     
        $this->response(['Item updated successfully.'], REST_Controller::HTTP_OK);
    }*/
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    /*public function index_delete($id)
    {
        $this->db->delete('items', array('id'=>$id));
       
        $this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
    }*/
    	
}