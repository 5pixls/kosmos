<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Ozonetel_webhook extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("enquiry_model");
    }
    public function ozonetelWebhook(){
        try{
            
            $res = $this->input->post();

            $data = array();
            $arr=$res['data'];
            $arr= json_decode($arr, true);
            if(isset($arr)){
                foreach($arr as $key => $value){
                    $data[$key] =$value;
                }
                $data["json_response"] = json_encode($res);
            }
            //file_put_contents("./uploads/newfile.txt", print_r($data, true));
            
            if(isset($data)){
                $insert["ucid"] = $data["monitorUCID"];
                $insert["called_no"] = $data["CallerID"]; 
                $insert["caller_id"] = $data["AgentPhoneNumber"];
                $insert["call_duration"] = $data["CallDuration"];
                $insert["call_start_time"] = $data["StartTime"];
                $insert["dialed_number"] = $data["DialedNumber"];
                $insert["dial_start_time"] = $res["StartTime"];
                $insert["dial_end_time"] = $data["EndTime"];
                $insert["recording_url"] = $data["AudioFile"];
                $insert["call_status"] = $data["Status"];
                $insert["call_type"] = $data["Type"];
                $insert["disconnect_type"] = $data["HangupBy"];
                $insert["disconnect_type"] = $data["HangupBy"];
                //$data["department"] = $res["Department"];
                //$data["extn"] = $res["Extn"];
                $insert["uui"] = $data["UUI"];
                $insert["json_response"] = $data["json_response"];
                echo $this->enquiry_model->insert_ozonetel_webhook($insert);
            }
        }catch(Exception $e){
            echo $e->getMessage();

        }
        
    }
}
