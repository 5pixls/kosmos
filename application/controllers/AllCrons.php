<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AllCrons extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('mailsmsconf');
        $this->load->model("enquiry_model");
    }

    public function enquiryReminder(){
        $enquiries = $this->enquiry_model->getenquiry_reminder();
        //print_r($enquiries);
        foreach ($enquiries as $data) {    
            if($data['days'] == 1 || $data['days'] == 2 || $data['days'] == 3) {        
                if($data['father_firstname'] !='' && $data['father_email'] !='' && $data['key'] !='') {
                   $personalised_link= base_url().'registration/details?key='.$data['key'];
                   $date = date('d-M-Y', strtotime($data['reg_date']. ' + 7 days'));
                   $sender_details = array(
                        'title' => $data['father_title'], 
                        'firstname' => $data['father_firstname'], 
                        'email' => $data['father_email'],
                        'personalised_link' => $personalised_link,
                        'weburl' => 'https://thekhaitanschool.org/', 
                        'logourl' => 'https://thekhaitanschool.org/wp-content/uploads/2015/09/TKS-Big.jpg',                      
                        'date' => $date, 
                        'apply_link' => base_url().'registration',
                        'disable_notification_link' => '#',
                        'copyright' => date('Y')
                    );
                  // print_r($sender_details);
                   $response=$this->mailsmsconf->mailsms('student_registration_link_reminder',$sender_details);     

                } 
            }
        }
    }

}
