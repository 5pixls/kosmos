<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Registrationfee extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('smsgateway');
        $this->load->library('mailsmsconf');
        $this->search_type        = $this->config->item('search_type');
        $this->sch_setting_detail = $this->setting_model->getSetting();
        $this->load->model('registrationfee_model');
    }

    public function index()
    {
        if (!$this->rbac->hasPrivilege('collect_fees', 'can_view')) {
            access_denied();
        } 

        //$this->session->set_userdata('top_menu', $this->lang->line('fees_collection'));
        //$this->session->set_userdata('sub_menu', 'studentfee/index');

        $this->session->set_userdata('top_menu', $this->lang->line('fees_collection'));
        $this->session->set_userdata('sub_menu', 'registrationfee');
        $data['class_list'] = $this->class_model->get();
        $data["search_query"] = "";
        $data["status"] = "";
        

        if (isset($_POST["search"])) {
            
            $registration_fee_date = $this->input->post("registration_fee_date");
            $search_query = $this->input->post("search_query");
            if (!empty($registration_fee_date)) {
                $exp = explode(" - ", $registration_fee_date);
                $date_from = date("Y-m-d", strtotime($exp[0]));
                $date_to = date("Y-m-d", strtotime($exp[1]));
            } else {
                $date_from = "";
                $date_to = "";
            }

            $data["search_query"] = $search_query;
            $registration_fee_list = $this->registrationfee_model->search_registration_fee($search_query, $date_from, $date_to);
           
        } else {
            
            $registration_fee_list = $this->registrationfee_model->get_registration_fee_list();
            
        }
        $data['registration_fee_list'] = $registration_fee_list;

        $this->load->view('layout/header');
        $this->load->view('studentfee/registrationfee', $data);
        $this->load->view('layout/footer');

    }
}    