<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class enquiry_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->current_session = $this->setting_model->getCurrentSession();
        $this->current_session_name = $this->setting_model->getCurrentSessionName();
        $this->start_month = $this->setting_model->getStartMonth();
    }

    public function getclasses($id = null) {
        $this->db->select()->from('classes');
        if ($id != null) {
            $this->db->where('id', $id);
        } else {
            $this->db->order_by('id');
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    function getCountrylist() {
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('status', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    function getPaymentInfo($enquiry_id) {
        $this->db->select('*');
        $this->db->from('razorpay_registration_fees');
        $this->db->where('enquiry_id', $enquiry_id);
        $this->db->where('type', '1');
        $query = $this->db->get();
        return $query->row_array();
    }
    function getStateCountrylist($country_id) {
        $this->db->select('*');
        $this->db->from('states');
        $this->db->where('country_id', $country_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_enquiry_type() {
        $this->db->select('*');
        $this->db->from('enquiry_type');
        $query = $this->db->get();
        return $query->result_array();
    }

    function getComplaintSource() {

        $this->db->select('*');
        $this->db->from('source');
        $query = $this->db->get();
        return $query->result_array();
    }

    function getComplaintType() {
        $this->db->select('*');
        $this->db->from('complaint_type');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_reference() {
        $this->db->select('*');
        $this->db->from('reference');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_sessions() {
        $this->db->select('*');
        $this->db->from('sessions');
        $this->db->where('is_active', 'yes');
        $query = $this->db->get();
        return $query->result_array();
    }
    

    public function add($data) {
        ////////////Lead assign to telecaller///////////////        
        $this->db->select('assigned');
        $this->db->from('enquiry');
        $this->db->where('status', 1);
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $lead= $query->row_array();

        $arr = array(0 =>3, 1 => 5);//telecaller ids 

        if(empty($lead) || $lead['assigned'] == ''){  
            $key = array_rand($arr);  // select random telecaller id  
            $assigned= $arr[$key]; // Display the random array element 
        }
        else {
            $del_id=$lead['assigned'];
            foreach (array_keys($arr, $del_id) as $key) {
                unset($arr[$key]);
             }
             $key=array_keys($arr)[0];//array_key_first($arr);
            $assigned=$arr[$key];
        }
        //////////////////////////

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        //=======================Code Start===========================
        $this->db->insert('enquiry', $data);

        $id=$this->db->insert_id();

        $this->db->where('id', $id);
        $this->db->update('enquiry', array('assigned'=>$assigned,'enquiryid' =>'E'.date('Y').'-'.$id,'key' => md5($id) ));

        $message      = INSERT_RECORD_CONSTANT." On  enquiry id ".$id;
        $action       = "Insert";
        $record_id    = $id;
        $this->log($message, $record_id, $action);
		//echo $this->db->last_query();die;
        //======================Code End==============================

        $this->db->trans_complete(); # Completing transaction
        /*Optional*/

        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;

        } else {
            return $id;
        }
        return $id;
    } 

    public function getenquiry_list($id = null,$status=null,$date_from='',$date_to='') {

        $userdata  = $this->customlib->getUserData();
        $role_id = $userdata["role_id"];

        $enquiry_status = $this->config->item('enquiry_status');
        if(in_array($role_id, array(9,10,11))) $enquiry_status = $this->config->item('enquiry_status'.'_'.$role_id);
        $statuses=array_keys($enquiry_status);

        $this->db->select('enquiry.*,CONCAT(staff.name, " ",staff.surname) as staffname,classes.class as classname');
        $this->db->join("classes","enquiry.class = classes.id","left");
        $this->db->join("staff","staff.id = enquiry.assigned","left");
        if(!empty($id)){
            $this->db->where("enquiry.id",$id);
        }
        else if(!empty($id) && !empty($status)){
            $this->db->where("enquiry.id",$id);
            $this->db->where('enquiry.status', $status);
        }
        else $this->db->where_in('enquiry.status', $statuses);

        if((!empty($date_from)) && (!empty($date_to))) {
            $this->db->where("DATE(enquiry.created_at) >= ", $date_from);
            $this->db->where("DATE(enquiry.created_at) <= ", $date_to);
        }

        $this->db->order_by("enquiry.id","desc");
        $query = $this->db->get("enquiry");
        //echo $this->db->last_query();
        if(!empty($id)){
            $result= $query->row_array();    
        }
        else if(!empty($id)  && !empty($status)){
            $result= $query->row_array();    
        }else{

          $result= $query->result_array();
          
        }
           // echo $this->db->last_query();
        return $result;

    }

    public function getenquiry_reminder() {

        $date = date('Y-m-d', strtotime("-3 day"));
        $this->db->select('id,father_firstname,father_email,key,reg_date,DATEDIFF(NOW(), reg_date ) as days');
        $this->db->where('enquiry.status', 2);
        $this->db->where('DATE(enquiry.reg_date) >=', $date);
        $this->db->order_by("enquiry.id","desc");
        $query = $this->db->get("enquiry");
        //echo $this->db->last_query();
        $result= $query->result_array();
        return $result;
    }

    public function getenquiry($key=null,$id=null) {
        $this->db->select('enquiry.*,classes.class as classname,session');
        $this->db->join("classes","enquiry.class = classes.id","left");
        $this->db->join("sessions","enquiry.session_id = sessions.id","left");
        if($key != null) {
            $this->db->where("enquiry.key",$key);
            $this->db->where("DATEDIFF(NOW(),enquiry.reg_date) <=",7);
        }
        else if($id != null) $this->db->where("enquiry.id",$id);
        $query = $this->db->get("enquiry");
        //echo $this->db->last_query();
        $result= $query->row_array();
        return $result;
    }

    public function getenquiryfields($id=null, $fields=null) {
        $this->db->select($fields);
        if($id != null) $this->db->where("enquiry.id",$id);
        $query = $this->db->get("enquiry");
        //echo $this->db->last_query();
        $result= $query->row_array();
        return $result;
    }

    public function get_lead_activity_log($leadId){
        $this->db->select('lead_logs.*, CONCAT(staff.name, " ",staff.surname) as name');
        $this->db->join('staff', 'lead_logs.staff_id =`staff`.`id`', 'left');
        $this->db->where('lead_logs.lead_id', $leadId);
        $this->db->order_by('lead_logs.created_at', "desc");
        $result=$this->db->get('lead_logs')->result_array();
        //echo $this->db->last_query(); die("---dfdf");
        return $result;
    }

    public function get_ozonetel_activity_log($leadId){
        $this->db->select('tblozonetel_response.*, CONCAT(staff.name, " ",staff.surname) as name, '.'tblozonetel_webhook.call_duration, '.'tblozonetel_webhook.call_start_time, '.'tblozonetel_webhook.dial_end_time, '.'tblozonetel_webhook.recording_url');
        
        $this->db->join('staff', 'tblozonetel_response.agent_id = '.'staff.id', 'left');
        $this->db->join('tblozonetel_webhook', 'tblozonetel_response.ucid = '.'tblozonetel_webhook.ucid', 'left');

        $this->db->where('tblozonetel_response.lead_id', $leadId);
        $this->db->order_by('tblozonetel_response.created_at', "desc");
        $result=$this->db->get('tblozonetel_response')->result_array();
        //echo $this->db->last_query(); die("---dfdf");
        return $result;
    }

    public function insert_tblozonetel_response($loggedInUserId, $lead_id, $data){
        $inputData = [];
        $inputData["response_message"] = $data["message"];
        $inputData["lead_id"] = $lead_id;
        $inputData["agent_id"] = $loggedInUserId;
        $inputData["status"] = $data["status"];
        $inputData["ucid"] = $data["message"];
        $inputData["api_json_response"] = json_encode($data);
        $this->db->insert('tblozonetel_response', $inputData);
        
    }

    public function get_ozonetel_info($loggedInUserId){
        $this->db->select('*');
        $this->db->where('staff.id', $loggedInUserId);
        $query = $this->db->get("staff");
        //echo $this->db->last_query();
        return $query->row_array();
    }

    public function get_lead_phone($leadId){
        $this->db->select('father_mobile as phonenumber');
        $this->db->where('enquiry.id', $leadId);
        return $this->db->get('enquiry')->row();
    }

    public function insert_ozonetel_webhook($data){
        $this->db->insert('tblozonetel_webhook', $data);
    }

    public function getFollowByEnquiry($id) {

        $query = $this->db->select("*")->where("enquiry_id", $id)->order_by("id", "desc")->get("follow_up");

        return $query->row_array();
    }

    public function lastFollowByEnquiry($id) {

        $query = $this->db->select("*")->where("enquiry_id", $id)->order_by("id", "desc")->limit(1)->get("follow_up");

        return $query->row_array();
    }


    public function getfollow_up_list($enquiry_id, $follow_up = null) {
        $this->db->select('follow_up.id,follow_up.enquiry_id,follow_up.date,follow_up.next_date,follow_up.response,follow_up.other_response,follow_up.note,CONCAT(staff.name, '.', staff.surname) AS followup_by')->from('follow_up');
        if ($follow_up != null) {
            $this->db->join("staff","staff.id = follow_up.followup_by","left");
            $this->db->where('follow_up.id', $follow_up);
            $this->db->where('follow_up.enquiry_id', $enquiry_id);
            $this->db->order_by('follow_up.id desc');
        } else {
            $this->db->join("staff","staff.id = follow_up.followup_by","left");
            $this->db->where('follow_up.enquiry_id', $enquiry_id);
            $this->db->order_by('follow_up.id desc');
        }
        $query = $this->db->get();
       // echo $this->db->last_query();
        if ($follow_up != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function add_follow_up($data) {
        $this->db->insert('follow_up', $data);
    }

    public function follow_up_update($enquiry_id, $follow_up_id, $data) {
        $this->db->where('id', $follow_up_id);
        $this->db->where('enquiry_id', $enquiry_id);
        $this->db->update('follow_up', $data);
        redirect('admin/enquiry/follow_up_edit/' . $enquiry_id . '/' . $follow_up_id . '');
    }

    public function enquiry_update($id, $data) {
		$this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        //=======================Code Start===========================
        $this->db->where('id', $id);
        $this->db->update('enquiry', $data);
        //echo $this->db->last_query();
		$message      = UPDATE_RECORD_CONSTANT." On  enquiry id ".$id;
        $action       = "Update";
        $record_id    = $id;
        $this->log($message, $record_id, $action);
		//======================Code End==============================

        $this->db->trans_complete(); # Completing transaction
        /*Optional*/

        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;

        } else {
         //return $id;
        }
        return $id;
       
    }

    public function enquiry_delete($id) {
		$this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        //=======================Code Start===========================
        $this->db->where('id', $id);
        $this->db->delete('enquiry');
		$message      = DELETE_RECORD_CONSTANT." On  enquiry id ".$id;
        $action       = "Delete";
        $record_id    = $id;
        $this->log($message, $record_id, $action);
		//======================Code End==============================
        $this->db->trans_complete(); # Completing transaction
        /*Optional*/
        if ($this->db->trans_status() === false) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        } else {
        //return $return_value;
        }
    }

    public function delete_follow_up($id) {
        $this->db->where('id', $id);
        $this->db->delete('follow_up');
    }

    public function next_follow_up_date($enquiry_id) {
        $this->db->select('*');
        $this->db->from('follow_up');
        $this->db->where('enquiry_id', $enquiry_id);
        $this->db->order_by("id","desc");
        $this->db->limit(1);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->row_array();
        /*$id = $data['id'];
        $this->db->select('*');
        $this->db->from('follow_up');
        $this->db->where('id', $id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();*/
    }

    public function changeStatus($data) {

        $this->db->where("id", $data["id"])->update("enquiry", $data);
    }

    public function searchEnquiry($source, $status='1', $date_from, $date_to, $enquiryid) {

        $this->load->library('customlib');
        $userdata  = $this->customlib->getUserData();
        $condition = 0;

        if (!empty($source)) {

            $condition = 1;
            $this->db->where("source", $source);
        }

        if($status != ''){
            $condition = 1;
            $this->db->where("status", $status);
        }else{
            $role_id = $userdata["role_id"];

            $enquiry_status = $this->config->item('enquiry_status');
            if(in_array($role_id, array(9,10,11))) $enquiry_status = $this->config->item('enquiry_status'.'_'.$role_id);
            $statuses=array_keys($enquiry_status);
            $this->db->where_in("status", $statuses);

            $condition = 1;
        }

        if((!empty($date_from)) && (!empty($date_to))) {
            $condition = 1;
            $this->db->where("DATE(enquiry.created_at) >= ", $date_from);
            $this->db->where("DATE(enquiry.created_at) <= ", $date_to);
        }

        if($condition == 0){

            $this->db->where("enquiry.status",1);
        }

        if($enquiryid != ''){

            $this->db->where("enquiry.enquiryid like", '%'.trim($enquiryid).'%');
        }

        $query = $this->db->select('enquiry.*,classes.class as classname')->join("classes", "classes.id = enquiry.class", "left")->get("enquiry");
        return $query->result_array();
    }

    public function gettelecallers() {
        $users = $this->db->query("SELECT `staff_id` as id FROM `staff`
        LEFT JOIN `staff_roles` ON `staff_roles`.`staff_id` = `staff`.`id`
        LEFT JOIN `roles` ON `roles`.`id` = `staff_roles`.`role_id`
        WHERE `staff_roles`.`role_id` IN (9,10)")->result_array();
        return $users;
    }

    public function getuser_list() {
        $users = $this->db->query("SELECT `staff`.* FROM `staff`
        LEFT JOIN `staff_roles` ON `staff_roles`.`staff_id` = `staff`.`id`
        LEFT JOIN `roles` ON `roles`.`id` = `staff_roles`.`role_id`
        WHERE `roles`.`is_superadmin` = 0")->result_array();
        return $users;
    }
    public function getassigneduser($id) {
        $users = $this->db->query("SELECT name,surname FROM `staff` WHERE `id` = ".$id)->row_array();
        return $users;
    }

    public function get_enquiry_counts($enquiry_status){
       $data= array();
       if(!empty($enquiry_status)){
           foreach ($enquiry_status as $key => $value) {
                $this->db->select('count(`id`) as count');
                $this->db->from('enquiry');
                $this->db->where('status', $key);
                $query = $this->db->get();
                $data[$key] = $query->row_array();
                //echo $this->db->last_query();
           }
       }
       //print_r($data);
       return $data;
    }

    public function getenquiryStatus($enquiry_id) {
        $this->db->select('status');
        $this->db->from('enquiry');
        $this->db->where('id', $enquiry_id);
        $query = $this->db->get();
        $data = $query->row_array();
        return $data['status'];
    }

    public function insert_registration_fee($data){
        $this->db->insert('razorpay_registration_fees', $data);
        //$this->db->last_query();
        return $id = $this->db->insert_id();
    }

    public function update_registration_fee($razorpay_payment_id, $data){
        $this->db->where('razorpay_payment_id', $razorpay_payment_id);
        $this->db->update('razorpay_registration_fees', $data);
    }

    public function insert_admission_link($data){
        $this->db->select('*');
        $this->db->from('enquiry_admission_link');
        $this->db->where('enquiry_id', $data["enquiry_id"]);
        $query = $this->db->get();
        $result = $query->result_array();
        
        if(count($result) == 0){
            $this->db->insert('enquiry_admission_link', $data);
            return $id = $this->db->insert_id();
        }else{
            $this->db->where('enquiry_id', $data["enquiry_id"]);
            $this->db->update('enquiry_admission_link', $data);
        }

        
    }

    public function disable_admission_link($enquiry_id, $data){
        $this->db->where('enquiry_id', $enquiry_id);
        $this->db->update('enquiry_admission_link', $data);
    }

    public function validateAdmissionLink($key){
        
        $this->db->select('*');
        $this->db->from('enquiry_admission_link');
        $this->db->where('encrypted_link', $key);
        $this->db->where('activated', 1);
        $this->db->where('valid_till >=', date('Y-m-d H:i:s'));
        $query = $this->db->get();
        return $result = $query->result_array();
    }

    public function findFee($payment_id){
        $this->db->select('*');
        $this->db->from('razorpay_registration_fees');
        $this->db->where('razorpay_payment_id', $payment_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function userfollow_up_list($users) {
        $this->db->select('follow_up.*, enquiryid, father_title,father_firstname,father_lastname')->from('follow_up');
            $this->db->join("enquiry","enquiry.id = follow_up.enquiry_id","left");
            $this->db->where_in('enquiry.assigned', $users);
            $this->db->where('DATE(next_date) >=', date('Y-m-d'));
            $this->db->order_by('follow_up.id desc');
        $query = $this->db->get();
       // echo $this->db->last_query();
        return $query->result_array();
    }

    public function getAdminIncharge($id) {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function todays_follow_up($user_id) {
        $result= $this->db->query('Select Count(*) as follow_up_count from follow_up, enquiry where follow_up.enquiry_id= enquiry.id AND DATE(next_date) = CURDATE() AND assigned='.$user_id)->row_array();
        return $result['follow_up_count'];
    }
    public function new_enquiry($user_id){
        $this->db->select('count(`id`) as new_count');
        $this->db->from('enquiry');
        $this->db->where('status', 1);
        $this->db->where('assigned', $user_id);
        $query = $this->db->get();
        $result= $query->row_array();
        return $result['new_count'];
    }

    public function enquiryReport($date_from, $date_to) {

        $this->db->select('*')->from('classes');        
        $this->db->order_by("sort", 'asc');
        $query = $this->db->get();
        // echo $this->db->last_query();
        $result=$query->result_array();
        $statuses=$this->config->item('enquiry_status');
        foreach ($result as $k => $v) {
            $sc=array();
            $sum=0;
            foreach ($statuses as $key => $value) {
                $this->db->select('COUNT(status) as count')->from('enquiry');
                $this->db->where("created_at >= ", $date_from);
                $this->db->where("created_at <= ", $date_to);                
                $this->db->where("status", $key);
                $this->db->where("class", $v['id']);
                $query = $this->db->get();
                $c=$query->row_array();
                //echo $this->db->last_query();
                if(!empty($c)){
                    $sum+= $c["count"];
                    $sc[$key]["count"] = $c["count"];
                }
                else $sc[$key]["count"] = 0;
                $sc[$key]["status"] = $value;
            }
            $result[$k]["statuses"] = $sc;
            $result[$k]["sum"] = $sum;             
        }
        //print_r($result);
        return $result;
    }

    public function enquiryReportTotal($date_from, $date_to) {
        
        $statuses=$this->config->item('enquiry_status');
        $sc=array();
        foreach ($statuses as $key => $value) {
            $this->db->select('count(status) as count')->from('enquiry');
            $this->db->where("created_at >= ", $date_from);
            $this->db->where("created_at <= ", $date_to);                
            $this->db->where("status", $key);
            $query = $this->db->get();
            $c=$query->row_array();
            //echo $this->db->last_query();
            if(!empty($c)){
                $sc[$key]["count"] = $c["count"];
            }
            else $sc[$key]["count"] = 0;
        }
        //print_r($sc);
        return $sc;
    }

    public function enquiryClassSourceReport($date_from, $date_to) {
        
        $this->db->select('*')->from('classes');  
        $this->db->order_by("sort", 'asc');      
        $query = $this->db->get();
       // echo $this->db->last_query();
        $result=$query->result_array();
        $sources=$this->getComplaintSource();
        //print_r($sources);
        foreach ($result as $k => $v) {
            $sc=array();
            $sum=0;
            foreach ($sources as $key => $value) {
                $this->db->select('COUNT(status) as count')->from('enquiry');
                $this->db->where("created_at >= ", $date_from);
                $this->db->where("created_at <= ", $date_to);                
                $this->db->where("source", $value['source']);
                $this->db->where("class", $v['id']);
                $query = $this->db->get();
                $c=$query->row_array();
                //echo $this->db->last_query();
                if(!empty($c)){
                    $sum+= $c["count"];
                    $sc[$key]["count"] = $c["count"];
                }
                else $sc[$key]["count"] = 0;
                $sc[$key]["source"] = $value['source'];
            }
            $result[$k]["sources"] = $sc;
            $result[$k]["sum"] = $sum;             
        }
        //print_r($result);
        return $result;
    }

    public function enquiryClassSourceReportTotal($date_from, $date_to) {
        
        
        $sources=$this->getComplaintSource();
        //print_r($sources);
        $sc=array();
        foreach ($sources as $key => $value) {
            $this->db->select('COUNT(status) as count')->from('enquiry');
            $this->db->where("created_at >= ", $date_from);
            $this->db->where("created_at <= ", $date_to);                
            $this->db->where("source", $value['source']);
            $query = $this->db->get();
            $c=$query->row_array();
            //echo $this->db->last_query();
            if(!empty($c)){
                $sum+= $c["count"];
                $sc[$key]["count"] = $c["count"];
            }
            else $sc[$key]["count"] = 0;
            $sc[$key]["source"] = $value['source'];
        }
        //print_r($sc);
        return $sc;
    }

    public function enquirySourceStatusReport($date_from, $date_to) {        
        
        $statuses=$this->config->item('enquiry_status');
        $result=$this->getComplaintSource();
        
        foreach ($result as $k => $v) {
            $sc=array();
            $sum=0;
            foreach ($statuses as $key => $value) {
                $this->db->select('COUNT(status) as count')->from('enquiry');
                $this->db->where("created_at >= ", $date_from);
                $this->db->where("created_at <= ", $date_to);                
                $this->db->where("source", $v['source']);
                $this->db->where("status", $key);
                $query = $this->db->get();
                $c=$query->row_array();
                //echo $this->db->last_query();
                if(!empty($c)){
                    $sum+= $c["count"];
                    $sc[$key]["count"] = $c["count"];
                }
                else $sc[$key]["count"] = 0;
                $sc[$key]["status"] = $value;
            }
            $result[$k]["statuses"] = $sc;
            $result[$k]["sum"] = $sum;             
        }
        //print_r($result);
        return $result;
    }
    public function enquirySourceStatusReportTotal($date_from, $date_to) {        
        
        $statuses=$this->config->item('enquiry_status');        
        $sc=array();
        foreach ($statuses as $key => $value) {
            $this->db->select('COUNT(status) as count')->from('enquiry');
            $this->db->where("created_at >= ", $date_from);
            $this->db->where("created_at <= ", $date_to);                
            $this->db->where("status", $key);
            $query = $this->db->get();
            $c=$query->row_array();
            //echo $this->db->last_query();
            if(!empty($c)){
                $sc[$key]["count"] = $c["count"];
            }
            else $sc[$key]["count"] = 0;
        }
        //print_r($result);
        return $sc;
    }

    public function adddoc($data)
    {
        $this->db->select('*')->from('enquiry_doc');
        $this->db->where("enquiry_id", $data['enquiry_id']);
        $this->db->where("title", $data['title']);
        $query = $this->db->get();
        $c=$query->row_array();
        if(!empty($c)){
            $this->db->where('id', $c['id']);
            $this->db->update('enquiry_doc', $data);
        }else {
            $this->db->insert('enquiry_doc', $data);
            return $this->db->insert_id();
        }
                
    }

    public function getDocumentsInfo($id)
    {
        $this->db->select('*')->from('enquiry_doc');
        $this->db->where("enquiry_id", $id);
        $query = $this->db->get();
        $c=$query->result_array();
        return $c;
    }
    
}
