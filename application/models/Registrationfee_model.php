<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Registrationfee_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->current_session = $this->setting_model->getCurrentSession();
        $this->current_session_name = $this->setting_model->getCurrentSessionName();
        $this->start_month = $this->setting_model->getStartMonth();
    }

    public function get_registration_fee_list() {
        
        $userdata  = $this->customlib->getUserData();
        ;

        $this->db->select('razorpay_registration_fees.*,enquiry.father_firstname, enquiry.father_lastname, enquiry.father_mobile, enquiry.name, enquiry.last_name,enquiry.enquiryid');
        $this->db->join("enquiry","razorpay_registration_fees.enquiry_id = enquiry.id","left");
        $this->db->order_by("razorpay_registration_fees.created_at","desc");
        $query = $this->db->get("razorpay_registration_fees");
        //echo $this->db->last_query();
        $result= $query->result_array();
        // echo $this->db->last_query();
        return $result;

    }

    public function search_registration_fee($search_query, $date_from, $date_to) {

        if((!empty($date_from)) && (!empty($date_to))) {
            $condition = 1;
            $this->db->where("razorpay_registration_fees.created_at >= ", $date_from);
            $this->db->where("razorpay_registration_fees.created_at <= ", $date_to);
        }
        if(!empty($search_query)){
            $this->db->group_start();
            $this->db->or_like('enquiry.father_firstname', $search_query);
            $this->db->or_like('enquiry.father_lastname', $search_query);
            $this->db->or_like('enquiry.name', $search_query);
            $this->db->or_like('enquiry.last_name', $search_query);
            $this->db->or_like('enquiry.father_mobile', $search_query);
            $this->db->or_like('razorpay_registration_fees.razorpay_payment_id', $search_query);
            $this->db->group_end();
        }

        $this->db->select('razorpay_registration_fees.*,enquiry.father_firstname, enquiry.father_lastname, enquiry.father_mobile, enquiry.name, enquiry.last_name ');
        $this->db->join("enquiry","razorpay_registration_fees.enquiry_id = enquiry.id","left");
        $this->db->order_by("razorpay_registration_fees.created_at","desc");
        $query = $this->db->get("razorpay_registration_fees");
        //$query->result_array();
        //echo $this->db->last_query();die("---fefe");
        return $query->result_array();
    }

}
