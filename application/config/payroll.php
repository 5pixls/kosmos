<?php

$config['staffattendance'] = array(
    'present' => 1,
    'half_day' => 4,
    'late' => 2,
    'absent' => 3,
    'holiday' => 5
);

$config['contracttype'] = array(
    'permanent' => lang('permanent'),
    'probation' => lang('probation'),
);

$config['status'] = array(
    'approve' => lang('approve'),
    'disapprove' => lang('disapprove'),
    'pending' => lang('pending'),
);

$config['marital_status'] = array(
    'Single' => lang('single'),
    'Married' => lang('married'),
    'Widowed' => lang('widowed'),
    'Seperated' => lang('seperated'),
    'Not Specified' => lang('not_specified'),
);

$config['payroll_status'] = array(
    'generated' => lang('generated'),
    'paid' => lang('paid'),
    'unpaid' => lang('unpaid'),
    'not_generate' => lang('not_generated'),
);
$config['payment_mode'] = array(
    'cash' => lang('cash'),
    'cheque' => lang('cheque'),
    'online' => lang('transfer_to_bank_account'),
);
$config['enquiry_status'] = array(
    '1' => 'New',
    '2' => 'Warm (Follow Ups)',
    '3' => 'Hot Lead',
    '4' => 'Not Interested',
    '5' => 'No Reply',
    '6' => 'Not Eligible',
    '7' => 'No Vacancy',
    '8' => 'Granted and Waiting',
    '9' => 'Granted and Not Admitted',
    '10' => 'Admitted'
);
////Jr. Tele Caller
$config['enquiry_status_9'] = array(
    '1' => 'New',
    '2' => 'Warm (Follow Ups)',
    //'3' => 'Hot Lead',
    '4' => 'Not Interested',
    '5' => 'No Reply',
    '6' => 'Not Eligible'
);
////Sr. Tele Caller

$config['enquiry_status_10'] = array(
    /*'1' => 'New',
    '2' => 'Warm (Follow Ups)',
  //  '3' => 'Hot Lead',
    '4' => 'Not Interested',
    '5' => 'No Reply',
    '6' => 'Not Eligible',
    '7' => 'No Vacancy',
    '10' => 'Admitted'*/
    '1' => 'New',
    '2' => 'Warm (Follow Ups)',
    '3' => 'Hot Lead',
    '4' => 'Not Interested',
    '5' => 'No Reply',
    '6' => 'Not Eligible',
    '7' => 'No Vacancy',
    '8' => 'Granted and Waiting',
    '9' => 'Granted and Not Admitted',
    '10' => 'Admitted'
);
////Admission In-charge

$config['enquiry_status_11'] = array(
    '2' => 'Warm (Follow Ups)',
    '3' => 'Hot Lead',
    '4' => 'Not Interested',
    '5' => 'No Reply',
    '6' => 'Not Eligible',
    '7' => 'No Vacancy',
    '8' => 'Granted and Waiting',
    '9' => 'Granted and Not Admitted',
    '10' => 'Admitted'
);
$config['status_color'] = 
    array('1'=>'#4bd1ec', '2'=>'#efb558', '3'=>'#ffa69e', '4'=>'#8d99ae', '5'=>'#c38e70', '6'=>'#d62828', '7'=>'#6f1d1b', '8'=>'#a3b18a', '9'=>'#577590', '10'=>'#3ea568'
);

$config['search_type'] = array(
    'today' => lang('today'),
    'this_week' => lang('this_week'),
    'last_week' => lang('last_week'),
    'this_month' => lang('this_month'),
    'last_month' => lang('last_month'),
    'last_3_month' => lang('last_3_month'),
    'last_6_month' => lang('last_6_month'),
    'last_12_month' => lang('last_12_month'),
    'this_year' => lang('this_year'),
    'last_year' => lang('last_year'),
    'period' => lang('period'),
);
