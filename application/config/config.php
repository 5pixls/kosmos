<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Base Site URL
  |--------------------------------------------------------------------------
  |
  | URL to your CodeIgniter root. Typically this will be your base URL,
  | WITH a trailing slash:
  |
  |	http://example.com/
  |
  | WARNING: You MUST set this value!
  |
  | If it is not set, then CodeIgniter will try guess the protocol and path
  | your installation, but due to security concerns the hostname will be set
  | to $_SERVER['SERVER_ADDR'] if available, or localhost otherwise.
  | The auto-detection mechanism exists only for convenience during
  | development and MUST NOT be used in production!
  |
  | If you need to allow multiple domains, remember that this file is still
  | a PHP script and you can easily do that on your own.
  |
 */
$config['base_url'] = 'https://kosmos.thekhaitanschool.org/';
//$config['base_url'] = 'http://kosmos/';
$config['csrf_protection'] = TRUE;  // Enable CSRF
$config['csrf_token_name'] = 'csrf_hash_name'; // Token name (You can update it)
$config['csrf_regenerate'] = TRUE; // Set TRUE to regenerate Hash

$config['ozonetel_api_key']='KK37269159d69c8196c66925bdc57eb09c'; //Ozonetel API KEY
$config['ozonetel_did']='911140747144'; //Ozonetel DID 
$config['ozonetel_username']='khaitan'; //Ozonetel user Name
//$config['razorpay_key']='rzp_live_j4R9APZNgPcz9M'; // live key id
//$config['razorpay_key_secret'] ='2hn5LJn9xgk5c7MeoZLpDDd9';  // Live key secret
$config['razorpay_key'] ='rzp_test_f1gLwNjJGQFFb3';  // test key id
$config['razorpay_key_secret'] ='ALlBQTaJ0I5GcGADSPx88Qw6';  // test key secret

/*  |--------------------------------------------------------------------------
  | Index File
  |--------------------------------------------------------------------------
  |
  | Typically this will be your index.php file, unless you've renamed it to
  | something else. If you are using mod_rewrite to remove the page set this
  | variable so that it is blank.
  |
 */
$config['index_page'] = '';
$config['upload_list'] =array('birth_certificate'=>'Birth certificate of the child - self attested photocopy','group_photo'=>'One group photo of both parents with the child (with a light background)','passport_photo'=>'Three copies of the latest passport size photograph of the child (with a light background)','father_aadhar'=>'Aadhar Card of the Father – one photocopy','mother_aadhar'=>'Aadhar Card of the Mother – one photocopy', 'child_aadhar'=>'Aadhar Card of the child – one photocopy', 'transfer_certificate'=>'Transfer Certificate (TC) from previous school (If the Transfering from Non CBSE Board, countersign of Education Officer required on TC).','report_card'=>'Report Card / Assessment Sheet from the previous school','medial_form'=>'Completed Medical Form (attached with email)','vaccination_form'=>'Completed Vaccination Form (attached with email). Provide photocopy of Child’s Vaccination Card','transportation_form'=>'Printout of Transportation Form','misc'=>'Misc');

$config['subject_list'] =array('Aero-Sapiens'=>'Aero-Sapiens','Astronomy Club'=>'Astronomy Club','CBSE Registration'=>'CBSE Registration','Educational Trip'=>'Educational Trip','Exams'=>'Exams','French Language'=>'French Language','Helen O&#39; Grady'=>'Helen O&#39; Grady','Inter School Competition'=>'Inter School Competition','Interact Club'=>'Interact Club','Ithias'=>'Ithias','Karate'=>'Karate','Championship'=>'Championship','Local Trips'=>'Local Trips','Photography Club'=>'Photography Club','R C Cars'=>'R C Cars','Robotics'=>'Robotics','School Functions'=>'School Functions','Taekwondo Championship'=>'Taekwondo Championship','Voluntary Contibution'=>'Voluntary Contibution');

$config['last_school_list'] =array('Aruna Play School'=>'Aruna Play School','Kangaroo School'=>'Kangaroo School','Khaitan Pre School'=>'Khaitan Pre School','Kidzee Play School'=>'Kidzee Play School','Ladders Play School'=>'Ladders Play School','Mother&#8217;S Prides'=>'Mother&#8217;S Prides','Ravi&#8217;S Noddy Play School'=>'Ravi&#8217;S Noddy Play School','Windows School'=>'Windows School','Others'=>'Others');

$config['previous_board_list'] =array('CBSE'=>'CBSE','ICSE'=>'ICSE','Others'=>'Others');
$config['previous_class_list'] =array('Nur'=>'Nur','Prep'=>'Prep','1'=>'1', '2'=>'2','3'=>'3', '4'=>'4', '5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12','Misc'=>'Misc');

$config['father_title_list'] =array('Mr.'=>'Mr.','Dr.'=>'Dr.','Bro.'=>'Bro.');
$config['guardian_title_list'] =array('Mr.'=>'Mr.','Dr.'=>'Dr.','Bro.'=>'Bro.','Ms.'=>'Ms.','Sr.'=>'Sr.','Mrs.'=>'Mrs.');
$config['parent_qualification_list'] =array('10th'=>'10th','Account'=>'Account','Amie'=>'Amie','Architecht'=>'Architecht','B-Pharm'=>'B-Pharm', 'B.A,Fashion Design'=>'B.A,Fashion Design', 'B.A.M.S'=>'B.A.M.S','B.D.S'=>'B.D.S','B.E'=>'B.E','B.ED'=>'B.ED', 'B.H.M.S'=>'B.H.M.S','B.P.ED'=>'B.P.ED','B.TECH'=>'B.TECH','B.V. SC'=>'B.V. SC','C.A'=>'C.A','C.S'=>'C.S','Consultant'=>'Consultant','D.H.M.S'=>'D.H.M.S','DAH'=>'DAH','Diploma'=>'Diploma','Graduation'=>'Graduation','Intermediate'=>'Intermediate','It'=>'It','Llb'=>'Llb','Llm'=>'Llm','M.A B.Ed'=>'M.A B.Ed','M.A.'=>'M.A.','M.B.A'=>'M.B.A','M.B.B.S.'=>'M.B.B.S.','M.C.A'=>'M.C.A','M.D'=>'M.D','M.E.'=>'M.E.','M.Ed'=>'M.Ed','M.Pharm'=>'M.Pharm','M.S'=>'M.S','M.Sc. Bio Technology'=>'M.Sc. Bio Technology','M.SC. IT.'=>'M.SC. IT.','M.Tech'=>'M.Tech','M.V.SC'=>'M.V.SC','MCA'=>'MCA','MCM'=>'MCM','PGDBM'=>'PGDBM','PGDCA'=>'PGDCA','PH.D'=>'PH.D','Post Graduation'=>'Post Graduation','Project Leader'=>'Project Leader','S.T.C'=>'S.T.C','Senior Executive'=>'Senior Executive','Under Matric'=>'Under Matric','Other'=>'Other');

$config['parent_occupation_list'] =array('Business'=>'Business','Doctor'=>'Doctor','Expired'=>'Expired','Farming'=>'Farming','Farming'=>'Farming', 'Private'=>'Private', 'Retired'=>'Retired','Service'=>'Service','Other'=>'Other');

$config['parent_designation_list'] =array('Account manager'=>'account Manager', 'Accountant'=> 'Accountant','Advocate'=>'Advocate','Agent'=>'Agent','Agm'=>'Agm','Agm Project'=>'Agm Project','Agriculturist'=>'Agriculturist','Application Lead'=>'Application Lead','Architech'=>'Architech','Army'=>'Army','Artist'=>'Artist','Assistant'=>'Assistant','Assistant Ambassador'=>'Assistant Ambassador','Assistant Director'=>'Assistant Director','Assistant Manager'=>'Assistant Manager','Associate Project Manager'=>'Associate Project Manager','Associate Director'=>'Associate Director','Asst. Commissioner'=>'Asst. Commissioner','Asst.Vice President'=>'Asst.Vice President ','BA &amp; B.ed'=>'BA &amp; B.ed','Banker'=>'Banker','Bhms'=>'Bhms','Branch Head'=>'Branch Head','Builder'=>'Builder','Business Associate'=>'Business Associate','Business Consultant'=>'Business Consultant','Business Head Development'=>'Business Head Development','CA'=>'CA','CAO'=>'CAO','Captain'=>'Captain','Carpenter'=>'Carpenter','Cashier'=>'Cashier','CCO'=>'CCO','Centre Head'=>'Centre Head','CEO'=>'CEO','CFO'=>'CFO','Chartered Accountant'=>'Chartered Accountant','Chemist'=>'Chemist','Chief Adviser'=>'Chief Adviser','Chief Engineer'=>'Chief Engineer','Chief Human Resource Officer'=>'Chief Human Resource Officer','Chief Manager'=>'Chief Manager','CIO'=>'CIO','CISO'=>'CISO','Clerk'=>'Clerk','Co-Ordinator'=>'Co-Ordinator','Company Secretary'=>'Company Secretary','Computer Operator'=>'Computer Operator','Conservator'=>'Conservator','Constable'=>'Constable','Consultant'=>'Consultant','Contractor'=>'Contractor','COO'=>'COO','Country Head'=>'Country Head','CTO'=>'CTO','D.M.'=>'D.M.','Delivery Head'=>'Delivery Head','Delivery Manager'=>'Delivery Manager','Dentist'=>'Dentist','Deputy Branch Manager'=>'Deputy Branch Manager','Deputy Commander BSF'=>'Deputy Commander BSF','Dev. Officer'=>'Dev. Officer','Development Officer'=>'Development Officer','DGM'=>'DGM','director'=>'director','doctor'=>'doctor','driver'=>'driver','dy. Director'=>'dy. Director','Dy. G.M.'=>'Dy. G.M.','Dy.Chief Engineer'=>'Dy.Chief Engineer','EDP Incharge'=>'EDP Incharge','Electrician'=>'Electrician','Engineer'=>'Engineer','Entrapreneur'=>'Entrapreneur','Environment Specialist'=>'Environment Specialist','Event Manager'=>'Event Manager','Faculty'=>'Faculty','Fashion Designer'=>'Fashion Designer','Finance Head'=>'Finance Head','Foreman'=>'Foreman','Free Lancer'=>'Free Lancer','Gardner'=>'Gardner','General manager'=>'General Manager','Govt. Emly.'=>'Govt. Emly.','Graphic Artist'=>'Graphic Artist','H.G.A'=>'H.G.A','Hardware'=>'Hardware','Head'=>'Head','Head Admin'=>'Head Admin','Head Constable'=>'Head Constable','Head Operator'=>'Head Operator','Head Sales'=>'Head Sales','House Wife'=>'House Wife','Hr Consultant'=>'Hr Consultant','Hr Professional'=>'Hr Professional','Inspector'=>'Inspector','Instructor'=>'Instructor','Interior Designer'=>'Interior Designer','IT Consultant'=>'IT Consultant','IT Manager'=>'IT Manager','IT Professional'=>'IT Professional','Joint Director'=>'Joint Director','Journalist'=>'Journalist','Lab Asst.'=>'Lab Asst.','Lab Technician'=>'Lab Technician','Lawyer'=>'Lawyer','Lecturer'=>'Lecturer','Manager'=>'Manager','Managing Director'=>'Managing Director','Managing Partner'=>'Managing Partner','MBA,MCA'=>'MBA,MCA','National Head'=>'National Head','National Sales Manager'=>'National Sales Manager','Nurse'=>'Nurse','O&Amp;M Head'=>'O&Amp;M Head','Office Assistance Cum Receiptionist'=>'Office Assistance cum Receiptionist','Officer'=>'Officer','Owner'=>'Owner','P.A'=>'P.A','partner'=>'Partner','Peon'=>'Peon','PGDPM'=>'PGDPM','PGT'=>'PGT','Pharmacist'=>'Pharmacist','Pharmasist'=>'Pharmasist','Physician'=>'Physician','Plant Head'=>'Plant Head','Police'=>'Police','Priest'=>'Priest','Principal'=>'Principal','PRO'=>'PRO','Production Co-Ordinator'=>'Production Co-Ordinator','Production Manager'=>'Production Manager','Professor'=>'Professor','Programme Manager'=>'Programme Manager','Project Leader'=>'Project Leader','Project Manager'=>'Project Manager','Property Dealer'=>'Property Dealer','Proprietor'=>'Proprietor','Purchase Manager'=>'Purchase Manager','QCA'=>'QCA','Quality Analyst'=>'Quality Analyst','Quality Inspector'=>'Quality Inspector','Ranger'=>'Ranger','RBM'=>'RBM','Reader'=>'Reader','Regional Head'=>'Regional Head','Regional Manager'=>'Regional Manager','RHM'=>'RHM','RSM'=>'RSM','Sales'=>'Sales','Sales Manager'=>'Sales Manager','Scientist'=>'Scientist','Sde'=>'Sde','Sea'=>'Sea','Secretary'=>'Secretary','Self Employed'=>'Self Employed','Senior Analyst'=>'Senior Analyst','Senior Ao'=>'Senior Ao','Senior Executive'=>'Senior Executive','Senior Merchant'=>'Senior Merchant','Sfm'=>'Sfm','Shop Keeper'=>'Shop Keeper','So'=>'So','Soft Developer'=>'Soft Developer','Software Consultant'=>'Software Consultant','Software Engineer'=>'Software Engineer','Software Professor'=>'Software Professor','Speclalist'=>'Speclalist','SPM'=>'SPM','SQA'=>'SQA','Sr  Sde'=>'Sr  Sde','Sr. Administrator'=>'Sr. Administrator','Sr. Consultant'=>'Sr. Consultant','Sr. G.M.'=>'Sr. G.M.','Sr. Manager'=>'Sr. Manager','Sr. Project manager'=>'Sr. Project manager','Sr. Seismologist'=>'Sr. Seismologist','Sr.Consultant'=>'Sr.Consultant','Sr.Vice President'=>'Sr.Vice President','State Head'=>'State Head','Store Officer'=>'Store Officer','Sub Registrar'=>'Sub Registrar','Superintendent'=>'Superintendent','Supervisor'=>'Supervisor','System Analyst'=>'System Analyst','Teacher'=>'Teacher','Team Lead'=>'Team Lead','Technical Leader'=>'Technical Leader','Technical Specialist'=>'Technical Specialist','Technician'=>'Technician','Tehsildar'=>'Tehsildar','Telecom Professional'=>'Telecom Professional','Tgt'=>'Tgt','Tour Manager'=>'Tour Manager','Transporter'=>'Transporter','Travel Counseller'=>'Travel Counseller','Under Secretary'=>'Under Secretary','V.P.'=>'V.P.','Vety. Doctor'=>'Vety. Doctor','Vice President'=>'Vice President','Zonal Compliance Head'=>'Zonal Compliance Head','ZSM'=>'ZSM','Other'=>'Other');

$config['parent_city_list'] =array('Bengaluru'=>'Bengaluru','Bulandshr'=>'Bulandshr','Delhi'=>'Delhi','Faridabad'=>'Faridabad','Ghaziabad'=>'Ghaziabad','Greater Noida West'=>'Greater Noida West','Gurgaon'=>'Gurgaon','Indirapuram'=>'Indirapuram','Indirapuram-Gzb'=>'Indirapuram-Gzb','Indirapuram.Gzbd'=>'Indirapuram.Gzbd','Lucknow'=>'Lucknow','Mayur Vihar Iii'=>'Mayur Vihar Iii','Mumbai'=>'Mumbai','Nagpur'=>'Nagpur','New Delhi'=>'New Delhi','Noida'=>'Noida','Noida Extension'=>'Noida Extension','Opp L-Pocket'=>'Opp L-Pocket','Other'=>'Other','Sec 82 Noida'=>'Sec 82 Noida','Sec 93 Noida'=>'Sec 93 Noida','Sec 93a Noida'=>'Sec 93a Noida','Sector 82 Noida'=>'Sector 82 Noida','Vaibhav Khand'=>'Vaibhav Khand','Vasundhra Encl'=>'Vasundhra Encl','Other'=>'Other');

$config['true-false'] =array('Yes'=>'Yes','No'=>'No');

$config['mother_title_list'] =array('Ms.'=>'Ms.','Sr.'=>'Sr.','Mrs.'=>'Mrs.','Dr.'=>'Dr.');

$config['nationality_list'] =array('American'=>'American','Canadian'=>'Canadian','Chinese'=>'Chinese','Indian'=>'Indian','Nepali'=>'Nepali','Pakistani'=>'Pakistani','USA'=>'USA');

$config['blood_group_list'] =array('A+'=>'A+','A-'=>'A-','AB+'=>'AB+','AB-'=>'AB-','B+'=>'B+','B-'=>'B-','O+'=>'O+','O-'=>'O-');

$config['mother_tongue_list'] =array('Assamese'=>'Assamese', 'Marathi'=>'Marathi', 'Nepali'=>'Nepali','Oriya'=>'Oriya','Punjabi'=>'Punjabi','Sanskrit'=>'Sanskrit','Sindhi'=>'Sindhi','Tamil'=>'Tamil','Telugu'=>'Telugu','Urdu'=>'Urdu','English'=>'English','Bengali'=>'Bengali','Bodo'=>'Bodo','Dogri'=>'Dogri','French'=>'French','Gujarati'=>'Gujarati','Hindi'=>'Hindi','Kannada'=>'Kannada','Maithili'=>'Maithili','Rajasthani'=>'Rajasthani','Kashmiri'=>'Kashmiri','Konkani'=>'Konkani','Malayalam'=>'Malayalam','Manipuri'=>'Manipuri','Other Languages'=>'Other Languages');

$config['locality_list'] =array('Crossing republic'=>'Crossing Republic','Eastend/Vasundhra Enclave'=>'Eastend/Vasundhra Enclave','Indira Puram'=>'Indira Puram','Mayur Vihar I'=>'Mayur Vihar I','Mayur Vihar III'=>'Mayur Vihar III','Noida'=>'Noida','Noida Extension'=>'Noida Extension','Vaishali'=>'Vaishali','Others'=>'Others');

$config['board_list'] =array('CBSE'=>'CBSE','ICSE'=>'ICSE','Others'=>'Others');

$config['gender_list'] =array('male'=>'Male','female'=>'Female');

$config['religion_list'] =array('Buddhist'=>'Buddhist','Christian'=>'Christian','Hindu'=>'Hindu','Jain'=>'Jain','Muslim'=>'Muslim','Sikh'=>'Sikh','Others'=>'Others');



/*
  |--------------------------------------------------------------------------
  | URI PROTOCOL
  |--------------------------------------------------------------------------
  |
  | This item determines which server global should be used to retrieve the
  | URI string.  The default setting of 'REQUEST_URI' works for most servers.
  | If your links do not seem to work, try one of the other delicious flavors:
  |
  | 'REQUEST_URI'    Uses $_SERVER['REQUEST_URI']
  | 'QUERY_STRING'   Uses $_SERVER['QUERY_STRING']
  | 'PATH_INFO'      Uses $_SERVER['PATH_INFO']
  |
  | WARNING: If you set this to 'PATH_INFO', URIs will always be URL-decoded!
 */
$config['uri_protocol'] = 'REQUEST_URI';

/*
  |--------------------------------------------------------------------------
  | URL suffix
  |--------------------------------------------------------------------------
  |
  | This option allows you to add a suffix to all URLs generated by CodeIgniter.
  | For more information please see the user guide:
  |
  | https://codeigniter.com/user_guide/general/urls.html
 */
$config['url_suffix'] = '';

/*
  |--------------------------------------------------------------------------
  | Default Language
  |--------------------------------------------------------------------------
  |
  | This determines which set of language files should be used. Make sure
  | there is an available translation if you intend to use something other
  | than english.
  |
 */
$config['language'] = 'english';

/*
  |--------------------------------------------------------------------------
  | Default Character Set
  |--------------------------------------------------------------------------
  |
  | This determines which character set is used by default in various methods
  | that require a character set to be provided.
  |
  | See http://php.net/htmlspecialchars for a list of supported charsets.
  |
 */
$config['charset'] = 'UTF-8';

/*
  |--------------------------------------------------------------------------
  | Enable/Disable System Hooks
  |--------------------------------------------------------------------------
  |
  | If you would like to use the 'hooks' feature you must enable it by
  | setting this variable to TRUE (boolean).  See the user guide for details.
  |
 */
$config['enable_hooks'] = FALSE;

/*
  |--------------------------------------------------------------------------
  | Class Extension Prefix
  |--------------------------------------------------------------------------
  |
  | This item allows you to set the filename/classname prefix when extending
  | native libraries.  For more information please see the user guide:
  |
  | https://codeigniter.com/user_guide/general/core_classes.html
  | https://codeigniter.com/user_guide/general/creating_libraries.html
  |
 */
$config['subclass_prefix'] = 'MY_';

/*
  |--------------------------------------------------------------------------
  | Composer auto-loading
  |--------------------------------------------------------------------------
  |
  | Enabling this setting will tell CodeIgniter to look for a Composer
  | package auto-loader script in application/vendor/autoload.php.
  |
  |	$config['composer_autoload'] = TRUE;
  |
  | Or if you have your vendor/ directory located somewhere else, you
  | can opt to set a specific path as well:
  |
  |	$config['composer_autoload'] = '/path/to/vendor/autoload.php';
  |
  | For more information about Composer, please visit http://getcomposer.org/
  |
  | Note: This will NOT disable or override the CodeIgniter-specific
  |	autoloading (application/config/autoload.php)
 */
$config['composer_autoload'] = FALSE;

/*
  |--------------------------------------------------------------------------
  | Allowed URL Characters
  |--------------------------------------------------------------------------
  |
  | This lets you specify which characters are permitted within your URLs.
  | When someone tries to submit a URL with disallowed characters they will
  | get a warning message.
  |
  | As a security measure you are STRONGLY encouraged to restrict URLs to
  | as few characters as possible.  By default only these are allowed: a-z 0-9~%.:_-
  |
  | Leave blank to allow all characters -- but only if you are insane.
  |
  | The configured value is actually a regular expression character group
  | and it will be executed as: ! preg_match('/^[<permitted_uri_chars>]+$/i
  |
  | DO NOT CHANGE THIS UNLESS YOU FULLY UNDERSTAND THE REPERCUSSIONS!!
  |
 */
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-@\=()';

/*
  |--------------------------------------------------------------------------
  | Enable Query Strings
  |--------------------------------------------------------------------------
  |
  | By default CodeIgniter uses search-engine friendly segment based URLs:
  | example.com/who/what/where/
  |
  | By default CodeIgniter enables access to the $_GET array.  If for some
  | reason you would like to disable it, set 'allow_get_array' to FALSE.
  |
  | You can optionally enable standard query string based URLs:
  | example.com?who=me&what=something&where=here
  |
  | Options are: TRUE or FALSE (boolean)
  |
  | The other items let you set the query string 'words' that will
  | invoke your controllers and its functions:
  | example.com/index.php?c=controller&m=function
  |
  | Please note that some of the helpers won't work as expected when
  | this feature is enabled, since CodeIgniter is designed primarily to
  | use segment based URLs.
  |
 */
$config['allow_get_array'] = TRUE;
$config['enable_query_strings'] = FALSE;
$config['controller_trigger'] = 'c';
$config['function_trigger'] = 'm';
$config['directory_trigger'] = 'd';

/*
  |--------------------------------------------------------------------------
  | Error Logging Threshold
  |--------------------------------------------------------------------------
  |
  | You can enable error logging by setting a threshold over zero. The
  | threshold determines what gets logged. Threshold options are:
  |
  |	0 = Disables logging, Error logging TURNED OFF
  |	1 = Error Messages (including PHP errors)
  |	2 = Debug Messages
  |	3 = Informational Messages
  |	4 = All Messages
  |
  | You can also pass an array with threshold levels to show individual error types
  |
  | 	array(2) = Debug Messages, without Error Messages
  |
  | For a live site you'll usually only enable Errors (1) to be logged otherwise
  | your log files will fill up very fast.
  |
 */
$config['log_threshold'] = 0;

/*
  |--------------------------------------------------------------------------
  | Error Logging Directory Path
  |--------------------------------------------------------------------------
  |
  | Leave this BLANK unless you would like to set something other than the default
  | application/logs/ directory. Use a full server path with trailing slash.
  |
 */
$config['log_path'] = '';

/*
  |--------------------------------------------------------------------------
  | Log File Extension
  |--------------------------------------------------------------------------
  |
  | The default filename extension for log files. The default 'php' allows for
  | protecting the log files via basic scripting, when they are to be stored
  | under a publicly accessible directory.
  |
  | Note: Leaving it blank will default to 'php'.
  |
 */
$config['log_file_extension'] = '';

/*
  |--------------------------------------------------------------------------
  | Log File Permissions
  |--------------------------------------------------------------------------
  |
  | The file system permissions to be applied on newly created log files.
  |
  | IMPORTANT: This MUST be an integer (no quotes) and you MUST use octal
  |            integer notation (i.e. 0700, 0644, etc.)
 */
$config['log_file_permissions'] = 0644;

/*
  |--------------------------------------------------------------------------
  | Date Format for Logs
  |--------------------------------------------------------------------------
  |
  | Each item that is logged has an associated date. You can use PHP date
  | codes to set your own date formatting
  |
 */
$config['log_date_format'] = 'Y-m-d H:i:s';

/*
  |--------------------------------------------------------------------------
  | Error Views Directory Path
  |--------------------------------------------------------------------------
  |
  | Leave this BLANK unless you would like to set something other than the default
  | application/views/errors/ directory.  Use a full server path with trailing slash.
  |
 */
$config['error_views_path'] = '';

/*
  |--------------------------------------------------------------------------
  | Cache Directory Path
  |--------------------------------------------------------------------------
  |
  | Leave this BLANK unless you would like to set something other than the default
  | application/cache/ directory.  Use a full server path with trailing slash.
  |
 */
$config['cache_path'] = '';

/*
  |--------------------------------------------------------------------------
  | Cache Include Query String
  |--------------------------------------------------------------------------
  |
  | Whether to take the URL query string into consideration when generating
  | output cache files. Valid options are:
  |
  |	FALSE      = Disabled
  |	TRUE       = Enabled, take all query parameters into account.
  |	             Please be aware that this may result in numerous cache
  |	             files generated for the same page over and over again.
  |	array('q') = Enabled, but only take into account the specified list
  |	             of query parameters.
  |
 */
$config['cache_query_string'] = FALSE;

/*
  |--------------------------------------------------------------------------
  | Encryption Key
  |--------------------------------------------------------------------------
  |
  | If you use the Encryption class, you must set an encryption key.
  | See the user guide for more info.
  |
  | https://codeigniter.com/user_guide/libraries/encryption.html
  |
 */
$config['encryption_key'] = '';

/*
  |--------------------------------------------------------------------------
  | Session Variables
  |--------------------------------------------------------------------------
  |
  | 'sess_driver'
  |
  |	The storage driver to use: files, database, redis, memcached
  |
  | 'sess_cookie_name'
  |
  |	The session cookie name, must contain only [0-9a-z_-] characters
  |
  | 'sess_expiration'
  |
  |	The number of SECONDS you want the session to last.
  |	Setting to 0 (zero) means expire when the browser is closed.
  |
  | 'sess_save_path'
  |
  |	The location to save sessions to, driver dependent.
  |
  |	For the 'files' driver, it's a path to a writable directory.
  |	WARNING: Only absolute paths are supported!
  |
  |	For the 'database' driver, it's a table name.
  |	Please read up the manual for the format with other session drivers.
  |
  |	IMPORTANT: You are REQUIRED to set a valid save path!
  |
  | 'sess_match_ip'
  |
  |	Whether to match the user's IP address when reading the session data.
  |
  |	WARNING: If you're using the database driver, don't forget to update
  |	         your session table's PRIMARY KEY when changing this setting.
  |
  | 'sess_time_to_update'
  |
  |	How many seconds between CI regenerating the session ID.
  |
  | 'sess_regenerate_destroy'
  |
  |	Whether to destroy session data associated with the old session ID
  |	when auto-regenerating the session ID. When set to FALSE, the data
  |	will be later deleted by the garbage collector.
  |
  | Other session cookie settings are shared with the rest of the application,
  | except for 'cookie_prefix' and 'cookie_httponly', which are ignored here.
  |
 */
$config['sess_driver'] = 'files';
$config['sess_cookie_name'] = 'ci_session';
$config['sess_expiration'] = 7200;
$config['sess_save_path'] = sys_get_temp_dir();
$config['sess_match_ip'] = FALSE;
$config['sess_time_to_update'] = 300;
$config['sess_regenerate_destroy'] = FALSE;

/*
  |--------------------------------------------------------------------------
  | Cookie Related Variables
  |--------------------------------------------------------------------------
  |
  | 'cookie_prefix'   = Set a cookie name prefix if you need to avoid collisions
  | 'cookie_domain'   = Set to .your-domain.com for site-wide cookies
  | 'cookie_path'     = Typically will be a forward slash
  | 'cookie_secure'   = Cookie will only be set if a secure HTTPS connection exists.
  | 'cookie_httponly' = Cookie will only be accessible via HTTP(S) (no javascript)
  |
  | Note: These settings (with the exception of 'cookie_prefix' and
  |       'cookie_httponly') will also affect sessions.
  |
 */
$config['cookie_prefix'] = '';
$config['cookie_domain'] = '';
$config['cookie_path'] = '/';
$config['cookie_secure'] = FALSE;
$config['cookie_httponly'] = FALSE;

/*
  |--------------------------------------------------------------------------
  | Standardize newlines
  |--------------------------------------------------------------------------
  |
  | Determines whether to standardize newline characters in input data,
  | meaning to replace \r\n, \r, \n occurrences with the PHP_EOL value.
  |
  | This is particularly useful for portability between UNIX-based OSes,
  | (usually \n) and Windows (\r\n).
  |
 */
$config['standardize_newlines'] = FALSE;

/*
  |--------------------------------------------------------------------------
  | Global XSS Filtering
  |--------------------------------------------------------------------------
  |
  | Determines whether the XSS filter is always active when GET, POST or
  | COOKIE data is encountered
  |
  | WARNING: This feature is DEPRECATED and currently available only
  |          for backwards compatibility purposes!
  |
 */
$config['global_xss_filtering'] = FALSE;

/*
  |--------------------------------------------------------------------------
  | Cross Site Request Forgery
  |--------------------------------------------------------------------------
  | Enables a CSRF cookie token to be set. When set to TRUE, token will be
  | checked on a submitted form. If you are accepting user data, it is strongly
  | recommended CSRF protection be enabled.
  |
  | 'csrf_token_name' = The token name
  | 'csrf_cookie_name' = The cookie name
  | 'csrf_expire' = The number in seconds the token should expire.
  | 'csrf_regenerate' = Regenerate token on every submission
  | 'csrf_exclude_uris' = Array of URIs which ignore CSRF checks
 */
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'school_csrf_name';
$config['csrf_cookie_name'] = 'school_cookie_name';
$config['csrf_expire'] = 7200;
$config['csrf_regenerate'] = TRUE;
$config['csrf_exclude_uris'] = array();

/*
  |--------------------------------------------------------------------------
  | Output Compression
  |--------------------------------------------------------------------------
  |
  | Enables Gzip output compression for faster page loads.  When enabled,
  | the output class will test whether your server supports Gzip.
  | Even if it does, however, not all browsers support compression
  | so enable only if you are reasonably sure your visitors can handle it.
  |
  | Only used if zlib.output_compression is turned off in your php.ini.
  | Please do not use it together with httpd-level output compression.
  |
  | VERY IMPORTANT:  If you are getting a blank page when compression is enabled it
  | means you are prematurely outputting something to your browser. It could
  | even be a line of whitespace at the end of one of your scripts.  For
  | compression to work, nothing can be sent before the output buffer is called
  | by the output class.  Do not 'echo' any values with compression enabled.
  |
 */
$config['compress_output'] = FALSE;

/*
  |--------------------------------------------------------------------------
  | Master Time Reference
  |--------------------------------------------------------------------------
  |
  | Options are 'local' or any PHP supported timezone. This preference tells
  | the system whether to use your server's local time as the master 'now'
  | reference, or convert it to the configured one timezone. See the 'date
  | helper' page of the user guide for information regarding date handling.
  |
 */
$config['time_reference'] = 'local';

/*
  |--------------------------------------------------------------------------
  | Rewrite PHP Short Tags
  |--------------------------------------------------------------------------
  |
  | If your PHP installation does not have short tag support enabled CI
  | can rewrite the tags on-the-fly, enabling you to utilize that syntax
  | in your view files.  Options are TRUE or FALSE (boolean)
  |
  | Note: You need to have eval() enabled for this to work.
  |
 */
$config['rewrite_short_tags'] = FALSE;

/*
  |--------------------------------------------------------------------------
  | Reverse Proxy IPs
  |--------------------------------------------------------------------------
  |
  | If your server is behind a reverse proxy, you must whitelist the proxy
  | IP addresses from which CodeIgniter should trust headers such as
  | HTTP_X_FORWARDED_FOR and HTTP_CLIENT_IP in order to properly identify
  | the visitor's IP address.
  |
  | You can use both an array or a comma-separated list of proxy addresses,
  | as well as specifying whole subnets. Here are a few examples:
  |
  | Comma-separated:	'10.0.1.200,192.168.5.0/24'
  | Array:		array('10.0.1.200', '192.168.5.0/24')
 */
$config['routine_session'] = 16;
$config['routine_update'] = 1611599400;
$config['proxy_ips'] = '';
$config['installed'] = true;
