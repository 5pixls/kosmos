<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mailer
{

    public $mail_config;
    private $sch_setting;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('emailconfig_model');
        $this->CI->mail_config = $this->CI->emailconfig_model->getActiveEmail();
        $this->CI->load->model('setting_model');
        $this->sch_setting = $this->CI->setting_model->get();
        $this->CI->config->load("mailsms");
    }

    public function send_mail($toemail, $subject, $body, $FILES = array(), $cc = "", $attachments = array())
    {
        $mail          = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $school_name   = $this->sch_setting[0]['name'];
        if ($this->CI->mail_config->email_type == "smtp") {

            $mail->IsSMTP();
            $mail->SMTPAuth   = true;
           // $mail->SMTPDebug = 2;
            $mail->SMTPSecure = $this->CI->mail_config->ssl_tls;
            $mail->Host       = $this->CI->mail_config->smtp_server;
            $mail->Port       = $this->CI->mail_config->smtp_port;
            $mail->Username   = $this->CI->mail_config->smtp_username;
            $mail->Password   = $this->CI->mail_config->smtp_password;
        }
        $school_name="The Khaitan School | Noida";

        $mail->SetFrom($this->CI->config->item('sendermail'), $school_name);
        $mail->AddReplyTo($this->CI->config->item('sendermail'), $this->CI->config->item('sendermail'));
        if (!empty($FILES)) {
            if (isset($_FILES['files']) && !empty($_FILES['files'])) {
                $no_files = count($_FILES["files"]['name']);
                for ($i = 0; $i < $no_files; $i++) {
                    if ($_FILES["files"]["error"][$i] > 0) {
                        echo "Error: " . $_FILES["files"]["error"][$i] . "<br>";
                    } else {
                        $file_tmp  = $_FILES["files"]["tmp_name"][$i];
                        $file_name = $_FILES["files"]["name"][$i];
                        $mail->AddAttachment($file_tmp, $file_name);
                    }
                }
            }
        }

        if (!empty($attachments)) {
            foreach ($attachments as $key => $value) {
                $file=explode('.', $value);
                if($key=='transportation') $filename='Transport Form';
                //else if($key=='medical') $filename='Medical & Vaccination Forms';
                else $filename=$key;

                $mail->AddAttachment($_SERVER["DOCUMENT_ROOT"]. $value, $filename.'.'.$file[1]);
            }
        }
        if ($cc != "") {

            $mail->AddCC($cc);
        }

        $mail->Subject = $subject;
        $mail->Body    = $body;
        $mail->AltBody = $body;
        $mail->AddAddress($toemail);
        if ($mail->Send()) {
            return true;
        } else {
            return $mail->ErrorInfo;
        }
    }

}
