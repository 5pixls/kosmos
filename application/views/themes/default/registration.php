<?php if(empty($enquiry_data) && $this->input->get('key') !='') { ?>
    <div class="linkexpired">The link has been expired. Please connect with school help desk.</div>
<?php } else {?>
<input type="hidden" class="form-control" id="enquiryid" value="<?php echo $enquiryid; ?>" name="enquiryid">
<div class="site-wizard">
    <ul class="sw-steps">
        <li class="active"><span>1. Child Details</span></li>
        <li><span>2. Parents Details</span></li>
        <li><span>3. Questionaire</span></li>
        <li><span>4. Acknowledgement/Payment</span></li>
    </ul>
    <div class="sw-body">
        <div class="error_alert"></div>
        <div class="sw-steps-body">
                <?php if($enquiry_data['enquiryid']) { ?>
                    <label><strong>Enquiry ID:</strong> <?php echo $enquiry_data['enquiryid']; ?></label>
                <?php }?>
                <div class="sw-steps-content active">
                    <form id="child-form" action="">
                        <div class="tshadow mb25 bozero sw-content">
                            <!-- <h4 class="pagetitleh2">Child Details</h4> -->
                            <div class="row around10">
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="pwd">First Name</label><small class="req"> *</small>  
                                    <input type="text" class="form-control" id="name_value" value="<?php echo set_value('name', $enquiry_data['name']); ?>" name="name">
                                    <span class="text-danger" id="name"></span>
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="pwd">Last Name</label><small class="req"> *</small>  
                                    <input type="text" class="form-control" id="last_name" value="<?php echo set_value('last_name', $enquiry_data['last_name']); ?>" name="last_name">
                                    <span class="text-danger" id="last_name"></span>
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="email">Gender</label> <small class="req"> *</small>
                                    <select name="gender" class="form-control"  >
                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $gender_list=$this->config->item('gender_list');
                                        foreach ($gender_list as $key => $value) {
                                            ?>

                                            <option value="<?php echo $key ?>" <?php if (set_value('gender', $enquiry_data['gender']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                                            <label for="dob">Date of Birth</label><small class="req"> *</small>
                                                            <input type="text" id="dob" name="dob" placeholder="dd/mm/yyyy" class="form-control dob" value="<?php
                                                            if (!empty($enquiry_data['dob'])) {
                                                                echo set_value('dob', date("d-m-Y", strtotime($enquiry_data['dob'])));
                                                            }
                                                            ?>" readonly="">
                                                        </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="pwd"><?php echo $this->lang->line('class'); ?></label> <small class="req"> *</small>
                                    <select name="class" class="form-control"  >
                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                            <?php
                                            foreach ($class_list as $key => $value) {
                                                ?>

                                            <option value="<?php echo $value['id'] ?>" <?php if (set_value('class', $enquiry_data['class']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['class'] ?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="email">Session</label> <small class="req"> *</small>
                                    <select name="session_id" id="session_id" class="form-control"  >
                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                            <?php
                                            foreach ($session_list as $value) {
                                                ?>

                                            <option value="<?php echo $value['id'] ?>" <?php if (set_value('session_id', $enquiry_data['session_id']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['session'] ?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="pwd">Child's Aadhaar Card No.:</label>
                                    <input type="text" maxlength="14" class="form-control aadhaar_input" id="child_aadhaar_no" value="<?php echo set_value('child_aadhaar_no', $enquiry_data['child_aadhaar_no']); ?>" name="child_aadhaar_no" placeholder="12 Digit Aadhaar No.">
                                    <span class="text-danger" id="child_aadhaar_no"></span>
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="email">Nationality</label><small class="req"> *</small>   
                                    <select name="nationality" class="form-control"  >
                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $nationality_list=$this->config->item('nationality_list');
                                        foreach ($nationality_list as $key => $value) {
                                            ?>

                                            <option value="<?php echo $key ?>" <?php if (set_value('nationality', $enquiry_data['nationality']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="dob">Blood Group:</label><small class="req"> *</small>  
                                    <select name="blood_group" class="form-control"  >
                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $blood_group_list=$this->config->item('blood_group_list');
                                        foreach ($blood_group_list as $key => $value) {
                                            ?>

                                            <option value="<?php echo $key ?>" <?php if (set_value('blood_group', $enquiry_data['blood_group']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                           
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="email">Mother Tongue</label><small class="req"> *</small>   
                                    <select name="mother_tongue" class="form-control"  >
                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $mother_tongue_list=$this->config->item('mother_tongue_list');
                                        foreach ($mother_tongue_list as $key => $value) {
                                            ?>

                                            <option value="<?php echo $key ?>" <?php if (set_value('mother_tongue', $enquiry_data['mother_tongue']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="home_town">Home Town</label><small class="req"> *</small>  
                                    <input id="home_town" name="home_town" placeholder="Home Town" type="text" class="form-control"  value="<?php echo set_value('home_town', $enquiry_data['home_town']); ?>" />
                                    <span class="text-danger"><?php echo form_error('home_town'); ?></span>
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="locality">Locality</label><small class="req"> *</small>   
                                    <select name="locality" class="form-control"  >
                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $locality_list=$this->config->item('locality_list');
                                        foreach ($locality_list as $key => $value) {
                                            ?>

                                            <option value="<?php echo $key ?>" <?php if (set_value('locality', $enquiry_data['locality']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="religion">Religion</label><small class="req"> *</small>  
                                    <select name="religion" class="form-control"  >
                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                            <?php $religion_list=$this->config->item('religion_list');
                                            foreach ($religion_list as $key => $value) {
                                                ?>

                                            <option value="<?php echo $key ?>" <?php if (set_value('religion', $enquiry_data['religion']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <?php if ($sch_setting->category) { ?>
                                <div class="col-sm-4 sw-col">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Social Category</label>
                                        <select  id="social_category" name="social_category" class="form-control" >
                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                            <?php foreach ($categorylist as $category) {   ?>
                                            <option value="<?php echo $category['id'] ?>" <?php if ($enquiry_data['social_category'] == $category['id']) {  echo "selected=selected";  } ?>><?php echo $category['category'] ?></option>
                                            <?php $count++; } ?>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('category_id'); ?></span>
                                    </div>
                                    </div>
                                <?php } ?>

                                <div class="col-sm-4 sw-col">
                                    <div class="form-group">
                                        <label>School Bus</label>

                                        <select name="school_bus" class="form-control"  >
                                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                ?>
                                                ?>
                                                <?php $school_bus=$this->config->item('true-false');
                                                foreach ($school_bus as $key => $value) {
                                                    ?>

                                                <option value="<?php echo $key ?>" <?php if (set_value('school_bus', $enquiry_data['school_bus']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                    <?php
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-sm-4 sw-col">
                                    <div class="form-group result-doc">
                                        <label for="pwd">Previous Year Result</label> 
                                        <input type="file" id="previous_result" class="filestyle form-control dropify" name="previous_result" multiple="multiple">
                                    </div>
                                </div>   
                                <div class="col-sm-4 sw-col">
                                    <div class="form-group child-image">
                                        <label for="pwd">Child Image</label> 
                                        <input type="file" id="child_image" class="filestyle form-control dropify" name="child_image" multiple="multiple">
                                        
                                    </div>
                                </div>           
                            </div>

                        </div>
                        <div class="tshadow mb25 bozero sw-content">
                    <h4 class="pagetitleh2">Address Details</h4>
                    <div class="row around10">
                                            <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Address1</label><small class="req"> *</small> 
                                <input id="address" name="address" placeholder="Address1" type="text" class="form-control"  value="<?php echo set_value('address', $enquiry_data['address']); ?>" />
                            </div>
                        </div>

                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Address2</label> <small class="req"> *</small>
                                <input id="address2" name="address2" placeholder="Address2" type="text" class="form-control"  value="<?php echo set_value('address2', $enquiry_data['address2']); ?>" />
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">City</label><small class="req"> *</small>
                                <input id="city" name="city" placeholder="City" type="text" class="form-control"  value="<?php echo set_value('city', $enquiry_data['city']); ?>" />
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Country</label><small class="req"> *</small> 
                                <?php if(!empty($enquiry_data) && $this->input->get('key') !='') { ?>
                                <select name="country_id"  id="country_id" class="form-control"  >
                                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php 
                                                                foreach ($country_list as $value) {
                                                                    ?>

                                                                     <option value="<?php echo $value['id'] ?>" <?php if (set_value('country_id', $enquiry_data['country_id']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['country']; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                </select>
                                <?php } else { ?>
                                <select name="country_id" id="country_id" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php 
                                    foreach ($country_list as $value) {
                                        ?>

                                         <option value="<?php echo $value['id'] ?>" ><?php echo $value['country']; ?></option>

                                        <?php
                                    }
                                    ?>
                                </select>
                            <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">State</label><small class="req"> *</small> 
                                <?php if(!empty($enquiry_data) && $this->input->get('key') !='') { ?>
                                <select name="state_id" id="state_id" class="form-control"  >
                                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                    <?php 
                                                                    foreach ($state_list as $value) {
                                                                        ?>

                                                                    <option value="<?php echo $value['id'] ?>" <?php if (set_value('state_id', $enquiry_data['state_id']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['state']; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                <?php } else { ?>
                               <select name="state_id"  id="state_id" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                </select>
                            <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Pin Code</label><small class="req"> *</small> 
                                <input id="pincode" name="pincode" placeholder="Pin Code" type="text" class="form-control"  value="<?php echo set_value('pincode', $enquiry_data['pincode']); ?>" />
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Landline</label> 
                                <input id="landline" name="landline" placeholder="Landline" type="text" class="form-control" value="<?php echo set_value('landline', $enquiry_data['landline']); ?>"/>
                            </div>
                        </div>
    </div>
</div>
                        <div class="sw-footer">
                            <div class="text-right form-group">
                                <button class="site-btn sw-btn btn sw-next-btn" type="button">Save & Next</button>
                            </div>
                        </div>
                    </form>
                </div>
            
            
                <div class="sw-steps-content">
                    <form id="guardian-form" action="">
                        <!-- Father details -->
                        <div class="tshadow mb25 bozero sw-content">
                    <h4 class="pagetitleh2">Father Details</h4>
                    <div class="row around10">
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_title">Title:</label> 
                            <select name="father_title" class="form-control"  >
                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                <?php $father_title_list=$this->config->item('father_title_list');
                                foreach ($father_title_list as $key => $value) {
                                    ?>

                                <option value="<?php echo $key ?>" <?php if (set_value('father_title', $enquiry_data['father_title']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                    <?php
                                }
                                ?>
                        </select>
                        </div>
                    </div>
                    
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_firstname">First Name</label><small class="req"> *</small>  
                            <input id="father_firstname" name="father_firstname" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('father_firstname', $enquiry_data['father_firstname']); ?>" />
                            <span class="text-danger"><?php echo form_error('father_firstname'); ?></span>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_lastname">Last Name</label> 
                            <input id="father_lastname" name="father_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('father_lastname', $enquiry_data['father_lastname']); ?>" />
                            <span class="text-danger"><?php echo form_error('father_lastname'); ?></span>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_email">Email</label> <small class="req"> *</small>
                            <input id="father_email" name="father_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('father_email', $enquiry_data['father_email']); ?>" />
                            <span class="text-danger"><?php echo form_error('father_email'); ?></span>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_mobile">Mobile</label><small class="req"> *</small>   
                            <input id="father_mobile" name="father_mobile" placeholder="Mobile" type="tel" class="form-control form-mobile" maxlength="14" value="<?php echo set_value('father_mobile', $enquiry_data['father_mobile']); ?>" />
                            <span class="text-danger"><?php echo form_error('father_mobile'); ?></span>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Aadhar Card No.</label> 
                            <input maxlength="14" id="father_aadhar_no" name="father_aadhar_no"  placeholder="12 Digit Aadhaar No." type="text" class="form-control aadhaar_input"  value="<?php echo set_value('father_aadhar_no', $enquiry_data['father_aadhar_no']); ?>" />
                            <span class="text-danger"><?php echo form_error('father_aadhar_no'); ?></span>
                        </div>
                    </div>                                       

                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Qualification:</label><small class="req"> *</small>   
                            <select name="father_qualification" class="form-control append_other"  >
                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    
                                    <?php $parent_qualification_list=$this->config->item('parent_qualification_list');
                                    foreach ($parent_qualification_list as $key => $value) {
                                        ?>

                                    <option value="<?php echo $key ?>" <?php if (set_value('father_qualification', $enquiry_data['father_qualification']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                        <div class="form-group <?php if($enquiry_data['father_qualification']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="father_qualification_other" name="father_qualification_other" placeholder="Other Qualification" type="text" class="form-control"  value="<?php echo set_value('father_qualification_other', $enquiry_data['father_qualification_other']); ?>" /> 
                                                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Occupation:</label> 
                            <select name="father_occupation" class="form-control append_other"  >
                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php $father_occupation_list=$this->config->item('parent_occupation_list');
                                    foreach ($father_occupation_list as $key => $value) {
                                        ?>

                                    <option value="<?php echo $key ?>" <?php if (set_value('father_occupation', $enquiry_data['father_occupation']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                        <div class="form-group <?php if($enquiry_data['father_occupation']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="father_occupation_other" name="father_occupation_other" placeholder="Other Occupation" type="text" class="form-control"  value="<?php echo set_value('father_occupation_other', $enquiry_data['father_occupation_other']); ?>" /> 
                                                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Employer</label><small class="req"> *</small>  
                            <input id="father_employer" name="father_employer" placeholder="Employer" type="text" class="form-control"  value="<?php echo set_value('father_employer', $enquiry_data['father_employer']); ?>" />
                            <span class="text-danger"><?php echo form_error('father_employer'); ?></span>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">

                            <label for="email">Designation</label> 
                            <select name="father_designation" class="form-control append_other"  >
                            <option value=""><?php echo $this->lang->line('select') ?></option>
                                <?php $father_designation_list=$this->config->item('parent_designation_list');
                                foreach ($father_designation_list as $key => $value){?>
                                    <option value="<?php echo $key ?>" <?php if ($enquiry_data['father_designation'] == $key) { ?> selected="selected" <?php } ?>><?php echo $value; ?></option>

                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group <?php if($enquiry_data['father_designation']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="father_designation_other" name="father_designation_other" placeholder="Other Designation" type="text" class="form-control"  value="<?php echo set_value('father_designation_other', $enquiry_data['father_designation_other']); ?>" /> 
                                                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Work Address</label> 
                            <input id="father_address" name="father_address" placeholder="Work Address" type="text" class="form-control"  value="<?php echo set_value('father_address', $enquiry_data['father_address']); ?>" />
                            
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Work City</label> 
                                <select name="father_city" class="form-control append_other"   >
                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php $father_city_list=$this->config->item('parent_city_list');
                                    foreach ($father_city_list as $key => $value) {
                                        ?>

                                    <option value="<?php echo $key ?>" <?php if (set_value('father_city', $enquiry_data['father_city']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                        <div class="form-group <?php if($enquiry_data['father_city']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="father_city_other" name="father_city_other" placeholder="Other City" type="text" class="form-control"  value="<?php echo set_value('father_city_other', $enquiry_data['father_city_other']); ?>" /> 
                                                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Work Pin Code</label> 
                            <input id="father_pincode" name="father_pincode" placeholder="Work Pin Code" type="text" class="form-control"  value="<?php echo set_value('father_pincode', $enquiry_data['father_pincode']); ?>" />
                            
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Work phone</label> 
                            <input id="father_workphone" name="father_workphone" placeholder="Work phone" type="text" class="form-control"  value="<?php echo set_value('father_workphone', $enquiry_data['father_workphone']); ?>" />
                            
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Annual Income</label> 
                            <input id="father_annualincome" name="father_annualincome" placeholder="Annual Income" type="text" class="form-control"  value="<?php echo set_value('father_annualincome', $enquiry_data['father_annualincome']); ?>" />
                            
                        </div>
                    </div>

                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label>Working wih TKS Noida</label>

                            <select name="father_woking_tks" class="form-control"  >
                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php $father_woking_tks_list=$this->config->item('true-false');
                                    foreach ($father_woking_tks_list as $key => $value) {
                                        ?>

                                    <option value="<?php echo $key ?>" <?php if (set_value('father_woking_tks', $enquiry_data['father_woking_tks']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_mobile">Email Prefrence</label>  
                            <div class="material-switch">
                                <input id="father_email_pref" name="father_email_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['father_email_pref']) == 1) ? TRUE : FALSE); ?>>
                                <label for="father_email_pref" class="label-success"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_mobile">SMS Prefrence</label>  
                            <div class="material-switch">
                                <input id="father_sms_pref" name="father_sms_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['father_sms_pref']) == 1) ? TRUE : FALSE); ?>>
                                <label for="father_sms_pref" class="label-success"></label>
                            </div>
                        </div>
                    </div> 
                    <p class="form-note"><strong>*Note:</strong>
                     Add one email and one SMS as a communication preference, either for Father or Mother.</p>
                    </div></div>

                    <!-- Mother details -->
                    <div class="tshadow mb25 bozero sw-content">
                        <h4 class="pagetitleh2">Mother Details</h4>
                        <div class="row around10">
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_title">Title:</label> 
                                <select name="mother_title" class="form-control"  >
                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php $mother_title_list=$this->config->item('mother_title_list');
                                    foreach ($mother_title_list as $key => $value) {
                                        ?>

                                    <option value="<?php echo $key ?>" <?php if (set_value('mother_title', $enquiry_data['mother_title']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>

                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_firstname">First Name</label> 
                                <input id="mother_firstname" name="mother_firstname" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('mother_firstname', $enquiry_data['mother_firstname']); ?>" />
                                <span class="text-danger"><?php echo form_error('mother_firstname'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_lastname">Last Name</label> 
                                <input id="mother_lastname" name="mother_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('mother_lastname', $enquiry_data['mother_lastname']); ?>" />
                                <span class="text-danger"><?php echo form_error('mother_lastname'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_email">Email</label> 
                                <input id="mother_email" name="mother_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('mother_email', $enquiry_data['mother_email']); ?>" />
                                <span class="text-danger"><?php echo form_error('mother_email'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_mobile">Mobile</label> 
                                <input id="mother_mobile" name="mother_mobile" placeholder="Mobile" type="tel" class="form-control form-mobile" maxlength="14" value="<?php echo set_value('mother_mobile', $enquiry_data['mother_mobile']); ?>" />
                                <span class="text-danger"><?php echo form_error('mother_mobile'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Aadhar Card No.</label> 
                                <input maxlength="14" id="mother_aadhar_no" name="mother_aadhar_no" placeholder="12 Digit Aadhaar No." type="text" class="form-control aadhaar_input"  value="<?php echo set_value('mother_aadhar_no', $enquiry_data['mother_aadhar_no']); ?>" />
                                <span class="text-danger"><?php echo form_error('mother_aadhar_no'); ?></span>
                            </div>
                        </div>
                                              

                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Qualification</label><small class="req"> *</small>  
                                <select name="mother_qualification" class="form-control append_other"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $parent_qualification_list=$this->config->item('parent_qualification_list');
                                        foreach ($parent_qualification_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('mother_qualification', $enquiry_data['mother_qualification']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                            <div class="form-group <?php if($enquiry_data['mother_qualification']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="mother_qualification_other" name="mother_qualification_other" placeholder="Other Qualification" type="text" class="form-control"  value="<?php echo set_value('mother_qualification_other', $enquiry_data['mother_qualification_other']); ?>" /> 
                                                        </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Occupation</label> 
                                <select name="mother_occupation" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $mother_occupation_list=$this->config->item('parent_occupation_list');
                                        foreach ($mother_occupation_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('mother_occupation', $enquiry_data['mother_occupation']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                            <div class="form-group <?php if($enquiry_data['mother_occupation']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="mother_occupation_other" name="mother_occupation_other" placeholder="Other Occupation" type="text" class="form-control"  value="<?php echo set_value('mother_occupation_other', $enquiry_data['mother_occupation_other']); ?>" /> 
                                                        </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Employer</label><small class="req"> *</small>  
                                <input id="mother_employer" name="mother_employer" placeholder="Employer" type="text" class="form-control"  value="<?php echo set_value('mother_employer', $enquiry_data['mother_employer']); ?>" />
                                <span class="text-danger"><?php echo form_error('mother_employer'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Designation</label> 
                                <select name="mother_designation" class="form-control append_other"  >
                                    <option value=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $mother_designation_list=$this->config->item('parent_designation_list');
                                        foreach ($mother_designation_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if ($enquiry_data['mother_designation'] == $key) { ?> selected="selected" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                            <div class="form-group <?php if($enquiry_data['mother_designation']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="mother_designation_other" name="mother_designation_other" placeholder="Other Designation" type="text" class="form-control"  value="<?php echo set_value('mother_designation_other', $enquiry_data['mother_designation_other']); ?>" /> 
                                                        </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Work Address</label> 
                                <input id="mother_address" name="mother_address" placeholder="Work Address" type="text" class="form-control"  value="<?php echo set_value('mother_address', $enquiry_data['mother_address']); ?>" />
                                
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Work City</label> 
                                    <select name="mother_city" class="form-control append_other"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        ?>
                                        ?>
                                        <?php $mother_city_list=$this->config->item('parent_city_list');
                                        foreach ($mother_city_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('mother_city', $enquiry_data['mother_city']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                            <div class="form-group <?php if($enquiry_data['mother_city']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="mother_city_other" name="mother_city_other" placeholder="Other City" type="text" class="form-control"  value="<?php echo set_value('mother_city_other', $enquiry_data['mother_city_other']); ?>" /> 
                                                        </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Work Pin Code</label> 
                                <input id="mother_pincode" name="mother_pincode" placeholder="Work Pin Code" type="text" class="form-control"  value="<?php echo set_value('mother_pincode', $enquiry_data['mother_pincode']); ?>" />
                                
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Work phone</label> 
                                <input id="mother_workphone" name="mother_workphone" placeholder="Work phone" type="text" class="form-control"  value="<?php echo set_value('mother_workphone', $enquiry_data['mother_workphone']); ?>" />
                                
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Annual Income</label> 
                                <input id="mother_annualincome" name="mother_annualincome" placeholder="Annual Income" type="text" class="form-control"  value="<?php echo set_value('mother_workphone', $enquiry_data['mother_workphone']); ?>" />
                                
                            </div>
                        </div>

                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label>Working wih TKS Noida</label>

                                <select name="mother_woking_tks" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $mother_woking_tks_list=$this->config->item('true-false');
                                        foreach ($mother_woking_tks_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('mother_woking_tks', $enquiry_data['mother_woking_tks']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="father_mobile">Email Prefrence</label>  
                                <div class="material-switch">
                                    <input id="mother_email_pref" name="mother_email_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['mother_email_pref']) == 1) ? TRUE : FALSE); ?>>
                                    <label for="mother_email_pref" class="label-success"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_mobile">SMS Prefrence</label>  
                                <div class="material-switch">
                                    <input id="mother_sms_pref" name="mother_sms_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['mother_sms_pref']) == 1) ? TRUE : FALSE); ?>>
                                    <label for="mother_sms_pref" class="label-success"></label>
                                </div>
                            </div>
                        </div> 
                        <p class="form-note"><strong>*Note:</strong>
                     Add one email and one SMS as a communication preference, either for Father or Mother.</p> 

                        </div></div>

                    <!-- Mother details end-->

                    <!-- Guardian details -->

                    <div class="tshadow mb25 bozero sw-content">
                        <h4 class="pagetitleh2">Guardian Details</h4>
                        <div class="row around10">
                            <!-- <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Name</label> 
                                <input id="guardian_name" name="guardian_name" placeholder="Name" type="text" class="form-control"  value="<?php echo set_value('guardian_name', $enquiry_data['guardian_name']); ?>" />
                                
                            </div>
                        </div>
                        
                             -->
                            
                            <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="guardian_title">Title:</label> 
                                <select name="guardian_title" class="form-control"  >
                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php $guardian_title_list=$this->config->item('guardian_title_list');
                                    foreach ($guardian_title_list as $key => $value) {
                                        ?>

                                    <option value="<?php echo $key ?>" <?php if (set_value('guardian_title', $enquiry_data['guardian_title']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>

                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="guardian_name">First Name</label> 
                                <input id="guardian_name" name="guardian_name" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('guardian_name', $enquiry_data['guardian_name']); ?>" />
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="guardian_lastname">Last Name</label> 
                                <input id="guardian_lastname" name="guardian_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('guardian_lastname', $enquiry_data['guardian_lastname']); ?>" />
                                <span class="text-danger"><?php echo form_error('guardian_lastname'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="guardian_email">Email</label> 
                                <input id="guardian_email" name="guardian_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('guardian_email', $enquiry_data['guardian_email']); ?>" />
                                <span class="text-danger"><?php echo form_error('guardian_email'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="guardian_mobile">Mobile</label> 
                                <input id="guardian_mobile" name="guardian_mobile" placeholder="Mobile" type="tel" class="form-control form-mobile" maxlength="14" value="<?php echo set_value('guardian_mobile', $enquiry_data['guardian_mobile']); ?>" />
                                <span class="text-danger"><?php echo form_error('guardian_mobile'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Aadhar Card No.</label> 
                                <input maxlength="14" id="guardian_aadhar_no" name="guardian_aadhar_no" placeholder="12 Digit Aadhaar No." type="text" class="form-control aadhaar_input"  value="<?php echo set_value('guardian_aadhar_no', $enquiry_data['guardian_aadhar_no']); ?>" />
                                <span class="text-danger"><?php echo form_error('guardian_aadhar_no'); ?></span>
                            </div>
                        </div>
                                            

                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Qualification</label>
                                <select name="guardian_qualification" class="form-control append_other"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $parent_qualification_list=$this->config->item('parent_qualification_list');
                                        foreach ($parent_qualification_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('guardian_qualification', $enquiry_data['guardian_qualification']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                            <div class="form-group <?php if($enquiry_data['guardian_qualification']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="guardian_qualification_other" name="guardian_qualification_other" placeholder="Other Qualification" type="text" class="form-control"  value="<?php echo set_value('guardian_qualification_other', $enquiry_data['guardian_qualification_other']); ?>" /> 
                                                        </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Occupation</label> 
                                <select name="guardian_occupation" class="form-control append_other"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $guardian_occupation_list=$this->config->item('parent_occupation_list');
                                        foreach ($guardian_occupation_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('guardian_occupation', $enquiry_data['guardian_occupation']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                            <div class="form-group <?php if($enquiry_data['guardian_occupation']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="guardian_occupation_other" name="guardian_occupation_other" placeholder="Other Occupation" type="text" class="form-control"  value="<?php echo set_value('guardian_occupation_other', $enquiry_data['guardian_occupation_other']); ?>" /> 
                                                        </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Employer</label>
                                <input id="guardian_employer" name="guardian_employer" placeholder="Employer" type="text" class="form-control"  value="<?php echo set_value('guardian_employer', $enquiry_data['guardian_employer']); ?>" />
                                <span class="text-danger"><?php echo form_error('guardian_employer'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Designation</label> 
                                <select name="guardian_designation" class="form-control append_other"  >
                                    <option value=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $guardian_designation_list=$this->config->item('parent_designation_list');
                                        foreach ($guardian_designation_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if ($enquiry_data['guardian_designation'] == $key) { ?> selected="selected" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                            <div class="form-group <?php if($enquiry_data['guardian_designation']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="guardian_designation_other" name="guardian_designation_other" placeholder="Other Designtion" type="text" class="form-control"  value="<?php echo set_value('guardian_designation_other', $enquiry_data['guardian_designation_other']); ?>" /> 
                                                        </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Address</label> 
                                <input id="guardian_address" name="guardian_address" placeholder="Address" type="text" class="form-control"  value="<?php echo set_value('guardian_address', $enquiry_data['guardian_address']); ?>" />
                                
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                                            <label for="email">City</label> 
                                                            <select name="guardian_city" class="form-control append_other"  >
                                                                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                                <?php $guardian_city_list=$this->config->item('parent_city_list');
                                                                                foreach ($guardian_city_list as $key => $value) {
                                                                                    ?>

                                                                                <option value="<?php echo $key ?>" <?php if (set_value('guardian_city', $enquiry_data['guardian_city']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                        </select>
                                                            
                                                            </div>
                                <div class="form-group <?php if($enquiry_data['guardian_city']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="guardian_city_other" name="guardian_city_other" placeholder="Other City" type="text" class="form-control"  value="<?php echo set_value('guardian_city_other', $enquiry_data['guardian_city_other']); ?>" /> 
                                                        </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">State</label> 
                                <input id="guardian_state" name="guardian_state" placeholder="State" type="text" class="form-control"  value="<?php echo set_value('guardian_state', $enquiry_data['guardian_state']); ?>" />
                                
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Pin Code</label> 
                                <input id="guardian_pincode" name="guardian_pincode" placeholder="Pin Code" type="text" class="form-control"  value="<?php echo set_value('guardian_pincode', $enquiry_data['guardian_pincode']); ?>" />
                                
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="email">Telephone</label> 
                                    <input id="guardian_telephone" name="guardian_telephone" placeholder="Telephone" type="text" class="form-control"  value="<?php echo set_value('guardian_telephone', $enquiry_data['guardian_telephone']); ?>" />
                                    
                                </div>
                            </div>
                        <!-- <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Work Address</label> 
                                <input id="guardian_address" name="guardian_address" placeholder="Work Address" type="text" class="form-control"  value="<?php echo set_value('guardian_address', $enquiry_data['guardian_address']); ?>" />
                                
                            </div>
                        </div> -->
                        <!-- <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Work City</label> 
                                    <select name="guardian_city" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        ?>
                                        ?>
                                        <?php $guardian_city_list=$this->config->item('parent_city_list');
                                        foreach ($guardian_city_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('guardian_city', $enquiry_data['guardian_city']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                        </div> -->
                        <!-- <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Work Pin Code</label> 
                                <input id="guardian_pincode" name="guardian_pincode" placeholder="Work Pin Code" type="text" class="form-control"  value="<?php echo set_value('guardian_pincode', $enquiry_data['guardian_pincode']); ?>" />
                                
                            </div>
                        </div> -->
                        <!-- <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Work phone</label> 
                                <input id="guardian_workphone" name="guardian_workphone" placeholder="Work phone" type="text" class="form-control"  value="<?php echo set_value('guardian_workphone', $enquiry_data['guardian_workphone']); ?>" />
                                
                            </div>
                        </div> -->
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Annual Income</label> 
                                <input id="guardian_annualincome" name="guardian_annualincome" placeholder="Annual Income" type="text" class="form-control"  value="<?php echo set_value('guardian_workphone', $enquiry_data['guardian_workphone']); ?>" />
                                
                            </div>
                        </div>

                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label>Working wih TKS Noida</label>

                                <select name="guardian_woking_tks" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        ?>
                                        ?>
                                        <?php $guardian_woking_tks_list=$this->config->item('true-false');
                                        foreach ($guardian_woking_tks_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('guardian_woking_tks', $enquiry_data['guardian_woking_tks']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="guardian_relation">Relation</label> 
                                    <input id="guardian_relation" name="guardian_relation" placeholder="Relation" type="text" class="form-control"  value="<?php echo set_value('guardian_relation', $enquiry_data['guardian_relation']); ?>" />
                                    
                                </div>
                            </div> 
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="father_mobile">Email Prefrence</label>  
                                <div class="material-switch">
                                    <input id="guadian_email_pref" name="guadian_email_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['guadian_email_pref']) == 1) ? TRUE : FALSE); ?>>
                                    <label for="guadian_email_pref" class="label-success"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_mobile">SMS Prefrence</label>  
                                <div class="material-switch">
                                    <input id="guadian_sms_pref" name="guadian_sms_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['guadian_sms_pref']) == 1) ? TRUE : FALSE); ?>>
                                    <label for="guadian_sms_pref" class="label-success"></label>
                                </div>
                            </div>
                        </div>

                                                                                                           
                        </div>
                    </div>
                    
                    <!-- guardian details end -->
                    <div class="sw-footer">
                        <div class="text-right form-group">
                            <button class="site-btn sw-btn btn sw-save-btn hide" type="button">Save Draft</button>
                            <button class="site-btn sw-btn btn sw-prev-btn" type="button">Prev</button>
                            <button class="site-btn sw-btn btn sw-next-btn" type="button">Save & Next</button>
                        </div>
                    </div>
                    </form>
                </div>
                
            
            
                <div class="sw-steps-content">
                    <form id="questionaire-form" action="">
                        <!-- Questionaire -->
                        <div class="tshadow mb25 bozero sw-content">
                        <h4 class="pagetitleh2">Questionaire</h4>
                        <div class="row around10">
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="pwd">School Last Attended:</label><small class="req"> *</small>
                                    <input type="text" class="form-control" id="school_last_attended" value="<?php echo set_value('school_last_attended', $enquiry_data['school_last_attended']); ?>" name="school_last_attended">
                                    <span class="text-danger" id="school_last_attended"></span>
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="email">Other Languages Child can Speak / Understand:</label> <small class="req"> *</small>
                                        <input type="text" class="form-control" id="languages_child_can_speak" value="<?php echo set_value('languages_child_can_speak', $enquiry_data['languages_child_can_speak']); ?>" name="languages_child_can_speak">
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="email">Real Brother / Sister School:</label><small class="req"> *</small> 
                                        <input type="text" class="form-control" id="sibbling_school" value="<?php echo set_value('sibbling_school', $enquiry_data['sibbling_school']); ?>" name="sibbling_school">
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="email">Real Brother / Sister Class:</label><small class="req"> *</small> 
                                        <input type="text" class="form-control" id="sibbling_class" value="<?php echo set_value('sibbling_class', $enquiry_data['sibbling_class']); ?>" name="sibbling_class">
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="email">Special Traits:</label><small class="req"> *</small> 
                                        <input type="text" class="form-control" id="special_traits" value="<?php echo set_value('special_traits', $enquiry_data['special_traits']); ?>" name="special_traits">
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="email">Interaction with Peer Group:</label><small class="req"> *</small> 
                                        <input type="text" class="form-control" id="interaction_with_peer_group" value="<?php echo set_value('interaction_with_peer_group', $enquiry_data['interaction_with_peer_group']); ?>" name="interaction_with_peer_group">
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="email">Whether Nuclear / Joint Family:</label><small class="req"> *</small> 
                                    <input type="text" class="form-control" id="nuclear_joint_family" value="<?php echo set_value('nuclear_joint_family', $enquiry_data['nuclear_joint_family']); ?>" name="nuclear_joint_family">
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="home_town">What according to you is the definition of a good school?</label><small class="req"> *</small> 
                                    <input id="definition_good_school" name="definition_good_school" placeholder="" type="text" class="form-control"  value="<?php echo set_value('definition_good_school', $enquiry_data['definition_good_school']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="home_town">State your views on inclusive education.</label><small class="req"> *</small> 
                                    <input id="inclusive_education" name="inclusive_education" placeholder="" type="text" class="form-control"  value="<?php echo set_value('inclusive_education', $enquiry_data['inclusive_education']); ?>" />
                                </div>
                            </div>
                            
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="home_town">What values would you like to inculcate in your child?<small class="req"> *</small></label>
                                    <input id="inculcate_values" name="inculcate_values" placeholder="" type="text" class="form-control"  value="<?php echo set_value('inculcate_values', $enquiry_data['inculcate_values']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="interests_hobbies">Mention atleast two interests / hobbies of your child.<small class="req"> *</small></label>
                                    <input id="interests_hobbies" name="interests_hobbies" placeholder="" type="text" class="form-control"  value="<?php echo set_value('interests_hobbies', $enquiry_data['interests_hobbies']); ?>" />
                                </div>
                            </div>

                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="spendtime_importance">As a father why do you think it is important to spend time with your child?<small class="req"> *</small></label> 
                                    <input id="spendtime_importance" name="spendtime_importance" placeholder="" type="text" class="form-control"  value="<?php echo set_value('spendtime_importance', $enquiry_data['spendtime_importance']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="spend_with_your_child">As a mother, how much time do you spend with your child and how?<small class="req"> *</small></label>
                                    <input id="spend_with_your_child" name="spend_with_your_child" placeholder="" type="text" class="form-control"  value="<?php echo set_value('spend_with_your_child', $enquiry_data['spend_with_your_child']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="home_town">Incase both parents are working who takes care of the child?<small class="req"> *</small></label>
                                    <input id="takes_care_child" name="takes_care_child" placeholder="" type="text" class="form-control"  value="<?php echo set_value('takes_care_child', $enquiry_data['takes_care_child']); ?>" />
                                </div>
                            </div>
                            
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="logical_answers">If your child is very inquisitive, do you always believe in providing logical answers to his / her question or avoid them?<small class="req"> *</small></label> 
                                    <input id="logical_answers" name="logical_answers" placeholder="" type="text" class="form-control"  value="<?php echo set_value('logical_answers', $enquiry_data['logical_answers']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="physical_illness">Did your child ever suffer from a handicap or ailment where the school needs to be cautious in handling him / her?<small class="req"> *</small></label>
                                    <input id="physical_illness" name="physical_illness" placeholder="" type="text" class="form-control"  value="<?php echo set_value('physical_illness', $enquiry_data['physical_illness']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="kps_impression">Write briefly your impression about The Khaitan School.<small class="req"> *</small></label>
                                    <input id="kps_impression" name="kps_impression" placeholder="" type="text" class="form-control"  value="<?php echo set_value('kps_impression', $enquiry_data['kps_impression']); ?>" />
                                </div>
                            </div>                                
                        </div>
                        </div>
                        <div class="sw-footer">
                            <div class="text-right form-group">
                                <button class="site-btn sw-btn btn sw-save-btn hide" type="button">Save Draft</button>
                                <button class="site-btn sw-btn btn sw-prev-btn" type="button">Prev</button>
                                <button class="site-btn sw-btn btn sw-next-btn" type="button">Save & Next</button>
                            </div>
                        </div>
                    
                        <!-- Agreement -->
                    </form>
                </div>
                
            
                <div class="sw-steps-content">
                    <form id="consent-form" action="">
                        <div class="tshadow mb25 bozero sw-content">
                        <h4 class="pagetitleh2">Terms to acknowledge</h4>
                        <div class="around10">
                            <!--  -->
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_1" name="terms_1" type="checkbox" class="chk" value="1">
                                    <small class="req"> *</small>
                                    <label for="terms_1" class="terms-label">I / we have seen the fee structure on the website of the school and am / are conversant with it. I / we abide to follow all the rules and regulations mentioned in the fee rules at all times.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_2" name="terms_2" type="checkbox" class="chk" value="1">
                                    <small class="req"> *</small>
                                    <label for="terms_2" class="terms-label">The information provided by me / us in the online registration is accurate to the best of my / our knowledge. All questions asked in the application were answered by either a parent or a guardian.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_3" name="terms_3" type="checkbox" class="chk" value="1">
                                    <small class="req"> *</small>
                                    <label for="terms_3" class="terms-label">I / we understand that incase of any non-conformance / wrong information in the application shall give the school the right to not offer a seat to my / our ward.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_4" name="terms_4" type="checkbox" class="chk" value="1">
                                    <small class="req"> *</small>
                                    <label for="terms_4" class="terms-label">I / we understand and agree that the Registration Fee of Rs 500 is non-refundable and does not guarantee admission.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_5" name="terms_5" type="checkbox" class="chk" value="1">
                                    <small class="req"> *</small>
                                    <label for="terms_5" class="terms-label">I / we understand that admissions decisions are at the discretion of the school authorities and that all decisions of the school are final.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_6" name="terms_6" type="checkbox" class="chk" value="1">
                                    <small class="req"> *</small>
                                    <label for="terms_6" class="terms-label">I / we undertake to inform The Khaitan School immediately of any changes to the information provided herein.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_7" name="terms_7" type="checkbox" class="chk" value="1">
                                    <small class="req"> *</small>
                                    <label for="terms_7" class="terms-label">I / we have not and will not make any payments related to admission to The Khaitan School except those set out in the fees schedule.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_8" name="terms_8" type="checkbox" class="chk" value="1">
                                    <small class="req"> *</small>
                                    <label for="terms_8" class="terms-label">I / we understand that I / we must report any attempt to subvert the admissions process or pay or solicit payments related to admissions except those set out in the fee schedule.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_9" name="terms_9" type="checkbox" class="chk" value="1">
                                    <small class="req"> *</small>
                                    <label for="terms_9" class="terms-label">I / we understand the <span><a href="javascript:void(0)" class="privacy-modal-toggle">Privacy Policy</a></span> and <span><a href="javascript:void(0)" class="terms-modal-toggle">Terms and conditions</a></span></label>
                                    <div id="terms_modal" class="terms_modal popup">
                                        <div class="terms_modal_inner">
                                            <div class="terms_modal_content">
                                                <span class="terms-modal-close close2 close">×</span>
                                                <h3>TERMS OF SERVICE</h3>
                                                <div>These Terms of Service outline the rules and regulations for the access to and the use of The Khaitan School's Website <strong>(“Website”)</strong> and the related services (collectively with the Website and the related services are referred to as the “Services”).
                                                By accessing this Website, we assume you accept these Terms of Service in full. Do not continue to use The Khaitan School's Website if you do not accept all of the Terms of Service stated on this page.
                                               
                                                <strong>PLEASE READ THESE TERMS OF SERVICES CAREFULLY. BY ACCESSING OR USING THE WEBSITE YOU AGREE TO BE BOUND BY THESE TERMS OF SERVICES.</strong>
                                                <strong>A.	LEGAL CONDITIONS</strong> 

                                                Your use of the Services is expressly conditioned upon your agreement to these Terms of Service. If you do not consent to these Terms of Service, you are not permitted to use any of the Services. 

                                                These Terms of Service, Privacy Policy (available on Website) together constitute a binding agreement between you and The Khaitan School <strong>(“Agreement”).</strong> The Privacy Policy is read with and incorporated into these Terms of Service and collectively govern your use of the Website.

                                                By accepting the Agreement, you affirm that you are <mark>18 years of age or</mark> above and are fully competent to enter into the Agreement, and to abide by and comply with the Agreement.
                                                The following terminology applies to these Terms of Service, Privacy Policy and Disclaimer Notice and any or all Agreements: "Client", “user”, “visitor” "You" and "Your" refers to you, the person accessing this Website and accepting the Website’s Terms of Service. "Ourselves", "We", "Our" and "Us", refers to The Khaitan School. "Party", "Parties", or "Us", refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client's needs in respect of provision of The Khaitan School ‘s stated services, in accordance with and subject to, prevailing law of . Any use of the above terminology or other words in the singular, plural, capitalisation and/or he/she or they, are taken as interchangeable and therefore as referring to same.

                                                <strong>B.	COOKIES</strong>

                                                We employ the use of cookies. By using The Khaitan School's Website you consent to the use of cookies in accordance with The Khaitan School's Privacy Policy.
                                                Most of the modern-day interactive web sites use cookies to enable us to retrieve user details for each visit. Cookies are used in some areas of our Website to enable the functionality of this area and ease of use for visitors. Some of our affiliate / advertising partners may also use cookies.
                                               
                                                <strong>INTELLECTUAL PROPERTY RIGHTS</strong> Unless otherwise stated- The Khaitan School and/or it's licensors own the intellectual property rights for all material on The Khaitan School. All intellectual property rights are reserved. You may view and/or print pages from thekhaitanschool.org/ for your own personal use subject to restrictions set in these terms of service.
                                                
                                                You acknowledge and agree that the materials on the Website, other than the user’s Comments that you licensed under these Terms of Service, including without limitation, the text, software, scripts, graphics, photos, sounds, music, videos, interactive features and the like <strong>("Materials")</strong> and the trademarks, service marks and logos contained therein <strong>("Marks"),</strong> are owned by or licensed to The Khaitan School, and are subject to copyright and other intellectual property rights under Indian laws, foreign laws and international treaties and/or conventions. In connection with the Services, the Website may display certain trademarks belonging to third parties. Use of these trademarks may be subject to the license granted by third parties to The Khaitan School. 
                                                
                                                You shall, in no event, reverse engineer, decompile, or disassemble such trademarks and nothing herein shall be construed to grant you any right in relation to such trademarks. Materials on the platform are provided to you for your information and personal use only and may not be used, copied, reproduced, distributed, transmitted, broadcast, displayed, sold, licensed, or otherwise exploited for any other purposes whatsoever without the prior written consent of the respective owners. 
                                                
                                                The Khaitan School reserves all rights not expressly granted herein to the Website and the Materials. You agree to not engage in the use, copying, or distribution of any of the Materials other than as expressly permitted herein, including any use, copying, or distribution of Materials of third parties obtained through the Website for any commercial purposes. If you download or print a copy of the Materials for personal use, you must retain all copyright and other proprietary notices contained therein. You agree not to circumvent, disable or otherwise interfere with security-related features of the Website or features that prevent or restrict use or copying of any Materials or enforce limitations on use of the Website or the Materials therein. The Website is protected to the maximum extent permitted by copyright laws, other laws, and international treaties and/or conventions. The Comment displayed on or through the Website is protected by copyright as a collective work and/or compilation, pursuant to copyrights laws, other laws, and international conventions. Any reproduction, modification, creation of derivative works from or redistribution of the Website, the Materials, or the collective work or compilation is expressly prohibited. Copying or reproducing the Website, the Materials, or any portion thereof to any other server or location for further reproduction or redistribution is expressly prohibited. 
                                                
                                                You further agree not to reproduce, duplicate or copy Comments or Materials from the Website, and agree to abide by any and all copyright notices and other notices displayed on the Website. 
                                                
                                                You must not:
                                                1.	Republish material from thekhaitanschool.org/
                                                2.	Sell, rent or sub-license material from thekhaitanschool.org/
                                                3.	Reproduce, duplicate or copy material from thekhaitanschool.org/
                                                4.	access, download, distribute, transmit, broadcast, display, sell, license, alter, modify or otherwise use any part of the Website or any Comment/Content except: (a) as expressly authorized by the Website; or (b) with prior written permission from us and, if applicable, the respective rights holders;Redistribute content from The Khaitan School (unless content is specifically made for redistribution).
                                                
                                                <strong>C.	TAKEDOWN POLICY</strong>

                                                The Khaitan School respects the intellectual property and personal rights of others, and we expect you to do the same. The Khaitan School’s Terms of Service do not allow posting, sharing, or sending any content that violates or infringes someone else’s personality, copyrights, trademarks or other intellectual property rights.
                                                
                                                <strong>Copyright</strong>
                                                
                                                Copyright is a legal right that protects original works of authorship (e.g., music, videos, etc.). Generally, copyright protects an original expression of an idea but does not protect underlying ideas or facts.
                                                
                                                <strong>Copyright Infringement</strong>
                                                
                                                We do not allow any content that infringes copyright. Any use of copyrighted content of others without proper authorization or legally valid reason may lead to a violation. In some cases, you may be required to provide evidence to prove that you are entitled to use copyrighted content of others.
                                                
                                                <strong>Removal of Content; Suspension or Termination of Account</strong>
                                                
                                                Any user content that infringes other person’s copyright may be removed. Your account may be suspended or terminated for multiple copyright violations in connection with your use of The Khaitan School Website, or other violations of the Terms of Service. .
                                                
                                                <strong>Trademark and Personal Rights</strong>
                                                
                                                A trademark is a word, symbol, slogan, design, or combination of any of the foregoing that identifies the source of a product or service and distinguishes it from other products or services.
                                                The Khaitan School’s policies prohibit any content that infringes on another person’s trademark or impersonates another person’s name, likeness and persona. Using another person’s trademark in a way that may mislead or confuse people to believe that you are affiliated with the trademark owner may be a violation of our Terms of Service. Further, using another person’s name, likeness and persona may amount to impersonation and may be a violation of our Terms of Service. 
                                                However, using another person’s trademark for the purpose of accurately referencing the trademark owner’s products or services, or to compare them to other products or services, or use of another person’s name, likeness and persona to identify the said person is generally not considered a violation of our Terms of Service.
                                                
                                                <strong>D.	USER COMMENTS/CONTENT</strong>
                                                
                                                1.	This Agreement shall begin on the date hereof.The Website offer the opportunity for users to post and exchange opinions, information, material and data <strong>('Comments/Content')</strong> in areas of the Website. The Khaitan School does not screen, edit, publish or review Comments prior to their appearance on the Website and Comments do not reflect the views or opinions of - The Khaitan School, its agents or affiliates. Comments reflect the view and opinion of the person who posts such view or opinion. To the extent permitted by applicable lawsThe Khaitan School shall not be responsible or liable for the Comments or for any loss cost, liability, damages or expenses caused and or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.
                                                2.	Comments are the responsibility of the person or entity that provides it to the Website. 

                                                3. <strong>REMOVAL OF COMMENTS/CONTENT</strong> The Khaitan School reserves the right to monitor all Comments and to remove any Comments which it considers in its absolute discretion to be inappropriate, offensive or otherwise in breach of these Terms of Service. Examples of inappropriate, unsuitable Comments include, but are not limited to, the following:
                                               
                                                •	Defamatory, malicious, obscene, intimidating, unlawful, offensive, discriminatory, harassing or threatening comments or hate propaganda;
                                                •	abusive, harassing, threatening, impersonating or intimidating other users.
                                                •	Comments that promotes violence against, threaten, or harass other people on the basis of race, ethnicity, national origin, caste, sexual orientation, gender, gender identity, religious affiliation, age, disability, or serious disease. 
                                                •	Comments that threatens or promotes terrorism or violent extremism
                                                •	Comments that violates any law or regulation;
                                                •	Comments deemed to constitute false or misleading; or promotes or encourages suicide or self-harm.
                                                •	Any potential infringement upon any intellectual property rights, including but not limited to, brand names, trade names, logos, copyrights or trade secrets of any person, business or place;
                                                •	Other content deemed to be off-topic or to disrupt of the Services, users; and
                                                •	Comments posted by fake or anonymous users.
                                                •	Comments which threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting any other nation.
                                                •	Comments that contains or links to pornography, graphic violence, threats, hate speech, or incitements to violence. 
                                                •	Sexually explicit, obscene content or content which violates privacy rights 
                                                •	Sensitive Personal Information
                                                •	Comments that might disturb the reputation and goodwill of The Khaitan School and its employees/ affiliates/other users.
                                                
                                                4.	You warrant and represent that: 

                                                •	You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;
                                                •	The Comments do not infringe any intellectual property right, including without limitation copyright, patent or trademark, or other proprietary right of any third party;
                                                •	The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material or material which is an invasion of privacy
                                                •	The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.
                                                •	You hereby grant to <strong>The Khaitan School</strong> a non-exclusive royalty-free license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.
                                               
                                                <strong>E.	RESERVATION OF RIGHTS</strong>
                                               
                                                We reserve the right at any time and in its sole discretion to request that you remove all links or any particular link to our Web site. You agree to immediately remove all links to our Web site upon such request. 

                                                The Khaitan School is constantly changing and improving the Website. We may also need to alter or discontinue the Website, or any part of it, in order to make performance or security improvements, change functionality and features, make changes to comply with law, or prevent illegal activities on or abuse of our systems or for scheduled maintenance or upgrades, for emergency repairs, or due to failure of telecommunications links and equipment that are beyond the control of the Khaitan School. These changes may affect all users, some users or even an individual user.  Whenever reasonably possible, we will provide notice when we discontinue or make material changes to our Website that will have an adverse impact on the use of our Website. However, you understand and agree that there will be times when we make such changes without notice, such as where we feel we need to take action to improve the security and operability of our Website, prevent abuse, or comply with legal requirements

                                                <strong>F.	REMOVAL OF LINKS FROM OUR WEBSITE</strong>
                                               
                                                If you find any link on our Website or any linked web site objectionable for any reason, you may contact us about this. We will consider requests to remove links but will have no obligation to do so or to respond directly to you.
                                                Whilst we endeavour to ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we commit to ensuring that the website remains available or that the material on the website is kept up to date.
                                               
                                                <strong>G.	THIRD-PARTY LINKS</strong> 
                                               
                                                <span class="light-highlight">The Service may contain links to third-party websites and online services that are not owned or controlled by The Khaitan School. We have no control over, and assumes no responsibility for, such websites and online services. Be aware when you leave the Website; we suggest you read the terms and privacy policy of each third-party website and online service that you visit.</span>

                                                <strong>H.	MODIFYING THIS AGREEMENT</strong>
                                               
                                                The Khaitan School reserves the right to change, modify, revise or otherwise amend any provision of these Terms of Services, and any other terms, policies or guidelines governing your use of the Website, at any time at its sole discretion by providing notice that the Terms of Services have been modified. Such notice may be provided by sending an email, or by posting a notice on the Website, or by posting the revised Terms of Services on the Website and revising the date at the top of these Terms of Services or by such other form of notice as determined by us. Your use of the Website or your purchase of new Services following the posting of the revised Terms of Services or other notice will constitute your acceptance of such changes or modifications. 
                                                
                                                <strong>I.	SEVERANCE</strong>
                                                
                                                If it turns out that a particular term of this Agreement is not enforceable for any reason, this will not affect any other terms.

                                                <strong>J.	GOVERNING LAW</strong>
                                                
                                                1.	This Agreement is governed by, and shall be construed in accordance with, the laws of India. The Parties submit to the exclusive jurisdiction of the courts at <mark>New Delhi</mark> with respect to any claim, dispute, or matter arising out of or relating to this Agreement.

                                                2.	Any dispute or differences shall be adjudicated through Arbitration in accordance with Arbitration and Conciliation Act, 1996. A Sole Arbitrator shall be appointed jointly by the Parties within 30 (thirty) days of either Party requesting the other to suggest or approve a Sole Arbitrator. If the Parties fail to agree on a Sole Arbitrator within the 30 (thirty) days period abovementioned then any of the Parties may request the relevant court in accordance with the Arbitration and Conciliation Act, 1996 to appoint the arbitrator. The seat and venue of arbitration shall be at New Delhi.  

                                                <strong>K.	SURVIVAL</strong>
                                                
                                                The terms and conditions of these Terms of Services which by their nature are intended to survive termination or expiration of Services (including, but not limited to, Indemnification, Warranty Disclaimer, Dispute Resolution and the Limitation of Liability) will survive any expiration or termination of these Terms of Services.

                                                <strong>L.	LIMITATION OF LIABILITY
                                                
                                                IN NO EVENT WILL THE KHAITAN SCHOOL OR ITS DIRECTORS, MEMBERS, EMPLOYEES OR AGENTS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY OTHER DAMAGES OF ANY KIND, INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, LOSS OF PROFITS OR LOSS OF DATA, WHETHER IN AN ACTION IN CONTRACT, TORT OR OTHERWISE, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF OR INABILITY TO USE OR VIEW THE WEBSITE, THE SERVICES, , THE CONTENT OR THE MATERIALS CONTAINED IN OR ACCESSED THROUGH THE SERVICES, INCLUDING ANY DAMAGES CAUSED BY OR RESULTING FROM YOUR RELIANCE ON ANY INFORMATION OBTAINED FROM THE KHAITAN SCHOOL’S WEBSITE OR THAT RESULT FROM MISTAKES, OMISSIONS, INTERRUPTIONS, DELETION OF FILES OR EMAIL, ERRORS, DEFECTS, VIRUSES, DELAYS IN OPERATION OR TRANSMISSION OR ANY TERMINATION, SUSPENSION OR OTHER FAILURE OF PERFORMANCE, WHETHER OR NOT RESULTING FROM ACTS OF GOD, COMMUNICATIONS FAILURE, THEFT, DESTRUCTION OR UNAUTHORIZED ACCESS TO THE KHAITAN SCHOOL’S RECORDS, PROGRAMS OR SERVICES.
                                                IN NO EVENT WILL THE AGGREGATE LIABILITY OF THE KHAITAN SCHOOL WHETHER IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE, WHETHER ACTIVE, PASSIVE OR IMPUTED), PRODUCT LIABILITY, STRICT LIABILITY OR OTHER THEORY, ARISING OUT OF OR RELATING TO THE USE OF OR INABILITY TO USE THE WEBSITE, THE SERVICES, THE CONTENT OR THE MATERIALS, EXCEED COMPENSATION YOU PAY, IF ANY, TO ___ THE KHAITAN SCHOOL FOR ACCESS TO OR USE OF THE WEBSITE OR THE SERVICES OR FOR THE PURCHASE OF PRODUCTS. CERTAIN STATE LAWS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF THESE LAWS APPLY TO YOU, SOME OR ALL OF THE ABOVE DISCLAIMERS, EXCLUSIONS, OR LIMITATIONS MAY NOT APPLY TO YOU, AND YOU MIGHT HAVE ADDITIONAL RIGHTS.</strong>
                                                
                                                <strong>M.	WARRANTY DISCLAIMER

                                                OTHER THAN AS EXPRESSLY STATED IN THIS AGREEMENT OR AS REQUIRED BY LAW, THE SERVICES ARE PROVIDED “AS IS” AND THE KHAITAN SCHOOL DOES NOT MAKE ANY SPECIFIC COMMITMENTS OR WARRANTIES ABOUT THE SERVICES.
                                                THE KHAITAN SCHOOL DOES NOT REPRESENT OR WARRANT THAT THE MATERIALS OR THE SERVICES ARE ACCURATE, COMPLETE, RELIABLE, CURRENT OR ERROR-FREE OR THAT THE WEBSITE, ITS SERVERS OR EMAIL SENT FROM THE KHAITAN SCHOOL OR THE WEBSITE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. THE KHAITAN SCHOOL IS NOT RESPONSIBLE FOR TYPOGRAPHICAL ERRORS OR OMISSIONS RELATING TO TEXT, PHOTOS OR VIDEOS OR CONTENT. THE KHAITAN SCHOOL ALSO MAKES NO REPRESENTATION OR WARRANTY REGARDING THE AVAILABILITY, RELIABILITY OR SECURITY OF THE WEBSITE AND WILL NOT BE LIABLE FOR ANY UNAUTHORIZED ACCESS TO OR ANY MODIFICATION, SUSPENSION, UNAVAILABILITY, OR DISCONTINUANCE OF THE WEBSITE.</strong> 

                                                <strong>N.	CONTACT INFORMATION</strong>
                                                
                                                <span class="light-highlight">If you have any queries regarding any of our terms, please contact us at info@thekhaitanschool.org</span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <div id="privacy_modal" class="privacy_modal popup">
                                        <div class="privacy_modal_inner">
                                            <div class="privacy_modal_content">
                                                <span class="privacy-modal-close close2 close">×</span>
                                                <h3>PRIVACY POLICY</h3>
                                                <div>Your privacy is important to us. <mark>The Khaitan School</mark> is committed to the privacy of its Users.

                                                It is <mark>The Khaitan School's</mark> policy to respect your privacy regarding any information we may collect from you while operating our Website. This Privacy Policy applies to thekhaitanschool.org/ Website <strong>(hereinafter, “Website” "us", "we", or "thekhaitanschool.org/").</strong> We respect your privacy and are committed to protect personally identifiable information you may provide to us through the Website. 

                                                The Privacy Policy sets forth our policies regarding the collection, use and protection of the personal information, data, material and content provided by you while operating our Website. Personal information means information that can be linked to a specific individual, such as name, address, telephone number, e-mail address. Content includes photos, videos, text, music, messages, information, comments, labels, tags, descriptions, categorizations, queries or other content or materials uploaded/posted by you. This Privacy Policy explains how we collects, uses and discloses information from and about you when you use our website and related services (collectively, the <strong>"Services"</strong>).

                                                We have adopted this privacy policy <strong>("Privacy Policy")</strong> to explain what information may be collected on our Website, how we use this information, and under what circumstances we may disclose the information to third parties. This Privacy Policy applies only to information we collect through the Website and does not apply to our collection of information from other sources.

                                                We encourage you to review our Privacy Policy, and become familiar with it, but you should know that we do not sell or rent our User’s personal information to third parties. This Privacy Policy, together with the Terms of service posted on our Website, set forth the general rules and policies governing your use of our Website. Depending on your activities when visiting our Website, you may be required to agree to additional terms of service.

                                                <strong>1. Website Visitors</strong>

                                                <strong>a)	Information we collect from you</strong>
                                                Like most website operators, The Khaitan School collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. The Khaitan School's purpose in collecting non-personally identifying information is to better understand how The Khaitan School's visitors use its website. From time to time, The Khaitan School may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.

                                                <strong>b)	Information we automatically collect from you</strong>

                                                The Khaitan School may collect certain information automatically from your device which may include, but is not limited to, potentially personally-identifying information like Internet Protocol (IP) addresses for logged in users and for users leaving comments on https://thekhaitanschool.org/ blog posts, device type, unique device identification numbers, browser-type (such as Firefox, Safari, or Internet Explorer), your Internet Service Provider (ISP), your operating system and carrier. For Website users, details of any referring website or exit pages as well as broad geographic location (e.g., country or city-level location) may also be collected. The Khaitan School only discloses logged in user and commenter IP addresses under the same circumstances that it uses and discloses personally-identifying information as described below.

                                                Collecting this information enables us to better understand the visitors who come to our website, where they come from, and what content on our website is of interest to them. We use this information for our internal analytics purposes and to improve the quality and relevance of our Services for our visitors. We may also use this automatic information to prevent and detect fraud. Some of this information may be collected using cookies and similar tracking technology.

                                                <strong>2. Personally-Identifying Information</strong>

                                                Certain visitors to The Khaitan School's Website choose to interact with The Khaitan School in ways that require us to gather personally-identifying information. The amount and type of information that The Khaitan School gathers depend on the nature of the interaction. For example, we ask visitors who leave a comment at https://thekhaitanschool.org/ to provide a username and email address.

                                                <strong>3. Security</strong>

                                                The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.

                                                <strong>4. Advertisements</strong>

                                                Ads appearing for our Website may be delivered to users by various advertising partners, who may set cookies. These cookies allow the ad server to recognise your computer each time they send you an online advertisement to compile information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most relevant to our users. This Privacy Policy covers the use of cookies by The Khaitan School and does not cover the use of cookies by any advertisers.

                                                <strong>5. Links To External Sites</strong>

                                                Our Service may contain links to external sites that are not operated by us. If you click on a third party link, you will be directed to that third party's website. We strongly advise you to review the privacy policy and terms of service of every such website you visit.

                                                We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites, products or services.

                                                The Khaitan School uses Google AdWords for remarketing. The Khaitan School uses the remarketing services to advertise on third party websites (including Google) to previous visitors to our site. It could mean that we advertise to previous visitors who haven't completed a task on our site, for example using the contact form to make an enquiry. This could be in the form of an advertisement on the Google search results page, or a site in the Google Display Network. Third-party vendors, including Google, use cookies to serve ads based on someone's past visits. Of course, any data collected will be used in accordance with our own privacy policy and Google's privacy policy.

                                                You can set preferences for how Google advertises to you using the Google Ad Preferences page, and if you want to you can opt out of interest-based advertising entirely by cookie settings or permanently using a browser plugin.

                                                <strong>6. Protection of Certain Personally-Identifying Information</strong>

                                                The Khaitan School discloses potentially personally-identifying and personally-identifying information only to those of its employees, contractors and affiliated organisations that (i) need to know that information in order to process it on The Khaitan School's behalf or to provide services available at The Khaitan School's Website, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organisations may be located outside of your home country; by using The Khaitan School's Website, you consent to the transfer of such information to them. The Khaitan School will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to its employees, contractors and affiliated organisations, as described above, The Khaitan School discloses potentially personally-identifying and personally-identifying information only in response to a subpoena, court order or other governmental request, or when The Khaitan School believes in good faith that disclosure is reasonably necessary to protect the property or rights of - The Khaitan School, third parties or the public at large. We will retain and use your Personal Data to the extent necessary to comply with our legal obligations (for example, if we are required to retain your data to comply with applicable laws), resolve disputes, and enforce our legal agreements and policies. We also may use personal information to create appropriately aggregated or anonymized data, which is not subject to this Privacy Policy, and which we may use or disclose for any purpose

                                                If you are a registered user of https://thekhaitanschool.org/ and have supplied your email address, - The Khaitan School may occasionally send you an email to update you about new features, solicit your feedback, or just keep you up to date with what's going on with - The Khaitan School. We primarily use our blog to communicate this type of information, so we expect to keep this type of email to a minimum. If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. - The Khaitan School takes all measures reasonably necessary to protect against the unauthorized access, use, alteration or destruction of potentially personally-identifying and personally-identifying information.

                                                <strong>7. Aggregated Statistics</strong>

                                                The Khaitan School may collect statistics about the behaviour of visitors to its Website. The Khaitan School may display this information publicly or provide it to others. However, The Khaitan School does not disclose your personally-identifying information. In general, we will use the personal information we collect from you only for the purposes described in this Privacy Policy or for purposes that we explain to you at the time we collect your personal information.

                                                <strong>8. Cookies</strong>

                                                To enrich and perfect your online experience, - The Khaitan School uses "Cookies", similar technologies and services provided by others to display personalised content, appropriate advertising and store your preferences on your computer.

                                                A cookie is a string of information that a website stores on a visitor's computer, and that the visitor's browser provides to the website each time the visitor returns. The Khaitan School uses cookies to help The Khaitan School identify and track visitors, their usage of https://thekhaitanschool.org/, and their Website access preferences. This Website may use cookies for the following general purposes, including but not limited to:

                                                •	To help us recognize you as a previous visitor and save and remember any preferences that may have been set while your browser was visiting our site. 

                                                •	To help us customize the content and advertisements provided to you on this website.

                                                •	To help measure and research the effectiveness of website features and offerings, advertisements, and email communications (by determining which emails you open and act upon).

                                                The Khaitan School visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using The Khaitan School's website’, with the drawback that certain features of The Khaitan School's website may not function properly without the aid of cookies.

                                                By continuing to navigate our Website without changing your cookie settings, you hereby acknowledge and agree to The Khaitan School's use of cookies.

                                                Period of Retention

                                                We will retain your Personal Data only for as long as is necessary for the purposes set out in this Privacy Policy. We will retain and use your Personal Data to the extent necessary to comply with our legal obligations (for example, if we are required to retain your data to comply with applicable laws), resolve disputes, and enforce our legal agreements and policies.

                                                <strong>9. Online Payments or Services</strong>

                                                Those who engage in transactions with The Khaitan School – by purchasing The Khaitan School's services or products, are asked to provide additional information, including as necessary the personal and financial information required to process those transactions. In each case, - The Khaitan School collects such information only insofar as is necessary or appropriate to fulfil the purpose of the visitor's interaction with The Khaitan School. - The Khaitan School does not disclose personally-identifying information other than as described above. And visitors can always refuse to supply personally-identifying information, with the caveat that it may prevent them from engaging in certain website-related activities.

                                                <strong>10. Sharing of Information</strong>

                                                We may share information about you as follows or as otherwise described in this Privacy Policy:

                                                •	With vendors, consultants and other service providers who need access to such information to carry out work on our behalf;

                                                •	In response to a request for information if we believe disclosure is in accordance with any applicable law, regulation or legal process, or as otherwise required by any applicable law, rule or regulation;

                                                •	If we believe your actions are inconsistent with the spirit or language of our user agreements or policies, or to protect the rights, property and safety of The Khaitan School;

                                                •	In connection with, or in anticipation of, any merger, divestiture, consolidation, bankruptcy, sale of company assets, financing or acquisition of all or a portion of our business to another company, or other significant corporate event (in which case the acquiring entity may use the information pursuant to its own privacy policies and procedures, to the extent legally permissible); and

                                                •	With your consent or at your direction, including if we notify you through our Services that the information you provide will be shared in a particular manner and you provide such information.

                                                We may also share aggregated or anonymized information that does not directly identify you. Our sharing of information is governed by our Terms of Service with the user who posted it.

                                                <strong>11. Privacy Policy Changes</strong>

                                                Although most changes are likely to be minor, The Khaitan School may change its Privacy Policy from time to time, and in The Khaitan School's sole discretion. The Khaitan School encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.

                                                <strong>12. Contact Information</strong>

                                                If you have any questions about our Privacy Policy, please contact us via email or phone.

                                                E: info@thekhaitanschool.org

                                                T: +91-120-400-7575

                                                Effective Date: <mark>31-10-2020</mark> The Courts at <mark>New Delhi</mark> in India shall have the exclusive jurisdiction to the exclusion of other courts and applicable law shall be Indian Law for usage of websites / apps including adherence/compliance/ performance of the terms and conditions, thereto.
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                
                            </div>
                            <!--  -->
                        </div>
                    </div>
                    <div class="sw-footer">
                        <div class="text-right form-group">
                            <button class="site-btn sw-btn btn sw-save-btn hide" type="button">Save Draft</button>
                            <button class="site-btn sw-btn btn sw-prev-btn" type="button">Prev</button>
                            <button class="site-btn sw-btn btn sw-submit" type="button">Submit & Pay</button>
                        </div>
                        <button class="site-btn sw-btn btn pay-now" style="visibility:hidden;" type="button">Pay Now</button>
                    </div>
                    </form>
                    <?php /* $fee_amount = 1 * 100; ?>
                    <form action="<?php echo base_url("registration/thankyou"); ?>" method="POST">
                        <script
                            src="https://checkout.razorpay.com/v1/checkout.js"
                            data-key="<?php echo 'rzp_test_4rLKk5OOulhJ3Z'; ?>" 
                            data-amount="<?php echo $fee_amount;?>" 
                            data-currency="INR"
                            data-id="<?php echo 'OID'.rand(10,100).'END';?>"
                            data-buttontext="Pay with Razorpay"
                            data-name="KOSMOS"
                            data-description="Registration Fees"
                            data-image="https://dev.kosmos.thekhaitanschool.org/uploads/school_content/admin_logo/1.jpg"
                            data-prefill.name="<?php echo $enquiry_data['father_firstname'].' '.$enquiry_data['father_lastname'];?>"
                            data-prefill.email="<?php echo $enquiry_data['father_email'];?>"
                            data-prefill.contact="<?php echo $enquiry_data['father_mobile'];?>"
                            data-theme.color="#F37254"
                        ></script>
                        <input type="hidden" custom="Hidden Element" name="enquiry_id" value="<?php echo $enquiry_data["id"] ?>">
                        <input type="hidden" custom="Hidden Element" name="fee_amount" value="<?php echo $fee_amount; ?>">
                    </form>
                    <?php */?>
                    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
                    <script>
                        var thankyouUrl = '<?php echo base_url("registration/thankyou"); ?>';
                        var insertRegistrationUrl = '<?php echo base_url("registration/insertRegistrationFee"); ?>';            
                        var enquiry_id = "";
                        var registrationAmount = (500*100); 
                        var razorpay_options = {

                        "key": "<?php echo $this->config->item('razorpay_key');?>",
                        "amount": registrationAmount, // 2000 paise = INR 20
                        "description": "Registration Fees",
                        "image": "<?php echo base_url("uploads/school_content/admin_logo/1.jpg");?>",
                        "handler": function (response){
                                console.log(response);
                                $('.loader-wrap').show();
                                $.ajax({
                                url: insertRegistrationUrl,
                                type: 'post',
                                dataType: 'json',
                                data: {
                                    razorpay_payment_id: response.razorpay_payment_id , 
                                    fee_amount : registrationAmount ,enquiry_id : enquiry_id, type : 1,
                                }, 
                                success: function (msg) {
                                    $('.loader-wrap').hide();
                                    //window.location.href = SITEURL + 'thank-you';
                                    window.location = thankyouUrl+"?key="+response.razorpay_payment_id;
                                }
                            });
                            
                        },
                        "prefill": {
                            "contact": '',
                            "email":   '',
                            "name": ''
                        },
                        "theme": {
                            "color": "#528FF0"
                        }
                        };
                        $('body').on('click', '.pay-now', function(e){
                        var totalAmount = $(this).attr("data-amount");
                        var product_id =  $(this).attr("data-id");
                        
                        var rzp1 = new Razorpay(razorpay_options);
                        rzp1.open();
                        e.preventDefault();
                        });
                    </script>

                </div>
            
        </div>
    </div>
    <div class="loader-wrap"><img src="../../uploads/admin_images/loader.gif" /></div>
</div>
<?php } ?>
<?php if($_GET['key'] != ''){?>
<script type="text/javascript">    
    $(document).ready(function(){ 
       $('.update-form a').attr('href','/registration/?key=<?php echo $_GET['key'];?>');
       $('.view-form a').attr('href','/registration/details?key=<?php echo $_GET['key'];?>');
    });  
</script>
<?php } else {?>
    <script type="text/javascript">    
        $(document).ready(function(){ 
           $('.update-form,.view-form').hide();
        });  
    </script>
<?php } ?>

<script>
    $(document).ready(function () {
        $('.dob').datepicker({
            format: 'dd-mm-yyyy'
        });

$(document).on("change", "#country_id", function(e) {
            e.preventDefault();
            _this = $(this);
            var country_id = _this.val();
            if (_this.val() != '') {
                $.ajax({
                    url: '<?php echo site_url('registration/getstates'); ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {country_id: _this.val()},
                    success: function(data) {
                       // console.log(data);
                       var form_id=_this.closest('form').attr("id");
                        $('#'+form_id+' #state_id').empty().append(data);                        
                    }
                });
            }
        });
});
</script>