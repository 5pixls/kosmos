<style>
    .navbar-ul{ display: none;}
</style>
<?php
    //echo "<pre>"; 
    $details = $details[0];
    if(empty($details)){ ?>
        <div class="linkexpired">Payment URL is invalid or Expired.</div>
<?php }else{ ?>

<div class="row">
    <div class="col-md-12">
    <div class="col-md-8">
    </div>
    <div class="col-md-4" style="float: right;">
        <label class=""><strong>Admission Fee:</strong> Rs.<?php echo ($details["fee_amount"]/100) ?></label>
        <button class="pay-now btn btn-info pull-right" style="margin-top: -12px;" ><strong>Pay</strong></button>                 
    </div>
    </div>
</div>
<div class="site-wizard view-details">
    <ul class="sw-steps">
        <li><span>Registration Details</span></li>
    </ul>
    <div class="sw-body">
        <?php if($enquiry_data['enquiryid']) { ?>
        <label><strong>Enquiry ID:</strong> <?php echo $enquiry_data['enquiryid']; ?></label>
    <?php }?>
        <div class="sw-steps-body">
                <div class="active">
                    <div class="tshadow mb25 bozero sw-content">
                                    <h4 class="pagetitleh2">Child Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>First Name:</strong> <?php echo $enquiry_data['name']; ?></label>
                                        <label class="col-sm-3"><strong>Last Name:</strong> <?php echo $enquiry_data['last_name']; ?></label>
                                        <label class="col-sm-3"><strong>Gender:</strong> <?php echo $gender_list[$enquiry_data['gender']];?>
                                       </label>
                                        <label class="col-sm-3"><strong>Date of Birth:</strong> <?php echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($enquiry_data['dob']));
                                         ?></label>
                                        <label class="col-sm-3"><strong>Session:</strong> <?php 
                                        foreach ($session_list as $value) {
                                                     if ($value['id']== $enquiry_data['session_id']) echo $value['session']; 
                                        }?>
                                        </label>
                                        <label class="col-sm-3"><strong>Class:</strong> <?php 
                                        foreach ($class_list as $value) {
                                                     if ($value['id']== $enquiry_data['class']) echo $value['class']; 
                                        }?>
                                        </label>
                                        <label class="col-sm-3"><strong>Nationality:</strong> <?php echo $nationality_list[$enquiry_data['nationality']]; ?></label>
                                        <label class="col-sm-3"><strong>Blood Group:</strong> <?php echo $enquiry_data['blood_group']; ?></label>
                                        <label class="col-sm-3"><strong>Child Image:</strong> <?php if($enquiry_data['child_image'] !='') echo "<a href='".base_url().$enquiry_data['child_image']."' target='_blank'><img src='".base_url().$enquiry_data['child_image']."' width='20px'></a>"; ?></label>
                                        <label class="col-sm-3"><strong>Mother Tongue:</strong> <?php echo $enquiry_data['mother_tongue']; ?></label>
                                        <label class="col-sm-3"><strong>Home Town:</strong> <?php echo $enquiry_data['home_town']; ?></label>
                                        <label class="col-sm-3"><strong>Locality:</strong> <?php echo $enquiry_data['locality']; ?></label>
                                        <label class="col-sm-3"><strong>Religion:</strong> <?php echo $enquiry_data['religion']; ?></label>
                                        <label class="col-sm-3"><strong>Social Category:</strong> <?php 
                                        foreach ($categorylist as $value) {
                                                     if ($value['id']== $enquiry_data['social_category']) echo $value['category']; 
                                        }?>
                                        </label>
                                        <label class="col-sm-3"><strong>Child's Aadhaar Card No.:</strong> <?php echo $enquiry_data['child_aadhaar_no']; ?></label>
                                        <label class="col-sm-3"><strong>Previous Result:</strong> <?php if($enquiry_data['previous_result'] !='') echo "<a href='".base_url().$enquiry_data['previous_result']."' target='_blank'>View File</a>"; ?></label>
                                        </div>
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                    <h4 class="pagetitleh2">Previous School Information</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Last School :</strong> <?php echo $enquiry_data['last_school']; ?></label>
                                            <label class="col-sm-3"><strong>Medium of Instruction:</strong> <?php echo $enquiry_data['medium_instruction']; ?></label>
                                            <label class="col-sm-3"><strong>Board:</strong> <?php echo $enquiry_data['previous_board']; ?></label>
                                            <label class="col-sm-3"><strong>Class:</strong> <?php echo $enquiry_data['previous_class']; ?></label>
                                        </div>
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                    <h4 class="pagetitleh2">Father Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Title:</strong> <?php echo $enquiry_data['father_title']; ?></label>
                                            <label class="col-sm-3"><strong>First Name:</strong> <?php echo $enquiry_data['father_firstname']; ?></label>
                                            <label class="col-sm-3"><strong>Last Name:</strong> <?php echo $enquiry_data['father_lastname']; ?></label>
                                            <label class="col-sm-3"><strong>Email:</strong> <?php echo $enquiry_data['father_email']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Mobile:</strong> <?php echo $enquiry_data['father_mobile']; ?></label>
                                            <label class="col-sm-3"><strong>Aadhar Card No.:</strong> <?php echo $enquiry_data['father_aadhar_no']; ?></label>
                                            <label class="col-sm-3"><strong>Qualification:</strong> <?php if($enquiry_data['father_qualification']=='Other') {echo $enquiry_data['father_qualification_other'];} else echo $enquiry_data['father_qualification']; ?></label>
                                            <label class="col-sm-3"><strong>Occupation:</strong> <?php if($enquiry_data['father_occupation']=='Other') {echo $enquiry_data['father_occupation_other'];} else echo $enquiry_data['father_occupation']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Employer:</strong> <?php echo $enquiry_data['father_employer']; ?></label>
                                            <label class="col-sm-3"><strong>Designation:</strong> <?php if($enquiry_data['father_designation']=='Other') {echo $enquiry_data['father_designation_other'];} else echo $enquiry_data['father_designation']; ?></label>
                                            <label class="col-sm-3"><strong>Work Address:</strong> <?php echo $enquiry_data['father_address']; ?></label>
                                            <label class="col-sm-3"><strong>Work City:</strong> <?php if($enquiry_data['father_city']=='Other') {echo $enquiry_data['father_city_other'];} else echo $enquiry_data['father_city']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Work Pin Code:</strong> <?php echo $enquiry_data['father_pincode']; ?></label>
                                            <label class="col-sm-3"><strong>Work phone:</strong> <?php echo $enquiry_data['father_workphone']; ?></label>
                                            <label class="col-sm-3"><strong>Annual Income:</strong> <?php echo $enquiry_data['father_annualincome']; ?></label>
                                            <label class="col-sm-3"><strong>Working wih TKS Noida:</strong> <?php echo $enquiry_data['father_woking_tks']; ?></label>  
                                        </div>
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                    <h4 class="pagetitleh2">Mother Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Title:</strong> <?php echo $enquiry_data['mother_title']; ?></label>
                                            <label class="col-sm-3"><strong>First Name:</strong> <?php echo $enquiry_data['mother_firstname']; ?></label>
                                            <label class="col-sm-3"><strong>Last Name:</strong> <?php echo $enquiry_data['mother_lastname']; ?></label>
                                            <label class="col-sm-3"><strong>Email:</strong> <?php echo $enquiry_data['mother_email']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Mobile:</strong> <?php echo $enquiry_data['mother_mobile']; ?></label>
                                            <label class="col-sm-3"><strong>Aadhar Card No.:</strong> <?php echo $enquiry_data['mother_aadhar_no']; ?></label>
                                            <label class="col-sm-3"><strong>Qualification:</strong> <?php echo $enquiry_data['mother_qualification']; ?></label>
                                            <label class="col-sm-3"><strong>Occupation:</strong> <?php if($enquiry_data['mother_occupation']=='Other') {echo $enquiry_data['mother_occupation_other'];} else echo $enquiry_data['mother_occupation']; ?></label>
                                          </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Employer:</strong> <?php echo $enquiry_data['mother_employer']; ?></label>
                                            <label class="col-sm-3"><strong>Designation:</strong> <?php if($enquiry_data['mother_designation']=='Other') {echo $enquiry_data['mother_designation_other'];} else echo $enquiry_data['mother_designation']; ?></label>
                                            <label class="col-sm-3"><strong>Work Address:</strong> <?php echo $enquiry_data['mother_address']; ?></label>
                                            <label class="col-sm-3"><strong>Work City:</strong> <?php if($enquiry_data['mother_city']=='Other') {echo $enquiry_data['mother_city_other'];} else echo $enquiry_data['mother_city']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Work Pin Code:</strong> <?php echo $enquiry_data['mother_pincode']; ?></label>
                                            <label class="col-sm-3"><strong>Work phone:</strong> <?php echo $enquiry_data['mother_workphone']; ?></label>
                                            <label class="col-sm-3"><strong>Annual Income:</strong> <?php echo $enquiry_data['mother_annualincome']; ?></label>
                                            <label class="col-sm-3"><strong>Working wih TKS Noida:</strong> <?php echo $enquiry_data['mother_woking_tks']; ?></label>  
                                        </div>
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                    <h4 class="pagetitleh2">Guardian Details</h4>
                                    <div class="row around10">
                                    <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Title:</strong> <?php echo $enquiry_data['guardian_title']; ?></label>
                                        <label class="col-sm-3"><strong>First Name:</strong> <?php echo $enquiry_data['guardian_name']; ?></label>
                                        <label class="col-sm-3"><strong>Last Name:</strong> <?php echo $enquiry_data['guardian_lastname']; ?></label>
                                        <label class="col-sm-3"><strong>Email:</strong> <?php echo $enquiry_data['guardian_email']; ?></label>
                                     </div>
                                     <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Mobile:</strong> <?php echo $enquiry_data['guardian_mobile']; ?></label>
                                        <label class="col-sm-3"><strong>Aadhar Card No.:</strong> <?php echo $enquiry_data['guardian_aadhar_no']; ?></label>
                                        <label class="col-sm-3"><strong>Qualification:</strong> <?php if($enquiry_data['guardian_qualification']=='Other') {echo $enquiry_data['guardian_qualification_other'];} else echo $enquiry_data['guardian_qualification']; ?></label>
                                        <label class="col-sm-3"><strong>Occupation:</strong> <?php if($enquiry_data['guardian_occupation']=='Other') {echo $enquiry_data['guardian_occupation_other'];} else echo $enquiry_data['guardian_occupation']; ?></label>
                                      </div>
                                     <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Employer:</strong> <?php echo $enquiry_data['guardian_employer']; ?></label>
                                        <label class="col-sm-3"><strong>Designation:</strong> <?php if($enquiry_data['guardian_designation']=='Other') {echo $enquiry_data['guardian_designation_other'];} else echo $enquiry_data['guardian_designation']; ?></label>
                                        <label class="col-sm-3"><strong>Work Address:</strong> <?php echo $enquiry_data['guardian_address']; ?></label>
                                        <label class="col-sm-3"><strong>Work City:</strong> 
                                            <?php if($enquiry_data['guardian_city']=='Other') {echo $enquiry_data['guardian_city_other']; } 
                                            else echo $enquiry_data['guardian_city']; ?></label>
                                     </div>
                                     <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Work Pin Code:</strong> <?php echo $enquiry_data['guardian_pincode']; ?></label>
                                        <label class="col-sm-3"><strong>Work phone:</strong> <?php echo $enquiry_data['guardian_workphone']; ?></label>
                                        <label class="col-sm-3"><strong>Annual Income:</strong> <?php echo $enquiry_data['guardian_annualincome']; ?></label>
                                        <label class="col-sm-3"><strong>Working wih TKS Noida:</strong> <?php echo $enquiry_data['guardian_woking_tks']; ?></label>  
                                        </div>
                                    <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Relation:</strong> <?php echo $enquiry_data['guardian_relation']; ?></label>
                                    </div>

                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                 <h4 class="pagetitleh2">Student Address Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Address1:</strong> <?php echo $enquiry_data['address']; ?></label>
                                            <label class="col-sm-3"><strong>Address2:</strong> <?php echo $enquiry_data['address2']; ?></label>
                                            <label class="col-sm-3"><strong>City:</strong> <?php echo $enquiry_data['city']; ?></label>
                                        
                                            <label class="col-sm-3"><strong>State:</strong> <?php 
                                            foreach ($state_list as $value) {
                                                         if ($value['id']== $enquiry_data['state_id']) echo $value['state']; 
                                            }?></label>
                                        </div>
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Country:</strong> <?php 
                                            foreach ($country_list as $value) {
                                                         if ($value['id']== $enquiry_data['country_id']) echo $value['country']; 
                                            }?>
                                             </label>
                                            <label class="col-sm-3"><strong>Pin Code:</strong> <?php echo $enquiry_data['pincode']; ?></label>
                                            <label class="col-sm-3"><strong>Landline:</strong> <?php echo $enquiry_data['landline']; ?></label>
                                        </div>
                                    
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                    <h4 class="pagetitleh2">Miscellaneous Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Next Follow Up Date:</strong> <?php echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($enquiry_data['follow_up_date']));
                                             ?></label>
                                            <label class="col-sm-3"><strong>Assigned:</strong> <?php foreach ($staffusers as $value) { 
                                                    if ($value['id']== $enquiry_data['assigned']) echo $value['name'].' '.$value['surname'];
                                                 }
                                                ?>
                                                </label>
                                            <label class="col-sm-3"><strong>Reference:</strong><?php foreach ($Reference as $value) { 
                                                    if ($value['reference']== $enquiry_data['reference']) echo $value['reference'];
                                                 }
                                                ?>
                                                 </label>
                                        
                                            <label class="col-sm-3"><strong>Source:</strong> <?php foreach ($source as $value) { 
                                                    if ($value['source']== $enquiry_data['source']) echo $value['source'];
                                                 }
                                                ?></label>
                                          </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Status:</strong><?php foreach ($statuses as $key=>$value) {
                                             if ($key == $enquiry_data['status']) echo $value; 
                                              } ?>
                                           </label>
                                            <label class="col-sm-3"><strong>Remarks:</strong> <?php echo $enquiry_data['note']; ?></label>
                                          </div>
                                    
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                 <h4 class="pagetitleh2">Questionnaire Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-6"><strong>School Last Attended:</strong> <?php echo $enquiry_data['school_last_attended']; ?></label>
                                            <label class="col-sm-6"><strong>Other Languages Child can Speak / Understand:</strong> <?php echo $enquiry_data['languages_child_can_speak']; ?></label>
                                       </div>
                                           <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>Real Brother / Sister School:</strong> <?php echo $enquiry_data['sibbling_school']; ?></label>
                                            
                                                <label class="col-sm-6"><strong>Real Brother / Sister Class:</strong> <?php echo $enquiry_data['sibbling_class']; ?></label>
                                          </div>
                                         <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>Special Traits:</strong> <?php echo $enquiry_data['special_traits']; ?></label>
                                                <label class="col-sm-6"><strong>Interaction with Peer Group:</strong> <?php echo $enquiry_data['interaction_with_peer_group']; ?></label>
                                         </div>
                                        <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>Whether Nuclear / Joint Family:</strong> <?php echo $enquiry_data['nuclear_joint_family']; ?></label>
                                                <label class="col-sm-6"><strong>What according to you is the definition of a good school?</strong> <?php echo $enquiry_data['definition_good_school']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>State your views on inclusive education.</strong> <?php echo $enquiry_data['inclusive_education']; ?></label>
                                                <label class="col-sm-6"><strong>What values would you like to inculcate in your child?</strong> <?php echo $enquiry_data['inculcate_values']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>Incase both parents are working who takes care of the child?</strong> <?php echo $enquiry_data['takes_care_child']; ?></label>
                                                <label class="col-sm-6"><strong>As a father why do you think it is important to spend time with your child?</strong> <?php echo $enquiry_data['spendtime_importance']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>As a mother, how much time do you spend with your child and how?</strong> <?php echo $enquiry_data['spend_with_your_child']; ?></label>
                                                <label class="col-sm-6"><strong>Mention atleast two interests / hobbies of your child.</strong> <?php echo $enquiry_data['interests_hobbies']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>Write briefly your impression about The Khaitan School.</strong> <?php echo $enquiry_data['logical_answers']; ?></label>
                                                <label class="col-sm-6"><strong>If your child is very inquisitive, do you always believe in providing logical answers to his / her question or avoid them?</strong> <?php echo $enquiry_data['physical_illness']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>Did your child ever suffer from a handicap or ailment where the school needs to be cautious in handling him / her?</strong> <?php echo $enquiry_data['kps_impression']; ?></label>
                                          </div>
                                    
                                    </div>
                            </div>
                </div>
            
        </div>
    </div>
    <div class="loader-wrap"><img src="../../uploads/admin_images/loader.gif" /></div>
</div>

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
    var thankyouUrl = '<?php echo base_url("registration/admissionsuccess"); ?>';
    var insertRegistrationUrl = '<?php echo base_url("registration/insertAdmissionFee"); ?>';            
    var enquiry_id = "";
    var admissionFee = <?php echo $details["fee_amount"];?>; 
    var details = <?php echo json_encode($details);?>;
    var razorpay_options = {

    "key": "<?php echo $this->config->item('razorpay_key');?>",
    "amount": admissionFee, // 2000 paise = INR 20
    "description": "Admission Fees",
    "image": "<?php echo base_url("uploads/school_content/admin_logo/1.jpg");?>",
    "handler": function (response){
            console.log(response);
            $('.loader-wrap').show();
            $.ajax({
            url: insertRegistrationUrl,
            type: 'post',
            dataType: 'json',
            data: {
                razorpay_payment_id: response.razorpay_payment_id , 
                details : details,
            }, 
            success: function (msg) {
                $('.loader-wrap').hide();
                console.log(msg);
                //window.location.href = SITEURL + 'thank-you';
                window.location = thankyouUrl+"?key="+response.razorpay_payment_id;
            }
        });
        
    },
    "prefill": {
        "contact": '',
        "email":   '',
        "name": ''
    },
    "theme": {
        "color": "#528FF0"
    }
    };
    $('body').on('click', '.pay-now', function(e){
    
    var rzp1 = new Razorpay(razorpay_options);
    rzp1.open();
    e.preventDefault();
    });
    var rzp1 = new Razorpay(razorpay_options);
    rzp1.open();
    //e.preventDefault();
</script>

<?php } ?>