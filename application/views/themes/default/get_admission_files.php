<style>
    .navbar-ul{ display: none;}
    #uploadform input {
        font-size: 12px !important;
    }
    #uploadform span {
        color: red;
        font-weight: bold;
    }
</style>
<?php
    //echo "<pre>"; 
    $details = $details[0];
    if(empty($details)){ ?>
        <div class="linkexpired">Payment URL is invalid or Expired.</div>
<?php }else{ ?>
<?php if ($this->session->flashdata('msg')) {?>
    <?php echo $this->session->flashdata('msg') ?>
<?php }?>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-8">
        </div>
        <div class="col-md-4" style="text-align: right;">
            <strong>Admission Fee:</strong> Rs.<?php echo ($details["fee_amount"]/100) ?>
        </div>
    </div>
</div>
<div class="site-wizard view-details">
    <ul class="sw-steps">
        <li><span>Registration Details</span></li>
    </ul>
    <div class="sw-body">
        <?php if($enquiry_data['enquiryid']) { ?>
        <label><strong>Enquiry ID:</strong> <?php echo $enquiry_data['enquiryid']; ?></label>
    <?php }?>
        <div class="sw-steps-body">
                <div class="active">
                    <div class="tshadow mb25 bozero sw-content">
                                    <h4 class="pagetitleh2">Child Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>First Name:</strong> <?php echo $enquiry_data['name']; ?></label>
                                        <label class="col-sm-3"><strong>Last Name:</strong> <?php echo $enquiry_data['last_name']; ?></label>
                                        <label class="col-sm-3"><strong>Gender:</strong> <?php echo $gender_list[$enquiry_data['gender']];?>
                                       </label>
                                        <label class="col-sm-3"><strong>Date of Birth:</strong> <?php echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($enquiry_data['dob']));
                                         ?></label>
                                        <label class="col-sm-3"><strong>Session:</strong> <?php 
                                        foreach ($session_list as $value) {
                                                     if ($value['id']== $enquiry_data['session_id']) echo $value['session']; 
                                        }?>
                                        </label>
                                        <label class="col-sm-3"><strong>Class:</strong> <?php 
                                        foreach ($class_list as $value) {
                                                     if ($value['id']== $enquiry_data['class']) echo $value['class']; 
                                        }?>
                                        </label>
                                        <label class="col-sm-3"><strong>Nationality:</strong> <?php echo $nationality_list[$enquiry_data['nationality']]; ?></label>
                                        <label class="col-sm-3"><strong>Blood Group:</strong> <?php echo $enquiry_data['blood_group']; ?></label>
                                        <label class="col-sm-3"><strong>Child Image:</strong> <?php if($enquiry_data['child_image'] !='') echo "<a href='".base_url().$enquiry_data['child_image']."' target='_blank'><img src='".base_url().$enquiry_data['child_image']."' width='20px'></a>"; ?></label>
                                        <label class="col-sm-3"><strong>Mother Tongue:</strong> <?php echo $enquiry_data['mother_tongue']; ?></label>
                                        <label class="col-sm-3"><strong>Home Town:</strong> <?php echo $enquiry_data['home_town']; ?></label>
                                        <label class="col-sm-3"><strong>Locality:</strong> <?php echo $enquiry_data['locality']; ?></label>
                                        <label class="col-sm-3"><strong>Religion:</strong> <?php echo $enquiry_data['religion']; ?></label>
                                        <label class="col-sm-3"><strong>Social Category:</strong> <?php 
                                        foreach ($categorylist as $value) {
                                                     if ($value['id']== $enquiry_data['social_category']) echo $value['category']; 
                                        }?>
                                        </label>
                                        <label class="col-sm-3"><strong>Child's Aadhaar Card No.:</strong> <?php echo $enquiry_data['child_aadhaar_no']; ?></label>
                                        <label class="col-sm-3"><strong>Previous Result:</strong> <?php if($enquiry_data['previous_result'] !='') echo "<a href='".base_url().$enquiry_data['previous_result']."' target='_blank'>View File</a>"; ?></label>
                                        </div>
                                    </div>
                            </div>
                            <div id='upload_documents_hide_show'>
                                                <form id="uploadform" action="" name="uploadform" method="post" accept-charset="utf-8" enctype="multipart/form-data">

                                                    <div class="row">
                                                        
                                                        <div class="col-md-12">
                                                            <div class="tshadow mb25 bozero sw-content">
                                                                <h4 class="pagetitleh2"><?php echo $this->lang->line('upload_documents'); ?></h4>
<div class="error_alert"></div>
                                                                <div class="row around10">
                                                                    <div class="col-md-6 upload-docs">
                                                                        
                                                                        <table class="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <th style="width: 10%">#</th>
                                                                                    <th style="width: 60%"><?php echo $this->lang->line('title'); ?></th>
                                                                                    <th style="width: 30%"><?php echo $this->lang->line('documents'); ?></th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>1.<span>*</span></td>
                                                                                    <td>
                                                                                        <input class="form-control" type='text' name='first_title' id="first_title" value="Proof of Birth" readonly="readonly">(passport / Aadhar / Birth Cert.)
                                                                                </td>
                                                                                <td>
                                                                                        <input class="filestyle form-control" type='file' name='first_doc' id="first_doc" >
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>2.<span>*</span></td>
                                                                                    <td>
                                                                                        <input class="form-control" type='text' name='second_title' id="second_title" value="Child's Passport Photo" readonly="readonly">
                                                                                        </td>
                                                                                    <td>
                                                                                        <input class="filestyle form-control" type='file' name='second_doc' id="second_doc" >
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3.<span>*</span></td>
                                                                                    <td>
                                                                                        <input class="form-control" type='text' name='third_title' id="third_title" value="Transportation Form" readonly="readonly">
                                                                                        </td>
                                                                                    <td>
                                                                                        <input class="filestyle form-control" type='file' name='third_doc' id="third_doc" >
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>4.</td>
                                                                                    <td>
                                                                                        <input class="form-control" type='text' name='fourth_title' id="fourth_title" value="Father Aadhar" readonly="readonly"></td>
                                                                                    <td>
                                                                                        <input class="filestyle form-control" type='file' name='fourth_doc' id="fourth_doc" >
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>5.</td>
                                                                                    <td>
                                                                                        <input class="form-control" type='text' name='fifth_title' id="fifth_title" value="Mother Aadhar" readonly="readonly">
                                                                                        </td>
                                                                                    <td>
                                                                                        <input class="filestyle form-control" type='file' name='fifth_doc' id="fifth_doc" >
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>6.</td>
                                                                                    <td>
                                                                                        <input class="form-control" type='text' name='sixth_title' id="sixth_title" value="Child Aadhar" readonly="readonly">
                                                                                        </td>
                                                                                    <td>
                                                                                        <input class="filestyle form-control" type='file' name='sixth_doc' id="sixth_doc" >
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                    </div>
                                                                    <div class="col-md-6 upload-docs">
                                                                        <table class="table">
                                                                            <tbody><tr>
                                                                                    <th style="width: 10%">#</th>
                                                                                    <th style="width: 45%"><?php echo $this->lang->line('title'); ?></th>
                                                                                    <th style="width: 45%"><?php echo $this->lang->line('documents'); ?></th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>7.</td>
                                                                                    <td>
                                                                                        <input class="form-control" type='text' name='seventh_title' id="seventh_title" value="Transfer Certificate" readonly="readonly"><br>
                                                 
                                                                                    </td>
                                                                                    <td>
                                                                                        <input class="filestyle form-control" type='file' name='seventh_doc' id="seventh_doc" >
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>8.</td>
                                                                                    <td>
                                                                                        <input class="form-control" type='text' name='eigth_title' id="eigth_title" value="Report Card" readonly="readonly">
                                                                                        </td>
                                                                                    <td>
                                                                                        <input class="filestyle form-control" type='file' name='eigth_doc' id="eigth_doc" >
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>9.</td>
                                                                                    <td>
                                                                                        <input class="form-control" type='text' name='ninth_title' id="ninth_title" value="Medical Form" readonly="readonly"></td>
                                                                                    <td>
                                                                                        <input class="filestyle form-control" type='file' name='ninth_doc' id="ninth_doc" >
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>10.</td>
                                                                                    <td>
                                                                                        <input class="form-control" type='text' name='tenth_title' id="tenth_title" value="Vaccination Form" readonly="readonly">
                                                                                        </td>
                                                                                    <td>
                                                                                        <input class="filestyle form-control" type='file' name='tenth_doc' id="tenth_doc" >
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>11.</td>
                                                                                    <td>
                                                                                        <input class="form-control" type='text' name='eleventh_title' id="eleventh_title" value="Group Photo" readonly="readonly">
                                                                                        </td>
                                                                                    <td>
                                                                                        <input class="filestyle form-control" type='file' name='eleventh_doc' id="eleventh_doc" >
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>12.</td>
                                                                                    <td>
                                                                                        <input class="form-control" type='text' name='twelveth_title' id="twelveth_title" value="Misc" readonly="readonly">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input class="filestyle form-control" type='file' name='twelveth_doc' id="twelveth_doc" >
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                        <div class="col-md-8">
                                                        </div>
                                                        <div class="col-md-4" style="float: right;">
                                                            <a onclick="upload_documents(<?php echo $enquiry_data['id'] ?>)"  class="next-btn btn btn-info pull-right">Continue & Pay</a> 
                                                        </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                            <!--<div class="tshadow mb25 bozero sw-content">
                                    <h4 class="pagetitleh2">Previous School Information</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Last School :</strong> <?php echo $enquiry_data['last_school']; ?></label>
                                            <label class="col-sm-3"><strong>Medium of Instruction:</strong> <?php echo $enquiry_data['medium_instruction']; ?></label>
                                            <label class="col-sm-3"><strong>Board:</strong> <?php echo $enquiry_data['previous_board']; ?></label>
                                            <label class="col-sm-3"><strong>Class:</strong> <?php echo $enquiry_data['previous_class']; ?></label>
                                        </div>
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                    <h4 class="pagetitleh2">Father Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Title:</strong> <?php echo $enquiry_data['father_title']; ?></label>
                                            <label class="col-sm-3"><strong>First Name:</strong> <?php echo $enquiry_data['father_firstname']; ?></label>
                                            <label class="col-sm-3"><strong>Last Name:</strong> <?php echo $enquiry_data['father_lastname']; ?></label>
                                            <label class="col-sm-3"><strong>Email:</strong> <?php echo $enquiry_data['father_email']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Mobile:</strong> <?php echo $enquiry_data['father_mobile']; ?></label>
                                            <label class="col-sm-3"><strong>Aadhar Card No.:</strong> <?php echo $enquiry_data['father_aadhar_no']; ?></label>
                                            <label class="col-sm-3"><strong>Qualification:</strong> <?php if($enquiry_data['father_qualification']=='Other') {echo $enquiry_data['father_qualification_other'];} else echo $enquiry_data['father_qualification']; ?></label>
                                            <label class="col-sm-3"><strong>Occupation:</strong> <?php if($enquiry_data['father_occupation']=='Other') {echo $enquiry_data['father_occupation_other'];} else echo $enquiry_data['father_occupation']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Employer:</strong> <?php echo $enquiry_data['father_employer']; ?></label>
                                            <label class="col-sm-3"><strong>Designation:</strong> <?php if($enquiry_data['father_designation']=='Other') {echo $enquiry_data['father_designation_other'];} else echo $enquiry_data['father_designation']; ?></label>
                                            <label class="col-sm-3"><strong>Work Address:</strong> <?php echo $enquiry_data['father_address']; ?></label>
                                            <label class="col-sm-3"><strong>Work City:</strong> <?php if($enquiry_data['father_city']=='Other') {echo $enquiry_data['father_city_other'];} else echo $enquiry_data['father_city']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Work Pin Code:</strong> <?php echo $enquiry_data['father_pincode']; ?></label>
                                            <label class="col-sm-3"><strong>Work phone:</strong> <?php echo $enquiry_data['father_workphone']; ?></label>
                                            <label class="col-sm-3"><strong>Annual Income:</strong> <?php echo $enquiry_data['father_annualincome']; ?></label>
                                            <label class="col-sm-3"><strong>Working wih TKS Noida:</strong> <?php echo $enquiry_data['father_woking_tks']; ?></label>  
                                        </div>
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                    <h4 class="pagetitleh2">Mother Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Title:</strong> <?php echo $enquiry_data['mother_title']; ?></label>
                                            <label class="col-sm-3"><strong>First Name:</strong> <?php echo $enquiry_data['mother_firstname']; ?></label>
                                            <label class="col-sm-3"><strong>Last Name:</strong> <?php echo $enquiry_data['mother_lastname']; ?></label>
                                            <label class="col-sm-3"><strong>Email:</strong> <?php echo $enquiry_data['mother_email']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Mobile:</strong> <?php echo $enquiry_data['mother_mobile']; ?></label>
                                            <label class="col-sm-3"><strong>Aadhar Card No.:</strong> <?php echo $enquiry_data['mother_aadhar_no']; ?></label>
                                            <label class="col-sm-3"><strong>Qualification:</strong> <?php echo $enquiry_data['mother_qualification']; ?></label>
                                            <label class="col-sm-3"><strong>Occupation:</strong> <?php if($enquiry_data['mother_occupation']=='Other') {echo $enquiry_data['mother_occupation_other'];} else echo $enquiry_data['mother_occupation']; ?></label>
                                          </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Employer:</strong> <?php echo $enquiry_data['mother_employer']; ?></label>
                                            <label class="col-sm-3"><strong>Designation:</strong> <?php if($enquiry_data['mother_designation']=='Other') {echo $enquiry_data['mother_designation_other'];} else echo $enquiry_data['mother_designation']; ?></label>
                                            <label class="col-sm-3"><strong>Work Address:</strong> <?php echo $enquiry_data['mother_address']; ?></label>
                                            <label class="col-sm-3"><strong>Work City:</strong> <?php if($enquiry_data['mother_city']=='Other') {echo $enquiry_data['mother_city_other'];} else echo $enquiry_data['mother_city']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Work Pin Code:</strong> <?php echo $enquiry_data['mother_pincode']; ?></label>
                                            <label class="col-sm-3"><strong>Work phone:</strong> <?php echo $enquiry_data['mother_workphone']; ?></label>
                                            <label class="col-sm-3"><strong>Annual Income:</strong> <?php echo $enquiry_data['mother_annualincome']; ?></label>
                                            <label class="col-sm-3"><strong>Working wih TKS Noida:</strong> <?php echo $enquiry_data['mother_woking_tks']; ?></label>  
                                        </div>
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                    <h4 class="pagetitleh2">Guardian Details</h4>
                                    <div class="row around10">
                                    <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Title:</strong> <?php echo $enquiry_data['guardian_title']; ?></label>
                                        <label class="col-sm-3"><strong>First Name:</strong> <?php echo $enquiry_data['guardian_name']; ?></label>
                                        <label class="col-sm-3"><strong>Last Name:</strong> <?php echo $enquiry_data['guardian_lastname']; ?></label>
                                        <label class="col-sm-3"><strong>Email:</strong> <?php echo $enquiry_data['guardian_email']; ?></label>
                                     </div>
                                     <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Mobile:</strong> <?php echo $enquiry_data['guardian_mobile']; ?></label>
                                        <label class="col-sm-3"><strong>Aadhar Card No.:</strong> <?php echo $enquiry_data['guardian_aadhar_no']; ?></label>
                                        <label class="col-sm-3"><strong>Qualification:</strong> <?php if($enquiry_data['guardian_qualification']=='Other') {echo $enquiry_data['guardian_qualification_other'];} else echo $enquiry_data['guardian_qualification']; ?></label>
                                        <label class="col-sm-3"><strong>Occupation:</strong> <?php if($enquiry_data['guardian_occupation']=='Other') {echo $enquiry_data['guardian_occupation_other'];} else echo $enquiry_data['guardian_occupation']; ?></label>
                                      </div>
                                     <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Employer:</strong> <?php echo $enquiry_data['guardian_employer']; ?></label>
                                        <label class="col-sm-3"><strong>Designation:</strong> <?php if($enquiry_data['guardian_designation']=='Other') {echo $enquiry_data['guardian_designation_other'];} else echo $enquiry_data['guardian_designation']; ?></label>
                                        <label class="col-sm-3"><strong>Work Address:</strong> <?php echo $enquiry_data['guardian_address']; ?></label>
                                        <label class="col-sm-3"><strong>Work City:</strong> 
                                            <?php if($enquiry_data['guardian_city']=='Other') {echo $enquiry_data['guardian_city_other']; } 
                                            else echo $enquiry_data['guardian_city']; ?></label>
                                     </div>
                                     <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Work Pin Code:</strong> <?php echo $enquiry_data['guardian_pincode']; ?></label>
                                        <label class="col-sm-3"><strong>Work phone:</strong> <?php echo $enquiry_data['guardian_workphone']; ?></label>
                                        <label class="col-sm-3"><strong>Annual Income:</strong> <?php echo $enquiry_data['guardian_annualincome']; ?></label>
                                        <label class="col-sm-3"><strong>Working wih TKS Noida:</strong> <?php echo $enquiry_data['guardian_woking_tks']; ?></label>  
                                        </div>
                                    <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Relation:</strong> <?php echo $enquiry_data['guardian_relation']; ?></label>
                                    </div>

                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                 <h4 class="pagetitleh2">Student Address Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Address1:</strong> <?php echo $enquiry_data['address']; ?></label>
                                            <label class="col-sm-3"><strong>Address2:</strong> <?php echo $enquiry_data['address2']; ?></label>
                                            <label class="col-sm-3"><strong>City:</strong> <?php echo $enquiry_data['city']; ?></label>
                                        
                                            <label class="col-sm-3"><strong>State:</strong> <?php 
                                            foreach ($state_list as $value) {
                                                         if ($value['id']== $enquiry_data['state_id']) echo $value['state']; 
                                            }?></label>
                                        </div>
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Country:</strong> <?php 
                                            foreach ($country_list as $value) {
                                                         if ($value['id']== $enquiry_data['country_id']) echo $value['country']; 
                                            }?>
                                             </label>
                                            <label class="col-sm-3"><strong>Pin Code:</strong> <?php echo $enquiry_data['pincode']; ?></label>
                                            <label class="col-sm-3"><strong>Landline:</strong> <?php echo $enquiry_data['landline']; ?></label>
                                        </div>
                                    
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                    <h4 class="pagetitleh2">Miscellaneous Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Next Follow Up Date:</strong> <?php echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($enquiry_data['follow_up_date']));
                                             ?></label>
                                            <label class="col-sm-3"><strong>Assigned:</strong> <?php foreach ($staffusers as $value) { 
                                                    if ($value['id']== $enquiry_data['assigned']) echo $value['name'].' '.$value['surname'];
                                                 }
                                                ?>
                                                </label>
                                            <label class="col-sm-3"><strong>Reference:</strong><?php foreach ($Reference as $value) { 
                                                    if ($value['reference']== $enquiry_data['reference']) echo $value['reference'];
                                                 }
                                                ?>
                                                 </label>
                                        
                                            <label class="col-sm-3"><strong>Source:</strong> <?php foreach ($source as $value) { 
                                                    if ($value['source']== $enquiry_data['source']) echo $value['source'];
                                                 }
                                                ?></label>
                                          </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Status:</strong><?php foreach ($statuses as $key=>$value) {
                                             if ($key == $enquiry_data['status']) echo $value; 
                                              } ?>
                                           </label>
                                            <label class="col-sm-3"><strong>Remarks:</strong> <?php echo $enquiry_data['note']; ?></label>
                                          </div>
                                    
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero sw-content">
                                 <h4 class="pagetitleh2">Questionnaire Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-6"><strong>School Last Attended:</strong> <?php echo $enquiry_data['school_last_attended']; ?></label>
                                            <label class="col-sm-6"><strong>Other Languages Child can Speak / Understand:</strong> <?php echo $enquiry_data['languages_child_can_speak']; ?></label>
                                       </div>
                                           <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>Real Brother / Sister School:</strong> <?php echo $enquiry_data['sibbling_school']; ?></label>
                                            
                                                <label class="col-sm-6"><strong>Real Brother / Sister Class:</strong> <?php echo $enquiry_data['sibbling_class']; ?></label>
                                          </div>
                                         <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>Special Traits:</strong> <?php echo $enquiry_data['special_traits']; ?></label>
                                                <label class="col-sm-6"><strong>Interaction with Peer Group:</strong> <?php echo $enquiry_data['interaction_with_peer_group']; ?></label>
                                         </div>
                                        <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>Whether Nuclear / Joint Family:</strong> <?php echo $enquiry_data['nuclear_joint_family']; ?></label>
                                                <label class="col-sm-6"><strong>What according to you is the definition of a good school?</strong> <?php echo $enquiry_data['definition_good_school']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>State your views on inclusive education.</strong> <?php echo $enquiry_data['inclusive_education']; ?></label>
                                                <label class="col-sm-6"><strong>What values would you like to inculcate in your child?</strong> <?php echo $enquiry_data['inculcate_values']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>Incase both parents are working who takes care of the child?</strong> <?php echo $enquiry_data['takes_care_child']; ?></label>
                                                <label class="col-sm-6"><strong>As a father why do you think it is important to spend time with your child?</strong> <?php echo $enquiry_data['spendtime_importance']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>As a mother, how much time do you spend with your child and how?</strong> <?php echo $enquiry_data['spend_with_your_child']; ?></label>
                                                <label class="col-sm-6"><strong>Mention atleast two interests / hobbies of your child.</strong> <?php echo $enquiry_data['interests_hobbies']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>Write briefly your impression about The Khaitan School.</strong> <?php echo $enquiry_data['logical_answers']; ?></label>
                                                <label class="col-sm-6"><strong>If your child is very inquisitive, do you always believe in providing logical answers to his / her question or avoid them?</strong> <?php echo $enquiry_data['physical_illness']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-6"><strong>Did your child ever suffer from a handicap or ailment where the school needs to be cautious in handling him / her?</strong> <?php echo $enquiry_data['kps_impression']; ?></label>
                                          </div>
                                    
                                    </div>
                            </div>-->
                            
                </div>
            
        </div>
    </div>
    <div class="loader-wrap"><img src="../../uploads/admin_images/loader.gif" /></div>
</div>

<script type="text/javascript">
function upload_documents(id) {
            erroralert('hide', "");
            var form_data = new FormData($("#uploadform")[0]);
           // var ext = $('#first_doc').val().split('.').pop().toLowerCase();
            if ($('#first_doc').val() == '' || $.inArray($('#first_doc').val().split('.').pop().toLowerCase(), ['pdf','gif', 'jpg', 'jpeg', 'png']) == -1) {
                    erroralert('show', "Please select valid file format for Proof of Birth.");
                    return false;
            }
            if ($('#second_doc').val() == '' || $.inArray($('#second_doc').val().split('.').pop().toLowerCase(), ['pdf','gif', 'jpg', 'jpeg', 'png']) == -1) {
                    erroralert('show', "Please select valid file format for Child's Passport Photo.");
                    return false;
            }
            if ($('#third_doc').val() == '' || $.inArray($('#third_doc').val().split('.').pop().toLowerCase(), ['pdf','gif', 'jpg', 'jpeg', 'png']) == -1) {
                    erroralert('show', "Please select valid file format for Transportation Form.");
                    return false;
            }
            if ($('#first_doc').val()!='') form_data.append('first_doc',$('#first_doc').prop('files')[0]);
            if ($('#second_doc').val()!=undefined) form_data.append('second_doc',$('#second_doc').prop('files')[0]);
            if ($('#third_doc').val()!=undefined) form_data.append('third_doc',$('#third_doc').prop('files')[0]);
            if ($('#fourth_doc').val()!=undefined) form_data.append('fourth_doc',$('#fourth_doc').prop('files')[0]);
            if ($('#fifth_doc').val()!=undefined) form_data.append('fifth_doc',$('#fifth_doc').prop('files')[0]);
            if ($('#sixth_doc').val()!=undefined) form_data.append('sixth_doc',$('#sixth_doc').prop('files')[0]);
            if ($('#seventh_doc').val()!=undefined) form_data.append('seventh_doc',$('#seventh_doc').prop('files')[0]);
            if ($('#eigth_doc').val()!=undefined) form_data.append('eigth_doc',$('#eigth_doc').prop('files')[0]);
            if ($('#ninth_doc').val()!=undefined) form_data.append('ninth_doc',$('#ninth_doc').prop('files')[0]);
            if ($('#tenth_doc').val()!=undefined) form_data.append('tenth_doc',$('#tenth_doc').prop('files')[0]);
            if ($('#eleventh_doc').val()!=undefined) form_data.append('eleventh_doc',$('#eleventh_doc').prop('files')[0]);
            if ($('#twelveth_doc').val()!=undefined) form_data.append('twelveth_doc',$('#twelveth_doc').prop('files')[0]);
            form_data.append('id',id);
            $.ajax({
                url: '<?php echo site_url('registration/uploaddocuments') ?>',
                type: 'POST',
                data: form_data,//$("#"+form_id).serialize(),
                dataType: 'json',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false
                success: function (data) {

                    if (data.status == "fail") {

                        var message = "";
                        $.each(data.error, function (index, value) {

                            message += value;
                        });
                        erroralert('show', message);
                    } else {
                        //erroralert('show', data.message);
                        window.location='/registration/payAdmissionFee?key=<?php echo $key;?>';
                    }
                },
                error: function () {
                    alert("Fail")
                }
            });

    }
</script>

<?php } ?>