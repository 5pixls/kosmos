<!DOCTYPE html>
<html dir="<?php echo ($front_setting->is_active_rtl) ? "rtl" : "ltr"; ?>" lang="<?php echo ($front_setting->is_active_rtl) ? "ar" : "en"; ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $page['title']; ?></title>
        <meta name="title" content="<?php echo $page['meta_title']; ?>">
        <meta name="keywords" content="<?php echo $page['meta_keyword']; ?>">
        <meta name="description" content="<?php echo $page['meta_description']; ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="<?php echo base_url($front_setting->fav_icon); ?>" type="image/x-icon">
        <link href="<?php echo $base_assets_url; ?>css/owl.carousel.css" rel="stylesheet">  
        <link href="<?php echo $base_assets_url; ?>css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo $base_assets_url; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_assets_url; ?>css/style_fonts.css" rel="stylesheet">
        <link href="<?php echo $base_assets_url; ?>css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo $base_assets_url; ?>datepicker/bootstrap-datepicker3.css"/>
        <!--file dropify-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/dropify.min.css">

        <script src="<?php echo $base_assets_url; ?>js/jquery.min.js"></script>
             <script type="text/javascript">
            var base_url = "<?php echo base_url() ?>";
        </script>
        <script src="<?php echo base_url(); ?>backend/dist/js/dropify.min.js"></script>
        <?php
        //$this->load->view('layout/theme');

        if ($front_setting->is_active_rtl) {
            ?>
            <link href="<?php echo $base_assets_url; ?>rtl/bootstrap-rtl.min.css" rel="stylesheet">
            <link href="<?php echo $base_assets_url; ?>rtl/style-rtl.css" rel="stylesheet">
            <?php
        }
        ?>
        <?php echo $front_setting->google_analytics; ?>



    </head>
    <body class="front-site">
        <div class="toparea">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <ul class="social">
                            <?php $this->view('/themes/default/social_media'); ?>

                        </ul>
                    </div><!--./col-md-3-->
                    <div class="col-md-6 col-sm-6">
                        <ul class="top-list">
                            <li><a href="mailto:<?php echo $school_setting->email; ?>"><i class="fa fa-envelope-o"></i><?php echo $school_setting->email; ?></a></li>
                            <li><i class="fa fa-phone"></i><?php echo $school_setting->phone; ?></li>
                        </ul>
                    </div><!--./col-md-5-->
                    
                </div>
            </div>
        </div><!--./toparea--> 

        <?php echo $header; ?>

        <?php echo $slider; ?>

        <?php if (isset($featured_image) && $featured_image != "") {
            ?>


            <?php
        }
        ?> 

        <div class="container sidebar-row-wrap">
            <div class="sidebar-wrap" id="sidebar-wrap">
                <div class="collapse-bar navbar-ul-wrap" id="navbar-ul-wrap">
                    <ul class="nav navbar-ul"> <!-- navbar-nav -->
                        <?php
                        foreach ($main_menus as $menu_key => $menu_value) {
                            $submenus = false;
                            $cls_menu_dropdown = "";
                            $menu_selected = "";
                            if ($menu_value['page_slug'] == $active_menu) {
                                $menu_selected = "active";
                            }
                            if (!empty($menu_value['submenus'])) {
                                $submenus = true;
                                $cls_menu_dropdown = "dropdown";
                            }
                            ?>

                            <li class="<?php echo $menu_selected . " " . $cls_menu_dropdown; ?> <?php echo strtolower(str_replace(' ','-',$menu_value['menu'])); ?> left-bar" >
                                <?php
                                if (!$submenus) {
                                    $top_new_tab = '';
                                    $url = '#';

                                    if ($menu_value['open_new_tab']) {
                                        $top_new_tab = "target='_blank'";
                                    }
                                    if ($menu_value['ext_url']) {
                                        $url = $menu_value['ext_url_link'];
                                    } else {
                                        $url = site_url($menu_value['page_url']);
                                    }
                                    ?>

                                    <a href="<?php echo $url; ?>" <?php echo $top_new_tab; ?>><?php echo $menu_value['menu']; ?></a>

                                    <?php
                                } else {
                                    $child_new_tab = '';
                                    $url = '#';
                                    ?>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $menu_value['menu']; ?> <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <?php
                                        foreach ($menu_value['submenus'] as $submenu_key => $submenu_value) {
                                            if ($submenu_value['open_new_tab']) {
                                                $child_new_tab = "target='_blank'";
                                            }
                                            if ($submenu_value['ext_url']) {
                                                $url = $submenu_value['ext_url_link'];
                                            } else {
                                                $url = site_url($submenu_value['page_url']);
                                            }
                                            ?>
                                            <li><a href="<?php echo $url; ?>" <?php echo $child_new_tab; ?> ><?php echo $submenu_value['menu'] ?></a></li>
                                            <?php
                                        }
                                        ?>

                                    </ul>

                                    <?php
                                }
                                ?>


                            </li>
                            <?php
                        }
                        ?>


                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
            <div class="row main-container-row"> 
                <?php
                // $page_colomn = "col-md-12";

                //if ($page_side_bar) {

                    $page_colomn = "col-md-9 col-sm-9 main-inner-container";
                //}
                ?>
                <div class="<?php echo $page_colomn; ?>">
                    <?php echo $content; ?> 
                </div>  
                <?php
                if ($page_side_bar) {
                    ?>   <?php  }?>

                    <div class="col-md-3 right-sidebar-contact">
                        <!--<div class="sidebar">
                            <?php
                       
                       
                            if (in_array('news', json_decode($front_setting->sidebar_options))) {
                                ?>
                                <div class="catetab"><?php echo $this->lang->line('latest_news'); ?></div>
                                <div class="newscontent">
                                    <div class="tickercontainer"><div class="mask"><ul id="ticker01" class="newsticker" style="height: 666px; top: 124.54px;">
                                                <?php
                                                if (!empty($banner_notices)) {
                                                

                                                    foreach ($banner_notices as $banner_notice_key => $banner_notice_value) {
                                                      
                                                        ?>
                                                        <li><a href="<?php echo site_url('read/' . $banner_notice_value['slug']) ?>"><div class="date"><?php echo date('d', strtotime($banner_notice_value['date'])); ?><span><?php $this->lang->line(strtolower(date('F', strtotime($banner_notice_value['date'])))); ?></span></div><?php echo $banner_notice_value['title']; ?>
                                                            </a></li>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <?php
                            }
                            ?>


                            <?php
                            if (in_array('complain', json_decode($front_setting->sidebar_options))) {
                                ?>
                                <div class="complain">
                                    <a href="<?php echo site_url('page/complain') ?>"><i class="fa fa-pencil-square"></i><?php echo $this->lang->line('complain'); ?></a>
                                </div>

                                <?php
                            }
                            ?>


                        </div>./sidebar-->  
                        <div class="sidebar">
                            <div class="side-card" style="display:none;">
                                <h5>Quick links (Suggested)</h5>
                                <div class="sidebar-header">
                                    <a href="" class="sh-link sh-submit-enquiry"><i class="text-info fa fa-edit"></i> Register a Sibling</a>
                                    <!-- <a href="" class="sh-link sh-schedule-tour"><i class="text-warning fa fa-calendar"></i> Schedule a Tour</a> -->
                                    <!-- <a href="" class="sh-link sh-rsvp"><i class="text-success fa fa-file"></i> RSVP for Open Days</a> -->

                                </div>
                            </div>
                            <div class="sidebar-body">
                                <div class="side-card">
                                    <h5>Campus locations</h5>
                                    <div class="map-location">
                                        <a href="https://www.google.com/maps/dir//the+khaitan+school+google+map/data=!4m6!4m5!1m1!4e2!1m2!1m1!1s0x390ce5c0f9e0ef23:0xf1576ae034fa1745?sa=X&ved=2ahUKEwjbnZep26zsAhXFgeYKHUGsAMoQ9RcwDHoECBEQBQ">
                                            <img src="../../uploads/admin_images/schoolMap.png" />
                                        </a>
                                        <span><strong>The Khaitan School</strong></br>
                                        1A/A, Block – F, Sector 40,</br> 
                                        Noida – 201303, U.P.</span>                                        
                                    </div>
                                    <div class="su-content">
                                        <span class="su-links"><i class="fa fa-phone"></i><a href="tel:+91-120-400-7575"> +91-120-400-7575</a> / <a href="tel:+91-120-250-0793">250-0793</a></span>
                                        <span class="su-links"><i class="fa fa-envelope-o"></i><a href="mailto:admissions@thekhaitanschool.org"> admissions@thekhaitanschool.org</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   


            </div><!--./row-->
        </div><!--./container-->  

        <?php echo $footer; ?>
        
        <script src="<?php echo $base_assets_url; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo $base_assets_url; ?>js/owl.carousel.min.js"></script>
         <script type="text/javascript" src="<?php echo $base_assets_url; ?>js/jquery.waypoints.min.js"></script>
        <script type="text/javascript" src="<?php echo $base_assets_url; ?>js/jquery.counterup.min.js"></script>
        <script src="<?php echo $base_assets_url; ?>js/ss-lightbox.js"></script>
        <script src="<?php echo $base_assets_url; ?>js/custom.js"></script>
        <!-- Include Date Range Picker -->
        <script type="text/javascript" src="<?php echo $base_assets_url; ?>datepicker/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript">
            $(function(){
    jQuery('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
    
        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');
    
            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
    
            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
            
            // Check if the viewport is set, else we gonna set it if we can.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }
    
            // Replace image with new SVG
            $img.replaceWith($svg);
    
        }, 'xml');
    
    });
});

        </script>
    </body>
</html>