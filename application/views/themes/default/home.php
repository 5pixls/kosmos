<?php if ($page['description'] != ""): ?>
    <?php echo $page['description']; ?>
<?php endif;?>


<div class="site-wizard">
    <ul class="sw-steps">
        <li role="button" tab-index="0" class="active"><span>Child Details</span></li>
        <li role="button" tab-index="0"><span>Parents Details</span></li>
        <li role="button" tab-index="0"><span>Questionaire</span></li>
        <li role="button" tab-index="0"><span>Acknowledgement</span></li>
    </ul>
    <div class="sw-body">
        <div class="sw-steps-body">
            <form id="wizard-form" action="">
                <div class="sw-steps-content active">
                    <div class="tshadow mb25 bozero sw-content">
                        <!-- <h4 class="pagetitleh2">Child Details</h4> -->
                        <div class="row around10">
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="pwd">First Name</label><small class="req"> *</small>  
                                <input type="text" class="form-control" id="name_value" value="<?php echo set_value('name', $enquiry_data['name']); ?>" name="name">
                                <span class="text-danger" id="name"></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="pwd">Last Name</label><small class="req"> *</small>  
                                <input type="text" class="form-control" id="last_name" value="<?php echo set_value('last_name', $enquiry_data['last_name']); ?>" name="last_name">
                                <span class="text-danger" id="last_name"></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Gender</label> <small class="req"> *</small>
                                <select name="gender" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php $gender_list=$this->config->item('gender_list');
                                    foreach ($gender_list as $key => $value) {
                                        ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('gender', $enquiry_data['gender']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="dob">Date of Birth</label><small class="req"> *</small>
                                <input type="text" id="dob" name="dob" placeholder="dd/mm/yyyy" class="form-control dob" value="<?php
                                if (!empty($enquiry_data['dob'])) {
                                    echo set_value('dob', date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($enquiry_data['dob'])));
                                }
                                ?>" readonly="">
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="pwd"><?php echo $this->lang->line('class'); ?></label> <small class="req"> *</small>
                                <select name="class" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php
                                        foreach ($class_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $value['id'] ?>" <?php if (set_value('class', $enquiry_data['class']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['class'] ?></option>

                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Session</label> <small class="req"> *</small>
                                <select name="session_id" id="session_id" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php
                                        foreach ($session_list as $value) {
                                            ?>

                                        <option value="<?php echo $value['id'] ?>" <?php if (set_value('session_id', $enquiry_data['session_id']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['session'] ?></option>

                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="pwd">Child's Aadhaar Card No.:</label>
                                <input type="text" maxlength="14" class="form-control aadhaar_input" id="child_aadhaar_no" value="<?php echo set_value('child_aadhaar_no', $enquiry_data['child_aadhaar_no']); ?>" name="child_aadhaar_no" placeholder="12 Digit Aadhaar No.">
                                <span class="text-danger" id="child_aadhaar_no"></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Nationality</label><small class="req"> *</small>   
                                <select name="nationality" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php $nationality_list=$this->config->item('nationality_list');
                                    foreach ($nationality_list as $key => $value) {
                                        ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('nationality', $enquiry_data['nationality']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="dob">Blood Group:</label><small class="req"> *</small>  
                                <select name="blood_group" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php $blood_group_list=$this->config->item('blood_group_list');
                                    foreach ($blood_group_list as $key => $value) {
                                        ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('blood_group', $enquiry_data['blood_group']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group result-doc">
                                <label for="pwd">Previous Year Result</label> 
                                <input type="file" id="child_image" class="filestyle form-control dropify" name="child_image" multiple="multiple">
                                                        <span class="text-danger"><?php echo form_error('message'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Mother Tongue</label><small class="req"> *</small>   
                                <select name="mother_tongue" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php $mother_tongue_list=$this->config->item('mother_tongue_list');
                                    foreach ($mother_tongue_list as $key => $value) {
                                        ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('mother_tongue', $enquiry_data['mother_tongue']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="home_town">Home Town</label><small class="req"> *</small>  
                                <input id="home_town" name="home_town" placeholder="Home Town" type="text" class="form-control"  value="<?php echo set_value('home_town', $enquiry_data['home_town']); ?>" />
                                <span class="text-danger"><?php echo form_error('home_town'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="locality">Locality</label><small class="req"> *</small>   
                                <select name="locality" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php $locality_list=$this->config->item('locality_list');
                                    foreach ($locality_list as $key => $value) {
                                        ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('locality', $enquiry_data['locality']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="religion">Religion</label><small class="req"> *</small>  
                                <select name="religion" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $religion_list=$this->config->item('religion_list');
                                        foreach ($religion_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('religion', $enquiry_data['religion']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                        </div>
                        <?php if ($sch_setting->category) { ?>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Social Category</label>
                                    <select  id="social_category" name="social_category" class="form-control" >
                                        <option value=""><?php echo $this->lang->line('select'); ?></option>
                                        <?php foreach ($categorylist as $category) {   ?>
                                        <option value="<?php echo $category['id'] ?>" <?php if ($enquiry_data['social_category'] == $category['id']) {  echo "selected=selected";  } ?>><?php echo $category['category'] ?></option>
                                        <?php $count++; } ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('category_id'); ?></span>
                                </div>
                                </div>
                            <?php } ?>

                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label>School Bus</label>

                                    <select name="school_bus" class="form-control"  >
                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                            ?>
                                            ?>
                                            <?php $school_bus=$this->config->item('true-false');
                                            foreach ($school_bus as $key => $value) {
                                                ?>

                                            <option value="<?php echo $key ?>" <?php if (set_value('school_bus', $enquiry_data['school_bus']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                                            
                        </div>
                    </div>
                    <div class="sw-footer">
                        <div class="text-right form-group">
                            <!-- <button class="site-btn sw-btn btn sw-save-btn" type="button">Save Draft</button> -->
                            <button class="site-btn sw-btn btn sw-next-btn" type="button">Next</button>
                        </div>
                    </div>
                </div>
                <!-- Father details -->
                <div class="sw-steps-content">
                <div class="tshadow mb25 bozero sw-content">
                    <h4 class="pagetitleh2">Father Details</h4>
                    <div class="row around10">
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_title">Title:</label> 
                            <select name="father_title" class="form-control"  >
                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                <?php $father_title_list=$this->config->item('father_title_list');
                                foreach ($father_title_list as $key => $value) {
                                    ?>

                                <option value="<?php echo $key ?>" <?php if (set_value('father_title', $enquiry_data['father_title']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                    <?php
                                }
                                ?>
                        </select>
                        </div>
                    </div>
                    
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_firstname">First Name</label><small class="req"> *</small>  
                            <input id="father_firstname" name="father_firstname" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('father_firstname', $enquiry_data['father_firstname']); ?>" />
                            <span class="text-danger"><?php echo form_error('father_firstname'); ?></span>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_lastname">Last Name</label> 
                            <input id="father_lastname" name="father_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('father_lastname', $enquiry_data['father_lastname']); ?>" />
                            <span class="text-danger"><?php echo form_error('father_lastname'); ?></span>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_email">Email</label> <small class="req"> *</small>
                            <input id="father_email" name="father_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('father_email', $enquiry_data['father_email']); ?>" />
                            <span class="text-danger"><?php echo form_error('father_email'); ?></span>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_mobile">Mobile</label><small class="req"> *</small>   
                            <input id="father_mobile" name="father_mobile" placeholder="Mobile" type="tel" class="form-control form-mobile" maxlength="14"
                             value="<?php echo set_value('father_mobile', $enquiry_data['father_mobile']); ?>" />
                            <span class="text-danger"><?php echo form_error('father_mobile'); ?></span>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Aadhar Card No.</label> 
                            <input maxlength="14" id="father_aadhar_no" name="father_aadhar_no"  placeholder="12 Digit Aadhaar No." type="text" class="form-control aadhaar_input"  value="<?php echo set_value('father_aadhar_no', $enquiry_data['father_aadhar_no']); ?>" />
                            <span class="text-danger"><?php echo form_error('father_aadhar_no'); ?></span>
                        </div>
                    </div>                                       

                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Qualification:</label><small class="req"> *</small>   
                            <select name="father_qualification" class="form-control"  >
                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    ?>
                                    ?>
                                    <?php $fprevious_board_list=$this->config->item('parent_previous_board_list');
                                    foreach ($fprevious_board_list as $key => $value) {
                                        ?>

                                    <option value="<?php echo $key ?>" <?php if (set_value('father_qualification', $enquiry_data['father_qualification']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Occupation:</label> 
                            <select name="father_occupation" class="form-control"  >
                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    ?>
                                    ?>
                                    <?php $father_occupation_list=$this->config->item('parent_occupation_list');
                                    foreach ($father_occupation_list as $key => $value) {
                                        ?>

                                    <option value="<?php echo $key ?>" <?php if (set_value('father_occupation', $enquiry_data['father_occupation']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Employer</label><small class="req"> *</small>  
                            <input id="father_employer" name="father_employer" placeholder="Employer" type="text" class="form-control"  value="<?php echo set_value('father_employer', $enquiry_data['father_employer']); ?>" />
                            <span class="text-danger"><?php echo form_error('father_employer'); ?></span>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">

                            <label for="email">Designation</label> 
                            <select name="father_designation" class="form-control"  >
                            <option value=""><?php echo $this->lang->line('select') ?></option>
                                <?php $father_designation_list=$this->config->item('parent_designation_list');
                                foreach ($father_designation_list as $key => $value){?>
                                    <option value="<?php echo $key ?>" <?php if ($enquiry_data['father_designation'] == $key) { ?> selected="selected" <?php } ?>><?php echo $value; ?></option>

                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Work Address</label> 
                            <input id="father_address" name="father_address" placeholder="Work Address" type="text" class="form-control"  value="<?php echo set_value('father_address', $enquiry_data['father_address']); ?>" />
                            
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Work City</label> 
                                <select name="father_city" class="form-control"  >
                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    ?>
                                    ?>
                                    <?php $father_city_list=$this->config->item('parent_city_list');
                                    foreach ($father_city_list as $key => $value) {
                                        ?>

                                    <option value="<?php echo $key ?>" <?php if (set_value('father_city', $enquiry_data['father_city']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Work Pin Code</label> 
                            <input id="father_pincode" name="father_pincode" placeholder="Work Pin Code" type="text" class="form-control"  value="<?php echo set_value('father_pincode', $enquiry_data['father_pincode']); ?>" />
                            
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Work phone</label> 
                            <input id="father_workphone" name="father_workphone" placeholder="Work phone" type="text" class="form-control"  value="<?php echo set_value('father_workphone', $enquiry_data['father_workphone']); ?>" />
                            
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="email">Annual Income</label> 
                            <input id="father_annualincome" name="father_annualincome" placeholder="Annual Income" type="text" class="form-control"  value="<?php echo set_value('father_annualincome', $enquiry_data['father_annualincome']); ?>" />
                            
                        </div>
                    </div>

                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label>Working wih TKS Noida</label>

                            <select name="father_woking_tks" class="form-control"  >
                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    ?>
                                    ?>
                                    <?php $father_woking_tks_list=$this->config->item('true-false');
                                    foreach ($father_woking_tks_list as $key => $value) {
                                        ?>

                                    <option value="<?php echo $key ?>" <?php if (set_value('father_woking_tks', $enquiry_data['father_woking_tks']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_mobile">Email Prefrence</label>  
                            <div class="material-switch">
                                <input id="father_email_pref" name="father_email_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['father_email_pref']) == 1) ? TRUE : FALSE); ?>>
                                <label for="father_email_pref" class="label-success"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 sw-col">
                        <div class="form-group">
                            <label for="father_mobile">SMS Prefrence</label>  
                            <div class="material-switch">
                                <input id="father_sms_pref" name="father_sms_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['father_sms_pref']) == 1) ? TRUE : FALSE); ?>>
                                <label for="father_sms_pref" class="label-success"></label>
                            </div>
                        </div>
                    </div> 

                    </div></div>

                    <!-- Mother details -->
                    <div class="tshadow mb25 bozero sw-content">
                        <h4 class="pagetitleh2">Mother Details</h4>
                        <div class="row around10">
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_title">Title:</label> 
                                <select name="mother_title" class="form-control"  >
                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php $mother_title_list=$this->config->item('mother_title_list');
                                    foreach ($mother_title_list as $key => $value) {
                                        ?>

                                    <option value="<?php echo $key ?>" <?php if (set_value('mother_title', $enquiry_data['mother_title']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>

                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_firstname">First Name</label> 
                                <input id="mother_firstname" name="mother_firstname" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('mother_firstname', $enquiry_data['mother_firstname']); ?>" />
                                <span class="text-danger"><?php echo form_error('mother_firstname'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_lastname">Last Name</label> 
                                <input id="mother_lastname" name="mother_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('mother_lastname', $enquiry_data['mother_lastname']); ?>" />
                                <span class="text-danger"><?php echo form_error('mother_lastname'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_email">Email</label> 
                                <input id="mother_email" name="mother_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('mother_email', $enquiry_data['mother_email']); ?>" />
                                <span class="text-danger"><?php echo form_error('mother_email'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_mobile">Mobile</label> 
                                <input id="mother_mobile" name="mother_mobile" placeholder="Mobile" type="tel" class="form-control form-mobile" maxlength="14" value="<?php echo set_value('mother_mobile', $enquiry_data['mother_mobile']); ?>" />
                                <span class="text-danger"><?php echo form_error('mother_mobile'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Aadhar Card No.</label> 
                                <input maxlength="14" id="mother_aadhar_no" name="mother_aadhar_no" placeholder="12 Digit Aadhaar No." type="text" class="form-control aadhaar_input"  value="<?php echo set_value('mother_aadhar_no', $enquiry_data['mother_aadhar_no']); ?>" />
                                <span class="text-danger"><?php echo form_error('mother_aadhar_no'); ?></span>
                            </div>
                        </div>
                                              

                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Qualification</label><small class="req"> *</small>  
                                <select name="mother_qualification" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        ?>
                                        ?>
                                        <?php $mprevious_board_list=$this->config->item('parent_previous_board_list');
                                        foreach ($mprevious_board_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('mother_qualification', $enquiry_data['mother_qualification']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Occupation</label> 
                                <select name="mother_occupation" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        ?>
                                        ?>
                                        <?php $mother_occupation_list=$this->config->item('parent_occupation_list');
                                        foreach ($mother_occupation_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('mother_occupation', $enquiry_data['mother_occupation']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Employer</label><small class="req"> *</small>  
                                <input id="mother_employer" name="mother_employer" placeholder="Employer" type="text" class="form-control"  value="<?php echo set_value('mother_employer', $enquiry_data['mother_employer']); ?>" />
                                <span class="text-danger"><?php echo form_error('mother_employer'); ?></span>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Designation</label> 
                                <select name="mother_designation" class="form-control"  >
                                    <option value=""><?php echo $this->lang->line('select') ?></option>
                                        <?php $mother_designation_list=$this->config->item('parent_designation_list');
                                        foreach ($mother_designation_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if ($enquiry_data['mother_designation'] == $key) { ?> selected="selected" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Work Address</label> 
                                <input id="mother_address" name="mother_address" placeholder="Work Address" type="text" class="form-control"  value="<?php echo set_value('mother_address', $enquiry_data['mother_address']); ?>" />
                                
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Work City</label> 
                                    <select name="mother_city" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        ?>
                                        ?>
                                        <?php $mother_city_list=$this->config->item('parent_city_list');
                                        foreach ($mother_city_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('mother_city', $enquiry_data['mother_city']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Work Pin Code</label> 
                                <input id="mother_pincode" name="mother_pincode" placeholder="Work Pin Code" type="text" class="form-control"  value="<?php echo set_value('mother_pincode', $enquiry_data['mother_pincode']); ?>" />
                                
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Work phone</label> 
                                <input id="mother_workphone" name="mother_workphone" placeholder="Work phone" type="text" class="form-control"  value="<?php echo set_value('mother_workphone', $enquiry_data['mother_workphone']); ?>" />
                                
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Annual Income</label> 
                                <input id="mother_annualincome" name="mother_annualincome" placeholder="Annual Income" type="text" class="form-control"  value="<?php echo set_value('mother_workphone', $enquiry_data['mother_workphone']); ?>" />
                                
                            </div>
                        </div>

                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label>Working wih TKS Noida</label>

                                <select name="mother_woking_tks" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                        ?>
                                        ?>
                                        <?php $mother_woking_tks_list=$this->config->item('true-false');
                                        foreach ($mother_woking_tks_list as $key => $value) {
                                            ?>

                                        <option value="<?php echo $key ?>" <?php if (set_value('mother_woking_tks', $enquiry_data['mother_woking_tks']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="father_mobile">Email Prefrence</label>  
                                <div class="material-switch">
                                    <input id="mother_email_pref" name="mother_email_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['mother_email_pref']) == 1) ? TRUE : FALSE); ?>>
                                    <label for="mother_email_pref" class="label-success"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_mobile">SMS Prefrence</label>  
                                <div class="material-switch">
                                    <input id="mother_sms_pref" name="mother_sms_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['mother_sms_pref']) == 1) ? TRUE : FALSE); ?>>
                                    <label for="mother_sms_pref" class="label-success"></label>
                                </div>
                            </div>
                        </div>  

                        </div></div>

                    <!-- Mother details end-->

                    <!-- Guardian details -->

                    <div class="tshadow mb25 bozero sw-content">
                        <h4 class="pagetitleh2">Guardian Details</h4>
                        <div class="row around10">
                            <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Name</label> 
                                <input id="guardian_name" name="guardian_name" placeholder="Name" type="text" class="form-control"  value="<?php echo set_value('guardian_name', $enquiry_data['guardian_name']); ?>" />
                                
                            </div>
                        </div>
                        
                            <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Address</label> 
                                <input id="guardian_address" name="guardian_address" placeholder="Address" type="text" class="form-control"  value="<?php echo set_value('guardian_address', $enquiry_data['guardian_address']); ?>" />
                                
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">City</label> 
                                <input id="guardian_city" name="guardian_city" placeholder="City" type="text" class="form-control"  value="<?php echo set_value('guardian_city', $enquiry_data['guardian_city']); ?>" />
                                
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">State</label> 
                                <input id="guardian_state" name="guardian_state" placeholder="State" type="text" class="form-control"  value="<?php echo set_value('guardian_state', $enquiry_data['guardian_state']); ?>" />
                                
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="email">Pin Code</label> 
                                <input id="guardian_pincode" name="guardian_pincode" placeholder="Pin Code" type="text" class="form-control"  value="<?php echo set_value('guardian_pincode', $enquiry_data['guardian_pincode']); ?>" />
                                
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="email">Telephone</label> 
                                    <input id="guardian_telephone" name="guardian_telephone" placeholder="Telephone" type="text" class="form-control"  value="<?php echo set_value('guardian_telephone', $enquiry_data['guardian_telephone']); ?>" />
                                    
                                </div>
                            </div>
                            <div class="col-sm-4 sw-col">
                                <div class="form-group">
                                    <label for="email">Relation</label> 
                                    <input id="guardian_relation" name="guardian_relation" placeholder="Relation" type="text" class="form-control"  value="<?php echo set_value('guardian_relation', $enquiry_data['guardian_relation']); ?>" />
                                    
                                </div>
                            </div>
                            
                            <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="father_mobile">Email Prefrence</label>  
                                <div class="material-switch">
                                    <input id="guadian_email_pref" name="guadian_email_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['guadian_email_pref']) == 1) ? TRUE : FALSE); ?>>
                                    <label for="guadian_email_pref" class="label-success"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 sw-col">
                            <div class="form-group">
                                <label for="mother_mobile">SMS Prefrence</label>  
                                <div class="material-switch">
                                    <input id="guadian_sms_pref" name="guadian_sms_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['guadian_sms_pref']) == 1) ? TRUE : FALSE); ?>>
                                    <label for="guadian_sms_pref" class="label-success"></label>
                                </div>
                            </div>
                        </div>                        
                        </div>
                    </div>
                    <p class="form-note"><strong>*Note:</strong>
                     Add one email and one SMS as a communication preference, either for Father or Mother.</p>
                    <!-- guardian details end -->
                    <div class="sw-footer">
                        <div class="text-right form-group">
                            <button class="site-btn sw-btn btn sw-save-btn" type="button">Save Draft</button>
                            <button class="site-btn sw-btn btn sw-prev-btn" type="button">Prev</button>
                            <button class="site-btn sw-btn btn sw-next-btn" type="button">Next</button>
                        </div>
                    </div>

                </div>
                <!-- Questionaire -->
                <div class="sw-steps-content">
                    <div class="tshadow mb25 bozero sw-content">
                        <h4 class="pagetitleh2">Questionaire</h4>
                        <div class="row around10">
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="pwd">School Last Attended:</label>
                                    <input type="text" class="form-control" id="school_last_attended" value="<?php echo set_value('school_last_attended', $enquiry_data['school_last_attended']); ?>" name="school_last_attended">
                                    <span class="text-danger" id="school_last_attended"></span>
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="email">Other Languages Child can Speak / Understand:</label> 
                                        <input type="text" class="form-control" id="languages_child_can_speak" value="<?php echo set_value('languages_child_can_speak', $enquiry_data['languages_child_can_speak']); ?>" name="languages_child_can_speak">
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="email">Real Brother / Sister School:</label> 
                                        <input type="text" class="form-control" id="sibbling_school" value="<?php echo set_value('sibbling_school', $enquiry_data['sibbling_school']); ?>" name="sibbling_school">
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="email">Real Brother / Sister Class:</label> 
                                        <input type="text" class="form-control" id="sibbling_class" value="<?php echo set_value('sibbling_class', $enquiry_data['sibbling_class']); ?>" name="sibbling_class">
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="email">Special Traits:</label> 
                                        <input type="text" class="form-control" id="special_traits" value="<?php echo set_value('special_traits', $enquiry_data['special_traits']); ?>" name="special_traits">
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="email">Interaction with Peer Group:</label> 
                                        <input type="text" class="form-control" id="interaction_with_peer_group" value="<?php echo set_value('interaction_with_peer_group', $enquiry_data['interaction_with_peer_group']); ?>" name="interaction_with_peer_group">
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="email">Whether Nuclear / Joint Family:</label> 
                                    <input type="text" class="form-control" id="nuclear_joint_family" value="<?php echo set_value('nuclear_joint_family', $enquiry_data['nuclear_joint_family']); ?>" name="nuclear_joint_family">
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="home_town">What according to you is the definition of a good school?</label> 
                                    <input id="definition_good_school" name="definition_good_school" placeholder="" type="text" class="form-control"  value="<?php echo set_value('definition_good_school', $enquiry_data['definition_good_school']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="home_town">State your views on inclusive education.</label> 
                                    <input id="inclusive_education" name="inclusive_education" placeholder="" type="text" class="form-control"  value="<?php echo set_value('inclusive_education', $enquiry_data['inclusive_education']); ?>" />
                                </div>
                            </div>
                            
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="home_town">What values would you like to inculcate in your child?</label> 
                                    <input id="inculcate_values" name="inculcate_values" placeholder="" type="text" class="form-control"  value="<?php echo set_value('inculcate_values', $enquiry_data['inculcate_values']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="home_town">Incase both parents are working who takes care of the child?</label> 
                                    <input id="takes_care_child" name="takes_care_child" placeholder="" type="text" class="form-control"  value="<?php echo set_value('takes_care_child', $enquiry_data['takes_care_child']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="spendtime_importance">As a father why do you think it is important to spend time with your child?</label> 
                                    <input id="spendtime_importance" name="spendtime_importance" placeholder="" type="text" class="form-control"  value="<?php echo set_value('spendtime_importance', $enquiry_data['spendtime_importance']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="spend_with_your_child">As a mother, how much time do you spend with your child and how?</label> 
                                    <input id="spend_with_your_child" name="spend_with_your_child" placeholder="" type="text" class="form-control"  value="<?php echo set_value('spend_with_your_child', $enquiry_data['spend_with_your_child']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="interests_hobbies">Mention atleast two interests / hobbies of your child.</label> 
                                    <input id="interests_hobbies" name="interests_hobbies" placeholder="" type="text" class="form-control"  value="<?php echo set_value('interests_hobbies', $enquiry_data['interests_hobbies']); ?>" />
                                </div>
                            </div>
                            
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="logical_answers">If your child is very inquisitive, do you always believe in providing logical answers to his / her question or avoid them?</label> 
                                    <input id="logical_answers" name="logical_answers" placeholder="" type="text" class="form-control"  value="<?php echo set_value('logical_answers', $enquiry_data['logical_answers']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="physical_illness">Did your child ever suffer from a handicap or ailment where the school needs to be cautious in handling him / her?</label> 
                                    <input id="physical_illness" name="physical_illness" placeholder="" type="text" class="form-control"  value="<?php echo set_value('physical_illness', $enquiry_data['physical_illness']); ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6 sw-col">
                                <div class="form-group">
                                    <label for="kps_impression">Write briefly your impression about The Khaitan School.</label> 
                                    <input id="kps_impression" name="kps_impression" placeholder="" type="text" class="form-control"  value="<?php echo set_value('kps_impression', $enquiry_data['kps_impression']); ?>" />
                                </div>
                            </div>                                
                        </div>
                        <div class="sw-footer">
                            <div class="text-right form-group">
                                <button class="site-btn sw-btn btn sw-save-btn" type="button">Save Draft</button>
                                <button class="site-btn sw-btn btn sw-prev-btn" type="button">Prev</button>
                                <button class="site-btn sw-btn btn sw-next-btn" type="button">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Agreement -->
                <div class="sw-steps-content">
                    <div class="tshadow mb25 bozero sw-content">
                        <h4 class="pagetitleh2">Terms to acknowledge</h4>
                        <div class="around10">
                            <!--  -->
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_1" name="terms_1" type="checkbox" class="chk" value="1">
                                    <label for="terms_1" class="terms-label">I / we have seen the fee structure on the website of the school and am / are conversant with it. I / we abide to follow all the rules and regulations mentioned in the fee rules at all times.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_2" name="terms_2" type="checkbox" class="chk" value="1">
                                    <label for="terms_2" class="terms-label">The information provided by me / us in the online registration is accurate to the best of my / our knowledge. All questions asked in the application were answered by either a parent or a guardian.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_3" name="terms_3" type="checkbox" class="chk" value="1">
                                    <label for="terms_3" class="terms-label">I / we understand that incase of any non-conformance / wrong information in the application shall give the school the right to not offer a seat to my / our ward.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_4" name="terms_4" type="checkbox" class="chk" value="1">
                                    <label for="terms_4" class="terms-label">I / we understand and agree that the Registration Fee of Rs 500 is non-refundable and does not guarantee admission.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_5" name="terms_5" type="checkbox" class="chk" value="1">
                                    <label for="terms_5" class="terms-label">I / we understand that admissions decisions are at the discretion of the school authorities and that all decisions of the school are final.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_6" name="terms_6" type="checkbox" class="chk" value="1">
                                    <label for="terms_6" class="terms-label">I / we undertake to inform The Khaitan School immediately of any changes to the information provided herein.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_7" name="terms_7" type="checkbox" class="chk" value="1">
                                    <label for="terms_7" class="terms-label">I / we have not and will not make any payments related to admission to The Khaitan School except those set out in the fees schedule.</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="terms-check">
                                    <input id="terms_8" name="terms_8" type="checkbox" class="chk" value="1">
                                    <label for="terms_8" class="terms-label">I / we understand that I / we must report any attempt to subvert the admissions process or pay or solicit payments related to admissions except those set out in the fee schedule.</label>
                                </div>
                            </div>
                            <!--  -->
                        </div>
                    </div>
                    <div class="sw-footer">
                        <div class="text-right form-group">
                            <button class="site-btn sw-btn btn sw-save-btn" type="button">Save Draft</button>
                            <button class="site-btn sw-btn btn sw-prev-btn" type="button">Prev</button>
                            <button class="site-btn sw-btn btn sw-submit" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>