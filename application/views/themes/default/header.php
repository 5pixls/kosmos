<header>
    <div class="container rrrr">
        <div class="row">
            <div class="col-md-12">
                
                <nav class="navbar">
                    <div class="navbar-inner">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand logo" href="<?php echo base_url(); ?>"><img src="<?php echo base_url($front_setting->logo); ?>" alt=""/></a>
                            <span class="school-title desktopShow">Khaitan Online School Management Operations System (KOSMOS)</span>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <ul class="top-right hide">
                                <li><a href="<?php echo site_url('site/userlogin') ?>"><i class="fa fa-sign-in"></i><span><?php echo $this->lang->line('login'); ?></span></a></li>
                                <li><a href="<?php echo site_url('site/register') ?>"><i class="fa fa-user"></i><span>Register</span></a></li>
                            </ul>
                        </div>
                    </div> <!-- / .navbar-inner -->
                    
                </nav><!-- /.navbar -->
            </div>
        </div>
    </div>   
</header> 