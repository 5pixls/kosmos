<footer>
    <div class="copy-right footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center">
                    <p><?php echo $front_setting->footer_text; ?></p>
                </div>
            </div><!--./row-->
        </div><!--./container-->
    </div><!--./copy-right-->
      <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> 
</footer>

