<?php //print_r($enquiry_data);?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-user-plus"></i> <?php echo $this->lang->line('student_information'); ?>            
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="pull-right box-tools impbtntitle">
                    <?php if ($this->rbac->hasPrivilege('import_student', 'can_view')) {   ?>
                        <a href="<?php echo site_url('student/import') ?>">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-upload"></i> <?php echo $this->lang->line('import_student'); ?></button>
                        </a>
                    <?php } 
                   
                    ?>
                </div>
				<form id="form1" action="<?php echo site_url('student/create') ?>"  id="employeeform" name="employeeform" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                 <?php //echo validation_errors(); ?>
                 <?php if($enquiry_data['id'] !='') {?>
                    <input type="hidden" name="enquiry_id" value="<?php echo $enquiry_data['id'];?>" id="enquiry_id">
                <?php } else {?>
                    <input type="hidden" name="enquiry_id" id="enquiry_id">
                <?php }?>
                    <div class="">
						<div class="bozero">
							<h4 class="pagetitleh-whitebg"><?php echo $this->lang->line('student'); ?> <?php echo $this->lang->line('admission'); ?> 
                            <?php if($enquiry_data['id'] !='') echo '<label>(Lead ID: '. $enquiry_data['id'].')</label>';?>
                                
                            </h4>
                            
                            <div class="around10">
                                <?php if ($this->session->flashdata('msg')) {?>
                                    <?php echo $this->session->flashdata('msg') ?>
                                <?php }?>
                                <?php if (isset($error_message)) {  ?>
                                    <div class="alert alert-warning"><?php echo $error_message; ?></div>
                                <?php } ?>
                                <?php echo $this->customlib->getCSRF(); ?>
                                    <input type="hidden" name="sibling_name" value="<?php echo set_value('sibling_name'); ?>" id="sibling_name_next">
                                    <input type="hidden" name="sibling_id" value="<?php echo set_value('sibling_id', 0); ?>" id="sibling_id">
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('first_name'); ?></label><small class="req"> *</small>
                                                <input id="firstname" name="firstname" placeholder="<?php echo $this->lang->line('first_name'); ?>" type="text" class="form-control" value="<?php if($enquiry_data['name'] !='') echo $enquiry_data['name'];?>" />
                                                <span class="text-danger"><?php echo form_error('firstname'); ?></span>
                                            </div>
                                        </div>
                                        <?php if ($sch_setting->lastname) {  ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('last_name'); ?></label>
                                                <input id="lastname" name="lastname" placeholder="<?php echo $this->lang->line('last_name'); ?>" type="text" class="form-control"  value="<?php if($enquiry_data['last_name'] !='') echo $enquiry_data['last_name'];?>" />
                                                <span class="text-danger"><?php echo form_error('lastname'); ?></span>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputFile"> <?php echo $this->lang->line('gender'); ?></label><small class="req"> *</small>
                                                <select class="form-control" name="gender">
                                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                    <?php
                                                    $gender =set_value('gender');
                                                    if($enquiry_data['gender'] !='') $gender =$enquiry_data['gender'];
                                                    foreach ($genderList as $key => $value) {
                                                        ?>
                                                        <option value="<?php echo $key; ?>" <?php if ( $gender== $key) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $value; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('gender'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('date_of_birth'); ?></label><small class="req"> *</small>
                                                <input id="dob" name="dob" placeholder="dd/mm/yyyy" type="text" class="form-control dob" value="<?php if($enquiry_data['dob'] !='') echo date("m/d/Y", strtotime($enquiry_data['dob']));?>" />
                                                <span class="text-danger"><?php echo form_error('dob'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                <?php if (!$adm_auto_insert) {  ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('admission_no'); ?></label> <small class="req"> *</small>

                                            <input autofocus="" id="admission_no" name="admission_no" placeholder="<?php echo $this->lang->line('admission_no'); ?>" type="text" class="form-control"  value="<?php echo set_value('admission_no'); ?>" />
                                            <span class="text-danger"><?php echo form_error('admission_no'); ?></span>
                                        </div>
                                    </div>
								<?php } ?>
										<?php if ($sch_setting->roll_no) {  ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('roll_no'); ?></label>
                                                <input id="roll_no" name="roll_no" placeholder="<?php echo $this->lang->line('roll_no'); ?>" type="text" class="form-control"  value="<?php echo set_value('roll_no'); ?>" />
                                                <span class="text-danger"><?php echo form_error('roll_no'); ?></span>
                                            </div>
                                        </div>
										<?php } ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('class'); ?></label><small class="req"> *</small>
                                                <select  id="class_id" name="class_id" class="form-control"  >
                                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                    <?php
                                                        foreach ($classlist as $class) {
                                                            ?>
                                                        <option value="<?php echo $class['id'] ?>"<?php if ($enquiry_data['class'] == $class['id']) {
                                                                    echo "selected=selected";
                                                                }
                                                                ?>><?php echo $class['class'] ?></option>
                                                        <?php

                                                        }
                                                        ?>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('class_id'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">

                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('section'); ?></label><small class="req"> *</small>
                                                <select  id="section_id" name="section_id" class="form-control" >
                                                    <option value=""   ><?php echo $this->lang->line('select'); ?></option>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('section_id'); ?></span>
                                            </div>
                                        </div>

                                    
										<?php 
                                        if ($sch_setting->category) {  
                                            $categoryid =set_value('category_id');
                                            if($enquiry_data['social_category'] !='') $categoryid =$enquiry_data['social_category'];
                                                    ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('category'); ?></label>
                                                <select  id="category_id" name="category_id" class="form-control" >
                                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                    <?php foreach ($categorylist as $category) {   ?>
                                                    <option value="<?php echo $category['id'] ?>" <?php if ($categoryid == $category['id']) {  echo "selected=selected";  } ?>><?php echo $category['category'] ?></option>
                                                    <?php $count++; } ?>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('category_id'); ?></span>
                                            </div>
                                        </div>
										<?php } if ($sch_setting->religion) { 
                                        $religion =set_value('religion');
                                            if($enquiry_data['religion'] !='') $religion =$enquiry_data['religion']; ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('religion'); ?></label>
                                                <select name="religion" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    <?php $religion_list=$this->config->item('religion_list');
                                                    foreach ($religion_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('religion', $enquiry_data['religion']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                            </div>
                                        </div>
										<?php } ?>
                                        <?php if ($sch_setting->national_identification_no) { ?>
                                        <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="pwd">Child's Aadhaar Card No.:</label>
                                            <input type="text" class="form-control aadhaar_input" id="adhar_no" value="<?php echo set_value('adhar_no', $enquiry_data['child_aadhaar_no']); ?>" name="adhar_no">
                                            <span class="text-danger" id="child_aadhaar_no"></span>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Nationality</label><small class="req"> *</small>   
                                           <select name="nationality" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                <?php $nationality_list=$this->config->item('nationality_list');

                                                foreach ($nationality_list as $key => $value) {
                                                    ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('nationality', $enquiry_data['nationality']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if ($sch_setting->is_blood_group) { ?>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="dob">Blood Group of the child:</label><small class="req"> *</small>  
                                            <select name="blood_group" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                <?php $blood_group_list=$this->config->item('blood_group_list');

                                                foreach ($blood_group_list as $key => $value) {
                                                    ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('blood_group', $enquiry_data['blood_group']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php  } ?>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Mother Tongue</label><small class="req"> *</small>   
                                           <select name="mother_tongue" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                <?php $mother_tongue_list=$this->config->item('mother_tongue_list');

                                                foreach ($mother_tongue_list as $key => $value) {
                                                    ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('mother_tongue', $enquiry_data['mother_tongue']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="home_town">Home Town</label><small class="req"> *</small>  
                                            <input id="home_town" name="home_town" placeholder="Home Town" type="text" class="form-control"  value="<?php echo set_value('home_town', $enquiry_data['home_town']); ?>" />
                                            <span class="text-danger"><?php echo form_error('home_town'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="locality">Locality</label><small class="req"> *</small>   
                                            <select name="locality" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                <?php $locality_list=$this->config->item('locality_list');

                                                foreach ($locality_list as $key => $value) {
                                                    ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('locality', $enquiry_data['locality']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Board Registration no.</label>
                                                <input id="board_registration" name="board_registration" placeholder="Board Registration no." type="text" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Optional Subject & Activities</label>
                                                <select name="subject" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $subject_list=$this->config->item('subject_list');
                                                    foreach ($subject_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>"><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                            </div>
                                        </div>
                                    <?php if ($sch_setting->cast) {  ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('cast'); ?></label>
                                            <input id="cast" name="cast" placeholder="<?php echo $this->lang->line('cast'); ?>" type="text" class="form-control"  value="<?php echo set_value('cast'); ?>" />
                                            <span class="text-danger"><?php echo form_error('cast'); ?></span>
                                        </div>
                                    </div>
									<?php } if ($sch_setting->mobile_no) {  ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('mobile_no'); ?></label>
                                            <input id="mobileno" name="mobileno" placeholder="<?php echo $this->lang->line('mobile_no'); ?>" type="text" class="form-control"  value="<?php echo set_value('mobileno'); ?>" />
                                            <span class="text-danger"><?php echo form_error('mobileno'); ?></span>
                                        </div>
                                    </div>
									<?php } if ($sch_setting->student_email) {  ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('email'); ?></label>
                                            <input id="email" name="email" placeholder="<?php echo $this->lang->line('email'); ?>" type="text" class="form-control"  value="<?php echo set_value('email'); ?>" />
                                            <span class="text-danger"><?php echo form_error('email'); ?></span>
                                        </div>
                                    </div>
									<?php } ?>
                                    
                                    
										
                                        <?php if ($sch_setting->admission_date) {  ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('admission_date'); ?></label>
                                                <input id="admission_date" name="admission_date" placeholder="dd/mm/yyyy" type="text" class="form-control date"  value="<?php echo set_value('admission_date', date($this->customlib->getSchoolDateFormat())); ?>" readonly="readonly" />
                                                <span class="text-danger"><?php echo form_error('admission_date'); ?></span>
                                            </div>
                                        </div>
										<?php }  if ($sch_setting->student_photo) {  ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputFile"><?php echo $this->lang->line('student') . " " . $this->lang->line('photo'); ?></label>
                                                <div><input class="filestyle form-control" type='file' name='file' id="file" size='20' />
                                                </div>
                                                <span class="text-danger"><?php echo form_error('file'); ?></span></div>
                                        </div>
	 									<?php } ?>

<?php

if ($sch_setting->is_student_house) {
?>
 <div class="col-md-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('house') ?></label>
                                                            <select class="form-control" rows="3" placeholder="<?php echo $this->lang->line('house') ?>" name="house">
                                                                <option value=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php foreach ($houses as $hkey => $hvalue) {
    ?>
                                                         <option value="<?php echo $hvalue["id"] ?>"><?php echo $hvalue["house_name"] ?></option>

                                                               <?php }?>
                                                            </select>
                                                            <span class="text-danger"><?php echo form_error('house'); ?></span>
                                                        </div>
                                                    </div>

<?php
}

?>

                                       
													<?php if ($sch_setting->student_height) {  ?>
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('height'); ?></label>
                                                           <?php

?>
                                                           <input type="text" name="height" class="form-control" value="<?php echo set_value('height'); ?>" >
                                                            <span class="text-danger"><?php echo form_error('height'); ?></span>
                                                        </div>
                                                    </div>
													<?php } if ($sch_setting->student_weight) { ?>
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('weight'); ?></label>
                                                           <?php

?>
                                                           <input type="text" name="weight" class="form-control" value="<?php echo set_value('weight'); ?>">
                                                            <span class="text-danger"><?php echo form_error('weight'); ?></span>
                                                        </div>
                                                    </div>
													<?php } if ($sch_setting->measurement_date) { ?>
                                                    <div class="col-md-3 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('measurement_date'); ?></label>
                                                           <?php

?>
                                                           <input type="text" id="measure_date" placeholder="dd/mm/yyyy" value="<?php echo set_value('measure_date', date($this->customlib->getSchoolDateFormat())); ?>" name="measure_date" class="form-control date">
                                                            <span class="text-danger"><?php echo form_error('measure_date'); ?></span>
                                                        </div>
                                                    </div>
													<?php } ?>
                                        <div class="col-md-3" style="display:none;">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('fees_discount'); ?></label>
                                                <input id="fees_discount" name="fees_discount" placeholder="<?php echo $this->lang->line('fees_discount'); ?>" type="text" class="form-control"  value="<?php echo set_value('fees_discount', 0); ?>"  />
                                                <span class="text-danger"><?php echo form_error('fees_discount'); ?></span>
                                            </div>
                                        </div>

                                        <div class="col-md-3 pt25">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button type="button" class="btn btn-sm mysiblings anchorbtn "><i class="fa fa-plus"></i> <?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('sibling'); ?></button>
                                                </div>
                                                <div class="col-md-6">
                                                    <div id='sibling_id' class="pt6"> <span id="sibling_name" class="label label-success "><?php echo set_value('sibling_name'); ?></span></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                 <div class="row">
                                    <?php
echo display_custom_fields('students');
?>
                                 </div>

                            </div>
                            </div>
                          <?php if ($sch_setting->previous_school_details) {  ?>
                             <div class="box-body">
                            <div class="tshadow mb25 bozero">
                            <h4 class="pagetitleh2">Previous School Information</h4>
                            <div class="row around10">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="last_school">Last School:</label> 
                                            <input id="previous_school" name="previous_school" placeholder="Last School" type="text" class="form-control"  value="<?php echo set_value('previous_school', $enquiry_data['last_school']); ?>" />

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="medium_instruction">Medium of Instruction:</label> 
                                            <input id="medium_instruction" name="medium_instruction" placeholder="Medium of Instruction" type="text" class="form-control"  value="<?php echo set_value('medium_instruction', $enquiry_data['medium_instruction']); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="previous_board">Board:</label> 
                                            <select name="previous_board" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $previous_board_list=$this->config->item('previous_board_list');
                                                    foreach ($previous_board_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('previous_board', $enquiry_data['previous_board']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="previous_class">Class:</label> 
                                            <select name="previous_class" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $previous_class_list=$this->config->item('previous_class_list');

                                                    foreach ($previous_class_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('previous_class', $enquiry_data['previous_class']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                        </div>
                          </div>  
                          </div> 
                        <?php }  ?>
                            <div class="box-body">
                                <div class="tshadow mb25 bozero">
                                <h4 class="pagetitleh2">Father Details</h4>
                                <div class="around10">
                                    <div class="row">
										<?php if ($sch_setting->father_name) {  ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Father Title</label>
                                             <select name="father_title" class="form-control"  >
                                            <option value=""><?php echo $this->lang->line('select') ?></option>
                                                <?php $father_title_list=$this->config->item('father_title_list');

                                                foreach ($father_title_list as $key => $value) {
                                                    ?>
<option value="<?php echo $key ?>" <?php if ($enquiry_data['father_title'] == $key) { ?> selected <?php } ?>><?php echo $value; ?></option>

                                                    <?php 
                                                }
                                                ?>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">First Name</label>
                                                <input id="father_name" name="father_name" placeholder="First Name" type="text" class="form-control"  value="<?php if($enquiry_data['father_firstname'] !='') echo $enquiry_data['father_firstname'];?>" />
                                                <span class="text-danger"><?php echo form_error('father_name'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Last Name</label>
                                                <input id="father_lastname" name="father_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php if($enquiry_data['father_lastname'] !='') echo $enquiry_data['father_lastname'];?>" />
                                                <span class="text-danger"><?php echo form_error('father_lastname'); ?></span>
                                            </div>
                                        </div>
										<?php } ?>
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email</label>
                                                <input id="father_email" name="father_email" placeholder="Email" type="text" class="form-control"  value="<?php if($enquiry_data['father_email'] !='') echo $enquiry_data['father_email'];?>" />
                                                <span class="text-danger"><?php echo form_error('father_email'); ?></span>
                                            </div>
                                        </div>
                                        
                                        <?php if ($sch_setting->father_phone) {  ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="father_phone">Mobile</label>
                                                <input id="father_phone" name="father_phone" placeholder="Mobile" type="text" class="form-control form-mobile" maxlength="14" value="<?php if($enquiry_data['father_mobile'] !='') echo $enquiry_data['father_mobile'];?>" />
                                                <span class="text-danger"><?php echo form_error('father_phone'); ?></span>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Aadhar Card No.</label>
                                                <input id="father_aadhar_no" name="father_aadhar_no" placeholder="12 Digit Aadhaar No." type="text" class="form-control aadhaar_input"  value="<?php if($enquiry_data['father_aadhar_no'] !='') echo $enquiry_data['father_aadhar_no'];?>" />
                                                <span class="text-danger"><?php echo form_error('father_aadhar_no'); ?></span>
                                            </div>
                                        </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Qualification:</label><small class="req"> *</small>   
                                            <select name="father_qualification" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $fprevious_board_list=$this->config->item('fprevious_board_list');

                                                    foreach ($fprevious_board_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('father_qualification', $enquiry_data['father_qualification']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if ($sch_setting->father_occupation) { ?>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Occupation:</label> 
                                            <select name="father_occupation" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $father_occupation_list=$this->config->item('parent_occupation_list');


                                                    foreach ($father_occupation_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('father_occupation', $enquiry_data['father_occupation']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                   <?php  } ?>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Employer</label><small class="req"> *</small>  
                                            <input id="father_employer" name="father_employer" placeholder="Employer" type="text" class="form-control"  value="<?php echo set_value('father_employer', $enquiry_data['father_employer']); ?>" />
                                            <span class="text-danger"><?php echo form_error('father_employer'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Designation</label> 
                                            <select name="father_designation" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $father_designation_list=$this->config->item('parent_designation_list');

                                                    foreach ($father_designation_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('father_designation', $enquiry_data['father_designation']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Address</label> 
                                            <input id="father_address" name="father_address" placeholder="Address" type="text" class="form-control"  value="<?php echo set_value('father_address', $enquiry_data['father_address']); ?>" />
                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">City</label> 
                                             <select name="father_city" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $father_city_list=$this->config->item('parent_city_list');

                                                    foreach ($father_city_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('father_city', $enquiry_data['father_city']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Pin Code</label> 
                                            <input id="father_pincode" name="father_pincode" placeholder="Pin Code" type="text" class="form-control"  value="<?php echo set_value('father_pincode', $enquiry_data['father_pincode']); ?>" />
                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Work phone</label> 
                                            <input id="father_workphone" name="father_workphone" placeholder="Work phone" type="text" class="form-control"  value="<?php echo set_value('father_workphone', $enquiry_data['father_workphone']); ?>" />
                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Annual Income</label> 
                                            <input id="father_annualincome" name="father_annualincome" placeholder="Annual Income" type="text" class="form-control"  value="<?php echo set_value('father_workphone', $enquiry_data['father_workphone']); ?>" />
                                           
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Working wih TKS Noida</label>

                                            <select name="father_woking_tks" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $father_woking_tks_list=$this->config->item('true-false');

                                                    foreach ($father_woking_tks_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('father_woking_tks', $enquiry_data['father_woking_tks']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
										<?php if ($sch_setting->father_pic) { ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputFile"><?php echo $this->lang->line('father'); ?> <?php echo $this->lang->line('photo'); ?></label>
                                                <div><input class="filestyle form-control" type='file' name='father_pic' id="file" size='20' />
                                                </div>
                                                <span class="text-danger"><?php echo form_error('file'); ?></span></div>
                                        </div>
										<?php } ?>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
<?php if ($sch_setting->mother_name) { ?>
                                    <div class="box-body">
                                <div class="tshadow mb25 bozero">
                                <h4 class="pagetitleh2">Mother Details</h4>

                                    <div class="row around10">
										
                                            <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="mother_title">Mother Title:</label> 
                                            <select name="mother_title" class="form-control"  >
                                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                <?php $mother_title_list=$this->config->item('mother_title_list');

                                                foreach ($mother_title_list as $key => $value) {
                                                    ?>

                                                <option value="<?php echo $key ?>" <?php if (set_value('mother_title', $enquiry_data['mother_title']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                    <?php
                                                }
                                                ?>
                                        </select>
                                        </div>
                                        </div>
                                        <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="mother_name">First Name</label> 
                                            <input id="mother_name" name="mother_name" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('mother_name', $enquiry_data['mother_firstname']); ?>" />
                                            <span class="text-danger"><?php echo form_error('mother_name'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="mother_lastname">Last Name</label> 
                                            <input id="mother_lastname" name="mother_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('mother_lastname', $enquiry_data['mother_lastname']); ?>" />
                                            <span class="text-danger"><?php echo form_error('mother_lastname'); ?></span>
                                        </div>
                                    </div>
										<?php } ?> 
                                        <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="mother_email">Email</label> 
                                            <input id="mother_email" name="mother_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('mother_email', $enquiry_data['mother_email']); ?>" />
                                            <span class="text-danger"><?php echo form_error('mother_email'); ?></span>
                                        </div>
                                    </div>
                                        <?php if ($sch_setting->mother_phone) { ?>
                                        <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="mother_phone">Mobile</label> 
                                            <input id="mother_phone" name="mother_phone" placeholder="Mobile" type="tel" class="form-control form-mobile" maxlength="14" value="<?php echo set_value('mother_phone', $enquiry_data['mother_mobile']); ?>" />
                                            <span class="text-danger"><?php echo form_error('mother_phone'); ?></span>
                                        </div>
                                    </div>
										<?php } ?>
                                        <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Aadhar Card No.</label> 
                                            <input id="mother_aadhar_no" name="mother_aadhar_no" placeholder="12 Digit Aadhaar No." type="text" class="form-control aadhaar_input"  value="<?php echo set_value('mother_aadhar_no', $enquiry_data['mother_aadhar_no']); ?>" />
                                            <span class="text-danger"><?php echo form_error('mother_aadhar_no'); ?></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Qualification</label><small class="req"> *</small>  
                                            <select name="mother_qualification" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $mprevious_board_list=$this->config->item('mprevious_board_list');

                                                    foreach ($mprevious_board_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('mother_qualification', $enquiry_data['mother_qualification']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if ($sch_setting->mother_occupation) { ?>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Occupation</label> 
                                            <select name="mother_occupation" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $mother_occupation_list=$this->config->item('parent_occupation_list');

                                                    foreach ($mother_occupation_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('mother_occupation', $enquiry_data['mother_occupation']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Employer</label><small class="req"> *</small>  
                                            <input id="mother_employer" name="mother_employer" placeholder="Employer" type="text" class="form-control"  value="<?php echo set_value('mother_employer', $enquiry_data['mother_employer']); ?>" />
                                            <span class="text-danger"><?php echo form_error('mother_employer'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Designation</label> 
                                            <select name="mother_designation" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $mother_designation_list=$this->config->item('parent_designation_list');


                                                    foreach ($mother_designation_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('mother_designation', $enquiry_data['mother_designation']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Address</label> 
                                            <input id="mother_address" name="mother_address" placeholder="Address" type="text" class="form-control"  value="<?php echo set_value('mother_address', $enquiry_data['mother_address']); ?>" />
                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">City</label> 
                                             <select name="mother_city" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $mother_city_list=$this->config->item('parent_city_list');


                                                    foreach ($mother_city_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('mother_city', $enquiry_data['mother_city']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Pin Code</label> 
                                            <input id="mother_pincode" name="mother_pincode" placeholder="Pin Code" type="text" class="form-control"  value="<?php echo set_value('mother_pincode', $enquiry_data['mother_pincode']); ?>" />
                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Work phone</label> 
                                            <input id="mother_workphone" name="mother_workphone" placeholder="Work phone" type="text" class="form-control"  value="<?php echo set_value('mother_workphone', $enquiry_data['mother_workphone']); ?>" />
                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Annual Income</label> 
                                            <input id="mother_annualincome" name="mother_annualincome" placeholder="Annual Income" type="text" class="form-control"  value="<?php echo set_value('mother_workphone', $enquiry_data['mother_workphone']); ?>" />
                                           
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Working wih TKS Noida</label>

                                            <select name="mother_woking_tks" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    
                                                    <?php $mother_woking_tks_list=$this->config->item('true-false');
                                                    array('Yes'=>'Yes','No'=>'No');
                                                    foreach ($mother_woking_tks_list as $key => $value) {
                                                        ?>

                                                    <option value="<?php echo $key ?>" <?php if (set_value('mother_woking_tks', $enquiry_data['mother_woking_tks']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                        
										<?php  if ($sch_setting->mother_pic) { ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputFile"><?php echo $this->lang->line('mother'); ?> <?php echo $this->lang->line('photo'); ?></label>
                                                <div><input class="filestyle form-control" type='file' name='mother_pic' id="file" size='20' />
                                                </div>
                                                <span class="text-danger"><?php echo form_error('file'); ?></span></div>
                                        </div>
										
                                    </div>
                                    </div>
                                    </div>
                                    <?php } ?>
                                    <div class="box-body">
                                <div class="tshadow mb25 bozero">
                                <h4 class="pagetitleh2">Guardian Details</h4>
                                    <div class="row around10">
                                        <div class="form-group col-md-12">
                                            <label><?php echo $this->lang->line('if_guardian_is'); ?><small class="req"> *</small>&nbsp;&nbsp;&nbsp;</label>
                                            <label class="radio-inline">
                                                <input type="radio" name="guardian_is" <?php
echo set_value('guardian_is') == "father" ? "checked" : "";
?>   value="father"> <?php echo $this->lang->line('father'); ?>
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="guardian_is" <?php
echo set_value('guardian_is') == "mother" ? "checked" : "";
?>   value="mother"> <?php echo $this->lang->line('mother'); ?>
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="guardian_is" <?php
echo set_value('guardian_is') == "other" ? "checked" : "";
?>   value="other"> <?php echo $this->lang->line('other'); ?>
                                            </label>
                                              <span class="text-danger"><?php echo form_error('guardian_is'); ?></span>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('guardian_name'); ?></label><small class="req"> *</small>
                                                        <input id="guardian_name" name="guardian_name" placeholder="<?php echo $this->lang->line('guardian_name'); ?>" type="text" class="form-control"  value="<?php echo set_value('guardian_name'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('guardian_name'); ?></span>
                                                    </div>
                                                </div>
												<?php  if ($sch_setting->guardian_relation) { ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('guardian_relation'); ?></label>
                                                        <input id="guardian_relation" name="guardian_relation" placeholder="<?php echo $this->lang->line('guardian_relation'); ?>" type="text" class="form-control"  value="<?php echo set_value('guardian_relation'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('guardian_relation'); ?></span>
                                                    </div>
                                                </div>
												<?php } ?>
                                            
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('guardian_phone'); ?></label><small class="req"> *</small>
                                                        <input id="guardian_phone" name="guardian_phone" placeholder="<?php echo $this->lang->line('guardian_phone'); ?>" type="text" class="form-control"  value="<?php echo set_value('guardian_phone'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('guardian_phone'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1"><?php echo $this->lang->line('guardian_occupation'); ?></label>
                                                        <input id="guardian_occupation" name="guardian_occupation" placeholder="<?php echo $this->lang->line('guardian_occupation'); ?>" type="text" class="form-control"  value="<?php echo set_value('guardian_occupation'); ?>" />
                                                        <span class="text-danger"><?php echo form_error('guardian_occupation'); ?></span>
                                                    </div>
                                                </div>
                                        
                                        </div>
                                    </div>
										<?php  if ($sch_setting->guardian_email) { ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1"><?php echo $this->lang->line('guardian_email'); ?></label>
                                                <input id="guardian_email" name="guardian_email" placeholder="<?php echo $this->lang->line('guardian_email'); ?>" type="text" class="form-control"  value="<?php echo set_value('guardian_email'); ?>" />
                                                <span class="text-danger"><?php echo form_error('guardian_email'); ?></span>
                                            </div>
                                        </div>
										<?php } if ($sch_setting->guardian_pic) { ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputFile"><?php echo $this->lang->line('guardian'); ?> <?php echo $this->lang->line('photo'); ?></label>
                                                <div><input class="filestyle form-control" type='file' name='guardian_pic' id="file" size='20' />
                                                </div>
                                                <span class="text-danger"><?php echo form_error('file'); ?></span></div>
                                        </div>
										<?php } if ($sch_setting->guardian_address) { ?>
                                        <div class="col-md-6">
                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('guardian_address'); ?></label>
                                            <textarea id="guardian_address" name="guardian_address" placeholder="<?php echo $this->lang->line('guardian_address'); ?>" class="form-control" rows="2"><?php echo set_value('guardian_address'); ?></textarea>
                                            <span class="text-danger"><?php echo form_error('guardian_address'); ?></span>
                                        </div>
										<?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--<div class="box-group collapsed-box">
                               <div class="panel box collapsed-box border0 mb0">
                                    <div class="addmoredetail-title">

                                            <a data-widget="collapse" data-original-title="Collapse" class="collapsed btn boxplus">
                                                <i class="fa fa-fw fa-plus"></i><?php //echo $this->lang->line('add_more_details'); ?>
                                            </a>

                                    </div>
                                        
                                </div>
                            </div>-->
                            <div class="box-body">
                                            <div class="tshadow mb25 bozero">
                                                <h4 class="pagetitleh2"><?php echo $this->lang->line('student'); ?> <?php echo $this->lang->line('address'); ?> <?php echo $this->lang->line('details'); ?></h4>

                                                <div class="row around10">
                                                    <?php if ($sch_setting->current_address) { ?>
                                                    <div class="col-md-6">
                                                        <div class="col-md-12" style="margin-bottom: 45px;"><b>Current Address</b></div>
                                                        
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="email">Address1</label> 
                                                                <input id="address" name="address" placeholder="Address1" type="text" class="form-control"  value="<?php echo set_value('address', $enquiry_data['address']); ?>" />
                                                                <span class="text-danger"><?php echo form_error('address'); ?></span>
                                                            </div>
                                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Address2</label> 
                                                <input id="address2" name="address2" placeholder="Address2" type="text" class="form-control"  value="<?php echo set_value('address2', $enquiry_data['address2']); ?>" />
                                                <span class="text-danger"><?php echo form_error('address2'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">City</label> 
                                                <input id="city" name="city" placeholder="City" type="text" class="form-control"  value="<?php echo set_value('city', $enquiry_data['city']); ?>" />
                                                <span class="text-danger"><?php echo form_error('city'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Country</label> 
                                                <select name="country_id"  id="country_id" class="form-control"  >
                                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    <?php 
                                                    foreach ($country_list as $value) {
                                                        ?>

                                                         <option value="<?php echo $value['id'] ?>" <?php if (set_value('country_id', $enquiry_data['country_id']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['country']; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6"><?php //print_r($state_list);?>
                                            <div class="form-group">
                                                <label for="email">State</label> 
                                               <select name="state" id="state" class="form-control"  >
                                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                        <?php 
                                                        foreach ($state_list as $value) {
                                                            ?>

                                                        <option value="<?php echo $value['id'] ?>" <?php if (set_value('state_id', $enquiry_data['state_id']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['state']; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Pin Code</label> 
                                                <input id="pincode" name="pincode" placeholder="Pin Code" type="text" class="form-control"  value="<?php echo set_value('pincode', $enquiry_data['pincode']); ?>" />
                                                <span class="text-danger"><?php echo form_error('pincode'); ?></span>
                                            </div>
                                        </div>
                                   </div>
                                   <?php } if ($sch_setting->permanent_address) { ?>
                                    <div class="col-sm-6">
                                        
                                    
                                        <div class="col-sm-12"> 
                                            <div class="col-sm-12">
                                            <div class="form-group"><b>Permanent Address</b>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" id="autofill_address"onclick="return auto_fill_address();">If current address is permanent address
                                                </label>
                                                </div>
                                         </div>
                                        </div>
                                            
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Address1</label> 
                                                <input id="permanent_address" name="permanent_address" placeholder="Address1" type="text" class="form-control"  />
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Address2</label> 
                                                <input id="permanent_address2" name="permanent_address2" placeholder="Address2" type="text" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">City</label> 
                                                <input id="permanent_city" name="permanent_city" placeholder="City" type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Country</label> 
                                                <select name="permanent_country_id"  id="permanent_country_id" class="form-control"  >
                                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                    <?php 
                                                    foreach ($country_list as $value) {
                                                        ?>

                                                         <option value="<?php echo $value['id'] ?>" ><?php echo $value['country']; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">State</label> 
                                               <select name="permanent_state_id" id="permanent_state_id" class="form-control"  >
                                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                        <?php 
                                                        foreach ($state_list as $value) {
                                                            ?>

                                                        <option value="<?php echo $value['id'] ?>" ><?php echo $value['state']; ?></option>

                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Pin Code</label> 
                                                <input id="permanent_pincode" name="permanent_pincode" placeholder="Pin Code" type="text" class="form-control"  />
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                                </div>

                                            </div>
                                            </div>
                                            <?php if ($sch_setting->route_list) { ?>
                                              <?php

if ($this->module_lib->hasActive('transport')) {
    ?>
                                                <div class="tshadow mb25 bozero">
                                                <h4 class="pagetitleh2">
                                                    <?php echo $this->lang->line('transport') . " " . $this->lang->line('details'); ?>
                                                </h4>

                                                <div class="row around10">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('route_list'); ?></label>
                                                            <select class="form-control" id="vehroute_id" name="vehroute_id">

                                                                <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                                <?php
foreach ($vehroutelist as $vehroute) {
        ?>
                                                                    <optgroup label=" <?php echo $vehroute->route_title; ?>">
                                                                        <?php
$vehicles = $vehroute->vehicles;
        if (!empty($vehicles)) {
            foreach ($vehicles as $key => $value) {
                ?>

                                                                                <option value="<?php echo $value->vec_route_id ?>" <?php echo set_select('vehroute_id', $value->vec_route_id); ?> data-fee="<?php echo $vehroute->fare; ?>">
                                                                                    <?php echo $value->vehicle_no ?>
                                                                                </option>
                                                                                <?php
}
        }
        ?>
                                                                    </optgroup>
                                                                    <?php
}
    ?>
                                                            </select>
                                                            <span class="text-danger"><?php echo form_error('transport_fees'); ?></span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        <?php }?>
                                              <?php

if ($this->module_lib->hasActive('hostel')) {
    ?>
<?php if ($sch_setting->hostel_id) {  ?>
                                                 <div class="tshadow mb25 bozero">
                                                <h4 class="pagetitleh2">
                                                    <?php echo $this->lang->line('hostel'); ?></label> <?php echo $this->lang->line('details'); ?></label>
                                                </h4>

                                                <div class="row around10">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('hostel'); ?></label>

                                                            <select class="form-control" id="hostel_id" name="hostel_id">

                                                                <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                                <?php

    foreach ($hostelList as $hostel_key => $hostel_value) {
        ?>

    <option value="<?php echo $hostel_value['id'] ?>" <?php echo set_select('hostel_id', $hostel_value['id']); ?>>
        <?php

        echo $hostel_value['hostel_name']; ?>
                                                                                </option>
                                                                                <?php
}
    ?>
                                                            </select>
                                                            <span class="text-danger"><?php echo form_error('hostel_id'); ?></span>
                                                        </div>
                                                    </div>
 <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('room_no'); ?></label>
                                                            <select  id="hostel_room_id" name="hostel_room_id" class="form-control" >
                                                    <option value=""   ><?php echo $this->lang->line('select'); ?></option>
                                                         </select>
                                                            <span class="text-danger"><?php echo form_error('hostel_room_id'); ?></span>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                    <?php }?> <?php } }?>
                                            
                                            <div id='upload_documents_hide_show'>
                                            <?php   if ($sch_setting->upload_documents) {  ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="tshadow bozero">
                                                            <h4 class="pagetitleh2"><?php echo $this->lang->line('upload_documents'); ?></h4>

                                                            <div class="row around10">
                                                                <div class="col-md-6 upload-docs">
                                                                    <table class="table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <th style="width: 10%">#</th>
                                                                                <th style="width: 45%"><?php echo $this->lang->line('title'); ?></th>
                                                                                <th style="width: 45%"><?php echo $this->lang->line('documents'); ?></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>1.</td>
                                                                                <td>
                                                                                    <input class="form-control" type='text' name='first_title' id="first_title" value="Birth Certificate" readonly="readonly">
                                                                            </td>
                                                                            <td>
                                                                                    <input class="filestyle form-control" type='file' name='first_doc' id="doc1" >
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>2.</td>
                                                                                <td>
                                                                                    <input class="form-control" type='text' name='second_title' id="second_title" value="Group Photo" readonly="readonly">
                                                                                    </td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='second_doc' id="doc1" >
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>3.</td>
                                                                                <td>
                                                                                    <input class="form-control" type='text' name='third_title' id="third_title" value="Passport Photo" readonly="readonly">
                                                                                    </td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='third_doc' id="doc1" >
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>4.</td>
                                                                                <td>
                                                                                    <input class="form-control" type='text' name='fourth_title' id="fourth_title" value="Father Aadhar" readonly="readonly"></td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='fourth_doc' id="doc1" >
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>5.</td>
                                                                                <td>
                                                                                    <input class="form-control" type='text' name='fifth_title' id="fifth_title" value="Mother Aadhar" readonly="readonly">
                                                                                    </td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='fifth_doc' id="doc1" >
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>6.</td>
                                                                                <td>
                                                                                    <input class="form-control" type='text' name='sixth_title' id="sixth_title" value="Child Aadhar" readonly="readonly">
                                                                                    </td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='sixth_doc' id="doc1" >
                                                                                </td>
                                                                            </tr>
                                                                        </tbody></table>
                                                                </div>
                                                                <div class="col-md-6 upload-docs">
                                                                    <table class="table">
                                                                        <tbody><tr>
                                                                                <th style="width: 10%">#</th>
                                                                                <th style="width: 45%"><?php echo $this->lang->line('title'); ?></th>
                                                                                <th style="width: 45%"><?php echo $this->lang->line('documents'); ?></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>7.</td>
                                                                                <td>
                                                                                    <input class="form-control" type='text' name='seventh_title' id="seventh_title" value="Transfer Certificate" readonly="readonly">
                                             
                                                                                </td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='seventh_doc' id="doc1" >
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>8.</td>
                                                                                <td>
                                                                                    <input class="form-control" type='text' name='eigth_title' id="eigth_title" value="Report Card" readonly="readonly">
                                                                                    </td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='eigth_doc' id="doc1" >
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>9.</td>
                                                                                <td>
                                                                                    <input class="form-control" type='text' name='ninth_title' id="ninth_title" value="Medical Form" readonly="readonly"></td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='ninth_doc' id="doc1" >
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>10.</td>
                                                                                <td>
                                                                                    <input class="form-control" type='text' name='tenth_title' id="tenth_title" value="Vaccination Form" readonly="readonly">
                                                                                    </td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='tenth_doc' id="doc1" >
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>11.</td>
                                                                                <td>
                                                                                    <input class="form-control" type='text' name='eleventh_title' id="eleventh_title" value="Transportation Form" readonly="readonly">
                                                                                    </td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='eleventh_doc' id="doc1" >
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>12.</td>
                                                                                <td>
                                                                                    <input class="form-control" type='text' name='twelveth_title' id="twelveth_title" value="Misc" readonly="readonly">
                                                                                </td>
                                                                                <td>
                                                                                    <input class="filestyle form-control" type='file' name='twelveth_doc' id="doc1" >
                                                                                </td>
                                                                            </tr>
                                                                        </tbody></table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            </div>

                                            <div class="tshadow mb25 bozero">
                                                <h4 class="pagetitleh2"><?php echo $this->lang->line('miscellaneous_details'); ?>
                                                </h4>
                                                <?php if ($sch_setting->bank_account_no) { ?>
                                                <div class="row around10">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('bank_account_no'); ?></label>
                                                            <input id="bank_account_no" name="bank_account_no" placeholder="<?php echo $this->lang->line('bank_account_no'); ?>" type="text" class="form-control"  value="<?php echo set_value('bank_account_no'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('bank_account_no'); ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('bank_name'); ?></label>
                                                            <input id="bank_name" name="bank_name" placeholder="<?php echo $this->lang->line('bank_name'); ?>" type="text" class="form-control"  value="<?php echo set_value('bank_name'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('bank_name'); ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('ifsc_code'); ?></label>
                                                            <input id="ifsc_code" name="ifsc_code" placeholder="<?php echo $this->lang->line('ifsc_code'); ?>" type="text" class="form-control"  value="<?php echo set_value('ifsc_code'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('ifsc_code'); ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <div class="row around10">
                                                    
                                                    <?php  if ($sch_setting->local_identification_no) { ?>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">
                                                                <?php echo $this->lang->line('local_identification_no'); ?>
                                                            </label>
                                                            <input id="samagra_id" name="samagra_id" placeholder="<?php echo $this->lang->line('local_identification_no'); ?>" type="text" class="form-control"  value="<?php echo set_value('samagra_id'); ?>" />
                                                            <span class="text-danger"><?php echo form_error('samagra_id'); ?></span>
                                                        </div>
                                                    </div>
                                                    <?php } if ($sch_setting->rte) { ?>
                                                     <div class="col-md-4">
                                                        <label><?php echo $this->lang->line('rte'); ?></label>
                                                        <div class="radio" style="margin-top: 2px;">
                                                            <label><input class="radio-inline" type="radio" name="rte" value="Yes"  <?php
                                                            echo set_value('rte') == "yes" ? "checked" : "";
                                                            ?>  ><?php echo $this->lang->line('yes'); ?></label>
                                                            <label><input class="radio-inline" checked="checked" type="radio" name="rte" value="No" <?php
                                                            echo set_value('rte') == "no" ? "checked" : "";
                                                            ?>  ><?php echo $this->lang->line('no'); ?></label>
                                                        </div>
                                                        <span class="text-danger"><?php echo form_error('rte'); ?></span>
                                                    </div>
                                                    <?php } ?>
                                                    <?php if ($sch_setting->student_note) {  ?>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1"><?php echo $this->lang->line('note'); ?></label>
                                                            <textarea class="form-control" rows="3" placeholder="<?php echo $this->lang->line('note'); ?>" name="note"></textarea>
                                                            <span class="text-danger"><?php echo form_error('note'); ?></span>
                                                        </div>
                                                    </div>
                                                    <?php } ?>


                                                </div>
                                            </div>
                                        </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
</div>
</section>
</div>



<div class="modal fade" id="mySiblingModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title modal_title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="box-body">
                        <div class="sibling_msg">

                        </div>
                        <input  type="hidden" class="form-control" id="transport_student_session_id"  value="0" readonly="readonly"/>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $this->lang->line('class'); ?></label>
                            <div class="col-sm-10">
                                <select  id="sibiling_class_id" name="sibiling_class_id" class="form-control"  >
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                    <?php
foreach ($classlist as $class) {
    ?>
                                        <option value="<?php echo $class['id'] ?>"<?php if (set_value('sibiling_class_id') == $class['id']) {
        echo "selected=selected";
    }
    ?>><?php echo $class['class'] ?></option>
                                        <?php
$count++;
}
?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label"><?php echo $this->lang->line('section'); ?></label>
                            <div class="col-sm-10">
                                <select  id="sibiling_section_id" name="sibiling_section_id" class="form-control">
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                </select>
                                <span class="text-danger" id="transport_amount_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label"><?php echo $this->lang->line('student'); ?>
                            </label>

                            <div class="col-sm-10">
                                <select  id="sibiling_student_id" name="sibiling_student_id" class="form-control" >
                                    <option value=""   ><?php echo $this->lang->line('select'); ?></option>
                                </select>
                                <span class="text-danger" id="transport_amount_fine_error"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-primary add_sibling" id="load" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"><i class="fa fa-user"></i> <?php echo $this->lang->line('add'); ?></button>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">


    $(document).ready(function () {
        var date_format = '<?php echo $result = strtr($this->customlib->getSchoolDateFormat(), ['d' => 'dd', 'm' => 'mm', 'Y' => 'yyyy']) ?>';
        var class_id = $('#class_id').val();
        var section_id = '<?php echo set_value('section_id', 0) ?>';
        var hostel_id = $('#hostel_id').val();
        var hostel_room_id = '<?php echo set_value('hostel_room_id', 0) ?>';
        getHostel(hostel_id, hostel_room_id);
        getSectionByClass(class_id, section_id);

        $(document).on('change', '#class_id', function (e) {
            $('#section_id').html("");
            var class_id = $(this).val();
           getSectionByClass(class_id, 0);
        });



        $('.datetime').datetimepicker({

        });
         $(".color").colorpicker();

        $("#btnreset").click(function () {
            $("#form1")[0].reset();
        });


         $(document).on('change', '#hostel_id', function (e) {
            var hostel_id = $(this).val();
            getHostel(hostel_id, 0);

        });

 function getSectionByClass(class_id, section_id) {

        if (class_id != "" ) {
            $('#section_id').html("");
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
              var url = "<?php $userdata = $this->customlib->getUserData();
if (($userdata["role_id"] == 2)) {echo "getClassTeacherSection";} else {echo "getByClass";}?>";

            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {'class_id': class_id},
                dataType: "json",
                  beforeSend: function(){
                 $('#section_id').addClass('dropdownloading');
                 },
                success: function (data) {
                    $.each(data, function (i, obj)
                    {
                        var sel = "";
                        if (section_id == obj.section_id) {
                            sel = "selected";
                        }
                        div_data += "<option value=" + obj.section_id + " " + sel + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                },
               complete: function(){
              $('#section_id').removeClass('dropdownloading');
               }
            });
        }
    }


    function getHostel(hostel_id, hostel_room_id) {
   if(hostel_room_id == ""){
    hostel_room_id=0;
   }

 if (hostel_id != "") {

          $('#hostel_room_id').html("");


            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
            $.ajax({
                type: "GET",
                url: baseurl + "admin/hostelroom/getRoom",
                data: {'hostel_id': hostel_id},
                dataType: "json",
                 beforeSend: function(){
                 $('#hostel_room_id').addClass('dropdownloading');
                 },
                success: function (data) {
                    $.each(data, function (i, obj)
                    {
                        var sel = "";
                        if (hostel_room_id == obj.id) {
                            sel = "selected";
                        }

                         div_data += "<option value=" + obj.id + " " + sel + ">" + obj.room_no+" ("+obj.room_type+")" + "</option>";

                    });
                    $('#hostel_room_id').append(div_data);
                },
                complete: function(){
              $('#hostel_room_id').removeClass('dropdownloading');
   }
            });
 }
  }

});
function auto_fill_guardian_address() {
    if ($("#autofill_current_address").is(':checked'))
    {
        $('#current_address').val($('#guardian_address').val());
    }
}
function auto_fill_address() {
    if ($("#autofill_address").is(':checked'))
    {
        $('#permanent_address').val($('#address').val());
        $('#permanent_address2').val($('#address2').val());
        $('#permanent_city').val($('#city').val());
        $("#permanent_country_id option[value='"+$('#country_id').val()+"']").prop('selected', true);
        $.ajax({
            url: '<?php echo site_url('admin/enquiry/getStates') ?>',
            type: 'POST',
            dataType: 'json',
            data: {country_id: $('#permanent_country_id').val()},
            success: function(data) {
               // console.log(data);
                $('#permanent_state_id').empty().append(data);                        
            }
        });
        $("#permanent_state_id option[value='"+$('#state').val()+"']").prop('selected', true);
        $('#permanent_pincode').val($('#pincode').val());
    }
}
    $('input:radio[name="guardian_is"]').change(
            function () {
                if ($(this).is(':checked')) {
                    var value = $(this).val();
                    if (value == "father") {
                        $('#guardian_name').val($('#father_name').val()+' '+$('#father_lastname').val());
                        $('#guardian_email').val($('#father_email').val());
                        $('#guardian_phone').val($('#father_phone').val());
                        $('#guardian_occupation').val($('#father_occupation').val());
                        $('#guardian_relation').val("Father")
                    } else if (value == "mother") {
                        $('#guardian_name').val($('#mother_name').val()+' '+$('#mother_lastname').val());
                        $('#guardian_email').val($('#mother_email').val());
                        $('#guardian_phone').val($('#mother_phone').val());
                        $('#guardian_occupation').val($('#mother_occupation').val());
                        $('#guardian_relation').val("Mother")
                    } else {
                        $('#guardian_name').val("");
                        $('#guardian_email').val("");
                        $('#guardian_phone').val("");
                        $('#guardian_occupation').val("");
                        $('#guardian_relation').val("")
                    }
                }
            });


</script>

<script type="text/javascript">
    $(".mysiblings").click(function () {
        $('.sibling_msg').html("");
        $('.modal_title').html('<b>' + "<?php echo $this->lang->line('sibling'); ?>" + '</b>');
        $('#mySiblingModal').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    });
</script>

<script type="text/javascript">
    $(document).on("change", "#country_id", function(e) {
            e.preventDefault();
            _this = $(this);
            var country_id = _this.val();
            if (_this.val() != '') {
                $.ajax({
                    url: '<?php echo site_url('admin/enquiry/getStates') ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {country_id: _this.val()},
                    success: function(data) {
                       // console.log(data);
                        $('#state').empty().append(data);                        
                    }
                });
            }
    });
    $(document).on("change", "#permanent_country_id", function(e) {
            e.preventDefault();
            _this = $(this);
            var country_id = _this.val();
            if (_this.val() != '') {
                $.ajax({
                    url: '<?php echo site_url('admin/enquiry/getStates') ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {country_id: _this.val()},
                    success: function(data) {
                       // console.log(data);
                        $('#permanent_state_id').empty().append(data);                        
                    }
                });
            }
    });
    $(document).on('change', '#sibiling_class_id', function (e) {
        $('#sibiling_section_id').html("");
        var class_id = $(this).val();
        var base_url = '<?php echo base_url() ?>';
        var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
        $.ajax({
            type: "GET",
            url: base_url + "sections/getByClass",
            data: {'class_id': class_id},
            dataType: "json",
            success: function (data) {
                $.each(data, function (i, obj)
                {
                    div_data += "<option value=" + obj.section_id + ">" + obj.section + "</option>";
                });
                $('#sibiling_section_id').append(div_data);
            }
        });
    });

    $(document).on('change', '#sibiling_section_id', function (e) {
        getStudentsByClassAndSection();
    });

    function getStudentsByClassAndSection() {
        $('#sibiling_student_id').html("");
        var class_id = $('#sibiling_class_id').val();
        var section_id = $('#sibiling_section_id').val();
        var student_id = '<?php echo set_value('student_id') ?>';
        var base_url = '<?php echo base_url() ?>';
        var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
        $.ajax({
            type: "GET",
            url: base_url + "student/getByClassAndSection",
            data: {'class_id': class_id, 'section_id': section_id},
            dataType: "json",
            success: function (data) {
                $.each(data, function (i, obj)
                {
                    var sel = "";
                    if (section_id == obj.section_id) {
                        sel = "selected=selected";
                    }
                    div_data += "<option value=" + obj.id + ">" + obj.firstname + " " + obj.lastname +" ("+obj.roll_no+") "+ "</option>";
                });
                $('#sibiling_student_id').append(div_data);
            }
        });
    }

    $(document).on('click', '.add_sibling', function () {
        var student_id = $('#sibiling_student_id').val();
        var base_url = '<?php echo base_url() ?>';
        if (student_id.length > 0) {
            $.ajax({
                type: "GET",
                url: base_url + "student/getStudentRecordByID",
                data: {'student_id': student_id},
                dataType: "json",
                success: function (data) {
                    $('#sibling_name').text("Sibling: " + data.firstname + " " + data.lastname);
                    $('#sibling_name_next').val(data.firstname + " " + data.lastname);
                    $('#sibling_id').val(student_id);
                    $('#father_name').val(data.father_name);
                    $('#father_phone').val(data.father_phone);
                    $('#father_occupation').val(data.father_occupation);
                    $('#mother_name').val(data.mother_name);
                    $('#mother_phone').val(data.mother_phone);
                    $('#mother_occupation').val(data.mother_occupation);
                    $('#guardian_name').val(data.guardian_name);
                    $('#guardian_relation').val(data.guardian_relation);
                    $('#guardian_address').val(data.guardian_address);
                    $('#guardian_phone').val(data.guardian_phone);
                    $('#state').val(data.state);
                    $('#city').val(data.city);
                    $('#pincode').val(data.pincode);
                    $('#current_address').val(data.current_address);
                    $('#permanent_address').val(data.permanent_address);
                    $('#guardian_occupation').val(data.guardian_occupation);
                    $("input[name=guardian_is][value='" + data.guardian_is + "']").prop("checked", true);
                    $('#mySiblingModal').modal('hide');
                }
            });
        } else {
            $('.sibling_msg').html("<div class='alert alert-danger'>No Student Selected</div>");
        }

    });
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/js/savemode.js"></script>