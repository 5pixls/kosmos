<div class="content-wrapper" style="min-height: 348px;">  
    <section class="content-header">
        <h1>
            <i class="fa fa-ioxhost"></i> <?php echo $this->lang->line('front_office'); ?></h1> 
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> <?php echo $this->lang->line('select_criteria'); ?></h3>
                    </div>
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('msg') ?>
                    </div>
                    <form role="form" action="<?php echo site_url('registrationfee') ?>" method="post" class="">
                        <div class="box-body row">

                            <?php echo $this->customlib->getCSRF(); ?>

                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label><?php echo "Registration Fee"; ?> <?php echo $this->lang->line('date'); ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" readonly="" autocomplete="off" name="registration_fee_date" class="form-control pull-right date" id="enquiry_date">
                                    </div>
                                                      <!-- <input type="text" class="form-control" autocomplete="off"  name="enquiry_date" id="enquiry_date">
                                    -->  <span class="text-danger"><?php echo form_error('enquiry_date'); ?></span>
                                </div>
                            </div> 

                            <div class="col-sm-4 col-md-4">
                                <div class="form-group">  
                                    <label><?php echo "Search"; ?></label>
                                    <input autofocus="" id="search_query" name="search_query" placeholder="" type="text" class="form-control" value="<?php echo $search_query;?>" autocomplete="off">
                                    <span class="text-danger"><?php echo form_error('search_query'); ?></span>
                                </div>  
                            </div>
                            

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" name="search" value="search_filter" class="btn btn-primary btn-sm checkbox-toggle pull-right"><i class="fa fa-search"></i> <?php echo $this->lang->line('search'); ?></button>
                                </div>
                            </div>
                       </div>     
                    </form>
            <div class="ptt10">

                <div class="bordertop">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix"> <?php echo "Registration Fee List";//$this->lang->line('front_office'); ?></h3>
                        
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="download_label"><?php echo "Registration"; ?> <?php echo $this->lang->line('list'); ?></div>
                        <div class="mailbox-messages">
                          <div class="table-responsive">  
                            <table class="table table-hover table-striped table-bordered" id="enquirytable">
                                <thead>
                                    <tr>
                                        <th style="display:none;"></th>
                                        <th>Enquiry ID</th>
                                        <th>Child Name</th>
                                        <th>Father Name</th>
                                        <th>Father Mobile</th>
                                        <th>Fee Amount</th>
                                        <th>Payment Id</th>
                                        <th>Type</th>
                                        <th>Payment Status</th>
                                        <th>Reg. Fee Deposit Date</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //echo "<pre>"; print_r($enquiry_list); echo "<pre>";die;
                                    if (empty($registration_fee_list)) {
                                        ?>
                                        <?php
                                    } else {

                                        foreach ($registration_fee_list as $key => $value) {
                                            
                                            ?>
                                            <tr>
                                                <td  style="display:none;"><?php echo $value['id']; ?></td>
                                                <td class="mailbox-name"><a href="<?php echo 'admin/enquiry/leaddetails/'.$value['enquiry_id'];?>"><?php echo $value['enquiryid']; ?></a></td>
                                                <td class="mailbox-name"><a href="<?php echo 'admin/enquiry/leaddetails/'.$value['enquiry_id'];?>"><?php echo $value['name']; ?> <?php echo $value['last_name']; ?></a></td>
                                                <td class="mailbox-name"><?php echo $value['father_firstname']." ".$value['father_lastname']; ?> </td>
                                                <td class="mailbox-name"><?php echo $value['father_mobile']; ?> </td>
                                                <td class="mailbox-name">Rs <?php echo $value['fee_amount']/100; ?></td>
                                                <td class="mailbox-name"><?php echo $value['razorpay_payment_id']; ?></td>
                                                <td class="mailbox-name"><?php echo ($value['type'] == 1)?"<span class='label label-info' style='background-color:#f39c12!important'>Registration</span>":"<span class='label label-info' style='background-color:#12a0f3!important'>Admission</span>"; ?></td>
                                                <td class="mailbox-name" style="text-align:center;"><span class="<?php echo ($value['status'] == "authorized")?"label label-info ":"label label-success"; ?>"><?php echo $value['status']; ?></span></td>
                                                <td class="mailbox-name" style="text-align:center;"> <?php
                                            if (!empty($value["created_at"])) {
                                                echo date($this->customlib->getSchoolDateFormat(), strtotime($value['created_at']));
                                            }
                                            ?></td>

                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table><!-- /.table -->
                          </div>  
                        </div><!-- /.mail-box-messages -->
                    </div><!-- /.box-body -->
                </div>
            </div>
          </div>  
        </div>

    </section>
    
    
</div>


<script type="text/javascript">
    $(document).ready(function () {


        $("#enquirytable").DataTable({
            pageLength: 20,
            searching: true,
            order : [[0, 'desc']],
            paging: true,
            bSort: true,
            info: true,
            dom: "Bfrtip",
            buttons: [

                {
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',

                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>',
                    titleAttr: 'CSV',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'

                    }
                },

                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    title: $('.download_label').html(),
                    customize: function (win) {
                        $(win.document.body)
                                .css('font-size', '10pt');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    },
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'colvis',
                    text: '<i class="fa fa-columns"></i>',
                    titleAttr: 'Columns',
                    title: $('.download_label').html(),
                    postfixButtons: ['colvisRestore']
                },
            ]
        });
    });




</script>