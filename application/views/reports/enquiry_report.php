
<div class="content-wrapper" style="min-height: 348px;">  
    <section class="content-header">
        <h1>
            <i class="fa fa-ioxhost"></i> <?php echo $this->lang->line('front_office'); ?></h1> 
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> <?php echo $this->lang->line('select_criteria'); ?></h3>
                        
                    </div>
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('msg') ?>
                    </div>
                    <form role="form" action="<?php echo site_url('report/enquiryreport') ?>" method="post" class="">
                        <div class="box-body row">

                            <?php echo $this->customlib->getCSRF(); ?>
                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('enquiry'); ?> <?php echo $this->lang->line('date'); ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" readonly="" autocomplete="off" name="enquiry_date" class="form-control pull-right date" id="enquiry_date">
                                    </div>
                                                      <!-- <input type="text" class="form-control" autocomplete="off"  name="enquiry_date" id="enquiry_date">
                                    -->  <span class="text-danger"><?php echo form_error('enquiry_date'); ?></span>
                                </div>
                            </div> 
                            <div class="col-sm-1" style="padding-top: 25px;">

                            <div class="form-group">
                                    <button type="submit" name="search" value="search_filter" class="btn btn-primary btn-sm checkbox-toggle pull-right"><i class="fa fa-search"></i> <?php echo $this->lang->line('search'); ?></button>
                                </div>
                            </div>
                            <div class="col-sm-8 col-md-8"></div>

                            
                            
                                
                       </div>     
                    </form>
            <div class="ptt10">

                <div class="bordertop">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix">Enquiry by Class/Status Report</h3>
                        
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="download_label"><?php echo $this->lang->line('admission_enquiry'); ?> <?php echo $this->lang->line('list'); ?></div>
                        <div class="mailbox-messages">
                          <div class="table-responsive"> 
                          <?php $statuses=$this->config->item('enquiry_status'); ?> 
                            <table class="table2 table-hover no-footer" id="enquirytable">
                                <thead>
                                    <tr>
                                        <th>Class</th>
                                        <?php foreach ($statuses as $key => $value) {?>
                                        <th><?php echo $value?></th>
                                        <?php } ?> 
                                        <th>Total</th>                  
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (empty($enquiry_report)) {
                                        ?>
                                        <?php
                                    } else {
                                        $statuses=$this->config->item('enquiry_status');
                                        $status_color=$this->config->item('status_color');
                                        foreach ($enquiry_report as $key => $value) {
                                            ?>
                                            <tr>
                                                <td class="mailbox-name" style="font-weight: bold;background: #bee1f8 !important;"><?php echo $value['class']; ?></td>
                                                <?php foreach ($value['statuses'] as $s) {?>
                                                  
                                                <td class="mailbox-name" style="text-align:center;">
                                                <?php echo $s['count']; ?></td>
                                                <?php } ?>
                                                <td class="mailbox-name" style="font-weight: bold;background: #bee1f8 !important;text-align:center;"><?php echo $value['sum']; ?></td>
                                            </tr>
                                        <?php
                                    }
                                }
                                if (!empty($enquiry_report_total)) { 
                                    ?>      
                                    <tr> 
                                        <td class="mailbox-name" style="font-weight: bold;background: #bee1f8 !important;">Total</td>       
                                        <?php                         
                                        $statuses=$this->config->item('enquiry_status');
                                        $sum=0;
                                        foreach ($enquiry_report_total as $key => $value) {
                                            ?>
                                            
                                                <td class="mailbox-name" style="font-weight: bold;text-align: center;background: #bee1f8 !important;"><?php echo $value['count'];
                                                $sum+= $value['count'];
                                                ?></td>
                                            
                                            <?php
                                        }?>
                                        <td class="mailbox-name" style="font-weight: bold;background: #bee1f8 !important;text-align: center;"><?php  echo $sum;?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                          </div>  
                        </div><!-- /.mail-box-messages -->
                    <!-- /.box-body -->
                    </div>
                    <div class="box-body">
                        <div class="box-header with-border">
                        <h3 class="box-title titlefix">Enquiry by Class/Source Report</h3>
                        
                    </div>
                     <div class="mailbox-messages">
                          <div class="table-responsive"> 
                          <?php $statuses=$this->config->item('enquiry_status'); ?> 
                            <table class="table2 table-hover no-footer" id="enquirytable">
                                <thead>
                                    <tr>
                                        <th>Class</th>
                                        <?php 
                                        foreach ($sources as $key => $value) {?>
                                        <th><?php echo $value['source']?></th>
                                        <?php } ?> 
                                        <th>Total</th>                  
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (empty($enquiry_report_class)) {
                                        
                                    } else {
                                        $statuses=$this->config->item('enquiry_status');
                                        $status_color=$this->config->item('status_color');
                                        //print_r($enquiry_report);
                                        foreach ($enquiry_report_class as $key => $value) {
                                            ?>
                                            <tr>
                                                <td class="mailbox-name" style="font-weight: bold;background: #bee1f8 !important;"><?php echo $value['class']; ?></td>
                                                <?php 
                                                foreach ($value['sources'] as $s) {?>
                                                  
                                                <td class="mailbox-name" style="text-align:center;">
                                                <?php echo $s['count']; ?></td>
                                                <?php } ?>
                                                <td class="mailbox-name" style="font-weight: bold;background: #bee1f8 !important;text-align:center;"><?php echo $value['sum']; ?></td>
                                            </tr>
                                        <?php
                                    }
                                }
                                if (!empty($enquiry_report_class_total)) { 
                                    ?>      
                                    <tr> 
                                        <td class="mailbox-name" style="font-weight: bold;background: #bee1f8 !important;">Total</td>       
                                        <?php                         
                                        $statuses=$this->config->item('enquiry_status');
                                        $sum=0;
                                        foreach ($enquiry_report_class_total as $key => $value) {
                                            ?>
                                            
                                                <td class="mailbox-name" style="font-weight: bold;text-align: center;background: #bee1f8 !important;"><?php echo $value['count'];
                                                $sum+= $value['count'];
                                                ?></td>
                                            
                                            <?php
                                        }?>
                                        <td class="mailbox-name" style="font-weight: bold;background: #bee1f8 !important;text-align: center;"><?php  echo $sum;?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                          </div>  
                        </div><!-- /.mail-box-messages -->
                    </div>  
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix">Enquiry by Source/Status Report</h3>
                        
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="download_label"><?php echo $this->lang->line('admission_enquiry'); ?> <?php echo $this->lang->line('list'); ?></div>
                        <div class="mailbox-messages">
                          <div class="table-responsive"> 
                          <?php $statuses=$this->config->item('enquiry_status'); ?> 
                            <table class="table2 table-hover no-footer" id="enquirytable">
                                <thead>
                                    <tr>
                                        <th>Source</th>
                                        <?php foreach ($statuses as $key => $value) {?>
                                        <th><?php echo $value?></th>
                                        <?php } ?> 
                                        <th>Total</th>                  
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (empty($enquiry_report_source)) {
                                        ?>
                                        <?php
                                    } else {
                                        $statuses=$this->config->item('enquiry_status');
                                        $status_color=$this->config->item('status_color');
                                        //print_r($enquiry_report);
                                        foreach ($enquiry_report_source as $key => $value) {
                                            ?>
                                            <tr>
                                                <td class="mailbox-name" style="font-weight: bold;background: #bee1f8 !important;"><?php echo $value['source']; ?></td>
                                                <?php foreach ($value['statuses'] as $s) {?>
                                                  
                                                <td class="mailbox-name" style="text-align:center;">
                                                <?php echo $s['count']; ?></td>
                                                <?php } ?>
                                                <td class="mailbox-name" style="font-weight: bold;background: #bee1f8 !important;text-align:center;"><?php echo $value['sum']; ?></td>
                                            </tr>
                                        <?php
                                    }
                                }
                                if (!empty($enquiry_report_source_total)) { 
                                    ?>      
                                    <tr> 
                                        <td class="mailbox-name" style="font-weight: bold;background: #bee1f8 !important;">Total</td>       
                                        <?php                         
                                        $statuses=$this->config->item('enquiry_status');
                                        $sum=0;
                                        foreach ($enquiry_report_source_total as $key => $value) {
                                            ?>
                                            
                                                <td class="mailbox-name" style="font-weight: bold;text-align: center;background: #bee1f8 !important;"><?php echo $value['count'];
                                                $sum+= $value['count'];
                                                ?></td>
                                            
                                            <?php
                                        }?>
                                        <td class="mailbox-name" style="font-weight: bold;background: #bee1f8 !important;text-align: center;"><?php  echo $sum;?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                          </div>  
                        </div><!-- /.mail-box-messages -->
                    </div><!-- /.box-body -->  
                </div>
            </div>
          </div>  
        </div>
        </div>
    </section>
</div>
                
                                    
<script>
    $(document).ready(function () {
        var date_format = '<?php echo $result = strtr('d/m/Y', ['d' => 'dd', 'm' => 'mm', 'Y' => 'yyyy',]) ?>';
        var currentDate = new Date();
        var sDate = '<?php echo date('d/m/Y', strtotime($sDate)); ?>';
        var eDate = '<?php echo date('d/m/Y', strtotime($eDate)); ?>';
        $('#enquiry_date').daterangepicker({
            autoUpdateInput: true,
            format: date_format,
            // endDate: "today",
            maxDate: currentDate,
            autoclose: true,
             opens: 'right',
            locale: {
                format: 'DD/MM/YYYY',
                cancelLabel: 'Clear'
            }           
        });
        $("#enquiry_date").data('daterangepicker').setStartDate(sDate);
        $("#enquiry_date").data('daterangepicker').setEndDate(eDate);


        $( "#enquiry_date" ).val( sDate + " - " + eDate );
        //$( "#enquiry_date" ).data('daterangepicker').startDate = moment( sDate, "DD/MM/YYYY" );
        //$( "#enquiry_date" ).data('daterangepicker').endDate = moment( eDate, "DD/MM/YYYY" );
        $( "#enquiry_date" ).data('daterangepicker').updateView();
        $( "#enquiry_date" ).data('daterangepicker').updateCalendars();
        /*,
            startDate:sDate.format('MM/DD/YYYY'),
            endDate:eDate.format('MM/DD/YYYY')*/
        $('#enquiry_date').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });

        $('#enquiry_date').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
        
        /*$("#enquirytable").DataTable({
           // pageLength: 10,
            searching: true,
           // order : [[5, 'desc']],
            paging: true,
            bSort: true,
            info: false,
            dom: "Bfrtip",
            buttons: [

                {
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',

                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>',
                    titleAttr: 'CSV',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'

                    }
                },

                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    title: $('.download_label').html(),
                    customize: function (win) {
                        $(win.document.body)
                                .css('font-size', '10pt');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    },
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'colvis',
                    text: '<i class="fa fa-columns"></i>',
                    titleAttr: 'Columns',
                    title: $('.download_label').html(),
                    postfixButtons: ['colvisRestore']
                },
            ]
        });*/
    });




</script>