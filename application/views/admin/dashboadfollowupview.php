<table class="table table-hover table-striped table-bordered" id="followuptable">
                                            <thead>
                                                <tr>
                                                    <th>Enquiry ID</th>
                                                    <th>Father Name</th>
                                                    <th>Followup Date</th>
                                                    <th>Followup Reason</th>
                                                </tr>
                                            </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($followups)) {
                                                foreach ($followups as $key => $value){
                                                    ?>
                                                    <tr>   
                                                        <td class="mailbox-name"><?php echo $value['enquiryid']; ?></td>
                                                        <td class="mailbox-name"><?php echo $value['father_title'].' '.$value['father_firstname'].' '.$value['father_lastname']; 
                                                        ?> </td>

                                                        <td class="mailbox-name"> <?php
                                                    if (!empty($value["next_date"])) {
                                                        echo date('d-M-Y h:i:s', strtotime($value['next_date']));
                                                    }
                                                    ?></td>
                                                    <td class="mailbox-name"><?php echo $value['response'].'-'.$value['other_response']; ?></td>

                                                        
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table><!-- /.table -->