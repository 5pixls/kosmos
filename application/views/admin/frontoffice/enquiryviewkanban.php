<link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/planner-style.css?v=1.3">
<div class="content-wrapper" style="min-height: 348px;">  
    <section class="content-header">
        <h1>
            <i class="fa fa-ioxhost"></i> <?php echo $this->lang->line('front_office'); ?></h1> 
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php $column=$this->config->item('status_color');?>
                <div class="box box-primary enquiry-content-wrap">
                    <div class="box-header with-border">
                        <h3 class="box-title enquiry-heading"><i class="fa fa-search"></i> Leads Kanban</h3>
                        <div id="reportrange" class="date-range-filter">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>
                        <div class="col-md-12 top-progress-cards">
                            <?php 
                                foreach ($enquiry_status as $key => $val) : 
                                   // if($key != 10) {?>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                  <div class="topprograssstart">
                                    <p class="text-uppercase mt5 clearfix"><i class="fa fa-ioxhost ftlayer"></i><span class="topprograssstart-text">
                                        <span class="kosmos-status" <?php if ($column[$key]) : ?>style="border-color:<?php echo $column[$key]; ?>; color:<?php echo $column[$key]; ?>" <?php endif; ?>><?php echo $val;?></span>
                                        </span><span class="pull-right"><?php echo ($enquiry_counts[$key]['count']) ? $enquiry_counts[$key]['count'] : 0; ?></span>
                                   </p>
                                  <div class="progress-group">
                                     <div class="progress progress-minibar">
                                          <div class="progress-bar progress-bar-aqua" style="width:100%"></div>
                                        </div>
                                      </div>
                                  </div><!--./topprograssstart-->
                                </div>
                                <?php //}
                            endforeach; ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <!----Kanban code-->                        
                        <div id="planner-container" class="limited planner-boards">

                            <div class="planner-header">
                                <?php 
                                foreach ($enquiry_status as $key => $val) : 
                                    //if($key != 10) {?>
                                    <div class="column" <?php if ($column[$key]) : ?>style="background-color:<?php echo $column[$key]; ?>" <?php endif; ?>>
                                        <h5 data-toggle="tooltip" title="<?php echo $val; ?>"><?php echo $val; ?></h5>
                                    </div>
                                <?php //}
                            endforeach; ?>
                            </div>
                            <div class="planner-body">
                                <?php foreach ($enquiry_status as $key => $value) : 
                                    //if($key != 10) {?>
                                    <div class="column sortable" data-update="column" data-column="<?php echo $key; ?>">
                                        <?php
                                        $this->load->view('admin/frontoffice/planner-column', array('tasks' => $enquiry_list, 'color'=> $column[$key],'column' => $key, 'viewMode' => 'columns')); ?>
                                    </div>
                                <?php //}
                            endforeach; ?>
                            </div>
                        </div>

                        <script src='<?php echo base_url(); ?>backend/dist/js/jquery.cookie.js'></script>
                        <script src='<?php echo base_url(); ?>backend/dist/js/planner.js'></script>

                        <!-- kanban code end -->
                     </div>
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('msg') ?>
                    </div>
                    
           
          </div>  
        </div>
        </div> 
    </section>

    <div class="modal fade" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-media-content">
                <div class="modal-header modal-media-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="box-title"><?php echo $this->lang->line('edit_admission_enquiry'); ?></h4>

                </div>
                <div class="modal-body pt0 pb0" id="getdetails">
                    <div id="alert_message">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="follow_up" tabindex="-1" role="dialog" aria-labelledby="follow_up">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-media-content">
                <div class="modal-header modal-media-header">
                    <button type="button" class="close" onclick="update()" data-dismiss="modal">&times;</button>
                    <h4 class="box-title"><?php echo $this->lang->line('follow_up_admission_enquiry'); ?></h4>
                </div>
                <div class="modal-body pt0 pb0 follow_up_body" id="getdetails_follow_up">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        // $('#enquiry_date').daterangepicker();
        var date_format = '<?php echo $result = strtr($this->customlib->getSchoolDateFormat(), ['d' => 'dd', 'm' => 'mm', 'Y' => 'yyyy',]) ?>';

       

        $('#enquiry_date').daterangepicker({
            autoUpdateInput: false,
            format: date_format,
            autoclose: true,
             opens: 'right',
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#enquiry_date').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });

        $('#enquiry_date').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
        $(document).on("change", "#country_id", function(e) {
            e.preventDefault();
            _this = $(this);
            var country_id = _this.val();
            if (_this.val() != '') {
                $.ajax({
                    url: '<?php echo site_url('admin/enquiry/getStates') ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {country_id: _this.val()},
                    success: function(data) {
                       // console.log(data);
                       var form_id=_this.closest('form').attr("id");
                        $('#'+form_id+' #state_id').empty().append(data);                        
                    }
                });
            }
        });


    });

   

    function getRecord(id,status) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/details/' + id+'/'+status,
            success: function (result) {
                $('#getdetails').html(result);
            }
        });
    }
    function postRecord(id) {

        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/editpost/' + id,
            type: 'POST',
            data: $("#myForm1").serialize(),
            dataType: 'json',
            success: function (data) {

                if (data.status == "fail") {

                    var message = "";
                    $.each(data.error, function (index, value) {

                        message += value;
                    });
                    errorMsg(message);
                } else {

                    successMsg(data.message);
                    window.location.reload(true);
                }
            },
            error: function () {
                alert("Fail")
            }
        });

    }

    function saveEnquiry() {


        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/add/',
            type: 'POST',
            dataType: 'json',
            data: $("#formadd").serialize(),
            success: function (data) {
                if (data.status == "fail") {

                    var message = "";
                    $.each(data.error, function (index, value) {

                        message += value;
                    });
                    errorMsg(message);
                } else {

                    successMsg(data.message);
                    window.location.reload(true);
                }

            },
            error: function () {
                alert("Fail")
            }
        });


    }


    function delete_enquiry(id) {

        if (confirm('<?php echo $this->lang->line('delete_confirm') ?>')) {
            $.ajax({
                url: '<?php echo base_url(); ?>admin/enquiry/delete/' + id,
                type: 'POST',
                dataType: 'json',

                success: function (data) {
                    if (data.status == "fail") {

                        var message = "";
                        $.each(data.error, function (index, value) {

                            message += value;
                        });
                        errorMsg(message);
                    } else {

                        successMsg(data.message);
                        window.location.reload(true);
                    }

                }
            })

        }

    }


function get_call_log_list(id){
    $.ajax({
        url: '<?php echo base_url(); ?>admin/enquiry/follow_up_call_loglist/' + id,
        success: function (data) {
            $('#followUp_tab_lead_calldetails').html(data);
            $("#calllogtable").DataTable({
                    pageLength: 10,
                    searching: true,
                    paging: true,
                    bSort: true,
                    info: false,
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'Copy',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            titleAttr: 'Excel',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i>',
                            titleAttr: 'CSV',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            titleAttr: 'PDF',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'

                            }
                        },

                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            titleAttr: 'Print',
                            title: $('.download_label').html(),
                            customize: function (win) {
                                $(win.document.body)
                                        .css('font-size', '10pt');
                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'colvis',
                            text: '<i class="fa fa-columns"></i>',
                            titleAttr: 'Columns',
                            title: $('.download_label').html(),
                            postfixButtons: ['colvisRestore']
                        },
                    ]
                });
        },
        error: function () {
            alert("Fail")
        }
    });
}   

function get_lead_log_list(id){
    $.ajax({
        url: '<?php echo base_url(); ?>admin/enquiry/lead_loglist/' + id,
        success: function (data) {
            $('#followUp_tab_lead_logdetails').html(data);
            $("#leadlogtable").DataTable({
                    pageLength: 10,
                    searching: true,
                    paging: true,
                    bSort: true,
                    info: false,
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'Copy',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            titleAttr: 'Excel',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i>',
                            titleAttr: 'CSV',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            titleAttr: 'PDF',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'

                            }
                        },

                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            titleAttr: 'Print',
                            title: $('.download_label').html(),
                            customize: function (win) {
                                $(win.document.body)
                                        .css('font-size', '10pt');
                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'colvis',
                            text: '<i class="fa fa-columns"></i>',
                            titleAttr: 'Columns',
                            title: $('.download_label').html(),
                            postfixButtons: ['colvisRestore']
                        },
                    ]
                });
        },
        error: function () {
            alert("Fail")
        }
    });
}

function follow_up(id, status) {
         
            $.ajax({
                url: '<?php echo base_url(); ?>admin/enquiry/follow_up/' + id + '/' + status,
                success: function (data) {
                    $('#getdetails_follow_up').html(data);
                    $.ajax({
                        url: '<?php echo base_url(); ?>admin/enquiry/follow_up_list/' + id,
                        success: function (data) {
                            $('#timeline').html(data);
                            get_follow_up_list(id);
                            get_call_log_list(id);
                            get_lead_log_list(id);
                        },
                        error: function () {
                            alert("Fail")
                        }
                    });
                },
                error: function () {
                    alert("Fail")
                }
            });
        }
    function get_follow_up_list(id){
        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/follow_up_list/' + id,
            success: function (data) {
                $('#timeline').html(data);
            },
            error: function () {
                alert("Fail")
            }
        });
    }

    function update() {

        window.location.reload(true);
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {

        $("#reportrange").hide();
        $("#enquirytable").DataTable({
            searching: true,

            paging: true,
            bSort: true,
            info: false,
            dom: "Bfrtip",
            buttons: [

                {
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',

                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>',
                    titleAttr: 'CSV',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'

                    }
                },

                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    title: $('.download_label').html(),
                    customize: function (win) {
                        $(win.document.body)
                                .css('font-size', '10pt');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    },
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'colvis',
                    text: '<i class="fa fa-columns"></i>',
                    titleAttr: 'Columns',
                    title: $('.download_label').html(),
                    postfixButtons: ['colvisRestore']
                },
            ]
        });
    });




</script>