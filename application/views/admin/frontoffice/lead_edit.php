<?php 
    $gender_list=$this->config->item('gender_list');
$nationality_list=$this->config->item('nationality_list');
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-user-plus"></i> <?php echo $this->lang->line('student_information'); ?>            
        </h1>
    </section>
    <!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    </div>
                <div class="col-md-8" style="float: right;" >
                    <?php if ($this->rbac->hasPrivilege('follow_up_admission_enquiry', 'can_view')) { ?>
                                                        <a class="btn btn-info pull-right" style="margin: 3px;" onclick="follow_up('<?php echo $enquiry_data['id']; ?>','<?php echo $enquiry_data['status']; ?>');"  data-target="#follow_up" data-toggle="modal"  title="<?php echo $this->lang->line('follow_up_admission_enquiry'); ?>">
                                                            <i class="fa fa-phone"></i> Call
                                                        </a>
                                                    <?php }
                                                    ?>
                    <?php if($enquiry_data['status'] < 3 || !$payment) {?>
                    <a onclick="sendRegistrationLink(<?php echo $enquiry_data['id'] ?>)" class="btn btn-info pull-right" style="margin: 3px;">Send Registration Link</a>
                    <?php } ?>
                    <?php if($enquiry_data['status'] == 3 || $enquiry_data['status'] == 8){ ?>
                    <a class="btn btn-info pull-right" style="margin: 3px;"   data-target="#admission_fee" data-toggle="modal"  title="<?php echo "Send Admission Fee Link"; ?>">
                         Send Admission Fee Link
                    </a>
                    <?php } ?>
                    <a onclick="printDiv('tab_lead_view')" class="btn btn-info pull-right" style="margin: 3px;">Print</a>                    
                </div>
            </div>
    </div>
    <div class="row">
        <div class="col-md-12">

                <!-- Custom Tabs (Pulled to the right) -->
                <div class="nav-tabs-custom theme-shadow">
                    <a href="javascript:void(0)" class="active-tab">View Details</a>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_lead_view" data-toggle="tab">View Details</a></li>
                        <li><a href="#tab_lead_details" data-toggle="tab">Lead Details</a></li>
                        <li><a href="#tab_lead_registration" data-toggle="tab">Registration Details</a></li>
                        <li><a href="#tab_lead_questionnaire" data-toggle="tab">Questionnaire Details</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane view-details active" id="tab_lead_view">
                                <div style="margin-bottom: 10px;"><strong>Enquiry ID:</strong> <?php echo $enquiry_data['enquiryid']; ?></div>
                            <div class="tshadow mb25 bozero">
                                    <h4 class="pagetitleh2">Child Details </h4>
                                    <div class="row around10">
                                    <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>First Name:</strong> <?php echo $enquiry_data['name']; ?></label>
                                        <label class="col-sm-3"><strong>Last Name:</strong> <?php echo $enquiry_data['last_name']; ?></label>
                                        <label class="col-sm-3"><strong>Gender:</strong> <?php echo $gender_list[$enquiry_data['gender']];?>
                                       </label>
                                        <label class="col-sm-3"><strong>Date of Birth:</strong> <?php if (!empty($enquiry_data['dob'])) {echo date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($enquiry_data['dob']));
                                    }
                                         ?></label>
                                        <label class="col-sm-3"><strong>Session:</strong> <?php 
                                        foreach ($session_list as $value) {
                                                     if ($value['id']== $enquiry_data['session_id']) echo $value['session']; 
                                        }?>
                                        </label>
                                        <label class="col-sm-3"><strong>Class:</strong> <?php 
                                        foreach ($class_list as $value) {
                                                     if ($value['id']== $enquiry_data['class']) echo $value['class']; 
                                        }?>
                                        </label>
                                        <label class="col-sm-3"><strong>Nationality:</strong> <?php echo $nationality_list[$enquiry_data['nationality']]; ?></label>
                                        <label class="col-sm-3"><strong>Blood Group:</strong> <?php echo $enquiry_data['blood_group']; ?></label>
                                        <label class="col-sm-3"><strong>Child Image:</strong> <?php if($enquiry_data['child_image'] !='') echo "<a href='".base_url().$enquiry_data['child_image']."' target='_blank'><img src='".base_url().$enquiry_data['child_image']."' width='20px'></a>"; ?></label>
                                        <label class="col-sm-3"><strong>Mother Tongue:</strong> <?php echo $enquiry_data['mother_tongue']; ?></label>
                                        <label class="col-sm-3"><strong>Home Town:</strong> <?php echo $enquiry_data['home_town']; ?></label>
                                        <label class="col-sm-3"><strong>Locality:</strong> <?php echo $enquiry_data['locality']; ?></label>
                                        <label class="col-sm-3"><strong>Religion:</strong> <?php echo $enquiry_data['religion']; ?></label>
                                        <label class="col-sm-3"><strong>Social Category:</strong> <?php 
                                        foreach ($categorylist as $value) {
                                                     if ($value['id']== $enquiry_data['social_category']) echo $value['category']; 
                                        }?>
                                        </label>
                                        <label class="col-sm-3"><strong>Child's Aadhaar Card No.:</strong> <?php echo $enquiry_data['child_aadhaar_no']; ?></label>
                                        <label class="col-sm-3"><strong>Previous Result:</strong> <?php if($enquiry_data['previous_result'] !='') echo "<a href='".base_url().$enquiry_data['previous_result']."' target='_blank'>View File</a>"; ?></label>
                                        </div>
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero">
                                    <h4 class="pagetitleh2">Previous School Information</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Last School :</strong> <?php echo $enquiry_data['last_school']; ?></label>
                                            <label class="col-sm-3"><strong>Medium of Instruction:</strong> <?php echo $enquiry_data['medium_instruction']; ?></label>
                                            <label class="col-sm-3"><strong>Board:</strong> <?php echo $enquiry_data['previous_board']; ?></label>
                                            <label class="col-sm-3"><strong>Class:</strong> <?php echo $enquiry_data['previous_class']; ?></label>
                                        </div>
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero">
                                    <h4 class="pagetitleh2">Father Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Title:</strong> <?php echo $enquiry_data['father_title']; ?></label>
                                            <label class="col-sm-3"><strong>First Name:</strong> <?php echo $enquiry_data['father_firstname']; ?></label>
                                            <label class="col-sm-3"><strong>Last Name:</strong> <?php echo $enquiry_data['father_lastname']; ?></label>
                                            <label class="col-sm-3"><strong>Email:</strong> <?php echo $enquiry_data['father_email']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Mobile:</strong> <?php echo $enquiry_data['father_mobile']; ?></label>
                                            <label class="col-sm-3"><strong>Aadhar Card No.:</strong> <?php echo $enquiry_data['father_aadhar_no']; ?></label>
                                            <label class="col-sm-3"><strong>Qualification:</strong> <?php if($enquiry_data['father_qualification']=='Other') {echo $enquiry_data['father_qualification_other'];} else echo $enquiry_data['father_qualification']; ?></label>
                                            <label class="col-sm-3"><strong>Occupation:</strong> <?php if($enquiry_data['father_occupation']=='Other') {echo $enquiry_data['father_occupation_other'];} else echo $enquiry_data['father_occupation']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Employer:</strong> <?php echo $enquiry_data['father_employer']; ?></label>
                                            <label class="col-sm-3"><strong>Designation:</strong> <?php if($enquiry_data['father_designation']=='Other') {echo $enquiry_data['father_designation_other'];} else echo $enquiry_data['father_designation']; ?></label>
                                            <label class="col-sm-3"><strong>Work Address:</strong> <?php echo $enquiry_data['father_address']; ?></label>
                                            <label class="col-sm-3"><strong>Work City:</strong> <?php if($enquiry_data['father_city']=='Other') {echo $enquiry_data['father_city_other'];} else echo $enquiry_data['father_city']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Work Pin Code:</strong> <?php echo $enquiry_data['father_pincode']; ?></label>
                                            <label class="col-sm-3"><strong>Work phone:</strong> <?php echo $enquiry_data['father_workphone']; ?></label>
                                            <label class="col-sm-3"><strong>Annual Income:</strong> <?php echo $enquiry_data['father_annualincome']; ?></label>
                                            <label class="col-sm-3"><strong>Working wih TKS Noida:</strong> <?php echo $enquiry_data['father_woking_tks']; ?></label>  
                                        </div>
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero">
                                    <h4 class="pagetitleh2">Mother Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Title:</strong> <?php echo $enquiry_data['mother_title']; ?></label>
                                            <label class="col-sm-3"><strong>First Name:</strong> <?php echo $enquiry_data['mother_firstname']; ?></label>
                                            <label class="col-sm-3"><strong>Last Name:</strong> <?php echo $enquiry_data['mother_lastname']; ?></label>
                                            <label class="col-sm-3"><strong>Email:</strong> <?php echo $enquiry_data['mother_email']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Mobile:</strong> <?php echo $enquiry_data['mother_mobile']; ?></label>
                                            <label class="col-sm-3"><strong>Aadhar Card No.:</strong> <?php echo $enquiry_data['mother_aadhar_no']; ?></label>
                                            <label class="col-sm-3"><strong>Qualification:</strong> <?php echo $enquiry_data['mother_qualification']; ?></label>
                                            <label class="col-sm-3"><strong>Occupation:</strong> <?php if($enquiry_data['mother_occupation']=='Other') {echo $enquiry_data['mother_occupation_other'];} else echo $enquiry_data['mother_occupation']; ?></label>
                                          </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Employer:</strong> <?php echo $enquiry_data['mother_employer']; ?></label>
                                            <label class="col-sm-3"><strong>Designation:</strong> <?php if($enquiry_data['mother_designation']=='Other') {echo $enquiry_data['mother_designation_other'];} else echo $enquiry_data['mother_designation']; ?></label>
                                            <label class="col-sm-3"><strong>Work Address:</strong> <?php echo $enquiry_data['mother_address']; ?></label>
                                            <label class="col-sm-3"><strong>Work City:</strong> <?php if($enquiry_data['mother_city']=='Other') {echo $enquiry_data['mother_city_other'];} else echo $enquiry_data['mother_city']; ?></label>
                                         </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Work Pin Code:</strong> <?php echo $enquiry_data['mother_pincode']; ?></label>
                                            <label class="col-sm-3"><strong>Work phone:</strong> <?php echo $enquiry_data['mother_workphone']; ?></label>
                                            <label class="col-sm-3"><strong>Annual Income:</strong> <?php echo $enquiry_data['mother_annualincome']; ?></label>
                                            <label class="col-sm-3"><strong>Working wih TKS Noida:</strong> <?php echo $enquiry_data['mother_woking_tks']; ?></label>  
                                        </div>
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero">
                                    <h4 class="pagetitleh2">Guardian Details</h4>
                                    <div class="row around10">
                                    <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Title:</strong> <?php echo $enquiry_data['guardian_title']; ?></label>
                                        <label class="col-sm-3"><strong>First Name:</strong> <?php echo $enquiry_data['guardian_name']; ?></label>
                                        <label class="col-sm-3"><strong>Last Name:</strong> <?php echo $enquiry_data['guardian_lastname']; ?></label>
                                        <label class="col-sm-3"><strong>Email:</strong> <?php echo $enquiry_data['guardian_email']; ?></label>
                                     </div>
                                     <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Mobile:</strong> <?php echo $enquiry_data['guardian_mobile']; ?></label>
                                        <label class="col-sm-3"><strong>Aadhar Card No.:</strong> <?php echo $enquiry_data['guardian_aadhar_no']; ?></label>
                                        <label class="col-sm-3"><strong>Qualification:</strong> <?php if($enquiry_data['guardian_qualification']=='Other') {echo $enquiry_data['guardian_qualification_other'];} else echo $enquiry_data['guardian_qualification']; ?></label>
                                        <label class="col-sm-3"><strong>Occupation:</strong> <?php if($enquiry_data['guardian_occupation']=='Other') {echo $enquiry_data['guardian_occupation_other'];} else echo $enquiry_data['guardian_occupation']; ?></label>
                                      </div>
                                     <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Employer:</strong> <?php echo $enquiry_data['guardian_employer']; ?></label>
                                        <label class="col-sm-3"><strong>Designation:</strong> <?php if($enquiry_data['guardian_designation']=='Other') {echo $enquiry_data['guardian_designation_other'];} else echo $enquiry_data['guardian_designation']; ?></label>
                                        <label class="col-sm-3"><strong>Work Address:</strong> <?php echo $enquiry_data['guardian_address']; ?></label>
                                        <label class="col-sm-3"><strong>Work City:</strong> 
                                            <?php if($enquiry_data['guardian_city']=='Other') {echo $enquiry_data['guardian_city_other']; } 
                                            else echo $enquiry_data['guardian_city']; ?></label>
                                     </div>
                                     <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Work Pin Code:</strong> <?php echo $enquiry_data['guardian_pincode']; ?></label>
                                        <label class="col-sm-3"><strong>Work phone:</strong> <?php echo $enquiry_data['guardian_workphone']; ?></label>
                                        <label class="col-sm-3"><strong>Annual Income:</strong> <?php echo $enquiry_data['guardian_annualincome']; ?></label>
                                        <label class="col-sm-3"><strong>Working wih TKS Noida:</strong> <?php echo $enquiry_data['guardian_woking_tks']; ?></label>  
                                        </div>
                                    <div class="col-sm-12">
                                        <label class="col-sm-3"><strong>Relation:</strong> <?php echo $enquiry_data['guardian_relation']; ?></label>
                                    </div>

                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero">
                                 <h4 class="pagetitleh2">Student Address Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Address1:</strong> <?php echo $enquiry_data['address']; ?></label>
                                            <label class="col-sm-3"><strong>Address2:</strong> <?php echo $enquiry_data['address2']; ?></label>
                                            <label class="col-sm-3"><strong>City:</strong> <?php echo $enquiry_data['city']; ?></label>
                                        
                                            <label class="col-sm-3"><strong>State:</strong> <?php 
                                            foreach ($state_list as $value) {
                                                         if ($value['id']== $enquiry_data['state_id']) echo $value['state']; 
                                            }?></label>
                                        </div>
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Country:</strong> <?php 
                                            foreach ($country_list as $value) {
                                                         if ($value['id']== $enquiry_data['country_id']) echo $value['country']; 
                                            }?>
                                             </label>
                                            <label class="col-sm-3"><strong>Pin Code:</strong> <?php echo $enquiry_data['pincode']; ?></label>
                                            <label class="col-sm-3"><strong>Landline:</strong> <?php echo $enquiry_data['landline']; ?></label>
                                        </div>
                                    
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero">
                                            <h4 class="pagetitleh2">Document Details</h4>
                                            <div class="row around10">
                                                 <?php if(!empty($documents)){
                                                    foreach ($documents as $value) {
                                                   ?>
                                                 <div class="col-sm-3">

                                                        <div class="form-group">
                                                            <label for="dtp_input1"><a href="<?php echo base_url().'/uploads/student_documents/'.date('Y').'/enquiry/'.$value['doc']; ?>" target="_blank"><i class="fa fa-check-circle"></i> <?php echo $value['title']; ?></a></label>
                                                        </div>
                                                </div>
                                                <?php }
                                                } else { ?>
                                                    <div class="col-sm-3">

                                                        <div class="form-group">
                                                            <label for="dtp_input1">No documents.</label>
                                                        </div>
                                                </div>
                                                    <?php }
                                                ?>
                                            </div>
                                        </div>
                            <div class="tshadow mb25 bozero">
                                    <h4 class="pagetitleh2">Miscellaneous Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Next Follow Up Date:</strong> <?php 
                                            if($enquiry_data['follow_up_date'] !='' || $enquiry_data['follow_up_date'] != NULL) echo date('d-M-Y H:i:s', strtotime($enquiry_data['follow_up_date']));
                                             ?></label>
                                            <label class="col-sm-3"><strong>Assigned:</strong> <?php foreach ($staffusers as $value) { 
                                                    if ($value['id']== $enquiry_data['assigned']) echo $value['name'].' '.$value['surname'];
                                                 }
                                                ?>
                                                </label>
                                            <label class="col-sm-3"><strong>Reference:</strong><?php foreach ($Reference as $value) { 
                                                    if ($value['reference']== $enquiry_data['reference']) echo $value['reference'];
                                                 }
                                                ?>
                                                 </label>
                                        
                                            <label class="col-sm-3"><strong>Source:</strong> <?php foreach ($source as $value) { 
                                                    if ($value['source']== $enquiry_data['source']) echo $value['source'];
                                                 }
                                                ?></label>
                                          </div>
                                         <div class="col-sm-12">
                                            <label class="col-sm-3"><strong>Status:</strong><?php foreach ($statuses as $key=>$value) {
                                             if ($key == $enquiry_data['status']) echo $value; 
                                              } ?>
                                           </label>
                                            <label class="col-sm-3"><strong>Remarks:</strong> <?php echo $enquiry_data['note']; ?></label>
                                          </div>
                                    
                                    </div>
                            </div>
                            <div class="tshadow mb25 bozero">
                                 <h4 class="pagetitleh2">Questionnaire Details</h4>
                                    <div class="row around10">
                                        <div class="col-sm-12">
                                            <label class="col-sm-12 line-break"><strong>School Last Attended:</strong> <?php echo $enquiry_data['school_last_attended']; ?></label>
                                            <label class="col-sm-12 line-break"><strong>Other Languages Child can Speak / Understand:</strong> <?php echo $enquiry_data['languages_child_can_speak']; ?></label>
                                       
                                       </div>
                                           <div class="col-sm-12">
                                                <label class="col-sm-12 line-break"><strong>Real Brother / Sister School:</strong> <?php echo $enquiry_data['sibbling_school']; ?></label>
                                            
                                                <label class="col-sm-12 line-break"><strong>Real Brother / Sister Class:</strong> <?php echo $enquiry_data['sibbling_class']; ?></label>
                                          </div>
                                         <div class="col-sm-12">
                                                <label class="col-sm-12 line-break"><strong>Special Traits:</strong> <?php echo $enquiry_data['special_traits']; ?></label>
                                                <label class="col-sm-12 line-break"><strong>Interaction with Peer Group:</strong> <?php echo $enquiry_data['interaction_with_peer_group']; ?></label>
                                         </div>
                                        <div class="col-sm-12">
                                                <label class="col-sm-6 line-break"><strong>Whether Nuclear / Joint Family:</strong> <?php echo $enquiry_data['nuclear_joint_family']; ?></label>
                                                <label class="col-sm-12 line-break"><strong>What according to you is the definition of a good school?</strong> <?php echo $enquiry_data['definition_good_school']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-12 line-break"><strong>State your views on inclusive education.</strong> <?php echo $enquiry_data['inclusive_education']; ?></label>
                                                <label class="col-sm-12 line-break"><strong>What values would you like to inculcate in your child?</strong> <?php echo $enquiry_data['inculcate_values']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-12 line-break"><strong>Incase both parents are working who takes care of the child?</strong> <?php echo $enquiry_data['takes_care_child']; ?></label>
                                                <label class="col-sm-12 line-break"><strong>As a father why do you think it is important to spend time with your child?</strong> <?php echo $enquiry_data['spendtime_importance']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-12 line-break"><strong>As a mother, how much time do you spend with your child and how?</strong> <?php echo $enquiry_data['spend_with_your_child']; ?></label>
                                                <label class="col-sm-12 line-break"><strong>Mention atleast two interests / hobbies of your child.</strong> <?php echo $enquiry_data['interests_hobbies']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-12 line-break"><strong>Write briefly your impression about The Khaitan School.</strong> <?php echo $enquiry_data['logical_answers']; ?></label>
                                                <label class="col-sm-12 line-break"><strong>If your child is very inquisitive, do you always believe in providing logical answers to his / her question or avoid them?</strong> <?php echo $enquiry_data['physical_illness']; ?></label>
                                          </div>
                                          <div class="col-sm-12">
                                                <label class="col-sm-12"><strong>Did your child ever suffer from a handicap or ailment where the school needs to be cautious in handling him / her?</strong> <?php echo $enquiry_data['kps_impression']; ?></label>
                                          </div>
                                    
                                    </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_lead_details">
                            <strong>Enquiry ID:</strong> <?php echo $enquiry_data['enquiryid']; ?>
                                <form  action="<?php echo site_url('admin/enquiry') ?>" id="leaddetails"  method="post"  class="ptt10" enctype="multipart/form-data">
                                        <div class="tshadow mb25 bozero">
                                            <h4 class="pagetitleh2">Child Details</h4>
                                            <div class="row around10">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="pwd">First Name</label><small class="req"> *</small>  
                                                            <input type="text" class="form-control" id="name_value" value="<?php echo set_value('name', $enquiry_data['name']); ?>" name="name">
                                                            <span class="text-danger" id="name"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="pwd">Last Name</label><small class="req"> *</small>  
                                                            <input type="text" class="form-control" id="last_name" value="<?php echo set_value('last_name', $enquiry_data['last_name']); ?>" name="last_name">
                                                            <span class="text-danger" id="last_name"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">Gender</label> <small class="req"> *</small>
                                                           <select name="gender" class="form-control"  >
                                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php 
                                                                foreach ($gender_list as $key => $value) {
                                                                    ?>

                                                                    <option value="<?php echo $key ?>" <?php if (set_value('gender', $enquiry_data['gender']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="dob">Date of Birth</label><small class="req"> *</small>
                                                            <input type="text" id="dob" name="dob" placeholder="dd/mm/yyyy" class="form-control dob" value="<?php
                                                            if (!empty($enquiry_data['dob'])) {
                                                                echo set_value('dob', date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($enquiry_data['dob'])));
                                                            }
                                                            ?>" readonly="">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="pwd"><?php echo $this->lang->line('class'); ?></label> <small class="req"> *</small>
                                                            <select name="class" class="form-control"  >
                                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                    <?php
                                                                    foreach ($class_list as $key => $value) {
                                                                        ?>

                                                                    <option value="<?php echo $value['id'] ?>" <?php if (set_value('class', $enquiry_data['class']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['class'] ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">Session</label> <small class="req"> *</small>
                                                           <select name="session_id" id="session_id" class="form-control"  >
                                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                    <?php
                                                                    foreach ($session_list as $value) {
                                                                        ?>

                                                                    <option value="<?php echo $value['id'] ?>" <?php if (set_value('session_id', $enquiry_data['session_id']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['session'] ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="tshadow mb25 bozero">
                                            <h4 class="pagetitleh2">Father Details</h4>
                                            <div class="row around10">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="father_title">Title:</label> 
                                                    <select name="father_title" class="form-control"  >
                                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                        <?php $father_title_list=$this->config->item('father_title_list');
                                                        foreach ($father_title_list as $key => $value) {
                                                            ?>

                                                        <option value="<?php echo $key ?>" <?php if (set_value('father_title', $enquiry_data['father_title']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                            <?php
                                                        }
                                                        ?>
                                                </select>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="father_firstname">First Name</label><small class="req"> *</small>  
                                                    <input id="father_firstname" name="father_firstname" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('father_firstname', $enquiry_data['father_firstname']); ?>" />
                                                    <span class="text-danger"><?php echo form_error('father_firstname'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="father_lastname">Last Name</label> 
                                                    <input id="father_lastname" name="father_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('father_lastname', $enquiry_data['father_lastname']); ?>" />
                                                    <span class="text-danger"><?php echo form_error('father_lastname'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="father_email">Email</label> <small class="req"> *</small>
                                                    <input id="father_email" name="father_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('father_email', $enquiry_data['father_email']); ?>" />
                                                    <span class="text-danger"><?php echo form_error('father_email'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="father_mobile">Mobile</label><small class="req"> *</small>   
                                                    <input id="father_mobile" name="father_mobile" placeholder="Mobile" type="tel" class="form-control form-mobile" maxlength="14" value="<?php echo set_value('father_mobile', $enquiry_data['father_mobile']); ?>" />
                                                    <span class="text-danger"><?php echo form_error('father_mobile'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="father_mobile">Email Prefrence</label>  
                                                    <div class="material-switch">
                                                        <input id="father_email_pref" name="father_email_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['father_email_pref']) == 1) ? TRUE : FALSE); ?>>
                                                        <label for="father_email_pref" class="label-success"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="father_mobile">SMS Prefrence</label>  
                                                    <div class="material-switch">
                                                        <input id="father_sms_pref" name="father_sms_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['father_sms_pref']) == 1) ? TRUE : FALSE); ?>>
                                                        <label for="father_sms_pref" class="label-success"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        
                                        <div class="tshadow mb25 bozero">
                                            <h4 class="pagetitleh2">Mother Details</h4>
                                            <div class="row around10">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="mother_title">Title:</label> 
                                                        <select name="mother_title" class="form-control"  >
                                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                            <?php $mother_title_list=$this->config->item('mother_title_list');
                                                            foreach ($mother_title_list as $key => $value) {
                                                                ?>

                                                            <option value="<?php echo $key ?>" <?php if (set_value('mother_title', $enquiry_data['mother_title']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                <?php
                                                            }
                                                            ?>
                                                    </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="mother_firstname">First Name</label> 
                                                        <input id="mother_firstname" name="mother_firstname" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('mother_firstname', $enquiry_data['mother_firstname']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_firstname'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="mother_lastname">Last Name</label> 
                                                        <input id="mother_lastname" name="mother_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('mother_lastname', $enquiry_data['mother_lastname']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_lastname'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="mother_email">Email</label> 
                                                        <input id="mother_email" name="mother_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('mother_email', $enquiry_data['mother_email']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_email'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="mother_mobile">Mobile</label> 
                                                        <input id="mother_mobile" name="mother_mobile" placeholder="Mobile" type="tel" class="form-control form-mobile" maxlength="14"  value="<?php echo set_value('mother_mobile', $enquiry_data['mother_mobile']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_mobile'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="father_mobile">Email Prefrence</label>  
                                                        <div class="material-switch">
                                                            <input id="mother_email_pref" name="mother_email_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['mother_email_pref']) == 1) ? TRUE : FALSE); ?>>
                                                            <label for="mother_email_pref" class="label-success"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="mother_mobile">SMS Prefrence</label>  
                                                        <div class="material-switch">
                                                            <input id="mother_sms_pref" name="mother_sms_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['mother_sms_pref']) == 1) ? TRUE : FALSE); ?>>
                                                            <label for="mother_sms_pref" class="label-success"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="tshadow mb25 bozero">
                                            <h4 class="pagetitleh2">Student Address Details</h4>
                                            <div class="row around10">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">Address1</label> 
                                                            <input id="address" name="address" placeholder="Address1" type="text" class="form-control"  value="<?php echo set_value('address', $enquiry_data['address']); ?>" />
                                                            <span class="text-danger"><?php echo form_error('address'); ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">Address2</label> 
                                                            <input id="address2" name="address2" placeholder="Address2" type="text" class="form-control"  value="<?php echo set_value('address2', $enquiry_data['address2']); ?>" />
                                                            <span class="text-danger"><?php echo form_error('address2'); ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">City</label> 
                                                            <input id="city" name="city" placeholder="City" type="text" class="form-control"  value="<?php echo set_value('city', $enquiry_data['city']); ?>" />
                                                            <span class="text-danger"><?php echo form_error('city'); ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">Country</label> 
                                                            <select name="country_id"  id="country_id" class="form-control"  >
                                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php 
                                                                foreach ($country_list as $value) {
                                                                    ?>

                                                                     <option value="<?php echo $value['id'] ?>" <?php if (set_value('country_id', $enquiry_data['country_id']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['country']; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">State</label> 
                                                           <select name="state_id" id="state_id" class="form-control"  >
                                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                    <?php 
                                                                    foreach ($state_list as $value) {
                                                                        ?>

                                                                    <option value="<?php echo $value['id'] ?>" <?php if (set_value('state_id', $enquiry_data['state_id']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['state']; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">Pin Code</label> 
                                                            <input id="pincode" name="pincode" placeholder="Pin Code" type="text" class="form-control"  value="<?php echo set_value('pincode', $enquiry_data['pincode']); ?>" />
                                                            <span class="text-danger"><?php echo form_error('pincode'); ?></span>
                                                        </div>
                                                    </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Landline</label> 
                                                        <input id="landline" name="landline" placeholder="Landline" type="text" class="form-control"  value="<?php echo set_value('landline', $enquiry_data['landline']); ?>" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="tshadow mb25 bozero">
                                            <h4 class="pagetitleh2">Miscellaneous Details</h4>
                                            <div class="row around10">
                                                <div class="col-sm-3">

                                                        <div class="form-group">
                                                            <label for="dtp_input1"><?php echo $this->lang->line('next_follow_up_date'); ?></label>

                                                            <div class="input-group date form_datetime col-md-5" data-date="2020-10-08T05:25:07Z" data-date-format="dd-M-yyyy HH:ii P" data-link-field="dtp_input1">
                                                                <input class="form-control" id="date_of_call_edit" size="16" type="text" placeholder="dd/mm/yyyy" name="follow_up_date" value="<?php
                                                            if (!empty($enquiry_data['follow_up_date'])) {
                                                                echo set_value('follow_up_date', date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($enquiry_data['follow_up_date'])));
                                                            }
                                                            ?>" readonly="">
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                                            </div>
                                                            <input type="hidden" id="dtp_input1" value="" />
                                                        </div></div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label><?php echo $this->lang->line('assigned'); ?></label>

                                                        <select name="assigned" class="form-control">
                                                             <option value=""><?php echo $this->lang->line('select') ?></option>
                                                            <?php foreach ($staffusers as $value) { ?>
                                                                <option value="<?php echo $value['id']; ?>" <?php if (set_value('assigned', $enquiry_data['assigned']) == $value['id']) { ?>selected=""<?php } ?>><?php echo $value['name'].' '.$value['surname']; ?></option>    
                                                        <?php }
                                                        ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="pwd"><?php echo $this->lang->line('reference'); ?></label>   
                                                        <select name="reference" class="form-control">
                                                             <option value=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php foreach ($Reference as $key => $value) { ?>
                                                                <option value="<?php echo $value['reference']; ?>" <?php if (set_value('reference', $enquiry_data['reference']) == $value['reference']) { ?>selected=""<?php } ?>><?php echo $value['reference']; ?></option>    
                                                            <?php }
                                                            ?>
                                                        </select>
                                                        <span class="text-danger"><?php echo form_error('reference'); ?></span>
                                                    </div>
                                                </div>    
                                                <div class="col-sm-3">
                                                    
                                                    <div class="form-group">
                                                        <label for="pwd"><?php echo $this->lang->line('source'); ?></label><small class="req"> *</small>
                                                        <select name="source" class="form-control">
                                                            <option value=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php foreach ($source as $key => $value) { ?>
                                                                <option value="<?php echo $value['source']; ?>"<?php if ($enquiry_data['source']==$value['source']) { echo "selected"; } ?>><?php echo $value['source']; ?></option>
                                                                <?php }
                                                                ?> 
                                                        </select>
                                                    </div>
                                                </div>      
                                                <div class="col-sm-3">
                                                                <div class="form-group">  
                                                                    <label><?php echo $this->lang->line('status'); ?></label>
                                                                    <select  id="status" name="status" class="form-control" <?php
                                                                                if ($enquiry_data['status']==10) {
                                                                                    echo "disabled";
                                                                                }
                                                                                ?>>
                                                                            <?php foreach ($statuses as $key=>$value) {
                                                                                ?>
                                                                            <option <?php
                                                                                if ($key == $enquiry_data['status']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ?> value="<?php echo $key ?>"
                                                                                
                                                                                 ><?php echo $value ?></option>

                                                                            <?php } ?>
                                                                    </select>
                                                                </div>  
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="email">Remarks</label>
                                                        <textarea name="description" class="form-control" ><?php echo set_value('description', $enquiry_data['description']); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>            
                                        <div class="row">    
                                          <div class="box-footer col-md-12">
                                              <a onclick="postRecord(<?php echo $enquiry_data['id'] ?>,'leaddetails')" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></a> 
                                              
                                          </div>
                                        </div>  
                                </form>
                           </div>
                        <div class="tab-pane" id="tab_lead_registration">
                            <strong>Enquiry ID:</strong> <?php echo $enquiry_data['enquiryid']; ?>
                            <form  action="<?php echo site_url('admin/enquiry') ?>" id="registration_details"  method="post"  class="ptt10" enctype="multipart/form-data">
                                    <div class="tshadow mb25 bozero">
                                        <h4 class="pagetitleh2">Child Details</h4>
                                        <div class="row around10">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="pwd">Child's Aadhaar Card No.:</label>
                                                    <input type="text" maxlength="14" class="form-control aadhaar_input" id="child_aadhaar_no" value="<?php echo set_value('child_aadhaar_no', $enquiry_data['child_aadhaar_no']); ?>" name="child_aadhaar_no" placeholder="12 Digit Aadhaar No.">
                                                    <span class="text-danger" id="child_aadhaar_no"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="email">Nationality</label><small class="req"> *</small>   
                                                   <select name="nationality" class="form-control"  >
                                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                        <?php 
                                                        foreach ($nationality_list as $key => $value) {
                                                            ?>

                                                            <option value="<?php echo $key ?>" <?php if (set_value('nationality', $enquiry_data['nationality']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                <?php
                                                            }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="dob">Blood Group:</label><small class="req"> *</small>  
                                                    <select name="blood_group" class="form-control"  >
                                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                        <?php $blood_group_list=$this->config->item('blood_group_list');
                                                        foreach ($blood_group_list as $key => $value) {
                                                            ?>

                                                            <option value="<?php echo $key ?>" <?php if (set_value('blood_group', $enquiry_data['blood_group']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                <?php
                                                            }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="pwd">Child Image</label> 
                                                    <input type="file" id="child_image" class="filestyle form-control" name="child_image" multiple="multiple">
                                                                            <span class="text-danger"><?php echo form_error('message'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="email">Mother Tongue</label><small class="req"> *</small>   
                                                   <select name="mother_tongue" class="form-control"  >
                                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                        <?php $mother_tongue_list=$this->config->item('mother_tongue_list');
                                                        foreach ($mother_tongue_list as $key => $value) {
                                                            ?>

                                                            <option value="<?php echo $key ?>" <?php if (set_value('mother_tongue', $enquiry_data['mother_tongue']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                <?php
                                                            }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="home_town">Home Town</label><small class="req"> *</small>  
                                                    <input id="home_town" name="home_town" placeholder="Home Town" type="text" class="form-control"  value="<?php echo set_value('home_town', $enquiry_data['home_town']); ?>" />
                                                    <span class="text-danger"><?php echo form_error('home_town'); ?></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="locality">Locality</label><small class="req"> *</small>   
                                                    <select name="locality" class="form-control"  >
                                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                        <?php $locality_list=$this->config->item('locality_list');
                                                        foreach ($locality_list as $key => $value) {
                                                            ?>

                                                            <option value="<?php echo $key ?>" <?php if (set_value('locality', $enquiry_data['locality']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="religion">Religion</label><small class="req"> *</small>  
                                                    <select name="religion" class="form-control"  >
                                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                            <?php $religion_list=$this->config->item('religion_list');
                                                            foreach ($religion_list as $key => $value) {
                                                                ?>

                                                            <option value="<?php echo $key ?>" <?php if (set_value('religion', $enquiry_data['religion']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                <?php
                                                            }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php if ($sch_setting->category) { ?>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Social Category</label><small class="req"> *</small> 
                                                        <select  id="social_category" name="social_category" class="form-control" >
                                                            <option value=""><?php echo $this->lang->line('select'); ?></option>
                                                            <?php foreach ($categorylist as $category) {   ?>
                                                            <option value="<?php echo $category['id'] ?>" <?php if ($enquiry_data['social_category'] == $category['id']) {  echo "selected=selected";  } ?>><?php echo $category['category'] ?></option>
                                                            <?php $count++; } ?>
                                                        </select>
                                                        <span class="text-danger"><?php echo form_error('category_id'); ?></span>
                                                    </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="col-sm-3">
                                                    <div class="form-group result-doc">
                                                        <label for="pwd">Previous Year Result</label> 
                                                        <input type="file" id="previous_result" class="filestyle form-control dropify" name="previous_result" multiple="multiple">
                                                    </div>
                                                </div>
                                            
                                        </div>
                                    </div>
                                    <div class="tshadow mb25 bozero">
                                        <h4 class="pagetitleh2">Previous School Information</h4>
                                            <div class="row around10">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="last_school">Last School:</label> 
                                                        <input id="last_school" name="last_school" placeholder="Last School" type="text" class="form-control"  value="<?php echo set_value('last_school', $enquiry_data['last_school']); ?>" />                                            
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="medium_instruction">Medium of Instruction:</label> 
                                                        <input id="medium_instruction" name="medium_instruction" placeholder="Medium of Instruction" type="text" class="form-control"  value="<?php echo set_value('medium_instruction', $enquiry_data['medium_instruction']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('medium_instruction'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="previous_board">Board:</label> 
                                                        <select name="previous_board" class="form-control"  >
                                                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                ?>
                                                                ?>
                                                                <?php $previous_board_list=$this->config->item('previous_board_list');
                                                                foreach ($previous_board_list as $key => $value) {
                                                                    ?>

                                                                <option value="<?php echo $key ?>" <?php if (set_value('previous_board', $enquiry_data['previous_board']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="previous_class">Class:</label> 
                                                         <input id="previous_class" name="previous_class" placeholder="Class" type="text" class="form-control"  value="<?php echo set_value('previous_class', $enquiry_data['previous_class']); ?>" />

                                                    </div>
                                                </div>
                                            </div>
                                    </div>

                                    <div class="tshadow mb25 bozero">
                                        <h4 class="pagetitleh2">Father Details</h4>
                                        <div class="row around10">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Aadhar Card No.</label> 
                                                        <input maxlength="14" id="father_aadhar_no" name="father_aadhar_no"  placeholder="12 Digit Aadhaar No." type="text" class="form-control aadhaar_input"  value="<?php echo set_value('father_aadhar_no', $enquiry_data['father_aadhar_no']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_aadhar_no'); ?></span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Qualification:</label><small class="req"> *</small>   
                                                        <select name="father_qualification" class="form-control append_other"  >
                                                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php $parent_qualification_list=$this->config->item('parent_qualification_list');
                                                                foreach ($parent_qualification_list as $key => $value) {
                                                                    ?>

                                                                <option value="<?php echo $key ?>" <?php if (set_value('father_qualification', $enquiry_data['father_qualification']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                        </select>
                                                    </div>
                                                        <div class="form-group <?php if($enquiry_data['father_qualification']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="father_qualification_other" name="father_qualification_other" placeholder="Other Qualification" type="text" class="form-control"  value="<?php echo set_value('father_qualification_other', $enquiry_data['father_qualification_other']); ?>" /> 
                                                        </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Occupation:</label> 
                                                        <select name="father_occupation" class="form-control append_other"  >
                                                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php $father_occupation_list=$this->config->item('parent_occupation_list');
                                                                foreach ($father_occupation_list as $key => $value) {
                                                                    ?>

                                                                <option value="<?php echo $key ?>" <?php if (set_value('father_occupation', $enquiry_data['father_occupation']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                        </select>
                                                    </div>
                                                        <div class="form-group <?php if($enquiry_data['father_occupation']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="father_occupation_other" name="father_occupation_other" placeholder="Other Occupation" type="text" class="form-control"  value="<?php echo set_value('father_occupation_other', $enquiry_data['father_occupation_other']); ?>" /> 
                                                        </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Employer</label><small class="req"> *</small>  
                                                        <input id="father_employer" name="father_employer" placeholder="Employer" type="text" class="form-control"  value="<?php echo set_value('father_employer', $enquiry_data['father_employer']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('father_employer'); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                               <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">

                                                            <label for="email">Designation</label> 
                                                            <select name="father_designation" class="form-control append_other"  >
                                                            <option value=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php $father_designation_list=$this->config->item('parent_designation_list');
                                                                foreach ($father_designation_list as $key => $value){?>
                                                                    <option value="<?php echo $key ?>" <?php if ($enquiry_data['father_designation'] == $key) { ?> selected="selected" <?php } ?>><?php echo $value; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group <?php if($enquiry_data['father_designation']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="father_designation_other" name="father_designation_other" placeholder="Other Designation" type="text" class="form-control"  value="<?php echo set_value('father_designation_other', $enquiry_data['father_designation_other']); ?>" /> 
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">Work Address</label> 
                                                            <input id="father_address" name="father_address" placeholder="Work Address" type="text" class="form-control"  value="<?php echo set_value('father_address', $enquiry_data['father_address']); ?>" />
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">Work City</label> 
                                                             <select name="father_city" class="form-control append_other"  >
                                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                    <?php $father_city_list=$this->config->item('parent_city_list');
                                                                    foreach ($father_city_list as $key => $value) {
                                                                        ?>

                                                                    <option value="<?php echo $key ?>" <?php if (set_value('father_city', $enquiry_data['father_city']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                        <?php
                                                                    }
                                                                    ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group <?php if($enquiry_data['father_city']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="father_city_other" name="father_city_other" placeholder="Other City" type="text" class="form-control"  value="<?php echo set_value('father_city_other', $enquiry_data['father_city_other']); ?>" /> 
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">Work Pin Code</label> 
                                                            <input id="father_pincode" name="father_pincode" placeholder="Work Pin Code" type="text" class="form-control"  value="<?php echo set_value('father_pincode', $enquiry_data['father_pincode']); ?>" />
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="email">Work phone</label> 
                                                    <input id="father_workphone" name="father_workphone" placeholder="Work phone" type="text" class="form-control"  value="<?php echo set_value('father_workphone', $enquiry_data['father_workphone']); ?>" />
                                                   
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="email">Annual Income</label> 
                                                    <input id="father_annualincome" name="father_annualincome" placeholder="Annual Income" type="text" class="form-control"  value="<?php echo set_value('father_annualincome', $enquiry_data['father_annualincome']); ?>" />
                                                   
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Working wih TKS Noida</label>

                                                    <select name="father_woking_tks" class="form-control"  >
                                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                            <?php $father_woking_tks_list=$this->config->item('true-false');
                                                            foreach ($father_woking_tks_list as $key => $value) {
                                                                ?>

                                                            <option value="<?php echo $key ?>" <?php if (set_value('father_woking_tks', $enquiry_data['father_woking_tks']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                <?php
                                                            }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>

                                       
                                        </div>
                                    </div>
                                    
                                    <div class="tshadow mb25 bozero">
                                        <h4 class="pagetitleh2">Mother Details</h4>
                                        <div class="row around10">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Aadhar Card No.</label> 
                                                        <input maxlength="14" id="mother_aadhar_no" name="mother_aadhar_no" placeholder="12 Digit Aadhaar No." type="text" class="form-control aadhaar_input"  value="<?php echo set_value('mother_aadhar_no', $enquiry_data['mother_aadhar_no']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_aadhar_no'); ?></span>
                                                    </div>
                                                </div>
                                        
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Qualification</label><small class="req"> *</small>  
                                                        <select name="mother_qualification" class="form-control append_other"  >
                                                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php $parent_qualification_list=$this->config->item('parent_qualification_list');
                                                                foreach ($parent_qualification_list as $key => $value) {
                                                                    ?>
                                                                <option value="<?php echo $key ?>" <?php if (set_value('mother_qualification', $enquiry_data['mother_qualification']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group <?php if($enquiry_data['mother_qualification']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="mother_qualification_other" name="mother_qualification_other" placeholder="Other Qualification" type="text" class="form-control"  value="<?php echo set_value('mother_qualification_other', $enquiry_data['mother_qualification_other']); ?>" /> 
                                                        </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Occupation</label> 
                                                        <select name="mother_occupation" class="form-control append_other"  >
                                                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php $mother_occupation_list=$this->config->item('parent_occupation_list');
                                                                foreach ($mother_occupation_list as $key => $value) {
                                                                    ?>

                                                                <option value="<?php echo $key ?>" <?php if (set_value('mother_occupation', $enquiry_data['mother_occupation']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group <?php if($enquiry_data['mother_occupation']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="mother_occupation_other" name="mother_occupation_other" placeholder="Other Occupation" type="text" class="form-control"  value="<?php echo set_value('mother_occupation_other', $enquiry_data['mother_occupation_other']); ?>" /> 
                                                        </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Employer</label><small class="req"> *</small>  
                                                        <input id="mother_employer" name="mother_employer" placeholder="Employer" type="text" class="form-control"  value="<?php echo set_value('mother_employer', $enquiry_data['mother_employer']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('mother_employer'); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Designation</label> 
                                                        <select name="mother_designation" class="form-control append_other"  >
                                                            <option value=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php $mother_designation_list=$this->config->item('parent_designation_list');
                                                                foreach ($mother_designation_list as $key => $value) {
                                                                    ?>

                                                                <option value="<?php echo $key ?>" <?php if ($enquiry_data['mother_designation'] == $key) { ?> selected="selected" <?php } ?>><?php echo $value; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group <?php if($enquiry_data['mother_designation']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="mother_designation_other" name="mother_designation_other" placeholder="Other Designation" type="text" class="form-control"  value="<?php echo set_value('mother_designation_other', $enquiry_data['mother_designation_other']); ?>" /> 
                                                        </div>
                                                </div>
                                            
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Work Address</label> 
                                                        <input id="mother_address" name="mother_address" placeholder="Work Address" type="text" class="form-control"  value="<?php echo set_value('mother_address', $enquiry_data['mother_address']); ?>" />
                                                       
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Work City</label> 
                                                         <select name="mother_city" class="form-control append_other"  >
                                                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php $mother_city_list=$this->config->item('parent_city_list');
                                                                foreach ($mother_city_list as $key => $value) {
                                                                    ?>

                                                                <option value="<?php echo $key ?>" <?php if (set_value('mother_city', $enquiry_data['mother_city']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group <?php if($enquiry_data['mother_city']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="mother_city_other" name="mother_city_other" placeholder="Other City" type="text" class="form-control"  value="<?php echo set_value('mother_city_other', $enquiry_data['mother_city_other']); ?>" /> 
                                                        </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Work Pin Code</label> 
                                                        <input id="mother_pincode" name="mother_pincode" placeholder="Work Pin Code" type="text" class="form-control"  value="<?php echo set_value('mother_pincode', $enquiry_data['mother_pincode']); ?>" />
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="email">Work phone</label> 
                                                    <input id="mother_workphone" name="mother_workphone" placeholder="Work phone" type="text" class="form-control"  value="<?php echo set_value('mother_workphone', $enquiry_data['mother_workphone']); ?>" />
                                                   
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="email">Annual Income</label> 
                                                    <input id="mother_annualincome" name="mother_annualincome" placeholder="Annual Income" type="text" class="form-control"  value="<?php echo set_value('mother_workphone', $enquiry_data['mother_workphone']); ?>" />
                                                   
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Working wih TKS Noida</label>

                                                    <select name="mother_woking_tks" class="form-control"  >
                                                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                            <?php $mother_woking_tks_list=$this->config->item('true-false');
                                                            foreach ($mother_woking_tks_list as $key => $value) {
                                                                ?>

                                                            <option value="<?php echo $key ?>" <?php if (set_value('mother_woking_tks', $enquiry_data['mother_woking_tks']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                <?php
                                                            }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>

                                        </div> 
                                    </div>
                                    
                                    <div class="tshadow mb25 bozero">
                                        <h4 class="pagetitleh2">Guardian Details</h4>
                                        <div class="row around10">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="guardian_title">Title:</label> 
                                                    <select name="guardian_title" class="form-control"  >
                                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                        <?php $guardian_title_list=$this->config->item('guardian_title_list');
                                                        foreach ($guardian_title_list as $key => $value) {
                                                            ?>

                                                        <option value="<?php echo $key ?>" <?php if (set_value('guardian_title', $enquiry_data['guardian_title']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                            <?php
                                                        }
                                                        ?>
                                                </select>
                                                </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="guardian_name">First Name</label> 
                                                        <input id="guardian_name" name="guardian_name" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('guardian_name', $enquiry_data['guardian_name']); ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="guardian_lastname">Last Name</label> 
                                                        <input id="guardian_lastname" name="guardian_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('guardian_lastname', $enquiry_data['guardian_lastname']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('guardian_lastname'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="guardian_email">Email</label> 
                                                        <input id="guardian_email" name="guardian_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('guardian_email', $enquiry_data['guardian_email']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('guardian_email'); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                             <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="guardian_mobile">Mobile</label> 
                                                        <input id="guardian_mobile" name="guardian_mobile" placeholder="Mobile" type="tel" class="form-control form-mobile" maxlength="14"  value="<?php echo set_value('guardian_mobile', $enquiry_data['guardian_mobile']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('guardian_mobile'); ?></span>
                                                    </div>
                                                </div>
                                            
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Aadhar Card No.</label> 
                                                        <input maxlength="14" id="guardian_aadhar_no" name="guardian_aadhar_no" placeholder="12 Digit Aadhaar No." type="text" class="form-control aadhaar_input"  value="<?php echo set_value('guardian_aadhar_no', $enquiry_data['guardian_aadhar_no']); ?>" />
                                                        <span class="text-danger"><?php echo form_error('guardian_aadhar_no'); ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Qualification</label>  
                                                        <select name="guardian_qualification" class="form-control append_other"  >
                                                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php $parent_qualification_list=$this->config->item('parent_qualification_list');
                                                                foreach ($parent_qualification_list as $key => $value) {
                                                                    ?>

                                                                <option value="<?php echo $key ?>" <?php if (set_value('guardian_qualification', $enquiry_data['guardian_qualification']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group <?php if($enquiry_data['guardian_qualification']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="guardian_qualification_other" name="guardian_qualification_other" placeholder="Other Qualification" type="text" class="form-control"  value="<?php echo set_value('guardian_qualification_other', $enquiry_data['guardian_qualification_other']); ?>" /> 
                                                        </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="email">Occupation</label> 
                                                        <select name="guardian_occupation" class="form-control append_other"  >
                                                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                <?php $guardian_occupation_list=$this->config->item('parent_occupation_list');
                                                                foreach ($guardian_occupation_list as $key => $value) {
                                                                    ?>

                                                                <option value="<?php echo $key ?>" <?php if (set_value('guardian_occupation', $enquiry_data['guardian_occupation']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group <?php if($enquiry_data['guardian_occupation']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="guardian_occupation_other" name="guardian_occupation_other" placeholder="Other Occupation" type="text" class="form-control"  value="<?php echo set_value('guardian_occupation_other', $enquiry_data['guardian_occupation_other']); ?>" /> 
                                                        </div>
                                                </div>
                                            </div>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">Employer</label>  
                                                            <input id="guardian_employer" name="guardian_employer" placeholder="Employer" type="text" class="form-control"  value="<?php echo set_value('guardian_employer', $enquiry_data['guardian_employer']); ?>" />
                                                            <span class="text-danger"><?php echo form_error('guardian_employer'); ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="email">Designation</label> 
                                                            <select name="guardian_designation" class="form-control append_other"  >
                                                                <option value=""><?php echo $this->lang->line('select') ?></option>
                                                                    <?php $guardian_designation_list=$this->config->item('parent_designation_list');
                                                                    foreach ($guardian_designation_list as $key => $value) {
                                                                        ?>

                                                                    <option value="<?php echo $key ?>" <?php if ($enquiry_data['guardian_designation'] == $key) { ?> selected="selected" <?php } ?>><?php echo $value; ?></option>

                                                                        <?php
                                                                    }
                                                                    ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group <?php if($enquiry_data['guardian_designation']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="guardian_designation_other" name="guardian_designation_other" placeholder="Other Designtion" type="text" class="form-control"  value="<?php echo set_value('guardian_designation_other', $enquiry_data['guardian_designation_other']); ?>" /> 
                                                        </div>
                                                    </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="email">Address</label> 
                                                                <input id="guardian_address" name="guardian_address" placeholder="Address" type="text" class="form-control"  value="<?php echo set_value('guardian_address', $enquiry_data['guardian_address']); ?>" />
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                            <label for="email">City</label> 
                                                            <select name="guardian_city" class="form-control append_other"  >
                                                                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                                <?php $guardian_city_list=$this->config->item('parent_city_list');
                                                                                foreach ($guardian_city_list as $key => $value) {
                                                                                    ?>

                                                                                <option value="<?php echo $key ?>" <?php if (set_value('guardian_city', $enquiry_data['guardian_city']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                        </select>
                                                            
                                                            </div>
                                                            <div class="form-group <?php if($enquiry_data['guardian_city']!='Other') {?>displaynone <?php } ?>">
                                                            <input id="guardian_city_other" name="guardian_city_other" placeholder="Other City" type="text" class="form-control"  value="<?php echo set_value('guardian_city_other', $enquiry_data['guardian_city_other']); ?>" /> 
                                                        </div>
                                                        </div>
                                                   </div>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="email">State</label> 
                                                                <input id="guardian_state" name="guardian_state" placeholder="State" type="text" class="form-control"  value="<?php echo set_value('guardian_state', $enquiry_data['guardian_state']); ?>" />
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="email">Pin Code</label> 
                                                                <input id="guardian_pincode" name="guardian_pincode" placeholder="Pin Code" type="text" class="form-control"  value="<?php echo set_value('guardian_pincode', $enquiry_data['guardian_pincode']); ?>" />
                                                                
                                                                </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="email">Telephone</label> 
                                                                <input id="guardian_telephone" name="guardian_telephone" placeholder="Telephone" type="text" class="form-control"  value="<?php echo set_value('guardian_telephone', $enquiry_data['guardian_telephone']); ?>" />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="email">Annual Income</label> 
                                                                <input id="guardian_annualincome" name="guardian_annualincome" placeholder="Annual Income" type="text" class="form-control"  value="<?php echo set_value('guardian_workphone', $enquiry_data['guardian_workphone']); ?>" />
                                                                
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="row">  
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label>Working wih TKS Noida</label>

                                                            <select name="guardian_woking_tks" class="form-control"  >
                                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                                    <?php $guardian_woking_tks_list=$this->config->item('true-false');
                                                                    foreach ($guardian_woking_tks_list as $key => $value) {
                                                                        ?>

                                                                    <option value="<?php echo $key ?>" <?php if (set_value('guardian_woking_tks', $enquiry_data['guardian_woking_tks']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

                                                                        <?php
                                                                    }
                                                                    ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                
                                                 
                                                            <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <label for="guardian_relation">Relation</label> 
                                                                        <input id="guardian_relation" name="guardian_relation" placeholder="Relation" type="text" class="form-control"  value="<?php echo set_value('guardian_relation', $enquiry_data['guardian_relation']); ?>" />
                                                                        
                                                                    </div>
                                                                </div> 
                                                  
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label for="father_mobile">Email Prefrence</label>  
                                                                    <div class="material-switch">
                                                                        <input id="guadian_email_pref" name="guadian_email_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['guadian_email_pref']) == 1) ? TRUE : FALSE); ?>>
                                                                        <label for="guadian_email_pref" class="label-success"></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label for="mother_mobile">SMS Prefrence</label>  
                                                                    <div class="material-switch">
                                                                        <input id="guadian_sms_pref" name="guadian_sms_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', $enquiry_data['guadian_sms_pref']) == 1) ? TRUE : FALSE); ?>>
                                                                        <label for="guadian_sms_pref" class="label-success"></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                 </div>
                                        </div> 
                                        <div class="row">    
                                          <div class="box-footer col-md-12">
                                              <a onclick="postRegRecord(<?php echo $enquiry_data['id'] ?>,'registration_details')" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></a> 
                                              
                                          </div>
                                        </div> 
                                    </div> 
                            <!--./row--> 
                            </form>
                        </div>

                        <div class="tab-pane" id="tab_lead_questionnaire">
                            <strong>Enquiry ID:</strong> <?php echo $enquiry_data['enquiryid']; ?>
                            <form  action="<?php echo site_url('admin/enquiry') ?>" id="questionnaire_details" method="post"  class="ptt10">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="pwd">School Last Attended:</label>
                                            <input type="text" class="form-control" id="school_last_attended" value="<?php echo set_value('school_last_attended', $enquiry_data['school_last_attended']); ?>" name="school_last_attended">
                                            <span class="text-danger" id="school_last_attended"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Other Languages Child can Speak / Understand:</label> 
                                               <input type="text" class="form-control" id="languages_child_can_speak" value="<?php echo set_value('languages_child_can_speak', $enquiry_data['languages_child_can_speak']); ?>" name="languages_child_can_speak">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Real Brother / Sister School:</label> 
                                               <input type="text" class="form-control" id="sibbling_school" value="<?php echo set_value('sibbling_school', $enquiry_data['sibbling_school']); ?>" name="sibbling_school">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Real Brother / Sister Class:</label> 
                                               <input type="text" class="form-control" id="sibbling_class" value="<?php echo set_value('sibbling_class', $enquiry_data['sibbling_class']); ?>" name="sibbling_class">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Special Traits:</label> 
                                               <input type="text" class="form-control" id="special_traits" value="<?php echo set_value('special_traits', $enquiry_data['special_traits']); ?>" name="special_traits">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Interaction with Peer Group:</label> 
                                               <input type="text" class="form-control" id="interaction_with_peer_group" value="<?php echo set_value('interaction_with_peer_group', $enquiry_data['interaction_with_peer_group']); ?>" name="interaction_with_peer_group">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Whether Nuclear / Joint Family:</label> 
                                           <input type="text" class="form-control" id="nuclear_joint_family" value="<?php echo set_value('nuclear_joint_family', $enquiry_data['nuclear_joint_family']); ?>" name="nuclear_joint_family">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="home_town">What according to you is the definition of a good school?</label> 
                                            <input id="definition_good_school" name="definition_good_school" placeholder="" type="text" class="form-control"  value="<?php echo set_value('definition_good_school', $enquiry_data['definition_good_school']); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="home_town">State your views on inclusive education.</label> 
                                            <input id="inclusive_education" name="inclusive_education" placeholder="" type="text" class="form-control"  value="<?php echo set_value('inclusive_education', $enquiry_data['inclusive_education']); ?>" />
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="home_town">What values would you like to inculcate in your child?</label> 
                                            <input id="inculcate_values" name="inculcate_values" placeholder="" type="text" class="form-control"  value="<?php echo set_value('inculcate_values', $enquiry_data['inculcate_values']); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="home_town">Incase both parents are working who takes care of the child?</label> 
                                            <input id="takes_care_child" name="takes_care_child" placeholder="" type="text" class="form-control"  value="<?php echo set_value('takes_care_child', $enquiry_data['takes_care_child']); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="spendtime_importance">As a father why do you think it is important to spend time with your child?</label> 
                                            <input id="spendtime_importance" name="spendtime_importance" placeholder="" type="text" class="form-control"  value="<?php echo set_value('spendtime_importance', $enquiry_data['spendtime_importance']); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="spend_with_your_child">As a mother, how much time do you spend with your child and how?</label> 
                                            <input id="spend_with_your_child" name="spend_with_your_child" placeholder="" type="text" class="form-control"  value="<?php echo set_value('spend_with_your_child', $enquiry_data['spend_with_your_child']); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="interests_hobbies">Mention atleast two interests / hobbies of your child.</label> 
                                            <input id="interests_hobbies" name="interests_hobbies" placeholder="" type="text" class="form-control"  value="<?php echo set_value('interests_hobbies', $enquiry_data['interests_hobbies']); ?>" />
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="logical_answers">If your child is very inquisitive, do you always believe in providing logical answers to his / her question or avoid them?</label> 
                                            <input id="logical_answers" name="logical_answers" placeholder="" type="text" class="form-control"  value="<?php echo set_value('logical_answers', $enquiry_data['logical_answers']); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="physical_illness">Did your child ever suffer from a handicap or ailment where the school needs to be cautious in handling him / her?</label> 
                                            <input id="physical_illness" name="physical_illness" placeholder="" type="text" class="form-control"  value="<?php echo set_value('physical_illness', $enquiry_data['physical_illness']); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="kps_impression">Write briefly your impression about The Khaitan School.</label> 
                                            <input id="kps_impression" name="kps_impression" placeholder="" type="text" class="form-control"  value="<?php echo set_value('kps_impression', $enquiry_data['kps_impression']); ?>" />
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">    
                                  <div class="box-footer col-md-12">
                                      <a onclick="postQuesRecord(<?php echo $enquiry_data['id'] ?>,'questionnaire_details')" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></a> 
                                      
                                  </div>
                                </div>  
                            
                            </form>
                        </div><!--./row--> 
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>



<div class="modal fade" id="mySiblingModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title modal_title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="box-body">
                        <div class="sibling_msg">

                        </div>
                        <input  type="hidden" class="form-control" id="transport_student_session_id"  value="0" readonly="readonly"/>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $this->lang->line('class'); ?></label>
                            <div class="col-sm-10">
                                <select  id="sibiling_class_id" name="sibiling_class_id" class="form-control"  >
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                    <?php
foreach ($classlist as $class) {
    ?>
                                        <option value="<?php echo $class['id'] ?>"<?php if (set_value('sibiling_class_id') == $class['id']) {
        echo "selected=selected";
    }
    ?>><?php echo $class['class'] ?></option>
                                        <?php
$count++;
}
?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label"><?php echo $this->lang->line('section'); ?></label>
                            <div class="col-sm-10">
                                <select  id="sibiling_section_id" name="sibiling_section_id" class="form-control">
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                </select>
                                <span class="text-danger" id="transport_amount_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label"><?php echo $this->lang->line('student'); ?>
                            </label>

                            <div class="col-sm-10">
                                <select  id="sibiling_student_id" name="sibiling_student_id" class="form-control" >
                                    <option value=""   ><?php echo $this->lang->line('select'); ?></option>
                                </select>
                                <span class="text-danger" id="transport_amount_fine_error"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-primary add_sibling" id="load" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"><i class="fa fa-user"></i> <?php echo $this->lang->line('add'); ?></button>
            </div>
        </div>
    </div>

</div>
<div class="loader-wrap"><img src="<?php echo base_url() ?>/uploads/admin_images/loader.gif" /></div>
<div class="modal fade" id="follow_up" tabindex="-1" role="dialog" aria-labelledby="follow_up">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-media-content">
                <div class="modal-header modal-media-header">
                    <button type="button" class="close" onclick="update()" data-dismiss="modal">&times;</button>
                    <h4 class="box-title"><?php echo $this->lang->line('follow_up_admission_enquiry'); ?></h4>
                </div>
                <div class="modal-body pt0 pb0" id="getdetails_follow_up">
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="admission_fee" tabindex="-1" role="dialog" aria-labelledby="admission_fee">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" onclick="update()" data-dismiss="modal">&times; </button>
                <h4 class="box-title"><?php echo "Send Admission Fee Link"; ?></h4>
            </div>
            <div class="modal-body pt0 pb0" id="getdetails_follow_up">
                <div class="row row-eq">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Admission Fee</label><small class="req"> *</small>
                            <input id="admission_fee_amount_without_discount" readonly name="admission_fee_amount_without_discount" placeholder="Admission Fee" type="text" class="form-control" value="55000" autocomplete="off">
                            <span class="text-danger hide">Invalid amount. eg: 5000</span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Discount Amount</label>
                            <input id="admission_fee_discount" name="admission_fee_discount" placeholder="Discount Amount" type="text" class="form-control" value="" autocomplete="off">
                            <span class="text-danger validate-admission-fee-discount hide">Invalid discount amount. eg: 500</span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Final Admission Fee</label><small class="req"> *</small>
                            <input id="admission_fee_amount" readonly name="admission_fee_amount" placeholder="Admission Fee" type="text" class="form-control" value="55000" autocomplete="off">
                            <span class="text-danger validate-admission-fee hide">Invalid amount. eg: 5000</span>
                        </div>
                    </div>
                    
                </div>
                <div class="row row-eq">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea id="admission_fee_description" name="admission_fee_description" placeholder="Description" type="textarea" class="form-control" autocomplete="off" rows="4" cols="50"></textarea>
                            <span class="text-danger validate-fee-description hide">Admission fee description is Mandatory.</span>
                        </div>
                    </div>
                </div>
                <div class="row row-eq">
                    <div class="col-md-12">
                        <div class="form-group">
                            <a onclick="sendAdmissionLink(<?php echo $enquiry_data['id'] ?>)" class="btn btn-info pull-right" style="margin: 3px;">Send Admission Link</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loader-wrap"><img src="/uploads/admin_images/loader.gif"></div>
</div>    


<script type="text/javascript">


    $(document).ready(function () {
        var date_format = '<?php echo $result = strtr($this->customlib->getSchoolDateFormat(), ['d' => 'dd', 'm' => 'mm', 'Y' => 'yyyy']) ?>';
        var class_id = $('#class_id').val();
        var section_id = '<?php echo set_value('section_id', 0) ?>';
        var hostel_id = $('#hostel_id').val();
        var hostel_room_id = '<?php echo set_value('hostel_room_id', 0) ?>';
        //getHostel(hostel_id, hostel_room_id);
        getSectionByClass(class_id, section_id);

        $(document).on('change', '.append_other', function (e) {
            var value = $(this).val();
            if(value=="Other"){
                $(this).parent().next('div').show();
            }
            else $(this).parent().next('div').hide();
        });

        $(document).on('change', '#class_id', function (e) {
            $('#section_id').html("");
            var class_id = $(this).val();
           getSectionByClass(class_id, 0);
        });

        $('.datetime').datetimepicker({

        });
         $(".color").colorpicker();

        $("#btnreset").click(function () {
            $("#form1")[0].reset();
        });


         $(document).on('change', '#hostel_id', function (e) {
            var hostel_id = $(this).val();
           // getHostel(hostel_id, 0);

        });

 function getSectionByClass(class_id, section_id) {

        if (class_id != "" ) {
            $('#section_id').html("");
            var base_url = '<?php echo base_url() ?>';
            var div_data = '<option value=""><?php echo $this->lang->line('select'); ?></option>';
              var url = "<?php $userdata = $this->customlib->getUserData();
if (($userdata["role_id"] == 2)) {echo "getClassTeacherSection";} else {echo "getByClass";}?>";

            $.ajax({
                type: "GET",
                url: base_url + "sections/getByClass",
                data: {'class_id': class_id},
                dataType: "json",
                  beforeSend: function(){
                 $('#section_id').addClass('dropdownloading');
                 },
                success: function (data) {
                    $.each(data, function (i, obj)
                    {
                        var sel = "";
                        if (section_id == obj.section_id) {
                            sel = "selected";
                        }
                        div_data += "<option value=" + obj.section_id + " " + sel + ">" + obj.section + "</option>";
                    });
                    $('#section_id').append(div_data);
                },
               complete: function(){
              $('#section_id').removeClass('dropdownloading');
               }
            });
        }
    }



    });
    function auto_fill_guardian_address() {
        if ($("#autofill_current_address").is(':checked'))
        {
            $('#current_address').val($('#guardian_address').val());
        }
    }
    function auto_fill_address() {
        if ($("#autofill_address").is(':checked'))
        {
            $('#permanent_address').val($('#current_address').val());
        }
    }
    $('input:radio[name="guardian_is"]').change(
            function () {
                if ($(this).is(':checked')) {
                    var value = $(this).val();
                    if (value == "father") {
                        $('#guardian_name').val($('#father_name').val());
                        $('#guardian_phone').val($('#father_phone').val());
                        $('#guardian_occupation').val($('#father_occupation').val());
                        $('#guardian_relation').val("Father")
                    } else if (value == "mother") {
                        $('#guardian_name').val($('#mother_name').val());
                        $('#guardian_phone').val($('#mother_phone').val());
                        $('#guardian_occupation').val($('#mother_occupation').val());
                        $('#guardian_relation').val("Mother")
                    } else {
                        $('#guardian_name').val("");
                        $('#guardian_phone').val("");
                        $('#guardian_occupation').val("");
                        $('#guardian_relation').val("")
                    }
                }
            });


</script>

<script>
    $(document).ready(function () {
        var url=window.location.href;
        var arr=url.split('#');
        if(arr[1]=='myModal') { 
            $("#myModal").modal('show');
        }
        // $('#enquiry_date').daterangepicker();
        var date_format = '<?php echo $result = strtr($this->customlib->getSchoolDateFormat(), ['d' => 'dd', 'm' => 'mm', 'Y' => 'yyyy',]) ?>';

       

        $('#enquiry_date').daterangepicker({
            autoUpdateInput: false,
            format: date_format,
            autoclose: true,
             opens: 'right',
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#enquiry_date').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });

        $('#enquiry_date').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
        $(document).on("change", "#country_id", function(e) {
            e.preventDefault();
            _this = $(this);
            var country_id = _this.val();
            if (_this.val() != '') {
                $.ajax({
                    url: '<?php echo site_url('admin/enquiry/getStates') ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {country_id: _this.val()},
                    success: function(data) {
                       // console.log(data);
                       var form_id=_this.closest('form').attr("id");
                        $('#'+form_id+' #state_id').empty().append(data);                        
                    }
                });
            }
        });


        $(document).on("focus", "#admission_fee_amount", function() {
            //console.log($(".validate-admission-fee").html());
            $(".validate-admission-fee").addClass("hide");
            return false;
        });
        $(document).on("focus", "#admission_fee_description", function() {
            //console.log($(".validate-admission-fee").html());
            $(".validate-fee-description").addClass("hide");
            return false;
        });
        $(document).on("focus", "#admission_fee_discount", function() {
            $(".validate-admission-fee-discount").addClass("hide");
            return false;
        });
        $(document).on("blur", "#admission_fee_discount", function() {
            if(isNaN($(this).val()) === false && $(this).val() > 0){
                var admissionFee = $("#admission_fee_amount_without_discount").val() - $(this).val();
                $("#admission_fee_amount").val(admissionFee);
            }else if($(this).val() == ""){
                $(".validate-admission-fee-discount").addClass("hide");    
            }else{
                $(".validate-admission-fee-discount").removeClass("hide");    
            }
            return false;
        });


    });

   

    function getRecord(id,status) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/details/' + id+'/'+status,
            success: function (result) {
                $('#getdetails').html(result);
            }
        });
    }
    function postRecord(id, form_id) {

        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/editpost/' + id,
            type: 'POST',
            data: $("#"+form_id).serialize(),
            dataType: 'json',
            success: function (data) {

                if (data.status == "fail") {

                    var message = "";
                    $.each(data.error, function (index, value) {

                        message += value;
                    });
                    errorMsg(message);
                } else {

                    successMsg(data.message);
                    window.location.reload(true);
                }
            },
            error: function () {
                alert("Fail")
            }
        });

    }
    function sendRegistrationLink(enquiryid){
        $('.loader-wrap').show();
        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/registrationlink',
            type: 'POST',
            data: {'enquiryid':enquiryid},
            dataType: 'json',
            success: function (data) {
                if (data.status == "fail") {
                    var message = data.message;
                    errorMsg(message);
                } else {                    
                    successMsg(data.message);
                    window.location.reload(true);
                }
                $('.loader-wrap').hide();
            },
            error: function () {
                $('.loader-wrap').hide();
                alert("Fail")
            }
        });

    }
    
    function sendAdmissionLink(enquiryid){
        // validate input fields
        var discount_amount = 0;
        var admissionFeeDesc = "";
        if(isNaN($("#admission_fee_discount").val()) === false && $("#admission_fee_discount").val() > 0){
            var admissionFee = $("#admission_fee_amount_without_discount").val() - $("#admission_fee_discount").val();
            $("#admission_fee_amount").val(admissionFee);
            discount_amount = $("#admission_fee_discount").val();

            admissionFeeDesc = $("#admission_fee_description").val();
            if(admissionFeeDesc == "" || admissionFeeDesc == null){
                $(".validate-fee-description").removeClass("hide");
                return;
            }
        }else if($("#admission_fee_discount").val() == ""){

        }else if(isNaN($("#admission_fee_discount").val())){
            $(".validate-admission-fee-discount").removeClass("hide");   
            return;
        }
        var admissionFee = $("#admission_fee_amount").val();
        if(admissionFee == "" || admissionFee == null || isNaN(admissionFee)){
            $(".validate-admission-fee").removeClass("hide");
            return;
        }
        var admissionFeeBeforeDiscount = $("#admission_fee_amount_without_discount").val();
        
        
        $('.loader-wrap').show();
        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/admissionlink',
            type: 'POST',
            data: {'enquiryid':enquiryid, 'admissionFee':admissionFee, 'discountAmount':discount_amount, 'admissionFeeBeforeDiscount':admissionFeeBeforeDiscount, 'admissionFeeDesc':admissionFeeDesc},
            dataType: 'json',
            success: function (data) {
                if (data.status == "fail") {
                    var message = data.message;
                    errorMsg(message);
                } else {                    
                    successMsg(data.message);
                    window.location.reload(true);
                }
                $('.loader-wrap').hide();
            },
            error: function () {
                $('.loader-wrap').hide();
                alert("Fail")
            }
        }); 

    }
    function postRegRecord(id, form_id) {
        var form_data = new FormData($("#"+form_id)[0]);
        if(form_id=='registration_details'){
            var child_image = $('#child_image').prop('files')[0];
            var ext = $('#child_image').val().split('.').pop().toLowerCase();
            if ($('#child_image').val() != '' && $.inArray(ext, ['gif', 'jpg', 'jpeg', 'png']) == -1) {
                appendalert('error', "Please select valid image file.");
                return false;
            }
            if ($('#child_image').val() != '')
                form_data.append('child_image', child_image);
        }
        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/editpost_register/' + id,
            type: 'POST',
            data: form_data,//$("#"+form_id).serialize(),
            dataType: 'json',
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false
            success: function (data) {

                if (data.status == "fail") {

                    var message = "";
                    $.each(data.error, function (index, value) {

                        message += value;
                    });
                    errorMsg(message);
                } else {

                    successMsg(data.message);
                    window.location.reload(true);
                }
            },
            error: function () {
                alert("Fail")
            }
        });

    }

    function postQuesRecord(id, form_id) {

        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/editpost_questionnair/' + id,
            type: 'POST',
            data: $("#"+form_id).serialize(),
            dataType: 'json',
            success: function (data) {

                if (data.status == "fail") {

                    var message = "";
                    $.each(data.error, function (index, value) {

                        message += value;
                    });
                    errorMsg(message);
                } else {

                    successMsg(data.message);
                    window.location.reload(true);
                }
            },
            error: function () {
                alert("Fail")
            }
        });

    }

    

    function update() {

        window.location.reload(true);
    }
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    printContents.fontsize(25);
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>