<?php
//$response = $this->customlib->getResponse();
//$enquiry_type = $this->customlib->getenquiryType();
//$Source = $this->customlib->getComplaintSource();
//$Reference = $this->customlib->getReference();
?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <!-- Horizontal Form -->
        <?php //echo '-----'; print_r($enquiry_data); die;?>
        <form  action="<?php echo site_url('admin/enquiry') ?>" id="myForm1"  method="post"  class="ptt10">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="pwd">Child First Name</label><small class="req"> *</small>  
                        <input type="text" class="form-control" placeholder="First Name" id="name_value" value="<?php echo set_value('name', $enquiry_data['name']); ?>" name="name">
                        <span class="text-danger" id="name"></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="pwd">Child Last Name</label><small class="req"> *</small>  
                        <input type="text" class="form-control" placeholder="Last Name" id="last_name" value="<?php echo set_value('last_name', $enquiry_data['last_name']); ?>" name="last_name">
                        <span class="text-danger" id="last_name"></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="email">Gender</label> 
                       <select name="gender" class="form-control"  >
                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
<?php $gender_list=array('male'=>'Male','female'=>'Female');
foreach ($gender_list as $key => $value) {
    ?>

                                <option value="<?php echo $key ?>" <?php if (set_value('gender', $enquiry_data['gender']) == $key) { ?> selected="" <?php } ?>><?php echo $value; ?></option>

    <?php
}
?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="dob">Date of Birth</label>
                        <input type="text" id="dob" name="dob" placeholder="dd/mm/yyyy" class="form-control date" value="<?php
                        if (!empty($enquiry_data['dob'])) {
                            echo set_value('dob', date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($enquiry_data['dob'])));
                        }
                        ?>" readonly="">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="pwd"><?php echo $this->lang->line('class'); ?></label> 
                        <select name="class" class="form-control"  >
                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
<?php
foreach ($class_list as $key => $value) {
    ?>

                                <option value="<?php echo $value['id'] ?>" <?php if (set_value('class', $enquiry_data['class']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['class'] ?></option>

    <?php
}
?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="email">Session</label> 
                       <select name="session_id" id="session_id" class="form-control"  >
                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
<?php
foreach ($session_list as $value) {
    ?>

                                <option value="<?php echo $value['id'] ?>" <?php if (set_value('session_id', $enquiry_data['session_id']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['session'] ?></option>

    <?php
}
?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6" style="min-height: 75px;"><div class="form-group"></div></div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="father_firstname">First Name</label> 
                        <input id="father_firstname" name="father_firstname" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('father_firstname', $enquiry_data['father_firstname']); ?>" />
                        <span class="text-danger"><?php echo form_error('father_firstname'); ?></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="father_lastname"> Last Name</label> 
                        <input id="father_lastname" name="father_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('father_lastname', $enquiry_data['father_lastname']); ?>" />
                        <span class="text-danger"><?php echo form_error('father_lastname'); ?></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="father_email"> Email</label> 
                        <input id="father_email" name="father_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('father_email', $enquiry_data['father_email']); ?>" />
                        <span class="text-danger"><?php echo form_error('father_email'); ?></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="father_mobile"> Mobile</label> 
                        <input id="father_mobile" name="father_mobile" placeholder="Mobile" type="tel" class="form-control"  value="<?php echo set_value('father_mobile', $enquiry_data['father_mobile']); ?>" />
                        <span class="text-danger"><?php echo form_error('father_mobile'); ?></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="mother_firstname"> First Name</label> 
                        <input id="mother_firstname" name="mother_firstname" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('mother_firstname', $enquiry_data['mother_firstname']); ?>" />
                        <span class="text-danger"><?php echo form_error('mother_firstname'); ?></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="mother_lastname"> Last Name</label> 
                        <input id="mother_lastname" name="mother_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('mother_lastname', $enquiry_data['mother_lastname']); ?>" />
                        <span class="text-danger"><?php echo form_error('mother_lastname'); ?></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="mother_email"> Email</label> 
                        <input id="mother_email" name="mother_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('mother_email', $enquiry_data['mother_email']); ?>" />
                        <span class="text-danger"><?php echo form_error('mother_email'); ?></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="mother_mobile"> Mobile</label> 
                        <input id="mother_mobile" name="mother_mobile" placeholder="Mobile" type="tel" class="form-control"  value="<?php echo set_value('mother_mobile', $enquiry_data['mother_mobile']); ?>" />
                        <span class="text-danger"><?php echo form_error('mother_mobile'); ?></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="email">Address1</label> 
                        <input id="address" name="address" placeholder="Address1" type="text" class="form-control"  value="<?php echo set_value('address', $enquiry_data['address']); ?>" />
                        <span class="text-danger"><?php echo form_error('address'); ?></span>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="email">Address2</label> 
                        <input id="address2" name="address2" placeholder="Address2" type="text" class="form-control"  value="<?php echo set_value('address2', $enquiry_data['address2']); ?>" />
                        <span class="text-danger"><?php echo form_error('address2'); ?></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="email">City</label> 
                        <input id="city" name="city" placeholder="City" type="text" class="form-control"  value="<?php echo set_value('city', $enquiry_data['city']); ?>" />
                        <span class="text-danger"><?php echo form_error('city'); ?></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="email">Country</label> 
                        <select name="country_id"  id="country_id" class="form-control"  >
                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
<?php 
foreach ($country_list as $value) {
    ?>

     <option value="<?php echo $value['id'] ?>" <?php if (set_value('country_id', $enquiry_data['country_id']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['country']; ?></option>

    <?php
}
?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3"><?php //print_r($state_list);?>
                    <div class="form-group">
                        <label for="email">State</label> 
                       <select name="state_id" id="state_id" class="form-control"  >
                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
<?php 
foreach ($state_list as $value) {
    ?>

                                <option value="<?php echo $value['id'] ?>" <?php if (set_value('state_id', $enquiry_data['state_id']) == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['state']; ?></option>

    <?php
}
?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="email">Pin Code</label> 
                        <input id="pincode" name="pincode" placeholder="Pin Code" type="text" class="form-control"  value="<?php echo set_value('pincode', $enquiry_data['pincode']); ?>" />
                        <span class="text-danger"><?php echo form_error('pincode'); ?></span>
                    </div>
                </div>
                
                <div class="col-sm-6" style="min-height: 75px;"><div class="form-group"></div></div>
                
                <!--<div class="col-sm-3">
                    <div class="form-group">
                        <label for="pwd"><?php //echo $this->lang->line('note'); ?></label> 
                        <textarea name="note" class="form-control" ><?php //echo set_value('note', $enquiry_data['note']); ?></textarea>

                    </div>
                </div>-->
                
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="pwd"><?php echo $this->lang->line('date'); ?></label>
                        <input type="text" id="date_edit" name="date" class="form-control date" value="<?php
                        if (!empty($enquiry_data['date'])) {
                            echo set_value('date', date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($enquiry_data['date'])));
                        }
                        ?>" readonly="">
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="pwd"><?php echo $this->lang->line('next_follow_up_date'); ?></label>
                        <input type="text" id="date_of_call_edit" name="follow_up_date"class="form-control date" value="<?php
                        if (!empty($enquiry_data['follow_up_date'])) {
                            echo set_value('follow_up_date', date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($enquiry_data['follow_up_date'])));
                        }
                        ?>" readonly="">
                    </div>

                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label><?php echo $this->lang->line('assigned'); ?></label>

                        <select name="assigned" class="form-control">
                             <option value=""><?php echo $this->lang->line('select') ?></option>
<?php foreach ($staffusers as $value) { ?>
                                <option value="<?php echo $value['id']; ?>" <?php if (set_value('assigned', $enquiry_data['assigned']) == $value['id']) { ?>selected=""<?php } ?>><?php echo $value['name'].' '.$value['surname']; ?></option>    
<?php }
?>
                        </select>
                    </div>
                </div>






                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="pwd"><?php echo $this->lang->line('reference'); ?></label>   
                        <select name="reference" class="form-control">
                             <option value=""><?php echo $this->lang->line('select') ?></option>
<?php foreach ($Reference as $key => $value) { ?>
                                <option value="<?php echo $value['reference']; ?>" <?php if (set_value('reference', $enquiry_data['reference']) == $value['reference']) { ?>selected=""<?php } ?>><?php echo $value['reference']; ?></option>    
<?php }
?>
                        </select>
                        <span class="text-danger"><?php echo form_error('reference'); ?></span>
                    </div>
                </div>    
                <div class="col-sm-3">
                    
                    <div class="form-group">
                        <label for="pwd"><?php echo $this->lang->line('source'); ?></label><small class="req"> *</small>
                        <select name="source" class="form-control">
                            <option value=""><?php echo $this->lang->line('select') ?></option>
<?php foreach ($source as $key => $value) { ?>
                                <option value="<?php echo $value['source']; ?>"<?php if ($enquiry_data['source']==$value['source']) { echo "selected"; } ?>><?php echo $value['source']; ?></option>
<?php }
?> 
                        </select>
                    </div>
                </div>      
                <div class="col-sm-3 col-md-3">
                                <div class="form-group">  
                                    <label><?php echo $this->lang->line('status'); ?></label>
                                    <select  id="status" name="status" class="form-control" >
                                            <?php foreach ($statuses as $key=>$value) {
                                                ?>
                                            <option <?php
                                                if ($key == $enquiry_data['status']) {
                                                    echo "selected";
                                                }
                                                ?> value="<?php echo $key ?>"><?php echo $value ?></option>

<?php } ?>
                                    </select>
                                </div>  
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="email">Remarks</label>
                        <textarea name="description" class="form-control" ><?php echo set_value('description', $enquiry_data['description']); ?></textarea>
                    </div>
                </div>
                <!--<div class="col-sm-3">                
                    <div class="form-group">
                        <label for="pwd"><?php echo $this->lang->line('number_of_child'); ?></label> 
                        <input type="number" class="form-control" min="1" value="<?php echo set_value('no_of_child', $enquiry_data['no_of_child']); ?>" name="no_of_child">
                        <span class="text-danger"><?php echo form_error('no_of_child'); ?></span>
                    </div>
                </div>-->                 
            <div class="row">    
              <div class="box-footer col-md-12">
                  <a onclick="postRecord(<?php echo $enquiry_data['id'] ?>)" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></a> <a target="_blank" href="/student/create/<?php echo $enquiry_data['id']; ?>" style="margin-right: 3px;" class="btn btn-info pull-right">Send Registration Link</a> 
                  
              </div>
            </div>  
        </div><!--./row--> 
        </form>
    </div><!--./col-md-12-->

  

</div><!--./row--> 

<script>
    
</script>