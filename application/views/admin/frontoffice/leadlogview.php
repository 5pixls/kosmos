<table class="table table-hover table-striped table-bordered" id="leadlogtable">
                                        <thead>
                                            <tr>
                                                <th>Agent Name</th>
                                                <th>Description</th>
                                                <th>Created At</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($lead_activity_log)) {
                                                foreach ($lead_activity_log as $key => $value){
                                                    ?>
                                                    <tr>   
                                                        <td class="mailbox-name"><?php echo $value['name']; ?></td>
                                                        <td class="mailbox-name"><?php echo $value['description']; ?> </td>

                                                        <td class="mailbox-name"> <?php
                                                    if (!empty($value["created_at"])) {
                                                        echo date('d-M-Y h:i:s', strtotime($value['created_at']));
                                                    }
                                                    ?></td>

                                                        
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table><!-- /.table -->