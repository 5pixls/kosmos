<div class="table-responsive">
<table class="table table-hover table-striped table-bordered" id="calllogtable">
                                        <thead>
                                            <tr>
                                                <th>Agent Name</th>
                                                <th>Response Message</th>
                                                <th>Call Start Time</th>
                                                <th>Call End Time</th>
                                                <th>Call Duration</th>
                                                <th>Audio File</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($ozonetel_activity_log)) {
                                                foreach ($ozonetel_activity_log as $key => $value){
                                                    /*$current_date = date("Y-m-d");
                                                    $next_date = $value["next_date"];
                                                    if (empty($next_date)) {

                                                        $next_date = $value["follow_up_date"];
                                                    }

                                                    if ($next_date < $current_date) {
                                                        $class = "class='danger'";
                                                    } else {
                                                        $class = "";
                                                    }*/
                                                    ?>
                                                    <tr>   
                                                        <td class="mailbox-name"><?php echo $value['name']; ?></td>
                                                        <td class="mailbox-name"><?php echo $value['response_message']; ?> </td>

                                                        <td class="mailbox-name"> <?php
                                                    if (!empty($value["call_start_time"])) {
                                                        echo date('d-M-Y h:i:s', strtotime($value['call_start_time']));
                                                    }
                                                    ?></td>

                                                        <td class="mailbox-name"> <?php
                                                            if (!empty($value["dial_end_time"])) {
                                                                echo date('d-M-Y  h:i:s', strtotime($value['dial_end_time']));
                                                            }
                                                            ?></td>
                                                        <td class="mailbox-name"> <?php echo $value['call_duration']; ?></td>
                                                        <td class="mailbox-name">
                                                            <?php if($value['recording_url'] !=''){ ?>
                                                            <a href="<?php echo $value['recording_url']; ?>" target="_blank">►</a> 
                                                        <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table><!-- /.table -->
                                        </div>