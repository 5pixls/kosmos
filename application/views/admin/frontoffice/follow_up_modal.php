<div class="row row-eq">
    <?php
    //print_r($enquiry_data);
    $admin = $this->customlib->getLoggedInUserData();
    // print_r($admin);
    ?>
    <div class="nav-tabs-custom theme-shadow">
        <a href="javascript:void(0)" class="active-tab">Lead Details</a>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#followUp_tab_lead_details" data-toggle="tab">Lead Details</a></li>
                        <li><a href="#followUp_tab_followup_details" data-toggle="tab">Follow up Details</a></li>
                        <li><a href="#followUp_tab_lead_calldetails" data-toggle="tab">Lead Call Logs</a></li>
                        <li><a href="#followUp_tab_lead_logdetails" data-toggle="tab">Activity Logs</a></li>
                    </ul>
                    <div class="tab-content">
                        
                    
                        <div class="tab-pane active" id="followUp_tab_lead_details">
                            <div class="col-md-12 paddlr">
                                    <div class="taskside">
                                        <?php //echo $enquiry_data['status'].'--'.print_r($statuses); ?>
                                        <?php $column=$this->config->item('status_color');?>
                                        <h4><?php 
                                        $name='<strong>Child Name: </strong>'.$enquiry_data['name'].' ' .$enquiry_data['last_name'];
                                            if($enquiry_data['name']=='') $name='<strong>Father Name: </strong>'.$enquiry_data['father_firstname'].' ' .$enquiry_data['father_lastname'];
                                            echo $name; ?>
                                            <div style="font-size: 15px;" class="box-tools pull-right">
                                                <label><strong><?php echo $this->lang->line('status'); ?>: </strong></label>
                                                <span class="kosmos-status" <?php if ($column[$enquiry_data['status']]) : ?>style="border-color:<?php echo $column[$enquiry_data['status']]; ?>; color:<?php echo $column[$enquiry_data['status']]; ?>" <?php endif; ?>>
                                                <?php if (array_key_exists($enquiry_data['status'], $statuses)) {
                                                  echo $statuses[$enquiry_data['status']];
                                                }
                                                ?>
                                                </span>
                                                
                                            </div>
                                        </h4>
                                        <!-- /.box-tools -->
                                        <h5 class="pt0 task-info-created">
                                            <small class="text-dark">Lead Assigned to: <span class="text-dark"><strong><?php echo $enquiry_data['staffname']; ?></strong></span></small>
                                        </h5>

                                        <hr class="taskseparator" />
                                        <div class="task-info task-single-inline-wrap task-info-start-date">
                                            <h5><i class="fa task-info-icon fa-fw fa-lg fa-calendar-plus-o pull-left fa-margin"></i>
                                        <b><?php echo $this->lang->line('enquiry'); ?> <?php echo $this->lang->line('date'); ?></b>: <?php echo date('d-M-Y H:i:s', strtotime($enquiry_data['created_at']));
                                         ?>                                      
                                            </h5>
                                        </div>

                                        <div class="task-info task-single-inline-wrap task-info-start-date">
                                            <h5><i class="fa task-info-icon fa-fw fa-lg fa-calendar-plus-o pull-left fa-margin"></i>
                                                <b>Follow Up Date</b>: <?php
                                                if (!empty($next_date)) {
                                                    echo date('d-M-Y H:i:s', strtotime($next_date['next_date']));
                                                }
                                                ?>                                      
                                            </h5>
                                        </div>
                                        <div class="task-info task-single-inline-wrap task-info-start-date"></div>
                                        <!--<div class="task-info task-single-inline-wrap task-info-start-date">
                                            <h5><i class="fa task-info-icon fa-fw fa-lg fa-calendar-plus-o pull-left fa-margin"></i>
                                                <?php echo $this->lang->line('next_follow_up_date'); ?>: <?php
                                                if (!empty($next_date)) {
                                                    echo date('d-M-Y H:i:s', strtotime($next_date[0]['next_date']));
                                                } else {
                                                    echo date('d-M-Y H:i:s',strtotime($enquiry_data['follow_up_date']));
                                                }
                                                ?>                                       
                                            </h5>
                                        </div>-->
                                        <div class="task-info task-info-labels task-single-inline-wrap ptt10">
                                            <?php $parent_phone=$enquiry_data['father_mobile'];
                                            if($enquiry_data['father_mobile']=='') $parent_phone=$enquiry_data['mother_mobile'];?>
                                            <label><strong><?php echo $this->lang->line('phone'); ?>:</strong> <a href="#" onclick="return callOzonetel(this);"  data-id="<?php echo $enquiry_data['id']; ?>"> <i class="fa fa-phone"></i> <?php echo $parent_phone; ?> </a>
                                             <p class="ozonetel-response"></p>
                                            </label>
                                            <label><strong><?php echo $this->lang->line('email'); ?>:</strong> <?php echo $enquiry_data['father_email']; ?></label>
                                            <label><strong><?php echo $this->lang->line('address'); ?>:</strong> <?php echo $enquiry_data['address']; ?></label>
                                            <label><strong><?php echo $this->lang->line('reference'); ?>:</strong> <?php echo $enquiry_data['reference']; ?></label>
                                            <label><strong>Remarks:</strong> <?php echo $enquiry_data['description']; ?></label>
                                            <!--<label><?php ///echo $this->lang->line('note'); ?>: <?php //echo $enquiry_data['note']; ?></label>-->
                                            <label><strong><?php echo $this->lang->line('source'); ?>:</strong> <?php echo $enquiry_data['source']; ?></label>
                                            <label><strong><?php echo $this->lang->line('assigned'); ?>:</strong> <?php echo $assigned_user['name'].' '.$assigned_user['surname']; ?></label>
                                            
                                            <label><strong><?php echo $this->lang->line('class'); ?>:</strong> <?php echo $enquiry_data['classname']; ?></label>
                                            <!--<label><?php //echo $this->lang->line('number_of_child'); ?>: <?php //echo $enquiry_data['no_of_child']; ?></label>-->
                                        </div> 
                                    </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="followUp_tab_followup_details">
                                <div class="col-lg-12 col-md-12 col-sm-12 paddlr">
                                    <!-- general form elements -->

                                    <form id="folow_up_data" method="post" class="ptt10"><input type="hidden" id="enquiry_id" name="enquiry_id" value="<?php echo $enquiry_data['id'] ?>"><input type="hidden" id="enquiry_status" name="enquiry_status" value="<?php echo $enquiry_data['status'] ?>">
                                        <div class="row">
                                            <!--<div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('follow_up_date'); ?></label><small class="req"> *</small>

                                                    
                                                    
                                                    <input type="text" id="follow_date" name="date" class="form-control date" value="<?php echo set_value('follow_up_date', date($this->customlib->getSchoolDateFormat(), $this->customlib->dateyyyymmddTodateformat($enquiry_data['date']))); ?>" readonly="">
                                                    <span class="text-danger" id="date_error"></span>
                                                </div>
                                            </div>-->
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <label for="dtp_inputfollow"><?php echo $this->lang->line('next_follow_up_date'); ?></label><small class="req"> *</small>
                                                    
                                                    <div class="input-group date form_datetime col-md-5" data-date="2020-10-08T05:25:07Z" data-date-format="dd-M-yyyy HH:ii P" data-link-field="dtp_inputfollow">
                                                        <input class="form-control" id="follow_date" size="16" type="text" placeholder="" name="follow_up_date" value="<?php echo set_value('follow_up_date') ?>" readonly="">
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                                    </div>
                                                    <input type="hidden" id="dtp_inputfollow" value="" />
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="form-group">
                                                    <label for="pwd"><?php echo $this->lang->line('response'); ?>/Comments</label><small class="req"> *</small> 
                                                    <select name="response" id="response" class="form-control" >
                                                        <option value="Not Shifting">Not Shifting</option>
                                                        <option value="Admission in Different School">Admission in Different School</option>
                                                        <option value="Sibling Admission Not Granted">Sibling Admission Not Granted</option>
                                                        <option value="Distance issue from home to school">Distance issue from home to school</option>
                                                        <option value="Other">Other</option>

                                                    </select> 
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 otherdiv" style="display:none;">
                                                <div class="form-group">
                                                    <label for="pwd">Other Response</label> 
                                                    <input id="other_response" name="other_response" placeholder="Other Response" type="text" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 followup-footer">
                                                <div class="form-group">
                                                    <?php 
                                                        if($this->rbac->hasPrivilege('follow_up_admission_enquiry','can_add')){
                                                    ?>
                                                        <a onclick="follow_save()" class="btn btn-info"><?php echo $this->lang->line('save'); ?></a>
                                                    <?php
                                                        }
                                                    ?> 
                                                </div>
                                            </div>
                                        </div><!-- /.box-body -->                                         

                                    </form>
                                    
                                 </div>
                                 <div class="col-lg-12 col-md-12 col-sm-12 paddlr followup-right">
                                    <div class="ptbnull">
                                        <h4 class="box-title titlefix pb5"><?php echo $this->lang->line('follow_up'); ?> Timeline</h4>
                                        <div class="box-tools pull-right">
                                        </div><!-- /.box-tools -->
                                    </div><!-- /.box-header -->
                                    <div class="pt20">

                                        <div class="tab-pane active" id="timeline">
                                            <!-- The timeline -->  
                                        </div>                                                        
                                    </div><!-- /.box-body -->
                                </div>
                        </div><!--/.col (left) -->
                        <div class="tab-pane" id="followUp_tab_lead_calldetails">
                            <div class="col-md-12">
                                <div class="table-responsive">  
                            <table class="table table-hover table-striped table-bordered" id="enquirytable">
                                <thead>
                                    <tr>

                                        <th>Child Name</th>
                                        <th><?php echo $this->lang->line('phone'); ?>
                                        </th>
                                        <th><?php echo $this->lang->line('source'); ?>
                                        </th>

                                        <th><?php echo $this->lang->line('enquiry'); ?> <?php echo $this->lang->line('date'); ?></th>
                                        <th><?php echo $this->lang->line('last_follow_up_date'); ?></th>
                                        <th><?php echo $this->lang->line('next_follow_up_date'); ?>
                                        </th>
                                        <th><?php echo $this->lang->line('status'); ?>
                                        </th>
                                        <th class="text-right"><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                                </table><!-- /.table -->
                              </div>  
                            </div>
                        </div>
                        <div class="tab-pane" id="followUp_tab_lead_logdetails">
                            <div class="col-md-12">
                                <div class="table-responsive">  
                            <table class="table table-hover table-striped table-bordered" id="enquirytable">
                                <thead>
                                    <tr>
                                        <th>Child Name</th>
                                        <th><?php echo $this->lang->line('phone'); ?>
                                        </th>
                                        <th><?php echo $this->lang->line('source'); ?>
                                        </th>

                                        <th><?php echo $this->lang->line('enquiry'); ?> <?php echo $this->lang->line('date'); ?></th>
                                        <th><?php echo $this->lang->line('last_follow_up_date'); ?></th>
                                        <th><?php echo $this->lang->line('next_follow_up_date'); ?>
                                        </th>
                                        <th><?php echo $this->lang->line('status'); ?>
                                        </th>
                                        <th class="text-right"><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                                </table><!-- /.table -->
                              </div>  
                            </div>
                        </div>
                    </div>
    </div> 
</div>
<script>
    $(document).ready(function () {
        $('#response').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            if(valueSelected == 'Other') $('.otherdiv').show();
            else $('.otherdiv').hide();
        });

    });
   /*
    function callOzonetel(cuObj){
          //console.log($(cuObj).attr("data-id"));      
          var lead_id = $(cuObj).attr("data-id");
          $.ajax({
             url: '<?php echo base_url(); ?>admin/enquiry/callOzonetel',
             dataType: 'text',
             type: 'post',
             contentType: 'application/x-www-form-urlencoded',
             data: {'lead_id':lead_id},
             success: function( data, textStatus, jQxhr ){
                console.log(data);
                var response = jQuery.parseJSON(data);
                var errElement = $(".ozonetel-response");
                if(response.status != "error"){
                   var successMsg = "Call is connecting...";//response.message;
                  // $.each(response.message, function (key, val) {
                  //    successMsg += val+" ";
                  // });
                   //var par = $(cuObj).closest("#lead_form");  
                   //var errElement = $(par).find(".ozonetel-response");
                  // var par = $(cuObj).closest("p"); 
                   
                   errElement.addClass("alert-success").addClass("show").removeClass("alert-danger").removeClass("hide").html(successMsg);
                   setTimeout(function() {
                      errElement.removeClass("show");
                      errElement.addClass("hide");
                   }, 20000);
                }else if(response.status == "error"){
                   var errMsg = response.message;
                  // $.each(response.message, function (key, val) {
                   //   errMsg += val+" ";
                   //});
                   //var par = $(cuObj).closest("#lead_form");  
                   //var errElement = $(par).find(".ozonetel-response");
                   //var par = $(cuObj).closest("p"); 
                   //var errElement = $(par).siblings(".ozonetel-response");
                   errElement.addClass("alert-danger").removeClass("alert-success").removeClass("hide").html(errMsg);
                   setTimeout(function() {
                      errElement.removeClass("show");
                      errElement.addClass("hide");
                   }, 20000);
                }
             
             },
             error: function( jqXhr, textStatus, errorThrown ){
                   console.log( errorThrown );
             }
          });
   }

    function follow_save() {
        //alert('Jai Shree Ram');
        var id = $('#enquiry_id').val();
         var status = $('#enquiry_status').val();
        var responce = $('#response').val();
        var follow_date = $('#follow_date').val();
        //  alert(follow_date);

        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/follow_up_insert',
            type: 'POST',
            dataType: 'json',
            data: $("#folow_up_data").serialize(),
            success: function (data) {

                //alert(data);

                if (data.status == "fail") {

                    var message = "";
                    $.each(data.error, function (index, value) {

                        message += value;
                    });
                    errorMsg(message);
                } else {

                    successMsg(data.message);
                    follow_up_new(id,status);
                }

               
            },

            error: function () {
                alert("Fail")
            }
        });


    }

    // function follow_up(id) {
    //     $.ajax({
    //         url: '<?php echo base_url(); ?>admin/enquiry/follow_up/' + id,
    //         success: function (data) {
    //             $('#getdetails_follow_up').html(data);
    //             $.ajax({
    //                 url: '<?php echo base_url(); ?>admin/enquiry/follow_up_list/' + id,
    //                 success: function (data) {
    //                     $('#timeline').html(data);
    //                 },
    //                 error: function () {
    //                     alert("Fail")
    //                 }
    //             });
    //         },
    //         error: function () {
    //             alert("Fail")
    //         }
    //     });
    // }


function follow_up_new(id, status) {
         
            $.ajax({
                url: '<?php echo base_url(); ?>admin/enquiry/follow_up/' + id + '/' + status,
                success: function (data) {
                    $('#getdetails_follow_up').html(data);
                    $.ajax({
                        url: '<?php echo base_url(); ?>admin/enquiry/follow_up_list/' + id,
                        success: function (data) {
                            $('#timeline').html(data);
                            get_call_log_list(id);
                            get_lead_log_list(id);
                        },
                        error: function () {
                            alert("Fail")
                        }
                    });
                },
                error: function () {
                    alert("Fail")
                }
            });
        }

    function changeStatus(status, id) {

       //alert(status+id);

        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/change_status/',
            type: 'POST',
            dataType: 'json',
            data: {status: status, id: id},
            success: function (data) {
                if (data.status == "fail") {

                    errorMsg(data.message);
                } else {

                    successMsg(data.message);
                    follow_up_new(id,status);
                }
            }

        })
    }*/

</script>