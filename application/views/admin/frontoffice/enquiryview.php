<div class="content-wrapper" style="min-height: 348px;">  
    <section class="content-header">
        <h1>
            <i class="fa fa-ioxhost"></i> <?php echo $this->lang->line('front_office'); ?></h1> 
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> <?php echo $this->lang->line('select_criteria'); ?></h3>
                        <div class="pull-right box-tools impbtntitle">
                            <?php if ($this->rbac->hasPrivilege('admission_enquiry', 'can_add')) {   ?>
                                <a href="<?php echo site_url('admin/enquiry/import') ?>">
                                    <button class="btn btn-primary btn-sm"><i class="fa fa-upload"></i> Import Leads</button>
                                </a>
                            <?php } 
                           
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('msg') ?>
                    </div>
                    <form role="form" action="<?php echo site_url('admin/enquiry') ?>" method="post" class="">
                        <div class="box-body row">

                            <?php echo $this->customlib->getCSRF(); ?>
                             <div class="col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('enquiry'); ?> ID</label>
                                        <input type="text" autocomplete="off" name="enquiryid" class="form-control pull-right" id="enquiryid" placeholder="Ex. E2020-10">
                                </div>
                            </div> 

                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('enquiry'); ?> <?php echo $this->lang->line('date'); ?></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" readonly="" autocomplete="off" name="enquiry_date" class="form-control pull-right" id="enquiry_date">
                                    </div>
                                                      <!-- <input type="text" class="form-control" autocomplete="off"  name="enquiry_date" id="enquiry_date">
                                    -->  <span class="text-danger"><?php echo form_error('enquiry_date'); ?></span>
                                </div>
                            </div> 

                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">  
                                    <label><?php echo $this->lang->line('source'); ?></label>
                                    <select  id="source" name="source" class="form-control" >
                                        <option value=""><?php echo $this->lang->line('select') ?></option>

                                        <?php foreach ($sourcelist as $key => $value) { ?>
                                            <option <?php
                                            if ($value["source"] == $source_select) {
                                                echo "selected";
                                            }
                                            ?> value="<?php echo $value["source"] ?>"><?php echo $value["source"] ?></option>
<?php } ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('source'); ?></span>
                                </div>  
                            </div>
                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">  
                                    <label><?php echo $this->lang->line('status'); ?></label>
                                    <select  id="status" name="status" class="form-control" >
                                        <option value=""><?php echo $this->lang->line('select') ?></option>
                                        <!--<option value="all" <?php
                                                if ($status == "all") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo $this->lang->line('all') ?></option>-->
                                            <?php foreach ($enquiry_status as $enkey => $envalue) {
                                                ?>
                                            <option <?php
                                                if ($enkey == $status) {
                                                    echo "selected";
                                                }
                                                ?> value="<?php echo $enkey ?>"><?php echo $envalue ?></option>

<?php } ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('status'); ?></span>
                                </div>  
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" name="search" value="search_filter" class="btn btn-primary btn-sm checkbox-toggle pull-right"><i class="fa fa-search"></i> <?php echo $this->lang->line('search'); ?></button>
                                </div>
                            </div>
                       </div>     
                    </form>
            <div class="ptt10">

                <div class="bordertop">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix"> <?php echo $this->lang->line('front_office'); ?></h3>
                        <div class="box-tools pull-right">
<?php if ($this->rbac->hasPrivilege('admission_enquiry', 'can_add')) { ?>
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> <?php echo $this->lang->line('add'); ?></button> 
<?php } ?>      
                        </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="download_label"><?php echo $this->lang->line('admission_enquiry'); ?> <?php echo $this->lang->line('list'); ?></div>
                        <div class="mailbox-messages">
                          <div class="table-responsive">  
                            <table class="table table-hover table-striped table-bordered" id="enquirytable">
                                <thead>
                                    <tr>

                                        <th style="display:none;"></th>
                                        <th>Enquiry ID</th>
                                        <th>Child Name</th>
                                        <th>Mobile
                                        </th>
                                        <th>Class</th>
                                        <th><?php echo $this->lang->line('source'); ?>
                                        </th>

                                        <th><?php echo $this->lang->line('enquiry'); ?> <?php echo $this->lang->line('date'); ?></th>
                                        <th style="display:none;"></th>
                                        <th><?php echo $this->lang->line('next_follow_up_date'); ?>
                                        </th>
                                        <th style="min-width: 105px;"><?php echo $this->lang->line('status'); ?>
                                        </th>
                                        <th style="min-width: 85px;" class="text-right"><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //echo "<pre>"; print_r($enquiry_list); echo "<pre>";die;
                                    if (empty($enquiry_list)) {
                                        ?>
                                        <?php
                                    } else {
                                        $column=$this->config->item('status_color');
                                        foreach ($enquiry_list as $key => $value) {
                                            $current_date = date("Y-m-d");
                                            $next_date = $value["next_date"];
                                            if (empty($next_date)) {

                                                $next_date = $value["follow_up_date"];
                                            }

                                            if ($next_date < $current_date) {
                                                $class = "class='danger'";
                                            } else {
                                                $class = "";
                                            }
                                            $parent_phone=$value['father_mobile'];
                                            if($value['father_mobile']=='') $parent_phone=$value['mother_mobile'];
                                            ?>
                                            <tr <?php echo $class ?>>

                                                <td  style="display:none;"><?php echo $value['id']; ?></td>
                                                <td class="mailbox-name"><?php echo $value['enquiryid']; ?></td>
                                                <td class="mailbox-name"><?php echo $value['name']; ?> <?php echo $value['last_name']; ?></td>
                                                <td class="mailbox-name"><?php echo $parent_phone; ?> </td>
                                                <td class="mailbox-name"><?php echo $value['classname']; ?></td>
                                                <td class="mailbox-name"><?php echo $value['source']; ?></td>

                                                <td class="mailbox-name"> <?php
                                            if (!empty($value["created_at"])) {
                                                echo date('d-M-Y h:i:s', strtotime($value['created_at']));
                                            }
                                            ?></td>
                                                <td class="mailbox-name" style="display: none;"> <?php 
                                                    if ($next_date!='' && $next_date!=NULL && $next_date!='0000-00-00 00:00:00') {
                                                        echo $next_date;
                                                        
                                                    }
                                                    ?></td>
                                                
                                                <td class="mailbox-name"> <?php 
                                                    if ($next_date!='' && $next_date!=NULL && $next_date!='0000-00-00 00:00:00') {
                                                        echo date('d-M-Y H:i:s', strtotime($next_date));
                                                        
                                                    }
                                                    ?></td>

                                                <td> <div>
                                                    <span class="kosmos-status" <?php if ($column[$value['status']]) : ?>style="border-color: <?php echo $column[$value['status']]; ?>; color: <?php echo $column[$value['status']]; ?>"<?php endif; ?>>
                                                        <?php if (array_key_exists($value['status'], $enquiry_status)) {
                                                        echo $enquiry_status[$value['status']];
                                                        }
                                                        ?>
                                                    </span>
                                                </div>
                                                </td>              
                                                <td class="mailbox-date text-right">
                                                    <?php  
                                                    if(($role_id ==1 || $role_id == 7 || $role_id ==11)) {?><a target="_blank" href="/student/create/<?php echo $value['id']; ?>" class="btn btn-default btn-xs" title="Covert to Student"><i class="fa fa-plus"></i>
                                                        </a>
                                                    <?php } ?>
        <?php if ($this->rbac->hasPrivilege('follow_up_admission_enquiry', 'can_view')) { ?>
                                                        <a class="btn btn-default btn-xs" onclick="follow_up('<?php echo $value['id']; ?>','<?php echo $value['status']; ?>');"  data-target="#follow_up" data-toggle="modal"  title="<?php echo $this->lang->line('follow_up_admission_enquiry'); ?>">
                                                            <i class="fa fa-phone"></i>
                                                        </a>
                                                    <?php }
                                                    ?>
        <?php if ($this->rbac->hasPrivilege('admission_enquiry', 'can_edit')) { ?>
                                                        
                                                        <!--<a  onclick="getRecord('<?php //echo $value['id']; ?>','<?php //echo $value['status']; ?>')" class="btn btn-default btn-xs" data-target="#myModaledit" data-toggle="modal"   title="<?php //echo $this->lang->line('edit'); ?>"><i class="fa fa-pencil"></i>
                                                        </a>-->
                                                        <a target="_blank" href="/admin/enquiry/leaddetails/<?php echo $value['id']; ?>" class="btn btn-default btn-xs" title="Edit"><i class="fa fa-pencil"></i>
                                                        </a>
                                                        

                                            <?php }
                                            ?>
                                            <?php if ($this->rbac->hasPrivilege('admission_enquiry', 'can_delete')) { ?>
                                                        <a data-placement="left" href="#" class="btn btn-default btn-xs" data-toggle="tooltip" title="" onclick="delete_enquiry('<?php echo $value["id"] ?>')" data-original-title="<?php echo $this->lang->line('delete'); ?>">
                                                            <i class="fa fa-remove"></i>
                                                        </a>
        <?php }
        ?>

                                                </td>


                                            </tr>
        <?php
    }
}
?>
                                </tbody>
                            </table><!-- /.table -->
                          </div>  
                        </div><!-- /.mail-box-messages -->
                    </div><!-- /.box-body -->
                </div>
            </div>
          </div>  
        </div>

    </section>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-media-content">
                <div class="modal-header modal-media-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="box-title">Add Lead Details</h4> 
                </div>

                <div class="modal-body pt0 pb0">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <form id="formadd" method="post" class="ptt10">
                                <div class="tshadow mb25 bozero">
                                    <h4 class="pagetitleh2">Child Details</h4>
                                    <div class="row around10">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="pwd">Child First Name</label><small class="req"> *</small>  
                                            <input type="text" id="name_add" placeholder="First Name" autocomplete="off" class="form-control" value="<?php echo set_value('name'); ?>" name="name">
                                            <span id="name_add_error" class="text-danger"></span>
                                        </div>

                                    </div>
                                    <div class="col-sm-3">
                                         <div class="form-group">
                                            <label for="pwd">Child Last Name</label><small class="req"> *</small>  
                                            <input type="text" id="last_name" placeholder="Last Name" autocomplete="off" class="form-control" value="<?php echo set_value('last_name'); ?>" name="last_name">
                                            <span id="last_name_error" class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="email">Gender</label> <small class="req"> *</small>
                                           <select name="gender" class="form-control"  >
                                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                                <?php $gender_list=array('male'=>'Male','female'=>'Female');
                                                foreach ($gender_list as $key => $value) {
                                                    ?>

                                                    <option value="<?php echo $key ?>"><?php echo $value; ?></option>

                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="pwd">Date of Birth</label><small class="req"> *</small>
                                            <input type="text" id="dob" name="dob" placeholder="dd/mm/yyyy" class="form-control dob" value="<?php echo set_value('dob', date($this->customlib->getSchoolDateFormat())); ?>" readonly="">
                                        </div>
                                    </div>  
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="pwd"><?php echo $this->lang->line('class'); ?></label> <small class="req"> *</small>
                                            <select name="class" class="form-control"  >
                                                <option value=""><?php echo $this->lang->line('select') ?></option>
                                        <?php
                                        foreach ($class_list as $key => $value) {
                                            ?>
                                                    <option value="<?php echo $value['id'] ?>" <?php if (set_value('class') == $value['id']) { ?> selected="" <?php } ?>><?php echo $value['class'] ?></option>
                                                <?php
                                            }
                                            ?>
                                            </select>                                            
                                        </div><!--./form-group-->
                                    </div> 
               <div class="col-sm-3">
                    <div class="form-group">
                        <label for="email">Session</label> <small class="req"> *</small>
                       <select name="session_id" id="session_id" class="form-control"  >
                            <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
<?php 
foreach ($session_list as $value) {
    ?>

                                <option value="<?php echo $value['id'] ?>"><?php echo $value['session'] ?></option>

    <?php
}
?>
                        </select>
                    </div>
                </div>
                </div>
                </div>
                <!-- <div class="col-sm-6" style="min-height: 75px;">
                                        <div class="form-group"></div>
                </div> -->
                <div class="tshadow mb25 bozero">
                    <h4 class="pagetitleh2">Father Details</h4>
                    <div class="row around10">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="father_title">Title:</label> 
                                <select name="father_title" class="form-control"  >
                                <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php $father_title_list=array('Mr.'=>'Mr.','Dr.'=>'Dr.','Bro.'=>'Bro.');
                                    foreach ($father_title_list as $key => $value) {
                                        ?>

                                    <option value="<?php echo $key ?>"><?php echo $value; ?></option>

                                        <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>
                    <div class="col-sm-3">
                            <div class="form-group">
                                <label for="father_firstname">First Name<small class="req"> *</small></label> 
                                <input id="father_firstname" name="father_firstname" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('father_firstname'); ?>" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="father_lastname">Last Name</label> 
                                <input id="father_lastname" name="father_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('father_lastname'); ?>" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="father_email">Email</label> 
                                <input id="father_email" name="father_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('father_email'); ?>" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="father_mobile">Mobile<small class="req"> *</small></label> 
                                <input id="father_mobile" name="father_mobile" placeholder="Mobile" type="tel" class="form-control form-mobile" maxlength="14" value="<?php echo set_value('father_mobile'); ?>" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="father_mobile">Email Prefrence</label>  
                                            <div class="material-switch">
                                                <input id="father_email_pref" name="father_email_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', '1') == 1) ? TRUE : FALSE); ?>>
                                                <label for="father_email_pref" class="label-success"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="father_mobile">SMS Prefrence</label>  
                                            <div class="material-switch">
                                                <input id="father_sms_pref" name="father_sms_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', '1') == 1) ? TRUE : FALSE); ?>>
                                                <label for="father_sms_pref" class="label-success"></label>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                </div>
                <div class="tshadow mb25 bozero">
                    <h4 class="pagetitleh2">Mother Details</h4>
                    <div class="row around10">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="mother_title">Title:</label> 
                        <select name="mother_title" class="form-control"  >
                        <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                            <?php $mother_title_list=array('Ms.'=>'Ms.','Sr.'=>'Sr.','Mrs.'=>'Mrs.','Dr.'=>'Dr.');
                                                foreach ($mother_title_list as $key => $value) {
                                                    ?>

                            <option value="<?php echo $key ?>"><?php echo $value; ?></option>

                                <?php
                            }
                            ?>
                    </select>
                    </div>
                </div>
                <!-- <div class="col-sm-9" style="min-height: 75px;">
                    <div class="form-group"></div>
                </div> -->
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="mother_firstname">First Name</label> 
                        <input id="mother_firstname" name="mother_firstname" placeholder="First Name" type="text" class="form-control"  value="<?php echo set_value('mother_firstname'); ?>" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="mother_lastname">Last Name</label> 
                        <input id="mother_lastname" name="mother_lastname" placeholder="Last Name" type="text" class="form-control"  value="<?php echo set_value('mother_lastname'); ?>" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="mother_email">Email</label> 
                        <input id="mother_email" name="mother_email" placeholder="Email" type="email" class="form-control"  value="<?php echo set_value('mother_email'); ?>" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="mother_mobile">Mobile</label> 
                        <input id="mother_mobile" name="mother_mobile" placeholder="Mobile" type="tel" class="form-control form-mobile" maxlength="14"  value="<?php echo set_value('mother_mobile'); ?>" />
                    </div>
                </div>
                <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="father_mobile">Email Prefrence</label>  
                                            <div class="material-switch">
                                                <input id="mother_email_pref" name="mother_email_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', '') == 1) ? TRUE : FALSE); ?>>
                                                <label for="mother_email_pref" class="label-success"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="mother_mobile">SMS Prefrence</label>  
                                            <div class="material-switch">
                                                <input id="mother_sms_pref" name="mother_sms_pref" type="checkbox" class="chk" value="1" <?php echo set_checkbox('is_active_sidebar', '1', (set_value('is_active_sidebar', '') == 1) ? TRUE : FALSE); ?>>
                                                <label for="mother_sms_pref" class="label-success"></label>
                                            </div>
                                        </div>
                                    </div>
                </div>
                </div>
                <div class="tshadow mb25 bozero">
                    <h4 class="pagetitleh2">Student Address Details</h4>
                    <div class="row around10">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="email">Address1</label> 
                                <input id="address" name="address" placeholder="Address1" type="text" class="form-control"  value="<?php echo set_value('address'); ?>" />
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="email">Address2</label> 
                                <input id="address2" name="address2" placeholder="Address2" type="text" class="form-control"  value="<?php echo set_value('address2'); ?>" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="email">City</label> 
                                <input id="city" name="city" placeholder="City" type="text" class="form-control"  value="<?php echo set_value('city'); ?>" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="email">Country</label> 
                                <select name="country_id" id="country_id" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                    <?php 
                                    foreach ($country_list as $value) {
                                        ?>

                                         <option value="<?php echo $value['id'] ?>" ><?php echo $value['country']; ?></option>

                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="email">State</label> 
                               <select name="state_id"  id="state_id" class="form-control"  >
                                    <option value="" selected=""><?php echo $this->lang->line('select') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="email">Pin Code</label> 
                                <input id="pincode" name="pincode" placeholder="Pin Code" type="text" class="form-control"  value="<?php echo set_value('pincode'); ?>" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="email">Landline</label> 
                                <input id="landline" name="landline" placeholder="Landline" type="text" class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-sm-6" style="min-height: 75px;">
                                        <div class="form-group"></div>
                                    </div> -->
                

                <!--<div class="col-sm-3">
                    <div class="form-group">
                        <label for="pwd"><?php //echo $this->lang->line('note'); ?></label> 
                        <textarea name="note" class="form-control" ><?php //echo set_value('note', $enquiry_data['note']); ?></textarea>

                    </div>
                </div>-->
                

                <div class="tshadow mb25 bozero">
                    <h4 class="pagetitleh2">Miscellaneous Details</h4>
                    <div class="row around10">    
                                    <!--<div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="pwd"><?php echo $this->lang->line('date'); ?></label>
                                            <input type="text" id="date" name="date" class="form-control date" value="<?php echo set_value('date', date($this->customlib->getSchoolDateFormat())); ?>" readonly="">
                                            <span id="date_add_error" class="text-danger"></span>
                                        </div>
                                    </div>-->
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="dtp_inputenq"><?php echo $this->lang->line('next_follow_up_date'); ?></label>
                                        
                                            <div class="input-group date form_datetime col-md-5" data-date="2020-10-08T05:25:07Z" data-date-format="dd-M-yyyy HH:ii P" data-link-field="dtp_inputenq">
                                                <input class="form-control" id="date_of_call" size="16" type="text" placeholder="dd/mm/yyyy" name="follow_up_date" value="<?php echo set_value('follow_up_date', date($this->customlib->getSchoolDateFormat())); ?>" readonly="">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                            </div>
                                            <input type="hidden" id="dtp_inputenq" value="" />

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('assigned'); ?></label>
                                            <select name="assigned" class="form-control">
                             <option value=""><?php echo $this->lang->line('select') ?></option>
<?php foreach ($staffusers as $value) { ?>
                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name'].' '.$value['surname']; ?></option>    
<?php }
?>
                        </select>
                                        </div><!--./form-group-->
                                    </div>



                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="pwd"><?php echo $this->lang->line('reference'); ?></label>   
                                            <select name="reference" class="form-control">
                                                <option value=""><?php echo $this->lang->line('select') ?></option>  
                                                <?php foreach ($Reference as $key => $value) { ?>
                                                    <option value="<?php echo $value['reference']; ?>" <?php if (set_value('reference') == $value['reference']) { ?>selected=""<?php } ?>><?php echo $value['reference']; ?></option>    
                                                <?php }
                                                ?>
                                            </select>
                                        </div><!--./form-group-->
                                    </div>
                                    <div class="col-sm-3">    
                                        <div class="form-group">
                                            <label for="pwd"><?php echo $this->lang->line('source'); ?></label> <small class="req"> *</small> 
                                            <select name="source" class="form-control">
                                                <option value=""><?php echo $this->lang->line('select') ?></option>  
                                                <?php foreach ($sourcelist as $key => $value) { ?>
                                                    <option value="<?php echo $value['source']; ?>"><?php echo $value['source']; ?></option>
                                                <?php }
                                                ?> 
                                            </select>
                                        </div><!--./form-group-->
                                    </div>    
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <label for="email">Remarks</label>
                                            <textarea name="description" class="form-control" ><?php echo set_value('description'); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                </div>
<!--
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="pwd"><?php //echo $this->lang->line('note'); ?></label> 
                                            <textarea name="note" class="form-control" ><?php //echo set_value('note'); ?></textarea>
                                        </div>
                                    </div>
                                       

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="pwd"><?php //echo $this->lang->line('number_of_child'); ?></label> 
                                            <input type="number" class="form-control" min="1" value="<?php //echo set_value('no_of_child'); ?>" name="no_of_child">
                                        </div>
                                    </div> -->   <!--./form-group--> 
                                <!-- </div>./row     -->
                            </form>                       
                        </div><!--./col-md-12-->       

                    </div><!--./row--> 

                
                 <div class="row">    
                    <div class="box-footer col-md-12">

                        <a  onclick="saveEnquiry()" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></a>
                    </div>
                </div>
            </div>
            </div>
        </div>    
    </div>
    <div class="modal fade" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-media-content">
                <div class="modal-header modal-media-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="box-title"><?php echo $this->lang->line('edit_admission_enquiry'); ?></h4>

                </div>
                <div class="modal-body pt0 pb0" id="getdetails">
                    <div id="alert_message">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="follow_up" tabindex="-1" role="dialog" aria-labelledby="follow_up">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-media-content">
                <div class="modal-header modal-media-header">
                    <button type="button" class="close" onclick="update()" data-dismiss="modal">&times;</button>
                    <h4 class="box-title"><?php echo $this->lang->line('follow_up_admission_enquiry'); ?></h4>
                </div>
                <div class="modal-body pt0 pb0" id="getdetails_follow_up">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var url=window.location.href;
        var arr=url.split('#');
        if(arr[1]=='myModal') { 
            $("#myModal").modal('show');
        }
        // $('#enquiry_date').daterangepicker();
       var date_format = '<?php echo $result = strtr('d/m/Y', ['d' => 'dd', 'm' => 'mm', 'Y' => 'yyyy',]) ?>';
        var currentDate = new Date();
        var sDate = '<?php echo date('d/m/Y', strtotime($sDate)); ?>';
        var eDate = '<?php echo date('d/m/Y', strtotime($eDate)); ?>';
        $('#enquiry_date').daterangepicker({
            autoUpdateInput: true,
            format: date_format,
            // endDate: "today",
            maxDate: currentDate,
            autoclose: true,
             opens: 'right',
            locale: {
                format: 'DD/MM/YYYY',
                cancelLabel: 'Clear'
            }           
        });
        $("#enquiry_date").data('daterangepicker').setStartDate(sDate);
        $("#enquiry_date").data('daterangepicker').setEndDate(eDate);


        $( "#enquiry_date" ).val( sDate + " - " + eDate );
        //$( "#enquiry_date" ).data('daterangepicker').startDate = moment( sDate, "DD/MM/YYYY" );
        //$( "#enquiry_date" ).data('daterangepicker').endDate = moment( eDate, "DD/MM/YYYY" );
        $( "#enquiry_date" ).data('daterangepicker').updateView();
        $( "#enquiry_date" ).data('daterangepicker').updateCalendars();
        /*,
            startDate:sDate.format('MM/DD/YYYY'),
            endDate:eDate.format('MM/DD/YYYY')*/
        $('#enquiry_date').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });

        $('#enquiry_date').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
        $(document).on("change", "#country_id", function(e) {
            e.preventDefault();
            _this = $(this);
            var country_id = _this.val();
            if (_this.val() != '') {
                $.ajax({
                    url: '<?php echo site_url('admin/enquiry/getStates') ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {country_id: _this.val()},
                    success: function(data) {
                       // console.log(data);
                       var form_id=_this.closest('form').attr("id");
                        $('#'+form_id+' #state_id').empty().append(data);                        
                    }
                });
            }
        });


    });

   

    function getRecord(id,status) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/details/' + id+'/'+status,
            success: function (result) {
                $('#getdetails').html(result);
            }
        });
    }
    function postRecord(id) {

        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/editpost/' + id,
            type: 'POST',
            data: $("#myForm1").serialize(),
            dataType: 'json',
            success: function (data) {

                if (data.status == "fail") {

                    var message = "";
                    $.each(data.error, function (index, value) {

                        message += value;
                    });
                    errorMsg(message);
                } else {

                    successMsg(data.message);
                    window.location.reload(true);
                }
            },
            error: function () {
                alert("Fail")
            }
        });

    }

    function saveEnquiry() {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/enquiry/add/',
            type: 'POST',
            dataType: 'json',
            data: $("#formadd").serialize(),
            success: function (data) {
                if (data.status == "fail") {

                    var message = "";
                    $.each(data.error, function (index, value) {

                        message += value;
                    });
                    errorMsg(message);
                } else {

                    successMsg(data.message);
                    window.location.reload(true);
                }

            },
            error: function () {
                alert("Fail")
            }
        });


    }


    function delete_enquiry(id) {

        if (confirm('<?php echo $this->lang->line('delete_confirm') ?>')) {
            $.ajax({
                url: '<?php echo base_url(); ?>admin/enquiry/delete/' + id,
                type: 'POST',
                dataType: 'json',

                success: function (data) {
                    if (data.status == "fail") {

                        var message = "";
                        $.each(data.error, function (index, value) {

                            message += value;
                        });
                        errorMsg(message);
                    } else {

                        successMsg(data.message);
                        window.location.reload(true);
                    }

                }
            })

        }

    }


/*function get_follow_up_list(id){
    $.ajax({
        url: '<?php echo base_url(); ?>admin/enquiry/follow_up_list/' + id,
        success: function (data) {
            $('#timeline').html(data);
        },
        error: function () {
            alert("Fail")
        }
    });
}

function get_call_log_list(id){
    $.ajax({
        url: '<?php echo base_url(); ?>admin/enquiry/follow_up_call_loglist/' + id,
        success: function (data) {
            $('#followup_tab_lead_calldetails').html(data);
            $("#calllogtable").DataTable({
                    pageLength: 10,
                    searching: true,
                    paging: true,
                    bSort: true,
                    info: false,
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'Copy',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            titleAttr: 'Excel',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i>',
                            titleAttr: 'CSV',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            titleAttr: 'PDF',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'

                            }
                        },

                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            titleAttr: 'Print',
                            title: $('.download_label').html(),
                            customize: function (win) {
                                $(win.document.body)
                                        .css('font-size', '10pt');
                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'colvis',
                            text: '<i class="fa fa-columns"></i>',
                            titleAttr: 'Columns',
                            title: $('.download_label').html(),
                            postfixButtons: ['colvisRestore']
                        },
                    ]
                });
        },
        error: function () {
            alert("Fail")
        }
    });
}

function get_lead_log_list(id){
    $.ajax({
        url: '<?php echo base_url(); ?>admin/enquiry/lead_loglist/' + id,
        success: function (data) {
            $('#followUp_tab_lead_logdetails').html(data);
            $("#leadlogtable").DataTable({
                    pageLength: 10,
                    searching: true,
                    paging: true,
                    bSort: true,
                    info: false,
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'Copy',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            titleAttr: 'Excel',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i>',
                            titleAttr: 'CSV',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            titleAttr: 'PDF',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'

                            }
                        },

                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            titleAttr: 'Print',
                            title: $('.download_label').html(),
                            customize: function (win) {
                                $(win.document.body)
                                        .css('font-size', '10pt');
                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'colvis',
                            text: '<i class="fa fa-columns"></i>',
                            titleAttr: 'Columns',
                            title: $('.download_label').html(),
                            postfixButtons: ['colvisRestore']
                        },
                    ]
                });
        },
        error: function () {
            alert("Fail")
        }
    });
}

function follow_up(id, status) {
         
            $.ajax({
                url: '<?php echo base_url(); ?>admin/enquiry/follow_up/' + id + '/' + status,
                success: function (data) {
                    $('#getdetails_follow_up').html(data);
                    get_follow_up_list(id);
                    get_call_log_list(id);
                    get_lead_log_list(id);
                },
                error: function () {
                    alert("Fail")
                }
            });
        }

    function update() {

        window.location.reload(true);
    }*/
</script>

<script type="text/javascript">
    $(document).ready(function () {


        $("#enquirytable").DataTable({
            pageLength: 20,
            searching: true,
            order : [[0, 'desc']],
            paging: true,
            bSort: true,
            info: true,
            dom: "Bfrtip",
            buttons: [

                {
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',

                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>',
                    titleAttr: 'CSV',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'

                    }
                },

                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    title: $('.download_label').html(),
                    customize: function (win) {
                        $(win.document.body)
                                .css('font-size', '10pt');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    },
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'colvis',
                    text: '<i class="fa fa-columns"></i>',
                    titleAttr: 'Columns',
                    title: $('.download_label').html(),
                    postfixButtons: ['colvisRestore']
                },
            ]
        });
    });




</script>