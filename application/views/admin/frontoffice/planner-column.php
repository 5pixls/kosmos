<?php

if (empty($thumbSize)) {
    $thumbSize = 40;
}

?>
<?php foreach ($tasks as $task) :  ?>
    <?php $users = $this->db->query("SELECT * FROM staff WHERE id = '".$task['assigned']."'")->row_array(); 
    ?>
    <?php if ($viewMode == 'columns' && ($task['status'] != $column)) continue; ?>

    <?php if ($viewMode == 'users' && !array_key_exists($users['id'], $users)) continue;
    ?>

    <div data-task="<?php echo $task['id']; ?>" class="task-box" style="background-color:<?php echo ($task['color']) ?: '#ffffff'; ?>">
        <div class="task-inner">
                <div class="task-head">
                    <div class="left pull-left">
                        <div class="task-title" data-toggle="tooltip" title="<?php echo $task['name']; ?>">
                            <a href="#" target="_blank">
                                <?php if($task['name'] !=''){echo "Child Name: ".$task['name'].' '.$task['last_name'];
                            echo "<BR>Parent Name: ".$task['father_firstname'].' '.$task['father_lastlastname'];
                        }
                                else {echo "Parent Name: ".$task['father_firstname'].' '.$task['father_lastlastname'];} ?>
                            </a>
                        </div> 
                    </div> 
                    <div class="right pull-left">
                        <div class="photos">
                            <?php if ($task['id']) : ?>
                                        <?php if ($this->rbac->hasPrivilege('follow_up_admission_enquiry', 'can_view')) { ?>
                                                                <a class="btn btn-default btn-xs" onclick="follow_up('<?php echo $task['id']; ?>','<?php echo $task['status']; ?>');"  data-target="#follow_up" data-toggle="modal"  title="<?php echo $this->lang->line('follow_up_admission_enquiry'); ?>">
                                                                    <i class="fa fa-phone"></i>
                                                                </a>
                                                            <?php }
                                                            ?>
                                    <?php if ($this->rbac->hasPrivilege('admission_enquiry', 'can_edit')) { ?>
                                                                <!--<a  onclick="getRecord('<?php //echo $task['id']; ?>','<?php //echo $task['status']; ?>')" class="btn btn-default btn-xs" data-target="#myModaledit" data-toggle="modal"   title="<?php //echo $this->lang->line('edit'); ?>"><i class="fa fa-pencil"></i>
                                                                </a>-->
                                                                <a target="_blank" href="/admin/enquiry/leaddetails/<?php echo $task['id']; ?>" class="btn btn-default btn-xs" title="Edit"><i class="fa fa-pencil"></i>
                                                        </a> 
                                                    <?php }
                                                    ?>                        
                                    <?php endif; ?>  
                        </div>
                    </div>
                </div>

                <div class="task-body">
                    <!--<div class="text"><?php //echo summarize($task['description'],120); ?></div>-->
                    <div class="text"><strong>Source:</strong>
                        <?php echo $task['source']; ?></div>
                    <div class="text"><strong>Assigned to:</strong>
                        <?php echo $users['name'].' '.$users['surname']; ?></div>
                    <div class="text"><strong>Created Date:</strong>
                        <?php 
                            if($task['created_at'] !='' || $task['created_at'] != NULL) echo date('d-M-Y H:i:s', strtotime($task['created_at']));
                             ?></div>
                    <div class="text"><strong>Follow Up Date:</strong>
                        <?php 
                            if($task['next_date'] !='' || $task['next_date'] != NULL) echo date('d-M-Y H:i:s', strtotime($task['next_date']));
                             ?></div>
                </div>

                <!--<div class="task-foot">
                    <div class="en-dates">
                        <br />
                        
                    </div>

                    <div class="actions text-right">
                        
                                                  
                    </div>
                </div>-->
            

            
        </div>
    </div>
<?php endforeach; ?>

<!-- Add new task -->
<?php if ($viewMode == 'columns') : ?>
    <div class="add_new_task">
        <a href="<?php echo base_url(); ?>/admin/enquiry#myModal" target="_blank" class="js_open_modal small-box-footer" style="margin: auto;" title="Add Enquiry"><span class="glyphicon">&#x2b;</span></a>
    </div>
<?php endif; ?>
<!-- End new task -->