
$(document).ready(function(){
                // Basic
                $('.filestyle').dropify();

                // Translated
                $('.dropify-fr').dropify({
                    messages: {
                        default: 'Glissez-déposez un fichier ici ou cliquez',
                        replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                        remove:  'Supprimer',
                        error:   'Désolé, le fichier trop volumineux'
                    }
                });

                // Used events
                var drEvent = $('#input-file-events').dropify();

                drEvent.on('dropify.beforeClear', function(event, element){
                    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                });

                drEvent.on('dropify.afterClear', function(event, element){
                    alert('File deleted');
                });

                drEvent.on('dropify.errors', function(event, element){
                    console.log('Has Errors');
                });

                var drDestroy = $('#input-file-to-destroy').dropify();
                drDestroy = drDestroy.data('filestyle')
                $('#toggleDropify').on('click', function(e){
                    e.preventDefault();
                    if (drDestroy.isDropified()) {
                        drDestroy.destroy();
                    } else {
                        drDestroy.init();
                    }
               }); 

        $(document).on('change', '.append_other', function (e) {
            var value = $(this).val();
            if(value=="Other"){
                $(this).parent().next('div').show();
            }
            else $(this).parent().next('div').hide();
        });  
 jQuery.fn.liScroll = function(settings) {
  settings = jQuery.extend({
    travelocity: 0.02
    }, settings);   
    return this.each(function(){
        var $strip = jQuery(this);
        $strip.addClass("newsticker")
        var stripHeight = 1;
        $strip.find("li").each(function(i){
          stripHeight += jQuery(this, i).outerHeight(true); // thanks to Michael Haszprunar and Fabien Volpi
        });
        var $mask = $strip.wrap("<div class='mask'></div>");
        var $tickercontainer = $strip.parent().wrap("<div class='tickercontainer'></div>");               
        var containerHeight = $strip.parent().parent().height();  //a.k.a. 'mask' width   
        $strip.height(stripHeight);     
        var totalTravel = stripHeight;
        var defTiming = totalTravel/settings.travelocity; // thanks to Scott Waye   
        function scrollnews(spazio, tempo){
        $strip.animate({top: '-='+ spazio}, tempo, "linear", function(){$strip.css("top", containerHeight); scrollnews(totalTravel, defTiming);});
        }
        scrollnews(totalTravel, defTiming);       
        $strip.hover(function(){
        jQuery(this).stop();
        },
        function(){
        var offset = jQuery(this).offset();
        var residualSpace = offset.top + stripHeight;
        var residualTime = residualSpace/settings.travelocity;
        scrollnews(residualSpace, residualTime);
        });     
    }); 
};

/*================Toggle sidebar================ */
$(document).on('click', '.navbar-inner .navbar-toggle', function() {
  $('#sidebar-wrap').toggleClass('closed');
})
/*=====================Mobile nav=================== */
function navSidebar() {
  if($(window).width() < 992) {
    $('#sidebar-wrap').addClass('closed');
  }
  else {
    $('#sidebar-wrap').removeClass('closed');
  }
}
navSidebar();
$(window).resize(function() {
  navSidebar();
});

$("body").delegate(".form-control.date", "focusin", function () {
  // var date_format = '<?php echo $result = strtr($this->customlib->getSchoolDateFormat(), ['d' => 'dd', 'm' => 'mm', 'Y' => 'yyyy',]) ?>';
  $(this).datepicker({
          todayHighlight: false,
          // format: date_format,
          autoclose: true,
          language: '<?php echo $language_name ?>'
      });
  });

  $("body").delegate(".dob", "focusin", function () {
  //var date_format = '<?php echo $result = strtr($this->customlib->getSchoolDateFormat(), ['d' => 'dd', 'm' => 'mm', 'Y' => 'yyyy',]) ?>';
  var currentDate = new Date();
  $(this).datepicker({
          todayHighlight: false,
          // format: date_format,
          endDate: "today",
          maxDate: currentDate,
          autoclose: true,
          language: '<?php echo $language_name ?>'
      });
  });

  $('.dropify').dropify();

  //form required validations
  function validate_form(self) {
    var col = self.closest('.sw-footer').siblings('.sw-content').find('.sw-col');
    var contentLen = col.length;
    for(i=0;i<contentLen;i++){
      var isReq = col.eq(i).find('.req');
      var isEmpty = col.eq(i).find('.form-control').val();
      var isCheckEmpty = col.eq(i).find('.chk').prop('checked');
      
      if(isReq.length > 0 && isEmpty == '' ) {
        col.eq(i).addClass('errorCol');
      }
      else if(isReq.length > 0 && isCheckEmpty == false) {
        col.eq(i).addClass('errorCol');
      }
      else {
        col.eq(i).removeClass('errorCol');
      }
    }
  }

  // wizard next
  $('.sw-next-btn, .sw-submit').on('click', function() {   
     
    var self = $(this);
    var index = self.parents('.sw-steps-content').next().index();    
    validate_form(self);
    if(self.parents('.sw-steps-content').find('.errorCol').length > 0) {
      var scrollVal = self.parents('.sw-steps-content').find('.errorCol').eq(0).offset().top;
      $('html,body').animate({scrollTop:scrollVal+10});
    }
    else {
      self.parents('.site-wizard').find('.loader-wrap').show();
      var form_id= $(this).closest('form').attr('id');
      var enquiryid= $("#enquiryid").val();
      var action_url=base_url +'registration/registerpost/'+enquiryid;
      var form_data = new FormData($("#"+form_id)[0]);
      erroralert('hide', "");
      if(form_id=='child-form'){
            var child_image = $('#child_image').prop('files')[0];
            var ext = $('#child_image').val().split('.').pop().toLowerCase();
            if ($('#child_image').val() != '' && $.inArray(ext, ['gif', 'jpg', 'jpeg', 'png']) == -1) {
                erroralert('show', "Please select valid image file.");
                return false;
            }
            if ($('#child_image').val() != '')
                form_data.append('child_image', child_image);
              ///////////////////////////////////
            var previous_result = $('#previous_result').prop('files')[0];
            var ext = $('#previous_result').val().split('.').pop().toLowerCase();
            if ($('#previous_result').val() != '' && $.inArray(ext, ['doc','docx','pdf','gif', 'jpg', 'jpeg', 'png']) == -1) {
                erroralert('show', "Please select valid file format.");
                return false;
            }
            if ($('#previous_result').val() != '')
                form_data.append('previous_result', previous_result);
              
    }
    if(form_id=='guardian-form'){
      if(!form_data.has('father_email_pref') && !form_data.has('mother_email_pref')){
            erroralert('show', "Please select email prefrence for either father OR mother.");
            return false;
      }
      if(!form_data.has('father_sms_pref') && !form_data.has('mother_sms_pref')){
            erroralert('show', "Please select SMS prefrence for either father OR mother.");
            return false;
      }
    }
    if(form_id=='consent-form'){
      var values = jQuery('#'+form_id+' input[type=checkbox]:not(:checked)').map(
                    function() {
                        return {"name": this.name, "value": 0}
                    }).get();
        if(values.length > 0) {
          for(i=0;i<values.length;i++) {
            if(values[i].value==0){
                erroralert('show', "Please agree to all acknowledgents.");
                return false;
            }
            form_data.append(values[i].name,values[i].value);
          }
        }
    }
    $.ajax({
            url: action_url,
            type: 'POST',
            data: form_data,//$("#"+form_id).serialize(),
            dataType: 'json',
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false
            success: function (data) {
              self.parents('.site-wizard').find('.loader-wrap').hide();  
              //console.log(data);            
                if (data.status == "fail") {
                    var message = "";
                    $.each(data.error, function (index, value) {

                        message += value;
                    });
                    errorMsg(message);
                } else {                    
                    $("#enquiryid").val(data.enquiryid);
                    enquiry_id = data.enquiryid;
                    if(!self.hasClass('sw-submit')) {
                      self.parents('.sw-steps-content').removeClass('active').next().addClass('active');
                      $('.sw-steps').find('li').eq(index).addClass('active').siblings('li').removeClass('active');
                      
                    }
                    else {
                      //successMsg(data.message);
                      //location.reload();
                      //window.location='/registration/thankyou';
                      razorpay_options["prefill"]["name"] = data.enquiry_fields.father_firstname;
                      razorpay_options["prefill"]["email"] = data.enquiry_fields.father_email;
                      razorpay_options["prefill"]["contact"] = data.enquiry_fields.father_mobile;
                      $(".pay-now").trigger("click");
                    }
                }
            },
            error: function () {
                self.parents('.site-wizard').find('.loader-wrap').hide();
                console.log("Fail");
            }
    });
  }
    
  });
  
  //Wizard prev 
  $('.sw-prev-btn').on('click', function() {
    var self = $(this);
    var index = self.parents('.sw-steps-content').prev().index();
    var parent = self.parents('.sw-steps-content').removeClass('active').prev().addClass('active');
    $('.sw-steps').find('li').eq(index).addClass('active').siblings('li').removeClass('active');
  });

  $('.sw-col input, .sw-col select').on('change', function() {
    var self = $(this);
    if(self.val() != '') {
      self.parents('.sw-col').removeClass('errorCol');
    }
    else {
      self.parents('.sw-col').addClass('errorCol');
    }
  })

  //Aadhar
  $('.aadhaar_input').keyup(function() {
    var self = $(this);
    var val = self.val();
    var finalVal = val.replace(/[^0-9]/gi, '').replace(/(.{4})/g, '$1 ');
    self.val(finalVal.trim());
  });

  //mobile number format
  function mobile_spacing(val) {
  var newVal = '';
  if(val.length > 5 && val.length <= 10) {
      newVal = val.substr(0,5) + ' ';
      newVal += val.substr(5);
  }
  else if(val.length == 11) {
      newVal = val.substr(0,1) + ' ';
      newVal += val.substr(1,5) + ' ';
      newVal += val.substr(6,5);
  }
  else if(val.length == 12) {
      newVal = '+' + val.substr(0,2) + '-';
      newVal += val.substr(2,5) + ' ';
      newVal += val.substr(7,5);
  }
  else if(val.length == 13) {
      newVal = val.substr(0,3) + '-';
      newVal += val.substr(3,5) + ' ';
      newVal += val.substr(8,5);
  }
  else {
      newVal = val;
  }
  return newVal;
  }

  var mob_ele = document.getElementsByClassName('form-mobile');
  var mobLen = mob_ele.length;
  for(i=0;i<mobLen;i++) {
  var mobile_val = mob_ele[i].value;
  mobile_val = mobile_val.replace(/[^A-Z0-9]+/ig, '');
  if(mobile_val) {
      var final_val = mobile_spacing(mobile_val);
      mob_ele[i].value = final_val;
  }
  }

  $('.form-mobile').keyup(function() {
  var val = this.value.replace(/\D/g, '');
  var value = mobile_spacing(val);
  this.value = value;
  });

  // Terms popup
  $(document).on('click', '.terms-modal-toggle', function() {
    $('#terms_modal').addClass('pop');
  });
  $(document).on('click', '.terms-modal-close', function() {
    $('#terms_modal').removeClass('pop');
  });
  $(document).on('click', '.privacy-modal-toggle', function() {
    $('#privacy_modal').addClass('pop');
  });
  $(document).on('click', '.privacy-modal-close', function() {
    $('#privacy_modal').removeClass('pop');
  });

});

$(function(){
    $("ul#ticker01").liScroll();
});


 /* ----------------------------------------------------------- */
  /* counter scroll
  /* ----------------------------------------------------------- */
 //        var a = 0;
 //        $(window).scroll(function() {

 //          var oTop = $('.counter').offset().top - window.innerHeight;
 //          if (a == 0 && $(window).scrollTop() > oTop) {
 //            $('.counter-value').each(function() {
 //              var $this = $(this),
 //                countTo = $this.attr('data-count');
 //              $({
 //                countNum: $this.text()
 //              }).animate({
 //                  countNum: countTo
 //                },

 //                {

 //                  duration: 2000,
 //                  easing: 'swing',
 //                  step: function() {
 //                    $this.text(Math.floor(this.countNum));
 //                  },
 //                  complete: function() {
 //                    $this.text(this.countNum);
 //                    //alert('finished');
 //                  }

 //                });
 //            });
 //            a = 1;
 //          }
 // });

           jQuery('.counter-value').counterUp({
                delay: 10,
                time: 2000
            });



        $(document).ready(function ($) {
                // delegate calls to data-toggle="lightbox"
                $(document).on('click', '[data-toggle="lightbox"]:not([data-gallery="navigateTo"]):not([data-gallery="example-gallery"])', function(event) {
                    event.preventDefault();
                    return $(this).fancyLightbox({
                        onShown: function() {
                            if (window.console) {
                                return console.log('Checking our the events huh?');
                            }
                        },
            onNavigate: function(direction, itemIndex) {
                            if (window.console) {
                                return console.log('Navigating '+direction+'. Current item: '+itemIndex);
                            }
            }
                    });
                });

                // disable wrapping
                $(document).on('click', '[data-toggle="lightbox"][data-gallery="example-gallery"]', function(event) {
                    event.preventDefault();
                    return $(this).fancyLightbox({
                        wrapping: false
                    });
                });

                //Programmatically call
                $('#open-image').click(function (e) {
                    e.preventDefault();
                    $(this).fancyLightbox();
                });
                $('#open-youtube').click(function (e) {
                    e.preventDefault();
                    $(this).fancyLightbox();
                });

       
            });

/* ----------------------------------------------------------- */
  /* header sticky
  /* ----------------------------------------------------------- */
$( document ).ready(function() {
$('#alert').affix({
    offset: {
      top: 10, 
      bottom: function () {
      }
    }
  })  
});



// $('.counter-count').each(function () {
//        $(this).prop('counter',0).animate({
//            Counter: $(this).text()
//        }, {
//            duration: 5000,
//            easing: 'swing',
//            step: function (now) {
//                $(this).text(Math.ceil(now));
//            }
//        });
//    });

    //Check to see if the window is top if not then display button
    jQuery(window).scroll(function(){
      if (jQuery(this).scrollTop() > 300) {
        jQuery('.scrollToTop').fadeIn();
      } else {
        jQuery('.scrollToTop').fadeOut();
      }
    });


     
    //Click event to scroll to top

    jQuery('.scrollToTop').click(function(){
      jQuery('html, body').animate({scrollTop : 0},800);
      return false;
    });  
//end Click event to scroll to top


$('#carouselExample').on('slide.bs.carousel', function (e) {

  
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $('.carousel-item').length;
    
    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});


// Clients carousel (uses the Owl Carousel library)
  $(".staff-carousel").owlCarousel({
    autoplay: false,
    nav    : true,
    dots: true,
    loop: true,
    navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    responsive: { 0: { items: 1 }, 768: { items: 3 }, 900: { items: 4 }
    }
  });

   // testimonial carousel (uses the Owl Carousel library)
  $(".testimonial-carousel").owlCarousel({
    autoplay: true,
    nav    : false,
    dots: true,
    loop: true,
    navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    responsive: { 0: { items: 1 }, 768: { items: 1 }, 900: { items: 1 }
    }
  });

   // course carousel (uses the Owl Carousel library)
  $(".courses-carousel").owlCarousel({
    autoplay: true,
    nav    : true,
    dots: false,
    loop: true,
    navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    responsive: { 0: { items: 1 }, 768: { items: 3 }, 900: { items: 3 }
    }
  });



  $(document).ready(function() {
    $('a.thumb').click(function(event){
      event.preventDefault();
      var content = $('.modal-body');
      content.empty();
        var title = $(this).attr("title");
        $('.modal-title').html(title);        
        content.html($(this).html());
        $(".modal-profile").modal({show:true});
    });
  });

  function erroralert(type, msg) {
    if(type=='show') { 
      $('.error_alert').html(msg).show();
      $('html, body').animate({scrollTop : 0},800);
      $('.loader-wrap').hide();
    }
    else $('.error_alert').empty().hide();
  }