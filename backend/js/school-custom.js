 $(document).on('focus', ':input', function() {
     $(this).attr('autocomplete', 'off');
 });
 $(document).ready(function() {

    // aadhar card
    $('.aadhaar_input').keyup(function() {
        var self = $(this);
        var val = self.val();
        var finalVal = val.replace(/[^0-9]/gi, '').replace(/(.{4})/g, '$1 ');
        self.val(finalVal.trim());
    })

    //Mobile number format
    function mobile_spacing(val) {
        var newVal = '';
        if(val.length > 5 && val.length <= 10) {
            newVal = val.substr(0,5) + ' ';
            newVal += val.substr(5);
        }
        else if(val.length == 11) {
            newVal = val.substr(0,1) + ' ';
            newVal += val.substr(1,5) + ' ';
            newVal += val.substr(6,5);
        }
        else if(val.length == 12) {
            newVal = '+' + val.substr(0,2) + '-';
            newVal += val.substr(2,5) + ' ';
            newVal += val.substr(7,5);
        }
        else if(val.length == 13) {
            newVal = val.substr(0,3) + '-';
            newVal += val.substr(3,5) + ' ';
            newVal += val.substr(8,5);
        }
        else {
            newVal = val;
        }
        return newVal;
    }

    var mob_ele = document.getElementsByClassName('form-mobile');
    var mobLen = mob_ele.length;
    for(i=0;i<mobLen;i++) {
        var mobile_val = mob_ele[i].value;
        mobile_val = mobile_val.replace(/[^A-Z0-9]+/ig, '');
        if(mobile_val) {
            var final_val = mobile_spacing(mobile_val);
            mob_ele[i].value = final_val;
        }
    }
    
    $('.form-mobile').keyup(function() {
        var val = this.value.replace(/\D/g, '');
        var value = mobile_spacing(val);
        this.value = value;
    })

     $('#sessionModal').modal({
         backdrop: 'static',
         keyboard: false,
         show: false
     })
     $('#activelicmodal').modal({
         backdrop: false,
         keyboard: false,
         show: false
     });
       $('#activelicmodal').on('show.bs.modal', function(event) {
         $('#purchase_code').trigger("reset");
          $('.lic_modal-body .error_message').html("");
        
       });
     $('#sessionModal').on('show.bs.modal', function(event) {
         var $modalDiv = $(event.delegateTarget);
         $('.sessionmodal_body').html("");
         $.ajax({
             type: "POST",
             url: baseurl + "admin/admin/getSession",
             dataType: 'text',
             data: {},
             beforeSend: function() {
                 $modalDiv.addClass('modal_loading');
             },
             success: function(data) {
                 $('.sessionmodal_body').html(data);
             },
             error: function(xhr) { // if error occured
                 $modalDiv.removeClass('modal_loading');
             },
             complete: function() {
                 $modalDiv.removeClass('modal_loading');
             },
         });
     })
     $(document).on('click', '.submit_session', function() {
         var $this = $(this);
         var datastring = $("form#form_modal_session").serialize();
         $.ajax({
             type: "POST",
             url: baseurl + "admin/admin/updateSession",
             dataType: "json",
             data: datastring,
             beforeSend: function() {
                 $this.button('loading');
             },
             success: function(data) {
                 if (data.status == 1) {
                     $('#sessionModal').modal('hide');
                     window.location.href = baseurl + "admin/admin/dashboard";
                     successMsg("Session change successful");
                 }
             },
             error: function(xhr) {
                 $this.button('reset');
             },
             complete: function() {
                 $this.button('reset');
             },
         });
     });
    
     //=============Sticky header==============
     $('#alert').affix({
         offset: {
             top: 10,
             bottom: function() {}
         }
     })
     $('#alert2').affix({
         offset: {
             top: 20,
             bottom: function() {}
         }
     })
     //========================================
     //==============User Quick session============
     $('#user_sessionModal').modal({
         backdrop: 'static',
         keyboard: false,
         show: false
     })
     $('#user_sessionModal').on('show.bs.modal', function(event) {
         var $modalDiv = $(event.delegateTarget);
         $('.user_sessionmodal_body').html("");
         $.ajax({
             type: "POST",
             url: baseurl + "common/getSudentSessions",
             dataType: 'text',
             data: {},
             beforeSend: function() {
                 $modalDiv.addClass('modal_loading');
             },
             success: function(data) {
                 $('.user_sessionmodal_body').html(data);
             },
             error: function(xhr) { // if error occured
                 $modalDiv.removeClass('modal_loading');
             },
             complete: function() {
                 $modalDiv.removeClass('modal_loading');
             },
         });
     });
     $(document).on('click', '.submit_usersession', function() {
         var $this = $(this);
         var datastring = $("form#form_modal_usersession").serialize();
         $.ajax({
             type: "POST",
             url: baseurl + "common/updateSession",
             dataType: "json",
             data: datastring,
             beforeSend: function() {
                 $this.button('loading');
             },
             success: function(data) {
                 if (data.status == 1) {
                     $('#sessionModal').modal('hide');
                     window.location.href = baseurl + "user/user/dashboard";
                     successMsg("Session change successful");
                 }
             },
             error: function(xhr) {
                 $this.button('reset');
             },
             complete: function() {
                 $this.button('reset');
             },
         });
     });
     //====================
     $('#commanSessionModal').modal({
         backdrop: 'static',
         keyboard: false,
         show: false
     });
     $('#commanSessionModal').on('show.bs.modal', function(event) {
         var $modalDiv = $(event.delegateTarget);
         $('.commonsessionmodal_body').html("");
         $.ajax({
             type: "POST",
             url: baseurl + "common/getAllSession",
             dataType: 'text',
             data: {},
             beforeSend: function() {
                 $modalDiv.addClass('modal_loading');
             },
             success: function(data) {
                 $('.commonsessionmodal_body').html(data);
             },
             error: function(xhr) { // if error occured
                 $modalDiv.removeClass('modal_loading');
             },
             complete: function() {
                 $modalDiv.removeClass('modal_loading');
             },
         });
     });
     $(document).on('click', '.submit_common_session', function() {
         var $this = $(this);
         var datastring = $("form#form_modal_commonsession").serialize();
         $.ajax({
             type: "POST",
             url: baseurl + "common/updateSession",
             dataType: "json",
             data: datastring,
             beforeSend: function() {
                 $this.button('loading');
             },
             success: function(data) {
                 if (data.status == 1) {
                     $('#sessionModal').modal('hide');
                     window.location.href = data.redirect_url;
                     successMsg("Session change successful");
                 }
             },
             error: function(xhr) {
                 $this.button('reset');
             },
             complete: function() {
                 $this.button('reset');
             },
         });
     });
     $("#purchase_code").submit(function(e) {
         var form = $(this);
         var url = form.attr('action');
         var $this = $(this);
         var $btn = $this.find("button[type=submit]");
         $.ajax({
             type: "POST",
             url: url,
             data: form.serialize(),
             dataType: 'JSON',
             beforeSend: function() {
                  $('.lic_modal-body .error_message').html("");
                 $btn.button('loading');
             },
             success: function(response, textStatus, xhr) {


                 if (xhr.status != 200) {
                     var $newmsgDiv = $("<div/>") // creates a div element              
                         .addClass("alert alert-danger") // add a class
                         .html(response.response);
                     $('.lic_modal-body .error_message').append($newmsgDiv);
                 }else if(xhr.status == 200){

                 if (response.status == 2) {
                     $.each(response.error, function(key, value) {
                         $('#input-' + key).parents('.form-group').find('#error').html(value);
                     });
                 }else if (response.status == 1) {
                     successMsg(response.message);
                     window.location.href=baseurl+'admin/admin/dashboard';
                     $('#activelicmodal').modal('hide');
                 }
             }
                 
             },
             error: function(xhr, status, error) {
               $btn.button('reset');
               var r = jQuery.parseJSON(xhr.responseText);          
               var $newmsgDiv = $("<div/>") // creates a div element              
                         .addClass("alert alert-danger") // add a class
                         .html(r.response);
                     $('.lic_modal-body .error_message').append($newmsgDiv);
              
             },
             complete: function() {
                 $btn.button('reset');
             },
         });
         e.preventDefault();
     });
    $('.upload-docs .filestyle.form-control').on('change', function() {
        var val = $(this).val();
        if(val) {
            $(this).parents('td').prev().addClass('uploaded');
        }
        else {
            $(this).parents('td').prev().removeClass('uploaded');
        }
    });
    $(document).on('click','.upload-docs button.dropify-clear', function() {
        console.log('çlicked')
        var val = $(this).siblings('.filestyle').val();
        console.log('val: ',val)
        if(val) {
            $(this).parents('td').prev().addClass('uploaded');
        }
        else {
            $(this).parents('td').prev().removeClass('uploaded');
        }
    });
    
 });

 $(document).ready(function () {
        $('#response').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            if(valueSelected == 'Other') $('.otherdiv').show();
            else $('.otherdiv').hide();
        });

    });
   
    function callOzonetel(cuObj){
          //console.log($(cuObj).attr("data-id"));      
          var lead_id = $(cuObj).attr("data-id");
          $.ajax({
             url: baseurl+'admin/enquiry/callOzonetel',
             dataType: 'text',
             type: 'post',
             contentType: 'application/x-www-form-urlencoded',
             data: {'lead_id':lead_id},
             success: function( data, textStatus, jQxhr ){
                console.log(data);
                var response = jQuery.parseJSON(data);
                var errElement = $(".ozonetel-response");
                if(response.status != "error"){
                   var successMsg = "Call is connecting...";//response.message;
                  // $.each(response.message, function (key, val) {
                  //    successMsg += val+" ";
                  // });
                   //var par = $(cuObj).closest("#lead_form");  
                   //var errElement = $(par).find(".ozonetel-response");
                  // var par = $(cuObj).closest("p"); 
                   
                   errElement.addClass("alert-success").addClass("show").removeClass("alert-danger").removeClass("hide").html(successMsg);
                   setTimeout(function() {
                      errElement.removeClass("show");
                      errElement.addClass("hide");
                   }, 20000);
                }else if(response.status == "error"){
                   var errMsg = response.message;
                  // $.each(response.message, function (key, val) {
                   //   errMsg += val+" ";
                   //});
                   //var par = $(cuObj).closest("#lead_form");  
                   //var errElement = $(par).find(".ozonetel-response");
                   //var par = $(cuObj).closest("p"); 
                   //var errElement = $(par).siblings(".ozonetel-response");
                   errElement.addClass("alert-danger").removeClass("alert-success").removeClass("hide").html(errMsg);
                   setTimeout(function() {
                      errElement.removeClass("show");
                      errElement.addClass("hide");
                   }, 20000);
                }
             
             },
             error: function( jqXhr, textStatus, errorThrown ){
                   console.log( errorThrown );
             }
          });
   }

    function follow_save() {
        //alert('Jai Shree Ram');
        var id = $('#enquiry_id').val();
         var status = $('#enquiry_status').val();
        var responce = $('#response').val();
        var follow_date = $('#follow_date').val();
        //  alert(follow_date);

        $.ajax({
            url: baseurl+'admin/enquiry/follow_up_insert',
            type: 'POST',
            dataType: 'json',
            data: $("#folow_up_data").serialize(),
            success: function (data) {

                //alert(data);

                if (data.status == "fail") {

                    var message = "";
                    $.each(data.error, function (index, value) {

                        message += value;
                    });
                    errorMsg(message);
                } else {

                    successMsg(data.message);
                    follow_up_new(id,status);
                }

               
            },

            error: function () {
                alert("Fail")
            }
        });


    }

   function get_follow_up_list(id){
    $.ajax({
        url: baseurl+'admin/enquiry/follow_up_list/' + id,
        success: function (data) {
            $('#timeline').html(data);
        },
        error: function () {
            alert("Fail")
        }
    });
}

function get_call_log_list(id){
    $.ajax({
        url: baseurl+'admin/enquiry/follow_up_call_loglist/' + id,
        success: function (data) {
            $('#followUp_tab_lead_calldetails').html(data);
            $("#calllogtable").DataTable({
                    pageLength: 10,
                    searching: true,
                    paging: true,
                    bSort: true,
                    info: false,
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'Copy',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            titleAttr: 'Excel',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i>',
                            titleAttr: 'CSV',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            titleAttr: 'PDF',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'

                            }
                        },

                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            titleAttr: 'Print',
                            title: $('.download_label').html(),
                            customize: function (win) {
                                $(win.document.body)
                                        .css('font-size', '10pt');
                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'colvis',
                            text: '<i class="fa fa-columns"></i>',
                            titleAttr: 'Columns',
                            title: $('.download_label').html(),
                            postfixButtons: ['colvisRestore']
                        },
                    ]
                });
        },
        error: function () {
            alert("Fail")
        }
    });
}

function get_lead_log_list(id){
    $.ajax({
        url: baseurl+'admin/enquiry/lead_loglist/' + id,
        success: function (data) {
            $('#followUp_tab_lead_logdetails').html(data);
            $("#leadlogtable").DataTable({
                    pageLength: 10,
                    searching: true,
                    paging: true,
                    bSort: true,
                    info: false,
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'Copy',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            titleAttr: 'Excel',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i>',
                            titleAttr: 'CSV',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            titleAttr: 'PDF',
                            title: $('.download_label').html(),
                            exportOptions: {
                                columns: ':visible'

                            }
                        },

                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i>',
                            titleAttr: 'Print',
                            title: $('.download_label').html(),
                            customize: function (win) {
                                $(win.document.body)
                                        .css('font-size', '10pt');
                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                            },
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'colvis',
                            text: '<i class="fa fa-columns"></i>',
                            titleAttr: 'Columns',
                            title: $('.download_label').html(),
                            postfixButtons: ['colvisRestore']
                        },
                    ]
                });
        },
        error: function () {
            alert("Fail")
        }
    });
}

function follow_up(id, status) {
         
            $.ajax({
                url: baseurl+'admin/enquiry/follow_up/' + id + '/' + status,
                success: function (data) {
                    $('#getdetails_follow_up').html(data);
                    get_follow_up_list(id);
                    get_call_log_list(id);
                    get_lead_log_list(id);
                },
                error: function () {
                    alert("Fail")
                }
            });
        }

    function update() {

        window.location.reload(true);
    }


function follow_up_new(id, status) {
         
            $.ajax({
                url: baseurl+'admin/enquiry/follow_up/' + id + '/' + status,
                success: function (data) {
                    $('#getdetails_follow_up').html(data);
                    $.ajax({
                        url: baseurl+'admin/enquiry/follow_up_list/' + id,
                        success: function (data) {
                            $('#timeline').html(data);
                            get_call_log_list(id);
                            get_lead_log_list(id);
                        },
                        error: function () {
                            alert("Fail")
                        }
                    });
                },
                error: function () {
                    alert("Fail")
                }
            });
        }

    function changeStatus(status, id) {

       //alert(status+id);

        $.ajax({
            url: baseurl+'admin/enquiry/change_status/',
            type: 'POST',
            dataType: 'json',
            data: {status: status, id: id},
            success: function (data) {
                if (data.status == "fail") {

                    errorMsg(data.message);
                } else {

                    successMsg(data.message);
                    follow_up_new(id,status);
                }
            }

        })
    }
